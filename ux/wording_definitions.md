# Begriffsabgrenzungen

## Erinnerung
Eine Erinnerung bezieht sich immer auf eine bestimmte Deadline um entsprechend des eingestellten Erinnerungsintervals (o.a. einmalig) die Studierenden mittels Pop-Ups o.Ä. zu erinnern. Erinnerungen können auch für Termine existieren, werden jedoch anders dargestellt (bspw. Erinnerungen für importierte Termine aus bestehenden Kalendern)

## Deadline
Eine "Abgabe" (App-internes Wording: **Deadline**) (digital, analog, postalisch etc.) mit festem Datum und Uhrzeit sowie einer Zuordnung eines bestimmten Studienmodul ("Modul-Tag"). Darin untergeordnet befinden sich to-do-Elemente um ausstehende Aufgaben bis zum jeweiligen Zeitpunkt überblicken zu können. Deadlines füllen darüber hinaus als einzige Elemente der Applikation die zentrale Listenansicht um 

## Termin
Ein Termin steht für ein generelles Ereignis analog zu bestehenden Kalendern. Termine sind im Deadline-Kalender klar zu Deadlines abgegrenzt und stellen lediglich Zeitpunkte für Vorlesungen, Praktika oder andere "Präsenztermine" dar.

## ToDo
Ein "To-do" (Aufgabe) bezieht sich **immer** auf eine übergeordnete Deadline und kann hier als "erledigt" oder "ausstehend" markiert werden. Einer Deadline können mehrere To-do's zugeordnet werden.


# Gesammelte Werke für Prototype-Merge (In Bearbeitung)

## Bottom Navigation 
- 3 Navigationspunkten (Links: Kalenderansicht, Mitte: Deadline-Overview (Listenansicht) mit App-Logo gekennzeichnet, Rechts: Profil)

## Kalenderansicht
- Geteilte Ansicht: Kalender(Tagesauswahl in Monatsansicht), Listenansicht im unteren Bereich stellt Termine und Deadlines des jeweiligen Tages dar. (Deadlines über App-Logo als Icon zu Terminen abgegrenzt)

## Deadline Overview (Listenansicht)
- Listenansicht mit Filterfunktion (absteigend sortiert)
- Beinhaltet lediglich Deadlines
- Default Screen (Bei App-Start)

## Profil
- Beinhaltet Nutzer*innenprofil mit Studiengang, Modulübersicht etc.
- Module können hinzugefügt und bearbeitet werden
- Über Zahnrad im oberen, rechten Eck können die allgemeinen App-Einstellungen angewählt werden