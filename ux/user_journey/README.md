# User Journey (Maps)

## Definition
A user journey describes all the interactions of the customer with the product. Creating maps of these interactions and emotions based on the user personas can help to understand how users are interacting with our app and what they might need in order to accomplish what's meaningful to them.

## Goal
The goal of said maps is to understand the individual user experience based on their interactions with the app. Each archetype will have different goals and motivation and therefore individual approaches on using the app to accomplish what they want. Taking all the different actions and approaches of users into consideration, an application might get much more intuitive and easy to use.

## Types
- Based on hypotheses (Before development)
- Based on research
- Based on research and feedback
