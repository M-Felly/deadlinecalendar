# Protokoll Usability Inspection

Für die Usability Inspection haben wir aus der Checkliste der Heuristiken folgende Fragen abgeleitet:

1. Ist der Systemstatus beim Import eines Kalenders erkennbar?
Ja, der Import kann 6 verschiedene Status anzeigen.
Hierbei wird z.B. angezeigt, ob gerade die Daten geholt oder erfolgreich importiert wurden. Dies ist ebenfalls an den Farben, Text sowie dem Fortschrittsbalken erkennbar.

2. Wurden Begriffe immer einheitlich benannt (Kürzel/Tag)?
Ja, im englischen wird der Begriff  "tag" und im deutschen "Kürzel" verwendet.

3. Kann bei der Erstellung eines Termins und einer Deadline der Vorgang abgebrochen werden?
Ja. Wenn man sich auf der Ansicht zum hinzufügen eines neuen Termins oder Deadline befindet, kann man diese jederzeit mit dem Button "X" abbrechen.


4. Kann eine Deadline/Kalendereintrag rückgängig oder wiederholt werden?
Nein. Dies wird mit der nächsten Version bereitgestellt.

5. Ist der Zurück-Button immer an der gleichen Stelle?
Ja, der Zurück-Button befindet sich immer an der oberen linken Ecke der App.

6. Ist das Layout der Aktion-Buttons konsistent?
Die Buttons werden immer in der gleichen Größe angezeigt. Lediglich die Farben werden je nach Aktion in unterschiedlichen dargestellt (Grün - bestätigen, rot - löschen).

7. Wurde ein einheitliches Farbkonzept verwendet?
Es wurde darauf geachtet, dass sich die Farben grau und rot durch das ganze Layout ziehen. Hierbei wurde nur bei dem Button "erledigt" die Farbe grün mit eingebracht.

8. Wurde bei Zeit und Datum ein spezieller Date-Picker/Time-Picker verwendet um ungültige Eingaben zu vermeiden?
Ja, bei jeder Auswahl von Datum oder Uhrzeit wurde der spezielle Time- bzw. Date-Picker verwendet, um ungültige Dateneingaben zu verhindern.

9. Wurde bei immer gleichen Auswahlmöglichkeiten ein Spinner statt einer Eingabe verwendet?
Ja hier werden bei immer gleichen Auswahlmöglichkeiten (z.B. Module, Erinnerungszeiten) diese in einem Spinner als Auswahlmöglichkeit zur Verfügung zustellen.

10. Wurden Bestätigungsabfragen bei Löschvorgängen verwendet?
Ja es wurde darauf geachtet, dass bei der Betätigung des Löschen-Buttons noch eine zusätzliche Abfrage erscheint. Hierbei wird der/die Benutzer*in gefragt, ob sie die Deadline/ den Kalendereintrag wirklich löschen möchte.

11. Werden Tage, an denen Termine/Deadlines stattfinden, im Kalender makiert?
Nein, dies soll in der nächsten Version hinzugefügt werden, da hier ein anderes Calendar-Widget verwendet werden muss.

12. Hat der/die Benutzer*in die Möglichkeit Erinnerungen für Termine und Deadlines individuell ein- und auszuschalten?
Ja in den Einstellungen gibt es unter dem Reiter "Erinnerungen", die Möglichkeit Erinnerungen für Deadlines und Termine jeweils ein- und auszuschalten.

13. Gibt es beim Import Fehlermeldungen, die den Fehler analysieren, beschreiben und einen Vorschlag zur Behebung enthält?
Ja die gibt es. Hierbei werden verschiedene Arten von Fehlern abgefangen (z.B. Netzwerkfehler, Verarbeitungsfehler) diese werden Kategorisiert und es werden entsprechende Behebungsmöglichkeiten vorgeschlagen.


## Fazit
Die meisten Kriterien wurden erfüllt. Die Kriterien, die bisher noch nicht erfüllt wurden, werden in der nächsten Version umgesetzt.