# Usability Inspection 
Usability Inspection kann durch eine Heuristische Auswertung, welche gegen eine kurze Checkliste allgemeiner Regeln von erfahrenen Inspektor*innen geprüft werden. Eine weitere Möglichkeit ist eine sogenannte Cognitive Walkthrough, wobei kleine Gruppen typische Aufgaben durchspielen. Eine dritte Möglichkeit ist es, die Richtlinien Prüfung oder Wertung hierbei prüft oder wertet ein*e einzelne Inspektor*in gegen detailliertes Regelwerk. 

## Zusammenfassung der Heuristiken
Die Heuristiken sind grundlegende Regeln/Richtlinien nach denen eine Benutzeroberfläche gestaltet und konzipiert werden soll. Hierbei wir versucht den Nutzer/*innen eine bestmögliche [Usability](../glossary.md#Usability) und [User Experience](../glossary.md#UX) zu bieten, damit diese zielgerichtet und effizient mit der Applikation arbeiten können. 
Bei der Überprüfung sollten die Punkte der folgende Checkliste beachtet werden:

### Checkliste der [Heuristiken](../glossary.md#Heuristiken)
- System Status erkennbar 
- System und reale Welt passen
- Steuerbarkeit und Freiheit
- Konsistenz und Standards
- Fehlervermeidung
- Erkennbarkeit statt Erinnerung
- Flexibilität und Effizienz
- Ästhetisches und minimalistisches Design
- Erkennung, Diagnose und Behebung von Fehlern
- Hilfe und Dokumentation


Für unsere App haben wir folgende konkrete Kriterien ausgewählt: 

### Konkrete Kriterien
1. Ist der Systemstatus beim Import eines Kalenders erkennbar?
2. Wurden Begriffe immer einheitlich benannt (Kürzel/Tag)?
3. Kann bei der Erstellung eines Termins und einer Deadline der Vorgang abgebrochen werden?
4. Kann eine Deadline/Kalendereintrag rückgängig oder wiederholt werden?
5. Ist der Zurück-Button immer an der gleichen Stelle?
6. Ist das Layout der Aktion-Buttons konsistent?
7. Wurde ein einheitliches Farbkonzept verwendet?
8. Wurde bei Zeit und Datum ein spezieller Date-Picker/Time-Picker verwendet um ungültige Eingaben zu vermeiden?
9. Wurde bei immer gleichen Auswahlmöglichkeiten ein Spinner statt einer Eingabe verwendet?
10. Wurden Bestätigungsabfragen bei Löschvorgängen verwendet?
11. Werden Tage, an denen Termine/Deadlines stattfinden, im Kalender makiert?
12. Hat der/die Benutzer*in die Möglichkeit Erinnerungen für Termine und Deadlines individuell ein- und auszuschalten?
13. Gibt es beim Import Fehlermeldungen, die den Fehler analysieren, beschreiben und einen Vorschlag zur Behebung enthält?