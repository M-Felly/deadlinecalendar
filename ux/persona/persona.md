# Persona
Hier dokumentieren Sie, wie Sie zu Ihren Persona gekommen sind. Die konkreten Persona hinterlegen Sie als PDF oder in einem bearbeitbaren Format (mit Bildern).

## Stakeholder
- User (Studierende)
- Entwickler\*innen
- Projektleiter\*in
- Projektmitarbeiter\*in
- Designer\*in
- Qualitätsmanagement
- Marketing/Vertrieb
- Tester\*innen
- Auftraggeber\*in (Dozent\*in)
- Kunde\*in
- Betreiber\*innen des Systems

## Personenliste (Umfeld)
- Marc
- Rieke
- Konstantin
- Svenja
- Edgar

## Kriterien zur Unterscheidung von Personen
1. Studiengang 
2. Fachsemster
3. Organisationsvermögen
4. Anzahl Module
5. Arbeitszeit
6. Arbeitsweise
7. Arbeitsgeschwindigkeit (geschätze Zeit - wirkliche Zeit)
8. Abgabeform
9. Sprache

## Relevante Kriterien zur Klassifikation mit Begründung
Die Relevanz beginnt bei dem wichtigsten.
1. Organisationsvermögen 
    <br/>
    Das Organisationsvermögen ist das wichtigste Kriterium, da dies zu sehr individueller und unterschiedlicher arbeitsweise der App führen sollte. Je nach dem wie gut organisiert die Studierenden sind muss die Erinnerung früher oder später kommen. 

2. Arbeitsweise
    <br/>
    Die Arbeitsweise ist das zweit wichtigste, da bei einer Gruppenarbeit zusätzlich berücksichtigt werden muss, dass vorlaufzeit für die Planung benötigt wird, die anderen Teilnehmer Zeit haben und die Gruppenarbeit zeitaufwendiger sein kann, als eine Einzelarbeit. 

3. Arbeitsgeschwindigkeit
    <br/>
    Jeder Studierende benötigt unterschiedlich lange für Aufgaben und ist somit auch ein wichtiges Kriterium für die App, da dies ebenfalls mit in die Erinnerungszeit eingerechnet werden muss.

4. Arbeitszeit
    <br/>
    Das System muss wissen, zur welcher Tageszeit eine Person am liebsten arbeitet und erstellt mit dieser Information die für die Person passende Erinnerung. Hier muss nicht mehr der Tag berechnet sondern lediglich die Tageszeit festgelegt werden, daher steht das Kriterium nicht an erster Stelle.

5. Abgabeform 
    <br/>
    Um berücksichtigen zu können, ob der Studierende noch eine Vorlaufzeit benötigt, um die Arbeit abzugeben kann hier noch ein zusätzlicher Zeitpuffer eingebaut werden. Hierbei handelt es sich nur um eine geringe Zeitspanne, daher ist dies nicht ganz so wichtig.

## Ausprägung der relevanten Kriterien der Personen des Umfelds
- Organisationsvermögen: langfristig, mittelfristig, kurzfristig
- Arbeitsweise: Einzelarbeit, Partnerarbeit, Gruppenarbeit
- Arbeitsgeschwindigkeit: schnell, mittel, langsam
- Arbeitszeit: Morgens, Mittags, Abends, Nachts
- Abgabeform: present, digital
<br>
Diese Ausprägungen sind für das Projekt relevant, da es für die Berechnung der automatisch generierten Erinnerungen benötigt wird. Diese Erinnerungen sollen je nach Eigenschaften früher oder später erscheinen.

## Personenliste (außerhalb)
Wir benötigen noch Personen mit folgenden Eigenschaften:
- Abgabeform: present
- Arbeitsweie: Einzelarbeit
- Organisationsvermögen: mittelfristig

Diese Eigenschaften erfüllen:
- Tom
- Sofia

## Erkenntnisse nach den Interviews
Es ist sehr individuell wie die Nutzer\*innen arbeiten und hierauf muss sehr geachtet werden, dass die verschiedenen Wünsche der Personen berücksichtigt werden.

## Tabelle der Persona mit ihren relevanten Kriterien
| Persona | Organisationsvermögen | Arbeitsweise  | Arbeitsgeschwindigkeit | Arbeitszeit              | Abgabeform |
|---------|-----------------------|---------------|------------------------|--------------------------|------------|
| Tom     | langfristig           | Einzelarbeit  | schnell                | Abends, Nachts           | digital    |
| Sofia   | mittelfristig         | Partnerarbeit | langsam                | Mittags, Abends, Nachts  | present    |
| Svenja  | kurzfristig           | Gruppenarbeit | schnell                | Morgens, Mittags, Abends | digital    |