# Zusammenfassung der Interviews

Es wurden insgesamt vier Interviews durchgeführt. Die Auswahl unserer Interviewten erfolgte auf Basis unserer Persona. Die Erkenntnisse aus den Interviews werden im Folgenden zusammengefasst: <br>


- Alle Interviewten weisen eine unstrukturierte Organisationsstrategie auf <br>
- Hinsichtlich einer Entwicklung der Organisationsstrategie kam es bei keinem der Interviewten zu einer verbesserten Situation <br>
- Die Interviewten waren aufgrund aufwendiger Eintragungen der Termine in diversen mobilen Applikationen abgeschreckt, sodass sie diese nach kurzer Zeit wieder deinstalliert haben <br>
- Aufgrund von fehlenden Erinnerungen kam es bei den Studierenden vermehrt zu Stresssituationen <br>
    -  In der finalen Phase des Semesters erhöhte sich das Stresslevel nochmals <br>
- Die befragten Studierenden haben wenig bis keinen Aufwand für die Planung ihrer Abgaben geleistet <br>
- Keiner der Interviewten hat sich zusätzlich zu den eingetragenen Terminen Notizen jeglicher Art gemacht

## Schlussfolgerung
- Eintragungsaufwand (vor allem bei unregelmäßigen Terminen) so gering wie möglich halten <br>
- Integration unterschiedlicher Kalenderabonnements wäre wünschenswert <br>
- Den Nutzer\*innen eine langfristige Organisationsstrategie bieten, die einfach zu verwalten ist <br>
- Bei den Nutzer\*innen gibt es keinen vorrangigen Bedarf Notizen zu notieren  
- Um eine kontinuierliche Organisationsstrategie der Nutzer\*innen zu gewährleisten, sind Optimierungsvorschläge seitens der App wünschenswert
