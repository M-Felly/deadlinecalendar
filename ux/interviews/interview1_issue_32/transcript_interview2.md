## Einleitung
### 19.11.2020 12:00 
### Der Interviewte ist einverstanden mit der Aufnahme.<br>

Das I steht für den Interviewten und M für den Interviewer.

M: Wenn du dich bereit fühlst, dann können wir jetzt mit dem Interview starten. <br>
I: Ja gerne<br>
M: Es gibt keine flaschen Antworten, wenn du irgendwas nicht beantworten willst, musst du das nicht. <br>
I: okay<br>
M: Ich bin Matthias und ein Teammitglied eines Hochschulprojektes was ein Interview zu diesem Projekt durchführt. <br>
   Möchstest du dich auch noch kurz vorstellen?<br>
I: Ich bin - studiere in Darmstadt jetzt im ersten Semester jetzt seit 3 Wochen <br>
M: Was studierst du?<br>
I: Ich studiere Wirtschaftsingeneurwesen mit der Fachrichtung Elektrotechnik und Informationstechnik.<br>
M: Interessant. Wie geht es dir momentan mit dem Unileben?<br>
I: Gut. Es läuft gut. Die Uni hat ja erst vor drei Wochen angefangen aber ich komm mit den Inhalten gut mit und verstehe die meisten Inhalte. Das Onlinesemester hat so seine Herausforderungen aber ich versuche die Chancen darin zu sehen. Soweit läuft es ziemlich gut.<br>
M: Ja das Semester hat ja grad erst angefangen. Aber hast du da jetzt schon regelmäßige Termine oder Abgaben ? verpflichtende Abgaben?<br>
I: verpflichtende Abgaben habe ich nicht. Ich habe freiwillige Abgaben mit denen ich mir bei der Klausur, die am Ende des Semesters ansteht einen Notenbonus errechnen kann. Wenn ich eine gewisse Anzahl an Punkten bei den freiwilligen Abgaben erreicht habe, kann ich mir die Note um ein, zwei oder sogar drei Notensprünge verbessern.<br>
M: Wow das ist ja schon einiges. Und schaffst du das auch so?<br>
I: Als in Mathe und Elektrotechnik haben wir die freiwilligen Abgaben. In diesen hatte ich jetzt schon jeweils zwei Abgaben und die liefen auch schon sehr gut. Daher bin ich jetzt auch optimistisch, dass ich den Zeitplan auch in Zukunft gut einhalten kann. Ich denke dann ist ein oder sogar zwei Notensprünge drin. <br>
M: Also zeitlich gesehen passt das?<br>
I: Ja. Das kriegt man ganz gut unter einen Hut.<br>
M: Hast du dafür eine besondere Struktur oder Strategie zugelegt, wie du deine Abgaben im Blick behälst?<br>
I: Ja 24 Stunden vorher mir überlegen, wie ich das am besten mache. Tatsächlich trage ich mir im Kalender ein, wann die verschiedenen Abgaben fällig sind und mach sie dann meistens am Tag vorher.<br>
M: Das heißt du hast dafür einen persönlichen Kalender?<br>
I: Ja ich benutze meinen IPhone-Kalender, wo ich meine Termine alle eintrage.<br>
M: Wieviel Zeit verwendest du darauf diese Eintragungen zu machen?<br>
I: Pro Abgabe 30 Sekunden<br>
M: Du machst das aber für jeden Kalender neu?<br>
I: Ja sobald es bekannt ist, wann die Abgaben fällig sind nehme ich mir direkt mein Handy und trage die Abgabe gleich ein.<br>
M: Das heißt auch, dass die Abgaben weniger ein Intervall haben sondern eher immer unterschiedlich?<br>
I: Doch es hat schon ein Intervall. Mathe ist bis Freitag um 10 Uhr fällig und Elektrotechnik bis Sonntag um 23:59 aber immer um eine Woche versetzt. <br>
M: Du hast ja jetzt erst neu angefangen. Das heißt ja du weißt okay wie ist das jetzt gerade und welche Anforderungen hat man an der Uni und da ist jetzt so die Frage: Was würdest du anderen Ersties empfehlen? Also was machst du gut und könntest das anderen weiterempfehlen?<br>
I: Als das wichtigste ist auf jeden Fall am Ball zu bleiben, weil auch wenn man sich die Inhalte anschaut und denkt man hat nur eine Vorlesung pro Modul pro Woche und vielleicht eine Übung alle zwei Wochen. Und dann denkt man könnte jetzt mal 2-3 Wochen nichts für die Vorlesung machen und dass dann an einem Wochenende nachholen. Das täuscht sehr. Also ich würde jedem empfehlen von anfang an bis zum ende konsequent mitzuarbeiten und am Ball zu bleiben und wenn es geht die Inhalte schon am Wochenende direkt zu wiederholen. Weil das sonst in der Klausurenphase alles auf einen zurückkommt wie eine Lawine. Und das unmöglich ist das von komplett neu aufzubereiten. Es ist auch wichtig gleich alles zu verstehen, da es ja aufeinander aufbaut.<br>
M: Du hast ja gesagt, dass du deine Abgaben erst 24 Stunden vorher organisierst und kommt es dann auch mal vor, dass du dann deswegen im Stress bist?<br>
I: Ja tatsächlich ist mir das so bei der letzten Matheabgabe passiert. Da sie ja am Freitag früh um 10 Uhr fällig sind und ich erst am Donnerstag mittag damit angefangen habe. Dabei ist mir dann aufgefallen, dass ich das Thema scheinbar doch noch nicht so gut verstanden habe. Und habe dann den ganzen Mittag daran rumgetüftelt und war dann am Abend sehr kaputt, um es noch weiter zu versuchen und musste mir dann für Freitag morgen um 7 Uhr den Wecker stellen, um es dann noch irgendwie zu schaffen. Und Abgeben habe ich dann tatsächlich um 9:50 Uhr. 10 Minuten vor dem final cut habe ich es noch geschafft aber das war natürlich kein schönes Gefühl. Wenn man dann um 7 Uhr aufsteht und man weiß, man muss dann direkt seinen Kopf anstrengen und es muss geliefert werden.<br>
M: Da ist ja dann auch das Stresslevel sehr hoch.<br>
I: Ja genau und das wäre eigentlich leicht zu vermeiden, wenn ich 48 Stunden vorher damit anfange.<br>
M: Hast du hierzu schon andere Hilfstellung für die Terminorganisation verwendet? z.B. spezielle Apps oder auch Handschriftlich?<br>
I: Nein tatsächlich noch nicht. Momentan mache ich die finalen Termine immer im Kalender wobei zwei Abgaben nicht so schwer sind, sich die im Kopf zu behalten. Aber was anderes außer meinen IPhone-Kalender habe ich noch nicht.<br>
M: Könntest du es dir vorstellen?<br>
I: Tatsächlich schon. Ich bin schon seit längerem auf der Suche nach einer ToDo-Liste mit einer Kalenderfunktion als App. Dass ich meine ToDo's quasi in einen Kalendertag eintragen kann und habe da allerdings bisher noch nichts gutes gefunden. <br>
M: Okay. Das sind ja schon gute Anregungen. Du hattest ja vorhin erzählt, dass du ja Stress mit deiner Abgabe hattest. Rekapitulierst du das dann auch und denkst dir so ja das war ja jetzt nicht so gut gelaufen. Wie kann ich das besser machen? Oder?<br>
I: Ja auf jeden Fall. Jetzt ist es ja zum Glück noch ganz gut gelaufen. Zum Glück. Aber wenn ich es jetzt garnicht mehr geschafft hätte, wäre das natürlich schlechter gewesen. Das ist aber natürlich nicht wünschenswert das so kurz vor knapp zu machen und ja das nächste mal werde ich versuchen damit schon Mittwoch oder Dienstags anzufangen. Für eine entspanntere Abgabe.<br>
M: Wir bleiben mal beim Mathethema. Immer so eine ganz interessante Frage bist du ein Statistiknarr? Manche mögen es ja so Statistiken zu haben oder so Zahlen.<br>
I: Ja ich liebe Statistiken.<br>
M: Also hättest du sowas auch gerne also auf deine Planung bezogen. Wann lief es gut und wann weniger gut?<br>
I: Ja auf jeden Fall. Ich bin ein großer Statistik-fan <br>
M: Hast du zufällig eine Idee, auf die Statistik bezogen, was du jetzt so gerne wissen würdest?<br>
I: Spontan fällt mir leider jetzt nichts ein.<br>
M: Okay ist nicht schlimm. Dann noch die Frage: Gibt es noch irgendwas, was du zum Thema Organisation/Planung loswerden möchtest?
Was di persönlich im Alltag sehr aufgefallen ist.<br>
I: Ja. Was mir tatsächlich fehlt ist eine allumfassende App oder Programm, indem ich Abgaben und Studenplan und sowas alles eintragen kann. Ich habe versucht mir sowas selber in Excel zu bauen aber das wurde sehr schnell sehr unübersichtlich.
Mein Traumzustand wäre quasi ein Kalender wo ich zum einen meinen Studenplan sehe - also alle Sachen, die ich so während dem Semester habe - aber daneben sowas wie eine Informationsspalte inder dann die Abgaben und bis wann sie fällig währen aufgelistet sind. Also dass das ganze nicht nur rein zeitlich aufgearbeitet wird sondern vielleicht und etwas inhaltlich. Sodass dann nicht nur da steht ich habe von 10 Uhr bis 12 Uhr Mathe sondern auch noch mit den Themen. Oder bis Sonntag nachts steht die Elektroabgabe zwei an und nicht nur Abgabe Elektrotechnik 23:59. <br>
M: und das hattest du versucht dir in Excel selbst zu bauen?<br>
I: Ja mit Tabelle und halbstündig dann organisiert. Um die Vorlesungen möglichst genau einzutragen. Mit z.B. einer Überschrift "Montag" und zwei unterspalten mit "Fach" und "Themen". Aber das wurde schnell zu groß und zu unübersichtlich. Und das jedes Mal von Hand zu aktuallisieren ist ja auch sehr anstrengend und die Formatierung bei zu behalten. Deswegen habe ich es dann auch sehr schnell gelassen. <br>
M: Ja verständlich. Okay. Dann sind wir jetzt am Ende des Interviews angelangt. Jetzt nochmal so die Allgemeine Frage. Wie hat dir denn das Interview gefallen?<br>
I: Ich fand die Interviewer sehr sympatisch und es war in einer sehr entspannten, angenehmen und fast schon heimischen Atmosphäre. <br>
M: So sollte es sein. <br>
M: Hast du noch irgendwelche Punkte, wo du sagst die könnten wir noch verbessern?<br>
I: Nee macht ihr gut. Weiter so.<br>
M: Das hört man doch gerne. Dann danken wir dir für deine Zeit und deinen offenen Antworten und wir würden dir gerne anbieten eine der ersten Nutzer unseres Pilotprojekts des "Deadline-Kalenders" zu sein und ansonsten wünschen wir dir noch viel Erfolg in deinem Studium!<br>
I: Vielen lieben Dank!<br>





      

