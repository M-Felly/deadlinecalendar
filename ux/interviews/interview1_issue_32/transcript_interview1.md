## Informationen
Start der Aufzeichnung um 18.11.2020 18:25 in Darmstadt. Aufzeichnungsdauer 14 Minuten und 30 Sekunden.<br>
Der Interviewte hat mit der im Anschluss anonymisierten Aufzeichnung zur analytischen Auswertung mündlich eingewilligt.<br>

## Transcript
Nadine: Wenn du dich bereits fühlst, würden wir das Interview beginnen.<br>
Interviewte: Ich fühle mich bereit<br>
Nadine: Es gibt keine Falsche Antworten, also du darfst alles sagen was du sagen möchtest und ja wenn du irgendwelche Fragen hast stell die gerne<br>
Nadine: Ich stell mich am besten kurz vor. Ich bin die Nadine. Ich bin Teil eines Hochschulprojekts und dafür führen wir ein Interview durch. Willst du dich vielleicht auch kurz vorstellen?<br>
Interviewte: Ich bin <Name wurde gebeept>, studiere Informatik an der TU. Ich weiß nicht, braucht ihr noch irgendwas? <br>
Nadine: Was studierst du?<br>
Interviewte: Informatik<br>
Nadine: In welchem Semester bist du?<br>
Interviewte: Ohje ich glaube im siebten<br>
Nadine: Wie gehts dir denn aktuell so mit dem Unileben?<br>
Interviewte: Ja, aktuell ist es so ein bisschen doof. Man hat sich aber jetzt schon wieder dran gewöhnt an die anderen Vorlesungen. Hat seine Vor- und Nachteile<br>
Nadine: Hast du generell Abgaben so im Studium? Also Hausarbeiten, Übungen, Praktika, Protokolle?<br>
Interviewte: Übungen durchaus. Also um einen Bonus zu bekommen müssen wir wöchentlich, in anderen monatlich abgeben und Hausarbeiten kommen auch ab und zu dazu. Also ja, Deadlines existieren.<br>
Nadine: Wie oft musst du dann Hausarbeiten abgeben?<br>
Interviewte: Also Hausarbeiten, kommt drauf an... meistens am Ende des Semesters und die Übungen wöchentlich<br>
Nadine: Schaffst du es deine Abgaben rechtzeitig abzugeben oder fällt dir das schwer?<br>
Interviewte: Also eigentlich funktioniert das ganz gut wenn ich sie mir vornehme abzugeben, also eigentlich klappt das ganz gut<br>
Nadine: Hast du deine Abgaben auch immer gut im Blick oder fällt dir das sehr schwer?<br>
Interviewte: Ab und zu merkt man dann eine Woche vorher "hmm da müsste man ja doch noch vorher was machen" oder "da kommt ja doch noch was auf einen zu" und dann wirds immer etwas stressiger. Aber das hat meistens eigentlich immer ganz gut geklappt.<br>
Nadine: Ok und wie organisierst du dich dann so im Studium? Trägst du dir das irgendwo in Todo-Listen ein oder im Kalender oder wie merkst du wann die Abgaben sind?<br>
Interviewte: Ich schreib mir das meistens in meinen Kalender ein. Damit ich immer einen Überblick hab. Zusätzlich gibt es von der TU die TUCAN-App wo die ganzen Appointments bzw. Abgaben immer direkt angezeigt bekommt was einem das auch leichter macht um einen Überblick zu behalten. Da sind allerdings nicht die wöchentlichen Übungen mit innbegriffen nur Projektabgaben<br>
Nadine: Ok und wie viel Zeit steckst du dann in diese Einträge rein also dass du dir diese Kalendereinträge erstellst? <br>
Interviewte: Meistens am Anfang des Semesters wenn man die Termine bekommt bzw. für die wöchentlichen Übungen die man von der Uni bekommt dann sind das so.... 10 bis 15 Minuten. Bzw 10 bis 15 Minuten ingesamt, Ja also am Anfang des Semester um die ganzen Übungen wöchentlich einzutragen und für die Abgaben am Ende des Semester bzw auch die Klausurtermine.<br>
Nadine:  Stellst du dir dann vorher auch nochmal so Erinnerungen oder hast du dann immer nur diesen einen Termin drinne und hoffst dass du es rechtzeitig vorher noch siehst.<br>
Interviewte: Gut meistens bekommt man vorher... hm. ja schwierig... eigentlich stell ich mir vorher  nicht nochmal extra Erinnerungen rein. Allerdings hab ich das auch immer so im Hinterkopf. Aber ja ne so wirklkich Erinnerungen vorher hab ich eigentlich nicht.<br>
Nadine: Und wie weit im vorhinein planst du wenn du weißt "ok du hast da jetzt eine Abgabe". Wie weit planst du das im vorhinen zu machen?<br>
Interviewte: Dadurch dass man bei meistens Projektarbeiten oder Hausarbeiten meistens so einen groben Plan abgeben muss, plant man das relativ am Anfang was man wann abgeben möchte. Daher hat man da schon ein bisschen ein Überblick. Ob man sich darain immer so hält sei mal dahingestellt. Damit plan ich das meistens. Für die wöchentlichen Übungen hat man das meistens immer so auf dem Schirm dass diese kommen.<br>
Nadine: Und wie gut klappt das bei dir? Also wenn du dir vorher diesen Plan erstellst. Wie gut hälst du das ein?<br>
Interviewte: Also die Termine hat bisher noch nie so gut geklappt dass man sich perfekt an die Termine gehalten hat. Also meistens ist das dann alles immer nach hinten rausgerutscht. Ich würde mal sagen: so mittelmäßig hat man sich daran gehalten, damit man so grob einen Plan hat "dann macht man das, dann macht man das". Aber ob das jetzt wirklich an dem Termin fertig war... nicht so oft.<br>
Nadine: Hätte es dir geholfen wenn du noch zusätzlich eine App gehabt hättest die dir genau an dem einen Tag gesagt hätte "ok jetzt fängst du an"? <br>
Interviewte: Also ich hätte bestimmt ein bisschen mehr dran gedacht, dass ich daran was machen müsste. Also ich beziehe das jetzt mehr auf meine Abgaben am Ende des Semesters hin. Also hätte ich bestimmt weniger Stress am Ende des Semester gehabt da ich dann einen besseren Überblick gehabt hätte wo ich mich gerade in dem Projekt befinde. Zu den Teilaufgaben...<br>
Nadine: Was würdest du denn so den Erstsemestlern raten bzw. also für ihre Planung in ihren Studienalltag so empfehlen?<br>
Interviewte: Besucht die Übungen! Das hilft schon sehr, damit man auch im Stoff drinne bleibt und auch eine Übersicht über die Themen behält. Gut und zu dem Projekt.... eigentlich nicht dass so zu machen wie ich das mache dass meistens grad am Ende hin hinaus läuft sondern immer Stückchenweise dran zu arbeiten. Das macht durchaus Sinn das so zu machen wie es Vorgeschlagen wird.<br>
Nadine: Also empfehlst du dich wirklich an deren Planung zu halten?<br>
Interviewte: Ja<br>
Nadine: Also erstellen die dann selber die Planung oder kriegen die da noch irgendeine Unterstüzung.<br>
Interviewte: Also bei mir war das so, dass in der O-Phase bekommt man das am Anfang meistens alles erklärt und da hat man dann auch Leute die man ansprechen kann falls man fragen oder so hat. Gut, und die TUCAN-App hilft da einem auch damit man initial einen Überblick behält über die wöchentlichen Abgaben. Meintest du das jetzt?<br>
Nadine: ja genau. Ist dir das dann am Anfang leicht gefallen oder hattest du dann schnell das Gefühl gehabt "ok ich weiß eigentlich gar nicht wann ich damit Anfangen soll weil ich weiß gar nicht genau wie viel Zeitaufwand das wirklich ist oder...."<br>
Interviewte: Also am Anfang hatte ich da ein bisschen mehr Respekt davor wie lange sowas dauern könnte. Da hat man meistens ein bisschen früher angefangen. Da war die Motivation zum Glück zu Beginn des Semesters noch ein bisschen höher, zum Ende des Semesters hat man dann auch gesagt "ok ich schaff das auch in kürzerer Zeit" und dann hat man etwas später angefangen.<br>
Nadine: Kamst du so mit der zeitlichen Planung so zurecht im ersten Semester?<br>
Interviewte: Im ersten Semester hat das noch besser geklappt als in den nachfolgenden Semestern<br>
Nadine: Wie hat sich denn deine Organisationsstrategie verändert in den letzten Semestern? also hast du das irgendwie noch optimiert? also abgesehen davon dass du sagst "ok ich brauch dann nich ganz so viel Zeit" dass du sagst "ok ja dann fang ich später an"?<br>
Interviewte: Also ich würde das nicht als "verbessert" bezeichnen sondern eher als verschlechter als man dann etwas fauler wurde und das alles etwas eingeschätzt hat als das man später anfangen könnte. <br>
Nadine: Also würdest dann schon irgendwas, Also für dich wäre es dann schon besser wenn du dann einfach so erinnert werden würdest...?<br>
Interviewte: ja genau<br>
Nadine: ... wenn dir irgendjemand sagen würde "so mach jetzt deine Aufgaben"<br>
Interviewte: genau das wäre schon praktsich weil das macht die TUCAN-App zb nicht<br>
Nadine: wie kommst du gerade so mit deiner aktuellen Planung zurecht und hast du aktuell eher Stress oder eher weniger?<br>
Interviewte: Aktuell hab ich eigentlich nicht so viel Stress. Das zieht sich dann eher zum Ende des Semester hin. Wo dann zusätzlich zu den Übungsabgaben noch die Klausurenphase kommt und die Abgabe von Projekten. Da wirds dann ein bisschen Stressiger. da verliert man dann auch schneller mal den Überblick.<br>
Nadine: Hast du bis auf den Kalender schonmal was anderes an Apps ausprobiert um deine Planung im Semester zu erstellen?<br>
Interviewte: Ganz am Anfang hab ich, cih weiß nicht wie die App hieß... gibts im Playstore. Die hatte ich ganz am Anfang des Semester verwendet nur wars da immer lästig jedes Semester neu die Kurse immer einzutragen, dann einzutragen wann welche Kurse statt finden, wann welche Abgaben sind. Und das muss man halt dort alles manuell eintragen und ... ja so viel unterscheidet sich die App vom normalen Kalender nicht. Aber es ein bisschen lästig als das nur im Kalender einzutragen<br>
Nadine: Und das hat sich dann für dich nicht ganz so rentiert oder du hast den Sinn... oder den Zeitaufwand den du mehr hattest... war für dich nicht so rentabel?<br>
Interviewte: Genau, es hatte nicht so viele Vorteile wie es einfach im Kalender einzutragen der zwar weniger Features hatte. Allerdings der Mehrwert war nicht groß genug als das ich es weiterhin genutzt hätte.<br>
Nadine: Aber könntest du dir vorstellen jetzt trotzdem mal wieder eine App auszuprobieren? oder irgendwelche anderen Hilfstmittel um dich besser zu struktorieren um deine Planung besser zu verwalten?<br>
Interviewte: Ich würde es machen wenn sie in das Uni-System mit integriert wäre. Wo das automatisch die Kurse rauszieht und mir die Termine einträgt. Dann würde ich das machen ja.<br>
Nadine: Also wenn dann direkt die Daten hinterlegt wären.<br>
Interviewte: Exakt ja. Also wenn da die Kurse einfach direkt eingetragen werden und die Termine... möglichst wenig selbst machen müsste<br>
Nadine: Ok also am besten eine Schnittstelle zum Unisystem... Ja hast du noch irgendwelche Fragen?<br>
Interviewte: Zu was für einer App wurde ich denn hier befragt oder was war das denn für eine Umfrage<br>
Nadine: Ja und zwar ist unsere Idee eben ein DeadlineKalender App zu erstellen womit man eben seine ganzen Abgaben besser im Blick hat. Und ja dass man das eben strukturiert sieht wann sind meine Abgaben und die App soll.. dir dann auch immer Erinnerunge dazu erstellen damit du weißt "wann muss ich mit meiner Arbeit anfagen? Wann sollte ich sie fertig haben? Wann muss ich sie abgeben und damit du alle Informationen auf einen Blick hast und dann eben so keine Abgaben mehr verpasst und kein Zeitlichen stress mehr hast"<br>
Interviewte: Sehr gut<br>
Nadine: Sonst noch irgendwelche Fragen?<br>
Interviewte: Eigentlich nicht, danke<br>
Nadine: Dann sind wir auch am Ende unseres Interviews angekommen. Ja wie hat dir denn das Interview gefallen?<br>
Interviewte: Ja war interessant. Bzw. ich konnte mir jetzt irgendwie wenig... ja ich konnte mir doch schon so ein gewisses Bild von dem machen was ihr da erstellt. Anhand der Fragen die ihr stellt. Aber ... ja hat mir gut gefallen#<br>
Nadine: Das ist schön. Hast du noch irgendwelche Verbesserungsvorschläge was wir beim nächsten Interview anders machen könnten?<br>
Interviewte: Eigentlich fand ich das hier ganz gut<br>
Nadine: Schön. Ja dann bedanke ich mich für deine Zeit und deine offenen Antworten und würden dir natürlich auch gerne anbieten dann einer unser ersten Benutzer unseres Pilotprojekts des DeadlineCalendars zu werrden wenn du hast möchtest.<br>
Interviewte: Ja gerne.<br>
Nadine: Dann wünschen wir dir auf jeden Fall noch viel Erfolg in deinem Studium und... vielen Dank.<br>
Interviewte: Danke<br>

--------------------------
Ende der Aufzeichnung  
