# Lebensgewohnheiteneitfaden für ein teilstandardisiertes Interview

## Hinweise

- Hinweis #1: Stellen Sie die richtigen Fragen: Stellen Sie nicht irgendwelche Fragen; die Fragen sollen Ihnen helfen, die NutzerInnen und deren spezifische Bedürfnisse, Lebensgewohnheiten, Wünsche und Nutzungskontexte besser zu verstehen. Die Antworten sind Basis für die Erarbeitung eines Anwendungsszenarios, sowie je einer funktionalen und qualitativen Anforderung (inkl. Quantifizierung) und einem Begeisterungsfaktor.ten vorzutragen
- Hinweis #2: Achten Sie auf eine “gute” Interviewvorbereitung und -durchführung: Je besser Sie das Interview planen und durchführen desto besser verwertbar sind die Resultate und Erkenntnisse. Beachten Sie deshalb die Prinzipien einer guten Interviewdurchführung (vgl. Videos und Links).
  - Stellen Sie offene Fragen, bei denen BenutzerInnen über ihre Erfahrungen berichten können
  - Vermeiden Sie Suggestivfragen – das ist vor allem wenn Sie das Interview das erste Mal durchführen sehr schwer umsetzbar; → bemühen Sie sich!
  - Fragen Sie nicht nach irgendwelchen technischen Details oder Features; diese sind für die Benutzung irrelevant.
  - Konzentrieren Sie sich auf die Ziele, den Anwendungskontext und die Lebenssituationen in denen ein Medium konsumiert wird.
  
### Ihre Hinweise auf Basis des durchgearbeiteten Materials


## Informationsbedarf für Persona
- Sprachkenntnisse (Herkunft) : Multilingualität der Anwendung
- Organisationsvermögen: Charakter der Person (organisiert, chaotisch etc.)
- Stresslevel während Studium und Prüfungsphase : Charakter der Person
- Arbeitsweise (Einzelarbeit, Partnerarbeit, Gruppenarbeit)
- Arbeitsgeschwindigkeit (langsam, schnell)
- Abgabeform (digital, present)
- Anzahl Module / Anzahl CP
- Aktuelles Fachsemester
- Studiengang


## Informationsbedarf für Anwendungsszenarien
- Anwendbarkeit der aktuellen Organisationsmethode (Erfahrungsbericht) : Visionäres Szenario
  - Wie kann die Organisationsmethode während des Studiums optimiert werden? (Evaluationsszenario)
- Wie organisieren sich Studierende? (Relevant für die angebotenen Organisationsformen)
- Welche Besonderheiten haben Studierenden an digitalen Hilfestellungen gefallen? (Trainingsszenario/ Einführung in die App)

## Merkmale für Benutzer*innengruppen
- Sozio-demographische Merkmale
  - Geschlecht
  - Alter (18+)
  - Familienstand
  - sozialer Status, Einkommen
  - Studienwahl
- Persönlichkeitsmerkmale
  - Emotionalität (emotional / sachlich)
  - Gemütsart (genügsam / ausdauernd / ungeduldig)
  - Humoreigenschaften (Humor in App? vgl. Discord)
- Gebrauchsspezifische Merkmale
  - Wahrnehmungsform (Notfications auditiv / visuell)
  - Horizontale- und vertikale Informationsdichte (Breites Spektrum && wenige Infos || Spezialisierung && Wenig Spektrum)

## Hypothesen
- Studierende haben i.d.R nicht alle Abgabefristen im Blick
- Die Anforderungen zur Einhaltung einer Frist können in Abhängigkeit zur anfordernden Person variieren
- Studierende tendieren eher zur Planung mittels digitaler Hilfsmittel
- Versäumnisse durch unzureichende Planung der Studierenden sind nicht unüblich
- Einfache und übersichtliche Planungshilfen motivieren im stressigen Alltag und verbessern den Gemütszustand
- Herkömmliche Kalender (analog u. digital) decken die Anforderungen des Studienalltages nur unzureichend ab
- Nutzer*innen wünschen sich zunehmend digitale Hilfsmittel
- Nutzer*innen begrüßen prägnante Informationsübersichten und optionale Einblendungen für mehr Informationen
- Nutzer*innen bevorzugen eine freundschaftlichere Kommunikation hinsichtlich App-Inhalte (Statusmeldungen und Menüführung weniger formal) 

### Vermutete mentale Modelle
1. Durch Tippen/Klicken des auf den Menüpunkt "Neuer Termin" geht der Benutzer davon aus, dass sich ein weiteres Menü öffnet, indem er zu einer neuen Terminserie Einstellungen wie Starttermin, Wiederholungen und Deadlines eintragen kann.
2. Ein Benutzer geht davon aus, dass ein längeres Antippen eines Termins im Kalender dazu führt, dass ein kleines Menü zum Bearbeiten, Löschen, etc. des bestehenden Termins erscheint.
3. Ein Benutzer geht davon aus, dass wenn er sich in der Monatsansicht befindet, mit den Fingern nach links ode rechts wischen kann, um sich den vorherigen bzw. nächsten Monat mit anstehenden Terminen und Deadlines anzeigen lassen zu können.
4. Da ein Student nicht immer alle Abgabefristen im Blick hat, wird er zur Vereinfachung zu einer Liste greifen. Aus diesem Grund sollten auch die Abgabelisten in Form einer "virtuellen Liste" dargestellt werden, um Bezug zu dem mentalen Modell des Nutzers herzustellen.

## Empfehlungen für gute Interviews
- Freundliche Begrüßung und Einleitung
- Gute und entspannte Atmosphäre schaffen
- Transparenz bezüglich etwaiger Aufzeichnungen während des Interviews
- Augenkontakt halten
- Denkpausen für den Gesprächspartner lassen
- Offene Fragen stellen
- Neutrale Fragen stellen
- Folgefragen stellen
- Paraphrasieren
- Aktives Zuhören
- Raum für Rückfragen, Kommentare oder Anregungen lassen
- Dankender Abschluss des Interviews

## Leitfaden (Vorbemerkungen zur Durchführung)

### Einstieg
#### Begrüßung
Hallo \<Name\>, danke, dass du dir Zeit nimmst. <br> 
Hast du gut hergefunden? <br>
Um später eine gute Auswertung machen zu können, würden wir das Gespräch gerne anonymisiert aufzeichnen. Bist du damit einverstanden? <br>
Wenn du dich bereit fühlst, können wir jetzt mit dem Interview beginnen.
Es gibt keine falschen Antworten und falls du eine Frage nicht beantworten möchtest, ist das natürlich auch in Ordnung. <br>
Ich bin \<Name Interviewer\> und Teammitglied des Hochschulprojektes DeadlineCalendar. Dieser soll Studierenden helfen eine bessere Organisationsstruktur zu ermöglichen. 
Hierzu führen wir im ersten Schritt Interviews durch, um alle Bedürfnisse der Studierenden abzudecken.<br>
Möchtest du dich kurz vorstellen und ein bisschen was über dein Studium erzählen? <br>
Wie geht es dir mit dem Unileben momentan?

#### Einführung in Rahmen des Interviews (Absichten klären)
- Um eine gute User Experience zu gewährleisten möchten wir unserer Zielgruppe in den Entwicklungsprozess mit einbeziehen.
  Konkret bedeutet das: 
  - Informationssammlung über Lernverhalten
  - Selbstorganisation (Erfahrungswerte)
  - Auskunft über Wunschvorstellung

### Themen
- Studienalltag
- Organisation und Aufgabenplanung
- Erfahrungswerte
- Idealvorstellung 

### Leitfragen
- Hast du gerade Abgaben (Hausarbeit, Übungen, Praktika, Protokolle)? 
  - Schaffst du alle Abgaben rechtzeitig fertigzubekommen?
  - Hast du alle Abgaben im Blick?
- Wie organisierst du dich unter dem Semester (ToDo-Liste, Kalendereinträge)?
- Wieviel Zeit bringst du für die Planung wöchentlich auf?
- Wie weit im Voraus planst du deine Aufgaben?organisierst
- Was würdst du einem Erstsemester-Studierenden für die Planung seines/ihres Studienalltags empfehlen?
- Wie hat sich deine Organisationsstrategie im Verlauf der vergangenen Semester entwickelt?
- Wie kommst du mit deiner aktuellen Planung zurecht und wie wirkt sich diese auf dein Stresslevel aus?
- Wurden schon digitale Hilfestellungen ausprobiert (z.B. in Form von Apps)?
  - Wenn ja, was hat dir daran gefallen bzw. nicht gefallen?
  - Welche Funktion war hier essenziell wichtig für dich?
  - Was war noch ausbaufähig?
- Was muss deiner Ansicht nach unbedingt in einer App vorhanden sein, damit du sie gerne nutzt? (zB. Features, Design, Ansichten) 
- Könntest du dir vorstellen eine zusätzliche Hilfe zu benutzen? Und wenn ja, wie würde diese aussehen?
- Würdest du dir wünschen vergangene Planungen und Abgaben reflektiert zu bekommen? (Recap)
- Könntest du dir vorstellen, dass sich durch rückblickende Statistiken über aufgewendete Zeiten, deine künftige Zeitplanung optimieren lässt?


### Abschluss  
Hast du noch Fragen oder Ergänzungen, die du noch einbringen möchtest? <br>
Dann sind wir jetzt am Ende des Interviews angelangt. <br>
Wie hat dir das Interview gefallen? Sind dir Punkte aufgefallen, die wir für künftige Interviews verbessern könnten? <br>
Danke für deine Zeit und offenen Antworten. <br>
Gerne würden wir dir anbieten einer der ersten Benutzer unseres Pilotprojekts Deadline-Kalender zu werden. <br>
Ich wünsche dir noch viel Erfolg bei deinem Studium.
