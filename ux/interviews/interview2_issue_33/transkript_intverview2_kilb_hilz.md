## Interview
### 18.11.2020 20:40
### Der Interviewte ist einverstanden mit der Aufnahme.

A steht für den anonymen Interviewten und I für den Interviewer.

I:  "Wir haben uns ja schon vorgestellt und wir kennen uns auch schon ein bisschen. Wäre schön, wenn du dich trotzdem einfach nochmal kurz vorstellst. Wer bist du? Was genau studierst du und in welchem Semester?" <br>
A:  "Ich bin der ..., 25 Jahre alt und studiere Maschinenbau an der Hochschule Darmstadt. Ich bin im achten Semester." <br>
I:  "Wie geht es dir im Studienalltag? Kommst du gut zurrecht?" <br>
A:  "Mal mehr, mal weniger stressig. Je mehr es dann auf die Klausurenphase zu geht, desto stressiger wird es für mich und desto unangenehmer wird es tatsächlich auch für mich." <br>
I:  "Hast du vor der Klausurenphase Testate oder Praktika? Hast du Abgaben, die du regelmäßig erledigen musst?" <br>
A:  "Ja, in diversen Praktika gibt es auf jeden Fall Abgaben, wo man dann auch Fristen einhalten muss. Da baut sich natülich schon mal ein gewisser Druck auf. Wenn es dann prüfungsvorbereitend ist, ist es schon mal ein bisschen stressig. Aber ist dann doch nochmal was anderes als eine Klausurenphase." <br>
I:  "Kann ich so bestätigen. Schaffst du dabei alle Abgaben rechtzeitig auch wenn es vielleicht mal eng wird. Schaffst du es dahingehend dich noch richtig zu organisieren, dass du es alles rechtzeitig schaffst?" <br>
A:  "Wenn ich gleich am Anfang von einem Modul merke das Praktikum wird mir zu viel, dann versuche ich es im vorhinein schon so zu sortieren, dass ich sage: Das Praktikum mache ich nicht, weil das wird mir zu viel und konzentriere mich dann eher auf die Dinge, die ich dann auch absolvieren möchte und das packe ich dann eigentlich auch." <br>
I:  "Okay. Wie organisierst du die Module die du dann am Ende machst. Erstellst du einen Plan?" <br>
A:  "Einen Plan habe ich nicht wirklich. Es ist ja auch so, dass man im Praktika dann auch in kleineren Gruppen arbeitet und da organisiert sich das dann schon relativ von selbst, würde ich sagen. Wenn man eine Dreier- oder Vierergruppe ist, dann macht man auch zusammen die Abgaben. Also ich denke, dass ich nicht so viel organisiere." <br>
I:  "Ja klar, da hat man natürlich auch den Vorteil, dass man wenig Planungsaufwand hat. <br> 
    Wenn jetzt jemand ganz frisch an die Uni kommt (1. Semester), was würdest du ihm an die Hand geben? Was würdest du ihm empfehlen, wenn er einfach seine 6 Module pro Semester machen möchte? Was kannst du ihm empfehlen, um sich am besten zu organisiseren? Was ist deine Idealvorstellung dabei?" <br>
A:  "Es ist auf jeden Fall von Vorteil, wenn man nicht nur in die Vorlesungen geht und es dabei sein lässt. Sondern, dass man noch ein bisschen mehr macht als das und den Stoff frisch im Kopf hat und nicht zu lange Pausen dazwischen lässt, damit man den Stoff wieder vergisst. <br>
    So geht es mir nämlich, dass ich dann während der Semesterferien und während den Vorlesungen nicht ganz dabei bin und am Ende das Problem habe, dass ich den ganzen Stoff nochmal erlernen muss. <br>
    Also ich kann jedem nur raten, dass er von Anfang an am Ball bleibt. Das ist schon der einfachste Weg, um gute Noten und am wenigsten Aufwand zu haben, denke ich." <br>
I:  "Du hattest gesagt, dass du dich mit einer Planungsstruktur oder mit irgendwelchen Hilfestellungen jetzt nicht organisierst. Könntest du dir das vorstellen? Beispielsweise mit einer analogen oder digitalen Hilfe?" <br>
A:  "Ja, ich denke schon. Also wenn man sich zum Beispiel einen Stundenplan wie in der Schule zurecht legt egal ob analog oder digital. Dass man sagt: Den Tag über habe ich meine Vorlesungen und nach den Vorlesungen lege ich mir einen Plan zurecht. Zu dem Zeitpunkt mache ich das, da lerne ich nochmal, da lese ich nochmal was, da rechne ich jetzt nochmal eine Aufgabe nach. <br>
    Dass man sich das digital oder analog nochmal aufschreibt, ist auf jeden Fall eine sinnvolle Sache." <br>
I:  "Okay super. Das war es von unserer Seite. Du kannst jetzt natürlich auch an uns noch Fragen stellen oder mal erzählen, wie dir das Interview gefallen hat." <br>
A:  "Die Fragen waren gut, leicht verständlich und waren einfach zu beantworten. Es war jetzt nicht schwierig sich irgendwelche Antworten ausdenken zu müssen." <br>
I:  "Okay. In diesem Sinne vielen Dank für deine Zeit und viel Erfolg noch bei deinem Studium." <br>
A:  "Dankeschön."
