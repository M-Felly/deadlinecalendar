## Interview
### 18.11.2020 20:20
### Der Interviewpartner ist einverstanden mit der Aufnahme.

**A** steht für den anonymen Interviewten und **I** für den Interviewer.

**I:** "Wir haben uns ja vor Aufnahmebeginn bereits kurz vorgestellt und dich ein wenig informiert über unsere Absichten. Würdest du dich bitte zu Beginn des Interviews nochmal kurz vorstellen?"<br>
**A:** "Klar doch, mein Name ist Lili, ich bin 29 jahre alt und studiere Pflege und Gesundheitsförderung an der evangelischen Hochschule Darmstadt"<br>
**I:** "Okay, vorab noch kurz: Es ist natürlich völlig in Ordnung, falls du nicht alle Fragen beantworten möchtest und es gibt auch keine falschen Antworten auf unsere Fragen"<br>
**A:** "*Signalisiert Einverständnis*"<br>
**I:** "Wie geht es dir denn derzeit mit dem Unileben im digitalen Semester und in welchem Semester befindest du dich derzeit?"<br>
**A:** "Also ich befinde mich im siebten Semester und ich finde es sehr schwierig momentan, da alles online stattfindet und das nicht sehr einfach ist sich zu Hause selbst zu strukturieren, da wir normalerweise einen festen Stundenplan haben und wir nicht viele Möglichkeiten haben von diesem abzuweichen, was ich bisher als sehr angenehm empfand."<br>
**I:** "Wie steht es bei dir mit Abgaben während des Semesters? Hast du Praktika oder Testate und wenn ja, wie gestalten sich diese bei dir im Studium?"<br>
**A:** "Also bei uns sind es eigentlich nur Hausarbeiten oder manchmal auch Arbeitsaufträge, welche jedoch nicht benotet sind und als Anwesenheitsnachweise fungieren. Die Hausarbeiten gelten dann als Modulprüfungen oder als Teil einer Modulprüfungen für die jeweiligen Fächer."<br>
**I:** "Wie viele Hausarbeiten schreibst du dann pro Semester?"<br>
**A:** "Es ist dann jeweils eine Hausarbeit pro Modul."<br>
**I:** "Hast du all diese Abgaben zum Semesterende dann noch im Blick?"<br>
**A:** "Wenn es nur Hausarbeiten sind, kann es schonmal unübersichtlich werden, da die Abgabetermine sehr variieren und ich zum Teil Vieles auf den letzten Drücker versuche zu erledigen."<br>
**I:** "Wie weit im Voraus planst du deine Abgaben und machst du dir hierfür einen Plan?"<br>
**A:** "Es ist tatsächlich so, dass ich mir in meinen Kalender schreibe, wann welche Abgabe ist und dann schaue ich mir an wie umfangreich es sein muss. Anschließend schreibe ich mir auf, dass ich bspw. zwei oder drei Wochen vorher anfangen muss Dieses oder Jenes abzuarbeiten."<br>
**I:** "Angenommen es kommt jemand ganz frisch an die Hochschule. Was würdest du dieser Person empfehlen bezüglich Studienplanung und Organisation? Hast du eine Idealvorstellung?"<br>
**A:** "Tatsächlich wäre es sinnvoll sich einen genauen Plan zu machen. Das hätte ich definitiv machen sollen. Jetzt, gegen Ende meines Studiums werde ich das wohl nicht mehr schaffen. Einen Wochenplan halte ich für sinnvoll, indem man festhält wann man wie viel für welches Fach machen sollte."<br>
**I:** "Würdest du sagen, dass sich deine Organisationsstrategie im Verlauf deines Studiums verändert bzw. angepasst hat?"<br>
**A:** "Nein, ich habe meine Strategie nie geändert, was vermehrt zu Stresssituationen geführt hat und auf Dauer sicherlich nicht gesund war."<br>
**I:** "Hast du während deines Studiums digitale Hilfsmittel als Planungshilfe ausprobiert oder in Erwägung gezogen?"<br>
**A:** "Nein, ich hatte lediglich das analoge Planen mittels Wochenplänen ausprobiert, was jedoch nicht sehr flexibel ist und die Nichteinhaltung der angelegten Pläne hat mich eher frustriert. Irgendwie habe ich die Abgaben immer geschafft und habe das dann auch immer so beibehalten. Auch wenn es mir dabei nicht immer so gut ging."<br>
**I:** "Du hast nun noch ein Semester bzw. musst nur noch deine Bachelorthesis schreiben. Könntest du dir hierbei vorstellen digitale Hilfsmittel zur Planung zu verwenden? Wie würdest du dir ein solches Hilfsmittel vorstellen?"<br>
**A:** "Ich habe es mir tatsächlich für die Thesis vorgenommen, meine Planung anders anzugehen, da mir die Endnote hierbei sehr wichtig ist und es nicht nur um das Bestehen geht. Ich glaube es würde mir sehr helfen, wenn einfach jemand anderes mir einen Plan erstellen würde an den ich mich dann halten muss. Besonders für die Literaturrecherche muss ich eine gute Planungsstrategie haben."<br>
**I:**  "Okay, dankeschön für deine ehrlichen Antworten. Hast du eventuell noch Fragen oder Feedback an uns zum Abschluss?" <br>
**A:**  "Gerne doch, das Interview war sehr angenehm und man konnte gut auf eure Fragen antworten!" <br>
**I:**  "Dankeschön, wir haben uns Mühe gegeben." <br>