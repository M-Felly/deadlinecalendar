# Thinking aloud (mit Paper-Prototypen)

## Hinweis
Für eine vollständig unabhängige und individuelle Evaluation zweier Paper-Prototypen im Usability-Check, wurden sowohl die Prototypen selbst, als auch die zugehörigen Aufgaben für die
Testpersonen ohne vorausgegangene Absprachen erstellt. Die Vergleichbarkeit wurde durch zuvor abgesprochene MVP-Inhalte gewährleistet.

## Einleitung
Hallo, 
schön, dass du dir die Zeit nimmst. Am besten erzähle ich dir kurz, worum es genau geht. Ich würde gerne mit dir einen Usabilitytest für unsere Deadline-Kalenderapp durchführen. 

Erstmal zur kurzen Erklärung. Unser Deadline-Kalender soll den Studierenden dabei helfen, ihre Abgaben nicht mehr zu verpassen und rechtzeitig mit diesen anzufangen. Hierbei sollen bei der Erstellung einer Deadline alle wichtigen Informationen eingetragen werden und die Einstellungen - wann eine Erinnerung vor einer Abgabe erscheinen sollen. Diese Deadlines können individuell angelegt werden. Für jede Deadline können verschiedene ToDo's angelegt werden, damit man auf einen Blick sieht, wieviel noch für die Abgabe getan werden muss und wieviel Zeit man für diese Abgabe noch hat. 

Um hierfür eine User freundliche App erstellen zu können haben wir einen Paper-Prototypen erstellt, den wir gerne mit dir testen würden. Hierbei möchten wir die Anordnung und Funktionen der App testen. Damit wir anschließend eine User freundliche App erstellen und eventuelle Verständnisprobleme vor der Entwicklung schon vermeiden können. Für den Test würden wir dir gerne ein paar Aufgaben stellen, damit du die verschieden Funktionen kennenlernst.


## Aufgaben
1. Erstelle ein neues Modul und richte die Erinnerungen für eine Abgabe ein.
2. Liste deine Termine für einen Tag auf
3. Füge eine neue Erinnerung hinzu für ein einmaliges Ereignis
4. Sehe deine ToDo's ein und hake ToDo's ab
5. Bearbeite eine Erinnerung auf ein anderes Datum
6. Bestätige eine Erinnerung, nachdem diese dich erinnert und bestätige diese als erledigt
7. Bestätige eine Erinnerung, nachdem diese dich erinnert und bestätige diese als noch nicht erledigt
8. Kalender importieren

## Abschluss
- Wie hat dir die App gefallen?
- Was hat dir nicht so gut gefallen?
- Hat dir das Design gefallen?
- War die App für dich logisch aufgebaut?

## Durchführung
Die zuvor gestalteten Screens sind auf Papier dargestellt. Der User drückt auf einen Button einer Activity und der, der den Test durchführt tauscht das Paper aus auf die passende nächste Activity. Das wird solange durchgeführt, bis alle Aufgaben abgearbeitet sind.  

## Prototyp 1
### Aufgabe 1: "Erstelle ein neues Modul"
- **Testperson 1**: Die Navigation über das Profil in der BottomNavigation wurde sofort angewendet. Die Umschaltung (Toggle) in den Bearbeitungsmodus war nicht gleich ersichtlich. Das Hinzufügen des Moduls anschließend jedoch schon.
- **Testperson 2**: Die Navigation über das Profil in der BottomNavigation wurde fast sofort gefunden. Zuerst wollte derjenige einen Termin erstellen. Ich habe die Frage dann nochmal wiederholt und dann hat er/sie es sofort gefunden.

### Aufgabe 2: "Liste deine Termine für einen Tag auf"
- **Testperson 1**: Sowohl die geteilte Listenansicht, als auch die dedizierte Listenansicht wurden gut wahrgenommen und waren intuitiv bedienbar.
- **Testperson 2**: Die Liste wurde sehr schnell gefunden, da es nur einen Klick erforderte.

### Aufgabe 3: "Füge einen bestehenden Kalender hinzu"
- **Testperson 1**: Über die Menüführung im Profil gab es keine Probleme in das entsprechende Untermenü der Einstellungen zu navigieren.
- **Testperson 2**: Es wurde ebenfalls schnell gefunden. Die Testperson merkte an, dass man bei den Kalender Abonnements noch einen Info- Button hinzufügen kann, da viele sich wahrscheinlich damit nicht auskennen.

### Aufgabe 4:  "Editiere deine Profilinformationen"
- **Testperson 1**: Nach Umschaltung in den Bearbeitungsmodus stellte das Editieren der Profilinformationen keine Hürde mehr dar.
- **Testperson 2**: Das Editieren der Profilinformationen hat ebenfalls reibungslos funktioniert.

### Aufgabe 5:  "Verändere das App-Erscheinungsbild"
- **Testperson 1**: Das entsprechende Untermenü wurde schnell gefunden.
- **Testperson 2**: Das App-Erscheinungsbild wurde schnell gefunden und der Dark Mode aktiviert. Diese Funktion war mit unseren Prototypen nicht umsetzbar, da es zu viel Aufwand erfordert hätte.

### Aufgabe 6: "Wechsle den Kalendertag und schaue dir die Termine des Tages an"
- **Testperson 1**: Der Wechsel des Tages konnte sofort umgesetzt werden (Bekannt aus anderen Kalendern).
- **Testperson 2**: Der Wechsel des Kalendertages konnte ebenfalls sofort umgeführt werden.

### Aufgabe 7: "Wechsle in die Listenansicht und filtere die anstehenden Abgaben nach einem bestimmten Modul"
- **Testperson 1**: Das Filtersymbol in der Listenansicht wurde als Solches identifiziert und die Filterung nach "Modul-Tags" kam als Idee sehr gut an.
- **Testperson 2**: Der Wechsel wurde schnell erreicht und die Abgaben erfolgreich gefiltert.

### Feedback
- **Testperson 1**: Gutes Design, einfache Navigation und geringe Komplexität. "Kein Schnick Schnack!"
- **Testperson 2**: "Die App ist super übersichtlich und bis auf die Sachen, die ich angemerkt habe, entspricht das meinen Bedürfnissen."

#### Allgemein
- Toggle für Bearbeitungsmodus weglassen und lieber mit klassichem Edit-Button arbeiten
- Farbgebung für Module gut abgrenzen zu funktionslosen Icons um
- Semesterferien einstellen und auf Termine anlegen, sodass bei diesen Terminen in den Semesterferien nichts erscheint.

#### Erstellung des Kalendereintrags
- Als wichtigste Funktion sofort erkennbar und einfach umsetzbar

#### Profil
- Einheitlicher Bearbeitungsmodus ohne große Einblendungen

#### ToDo-Liste
- To-Do Liste innerhalb eines Deadline Objektes ersichtlich und einfach zu verwalten
- Liste nur für ein Intervall anzeigen lassen -> ist befriedingender, wenn weniger in der Liste steht

#### Modulerstellung
- Farbauswahl intuitiver gestalten (Evtl. Prägnante Farbe als Default setzen, Beschriftung einfügen oder zu anderen interaktionslosen Icons abgrenzen)

#### Kalenderansicht
- Evtl. Wochentage etwas größer gestalten

#### Bearbeitungsmodus
- Termine und Deadlines können nicht gelöscht werden
- Perioden für die Termine und Deadlines (z.B. alle 2 Wochen)

#### 'Neue Deadline'-Modus
- Neue Deadline anlegen ohne Datum
- bei Reiter soll es auch ok sein nichts anzugeben

#### 'Neuer Termin'-Modus
- tag's für die Unterscheidung von Vorlesung, Übung, Praktika,...

#### Anmerkung:
- Bottomnavigation mit nur drei Elementen sorgt für geringe Komplexität und ist sehr gut bedienbar bedienbar (Fitt's Law)
- Die App ist übersichtlicher gestaltet als der aktuell genutzte Kalender der Testperson

## Prototyp 2

### Aufgabe 1: "Erstelle ein neues Modul und richte die Erinnerungen für eine Abgabe ein"
- Testperson 1: Nicht gleich einleuchtend, dass man das Profil über den Button "Mensch" hinzufügen kann. <br>
- Testperson 2: Versuchte, über die Listenansicht ein neues Modul anzulegen -> nicht sofort erkennbar <br>

### Aufgabe 2: "Liste deine Termine für einen Tag auf"
- Testperson 1: Es wurde nicht gleich verstanden, dass die farbliche Hervorhebung dafür steht, dass hier Abgaben eingetragen sind. <br>
- Testperson 2: Es wurde auf einen beliebigen Tag des Kalenders gedrückt. Die farbliche Hervorhebung eines Tages, das für einen an dem Tag bestehenden Termin steht, wurde nicht wahrgenommen. <br>

### Aufgabe 3: "Füge eine neue Erinnerung hinzu für ein einmaliges Ereignis"
- Testperson 1: Der/Die Tester\*in war sich nicht sicher, ob neue Abgaben in der Todo-Liste hinzufügt werden sollen oder im Kalender. <br>
- Testperson 2: Die Testperson fand sofort den Button zum Hinzufügen einer Erinnerung. <br>

### Aufgabe 4:  "Sehe deine ToDo's ein und hake ToDo's ab"
- Testperson 1: War sofort klar, wohin er/sie musste. <br>
- Testperson 2: Aufgabe wurde schnell und ohne Missverständnisse gelöst. <br>

### Aufgabe 5:  "Bearbeite eine Erinnerung auf ein anderes Datum"
- Testperson 1: Option Bearbeitung in der To-Do-Liste und Kalender wurde als sehr positiv empfunden. <br>
- Testperson 2: Die Option zum Bearbeiten wurde sofort erfasst und durchgeführt. <br>

### Aufgabe 6: "Bestätige eine Erinnerung, nachdem diese dich erinnert und bestätige diese als erledigt"
- Testperson 1: Diese Option war sehr verständlich für den Tester\*in. <br>
- Testperson 2: Handelte schnell und intuitiv. <br>

### Aufgabe 7: "Bestätige eine Erinnerung, nachdem diese dich erinnert und bestätige diese als noch nicht erledigt"
- Testperson 1: Hierbei wollte der/die Tester\*in seine eigene Einstellung zu der erneuten Erinnerung machen. <br>
- Testperson 2: Handelte schnell und intuitiv. <br>

### Feedback
- Testperson 1: Der/Die Tester\*in war sehr begeistert von der App. Er/Sie fande die App logisch aufgebaut.
- Testperson 2: Die Testperson fand die Idee der App sehr gut aber äußerte sich über die schwer zu verstehende Struktur hinter Erinnerungen, Deadlines und Modulen. Sie hätte sich eine klarere Differenzierung gewünscht. Die Bottom Navigation mit nur 3 übersichtlichen Buttons wurde als sehr positiv bewertet.

Beide Testpersonen brachten noch einige Verbesserungvorschläge:

#### Allgemein
- ToDo-Liste ist ein passender Startscreen
- swipen, um die activities zu wechseln
- überschriften in die Mitte
- Symbol für die Bottom-Navigation ändern für personenbezogene Daten / Module
- Proportionen von bootom-navbar anpassen
- Klare Differenzierung von Erinnerungen, Deadlines und Modulen
- Über "Hinzufügen"-Button auch Module hinzufügen können

#### Erstellung des Kalendereintrags
- extra Punkt für Module mit dropdown Menu in denen die hinterlegten Module ausgewählt werden können
- bei der Auswahl des Datums sollte auch der Wochentag angezeigt werden

#### Profil 
- der/die Tester\*in würde kein Semester, Studiengang und eventuell auch keinen Namen
- Übersicht module: nur ein Stift statt viele (alle Module auf einmal bearbeiten) oder lange drücken um ein Modul zu bearbeiten

#### ToDo-Liste 
- ToDo's logisch trennen 
    - eine Box für alle Abgaben die heute anstehen, diese Woche und später und abgehakte ToDo's
- Datum sollte unter dem ToDo angezeigt werden
- unterpunkte für die ToDos 
- ToDo als Button 
    - kurz drücken: "drop-down-menu" anzeigen, um unter Punkte anzeigen zu lassen
    - lang drücken: bearbeiten -> statt die Bearbeitungsstifte
- langes Draufdrücken/ ANklicken zum Bearbeiten einer Abgabe


#### Kalenderansicht
- swipen zum Monat wechseln oder beim kalender pfeile um monate zu wechslen 
- auf Tag lange drücken Pop-up erscheint und Ausblenden, sobald man es loslässt 
-Rauszoomen zur Jahresansicht/ Wochenansicht
- Swipen für nächsten Tag/ Monat/ Jahr

#### Erinnerung: 
- nicht nur die Option x Tage vorher erinnern sondern auch alle 2 Tage und individuelle Einstellung

#### Anmerkung: 
- Testperson 2 merkte an, dass auch die Differenzierung zwischen normalem Kalender und  DeadlineCalendar noch weiter ausgeführt werden sollte.

## Beschluss
Nach Besprechung aller Ergebnisse und den daraus gewonnenen Erkenntnissen, haben wir uns gemeinsam gegen das Erstellen eines dritten Prototypen entschieden. Mit der Zusammenführung beider Prototypen zu einer finalen Version, sind unter Berücksichtigung des User-Feedbacks, alle MVP-spezifischen Funktionalitäten, Design-Ideen sowie besprochene Navigationselemente umgesetzt.