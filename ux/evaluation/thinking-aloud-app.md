# thinking aloud der App
prinzipieller Ablauf, was gilt es zu beachten?

## Einleitung
### Begrüßung
Hallo \<Name\>, danke, dass du dir Zeit nimmst. <br> 
Hast du gut hergefunden? <br>
Ich bin \<Name Interviewer\> und Teammitglied des Hochschulprojektes DeadlineCalendar. Dieser soll Studierenden eine bessere Organisationsstruktur ermöglichen.<br>
Bei einem thinking-aloud geht es im Wesentlichen darum, dass du ohne Einflussnahme durch uns (Tipps etc.) versuchst, gestellte Aufgaben innerhalb der Applikation zu bewältigen.

Ziel ist es herauszufinden wie intuitiv die Applikation ohne Hilfestellung bedienbar ist. Hierbei ist es sehr hilfreich, wenn du bei allen Aufgaben deine Gedanken zur aktuellen Situation laut aussprichst
und somit Feedback darüber gibst, wie du in der jeweiligen Situation zurechtkommst, und was dir positiv oder negativ zu Design und Bedienbarkeit auffällt.<br>
Während des gesamten Testzeitraumes, solltest du dich keinesfalls durch die Aufgaben unter Druck gesetzt fühlen. Falls etwas nicht ganz verständlich ist, kannst du uns gerne Fragen. Auch dies dient zur Verbesserung der User Experience.<br>
Um später eine gute Auswertung machen zu können, würden wir den gesamten Test gerne anonymisiert aufzeichnen. Bist du damit einverstanden? <br>

Wenn du dich bereit fühlst, können wir jetzt mit dem Usability Test beginnen.<br>

## Aufgaben
1. Starte die App, und schließe den Einrichtungsassistenten ab (Pflichtmodul: Mathe M)
    - **Erfolgsszenario:** NutzerIn hat mind. ein Modul hinzugefügt und landet in der DeadlineView
2. Importiere den bestehenden Uni-Stundenplan (Der Link zum Kalender befindet sich bereits im Zwischenspeicher des Smartphones)
    - **Erfolgsszenario:** Kalenderdaten sind importiert
3. Informiere dich, wann und wo deine nächste Vorlesung stattfindet
    - **Erfolgsszenario:** Vorlesung wurde gefunden, Informationen konnten ausgelesen werden
4. Falls noch nicht erledigt: Kehre wieder auf den aktuellen Tag zurück
    - **Erfolgsszenario:** Rückkehr auf aktuelles Datum ist erfolgt (Über Shortcut oder Kalender)
5. Erstelle das entsprechende Modul zur genannten Vorlesung
    - **Erfolgsszenario:** Das Modul wurde mit Kürzel und Name hinzugefügt
6. Erstelle eine neue Deadline mit gleichem Abgabedatum wie der Vorlesung
    - **Erfolgsszenario:** Deadline wurde mit richtigem Datum angelegt
7. Vermerke hier, dass du bis dahin noch 3 verschiedene Themengebiete aufarbeiten und eine Email an die lehrende Person senden musst.
    - **Erfolgsszenario:** Anlage von 2 oder 4 ToDos ist erfolgt und die Deadline gespeichert
8. Überprüfe die verbleibende Zeit, welche dir für die Erledigung dieser Tätigkeiten noch bleibt
    - **Erfolgsszenario:** Zeit konnte abgelesen werden
9. Sieh dir die Deadline im Kalender an
    - **Erfolgsszenario:** Deadline wurde im Kalender gesichtet
10. Erstelle eine Deadline für das Mathe-Modul und lasse dich hierfür einen Tag vor dem Abgabedatum erinnern.
    - **Erfolgsszenario:** Erstellung der Deadline erfolgt und Erinnerung korrekt angegeben
11. Lasse dir nun in der Deadline-Übersicht nur die Deadline für das Mathe-Modul anzeigen
    - **Erfolgsszenario:** Die richtige Filteroption wurde gesetzt und es erscheint nur das Mathe-Modul
12. Erstelle nun einen neuen Termin für ein Treffen mit Max Mustermann oder Erika Musterfrau zu einem Datum und Uhrzeit deiner Wahl und lasse dich dafür 1 std. zuvor erinnern.
    - **Erfolgsszenario:** Terminerstellung erfolgt und Erinnerung korrekt angegeben
13. Lösche eine der erfassten Tätigkeiten der ersten Deadline und erledige die Restlichen.
    - **Erfolgsszenario:** Löschung eines ToDos ist erfolgt und Deadline completion wird grün angezeigt.
14. Schließe nun die Deadline ab
    - **Erfolgsszenario:** Deadline befindet sich nicht mehr in der DeadlineView
15. Schalte die Benachrichtigungen für alle Deadlines aus
    - **Erfolgsszenario:** Benachrichtigungen für Deadlines wurden ausgeschaltet
16. Editiere den Namen der zweiten Deadline und ändere den Abgabetyp auf "postalisch / postal"
    - **Erfolgsszenario:** Name der Deadline und Abgabetyp wurden entsprechend angepasst
17. Lösche den zuvor erstellten Termin
    - **Erfolgsszenario:** Termin wurde gelöscht
18. Ändere deine Matrikelnummer auf 123456
    - **Erfolgsszenario:** Matrikelnummer wurde angepasst
19. Lösche das Modul Mathe
    - **Erfolgsszenario:** Das Modul wurde gelöscht und die zugehörigen Deadlines sind verschwunden
20. Informiere dich wo du Hilfestellung zur App bekommst
    - **Erfolgsszenario:** Die/Der NutzerIn hat gelächelt

## Abschluss
Hast du noch Fragen oder Ideen/Vorschläge, die du noch einbringen möchtest? <br>
Dann sind wir jetzt am Ende des thinking-alouds angelangt. <br>
Wie hat dir der Test gefallen? Sind dir Punkte aufgefallen, die wir für künftige Durchführungen verbessern könnten? <br>
Danke für deine Zeit und die geteilten Gedanken <br>
Ich wünsche dir noch viel Erfolg bei deinem Studium.<br>


## Durchführung
## **Testperson 1**
#### 29.01.2021
#### Student*in
#### Ausführung der App in englischer Sprache und im Dark mode

### **Aufgabe 1:** Starte die App, und schließe den Einrichtungsassistenten ab (Pflichtmodul: Mathe )

#### **Benutzer:**
"Ok, dann trage ich mich mal ein. Oh, man kann das ja auch überspringen, wenn man möchte. Also dann trage ich nun Mathe im Tag ein. Achso, moment, das ist doch womöglich nur wie es nachher angezeigt wird.Das ist ein wenig irritierend. Jetzt schließe ich das mit einem Klick auf den Haken ab."
#### **Effektivität:**
Aufgabe erfolgreich abgeschlossen und nach kurzem Überlegen auch selbsterklärend.

#### **Effizienz:**
Es konnten keine überflüssigen Touchgesten oder Screenwechsel festgestellt werden.

##### **Zufriedenheit:**
Nutzer*in war kurz irritiert über die Funktion des Tagnames, konnte sich dies aber selbst erklären.

##### **Robustheit:*
Ist gegeben, bei dem Test sind keine Systemfehler aufgetreten.

### **Aufgabe 2:** Importiere den bestehenden Uni-Stundenplan (Der Link zum Kalender befindet sich bereits im Zwischenspeicher des Smartphones)

#### **Benutzer*in:**
"Vermutlich muss man hier doch auf das Plus drücken, nein das erstellt einen neuen Termin. Ach, das weiß ich noch nichtmal bei anderen Apps wie das funktioniert. Dann wohl hier unter Profile und dann Settings. Oh ja, ich habe es gefunden. Es hat geklappt. Leider geht hier die Tastatur nicht weg und sonst wird mir hier auch nichts weiter angezeigt. Ist es nun da? Achja doch im Kalender hier steht das nun. Ich hätte diese Importfunktion nun lieber im Kalender selbst gehabt."

#### **Effektivität:**
Aufgabe nach kurzem Ausprobieren anderer Funktionen erfolgreich abgeschlossen.

#### **Effizienz:**
Es konnte ein irrtümlicher Screenwechsel in die AddAppointmentView festgestellt werden.

##### **Zufriedenheit:**
Nutzer*in schlägt vor, die Importfunktion lieber in die CalendarView zu integrieren.

##### **Robustheit:**
Ist gegeben, bei dem Test sind keine Systemfehler aufgetreten.

### **Aufgabe 3:** Informiere dich, wann und wo deine nächste Vorlesung stattfindet

#### **Benutzer*in:**
"Ich nehme an, dass ich gleich den Termin in 2 Std. nehme. Das wäre dann Datenbanken 2 um 14:15."

#### **Effektivität:**
Aufgabe wurde schnell und erfolgreich abgeschlossen.

#### **Effizienz:**
Es konnten keine überflüssigen Touchgesten oder Screenwechsel festgestellt werden.

##### **Zufriedenheit:**
Nutzer*in ist zufrieden.

##### **Robustheit:**
Ist gegeben, bei dem Test sind keine Systemfehler aufgetreten.

### **Aufgabe 4:** Falls noch nicht erledigt: Kehre wieder auf den aktuellen Tag zurück

#### **Benutzer*in:**
"Oh man kann das Zurückkehren auch über das Symbol oben machen."

#### **Effektivität:**
Aufgabe wurde schnell und erfolgreich abgeschlossen.

#### **Effizienz:**
Es konnten keine überflüssigen Touchgesten oder Screenwechsel festgestellt werden.

##### **Zufriedenheit:**
Nutzer*in schlägt vor, einen Indikator im Kalender hinzuzufügen, ob am jeweiligen Tag Ereignisse stattfinden oder nicht.

##### **Robustheit:**
Ist gegeben, bei dem Test sind keine Systemfehler aufgetreten.

### **Aufgabe 5:** Erstelle das entsprechende Modul zur genannten Vorlesung

#### **Benutzer*in:**
"Ein Modul. Vielleicht hier bei den Deadlines. New Deadline. Nein, das geht hier nicht. Dann müsste es ja über den Termin der Vorlesung funktionieren. Auch nicht. Also muss ich das separat erledige. Dann womöglich über das Profil. Achja hier das kenne ich ja schon."

#### **Effektivität:**
Aufgabe wurde nach mehreren Anläufen erfolgreich gelöst.

#### **Effizienz:**
Es wurden mehr Screenwechsel und Touchgesten benötigt um zum Ziel zu gelangen als eigentlich notwendig.

##### **Zufriedenheit:**
Nutzer*in hat die Erstellung eines Moduls in der jeweiligen Detailansicht des Termines vermutet und würde sich dies künftig wünschen. Zudem wäre die individuelle Auswahl der Tagfarben ein gutes Feature.

##### **Robustheit:**
Ist gegeben, bei dem Test sind keine Systemfehler aufgetreten.

### **Aufgabe 6:** Erstelle eine neue Deadline mit gleichem Abgabedatum wie der Vorlesung

#### **Benutzer*in:**
"Ich gehe auf die Deadline Ansicht und drücke auf Plus. Ok jetzt gebe ich hier als erstes einen Namen ein und wähle das richtige Modul aus. Das Abgabedatum stelle ich hier nun einfach auf präsent. Hier bin ich mir aber nicht sicher ob das sinnvoll ist hierfür. So und dann soll das natürlich bis zur Vorlesung erledigt sein. Wann war die Vorlesung noch gleich? Ok, dann muss ich nochmal nachschauen. Ich lasse mich nun einfach mal eine Stunde vorher erinnern. Aber es wäre natürlich schön, wenn man sich das auch selbst aussuchen könnte zusätzlich zu den vordefinierten Intervallen."

#### **Effektivität:**
Nutzer*in hat die Aufgabe erfolgreich gelöst.

#### **Effizienz:**
Es wurde nur ein zusätzlicher Screenwechsel zur Vergewisserung der Uhrzeit der Veranstaltung durchgeführt. Ansonsten wurden die nötigen Screens und Navigationselemente auf Anhieb lokalisiert und korrekt benutzt.

##### **Zufriedenheit:**
Nutzer*in ist zufrieden mit der Navigation an sich, würde sich jedoch mehr Individualität bei der Erinnerungsfunktion wünschen.

##### **Robustheit:**
Ist gegeben, bei dem Test sind keine Systemfehler aufgetreten.

### **Aufgabe 7:** Vermerke hier, dass du bis dahin noch 3 verschiedene Themengebiete aufarbeiten und eine Email an die lehrende Person senden musst.

#### **Benutzer*in:**
"Nun füge ich die ToDos hinzu. Hey cool, Enter bringt mich gleich weiter in die nächste Zeile! Aber die Anfänge werden hier nicht automatisch groß geschrieben. Und dann speichern wir das mal. Die Detailansicht sieht gut aus! Ok, also kann ich das hier nun abschließen."

#### **Effektivität:**
Nutzer*in hat die Aufgabe erfolgreich gelöst.

#### **Effizienz:**
Es konnten keine überflüssigen Touchgesten oder Screenwechsel festgestellt werden.

##### **Zufriedenheit:**
Nutzer*in ist zufrieden mit der Umsetzung der ToDos und der Speicherung der Deadline, jedoch wäre eine Autokorrektur bei den Satzanfängen der ToDos wünschenswert.

##### **Robustheit:**
Ist gegeben, bei dem Test sind keine Systemfehler aufgetreten.

### **Aufgabe 8:** Überprüfe die verbleibende Zeit, welche dir für die Erledigung dieser Tätigkeiten noch bleibt

#### **Benutzer*in:**
"Ok, das ist sehr prägnant. Das macht einem ja Druck."

#### **Effektivität:**
Nutzer*in hat die Aufgabe erfolgreich gelöst.

#### **Effizienz:**
Es konnten keine überflüssigen Touchgesten oder Screenwechsel festgestellt werden.

##### **Zufriedenheit:**
Nutzer*in ist zufrieden.

##### **Robustheit:**
Ist gegeben, bei dem Test sind keine Systemfehler aufgetreten.

### **Aufgabe 9:** Sieh dir die Deadline im Kalender an

#### **Benutzer*in:**
"Ok, dann gehe ich hier nun auf den Kalender und ja da ist sie schon. Das farbige Symbol hebt es nochmal vom darüberliegenden Termin ab. Oh aber die Zeit ist um eine Stunde nach vorne verschoben. In der Detailansicht stimmt sie. Aber hier in der Kalenderübersicht nicht."

#### **Effektivität:**
Nutzer*in hat die Aufgabe erfolgreich gelöst.

#### **Effizienz:**
Es konnten keine überflüssigen Touchgesten oder Screenwechsel festgestellt werden.

##### **Zufriedenheit:**
Nutzer*in ist zufrieden und merkt an, dass die Darstellung gefällt.

##### **Robustheit:**
Ist nicht vollständig gegeben. Die Anzeige der um 1 Std. verschobenen Zeit in der Kalenderansicht ist nicht vorgesehen.

### **Aufgabe 10:** Erstelle eine Deadline für das Mathe-Modul und lasse dich hierfür einen Tag vor dem Abgabedatum erinnern.

#### **Benutzer*in:**
"Ok, dann legen wir mal los. Ich nenne das nun einfach mal Übungen. Den Zeitpunkt setze ich nun mal auf nächsten Dienstag um 10 uhr morgens und lasse mich einen Tag vorher erinnern und definiere keine ToDos. Oh da ist ein grüner Haken weil ich schon alle ToDos gemacht habe. Naja wohl eher 0/0 gemacht. Aber das stimmt ja schon."

#### **Effektivität:**
Nutzer*in hat die Aufgabe erfolgreich gelöst.

#### **Effizienz:**
Es konnten keine überflüssigen Touchgesten oder Screenwechsel festgestellt werden.

##### **Zufriedenheit:**
Nutzer*in ist zufrieden und mag die ausgewählten Symbole.

##### **Robustheit:**
Ist gegeben, bei dem Test sind keine Systemfehler aufgetreten.

### **Aufgabe 11:** Lasse dir nun in der Deadline-Übersicht nur die Deadline für das Mathe-Modul anzeigen

#### **Benutzer*in:**
"Ok dann klicke ich hier mal auf das Filtersymbol und wähle Mathe aus. Ja dann verschwindet die andere Deadline. Wenn ich das nun nochmal anders herum probiere klappt das auch."

#### **Effektivität:**
Nutzer*in hat die Aufgabe erfolgreich gelöst.

#### **Effizienz:**
Es konnten keine überflüssigen Touchgesten oder Screenwechsel festgestellt werden. Das Filtersymbol wurde als solches sofort erkannt.

##### **Zufriedenheit:**
Nutzer*in ist zufrieden

##### **Robustheit:**
Ist gegeben, bei dem Test sind keine Systemfehler aufgetreten.

### **Aufgabe 12:** Erstelle nun einen neuen Termin für ein Treffen mit Max Mustermann oder Erika Musterfrau zu einem Datum und Uhrzeit deiner Wahl und lasse dich dafür 1 std. zuvor erinnern.

#### **Benutzer*in:**
"Oh man kann nicht zur Seite wischen. Diese Gesten zum Screenwechsel wären super. Dann setzen wir nun den Termin mal auf nächsten Mittwoch. Nennen den Treffen und lassen uns vorher noch erinnern. Ok gut, also ich finde es interessant, dass man dann immer nochmal auf die Detailansicht kommt. Ein Zurückkehren auf die Übersicht wäre hier schon besser."

#### **Effektivität:**
Nutzer*in hat die Aufgabe erfolgreich gelöst.

#### **Effizienz:**
Es konnten keine überflüssigen Touchgesten oder Screenwechsel festgestellt werden.

##### **Zufriedenheit:**
Nutzer*in ist zufrieden, würde sich aber noch eine Gestensteuerung für den Screenwechsel wünschen um nicht immer die BottomNavigation nutzen zu müssen. Zudem wäre es sinnvoller nach dem Erstellen eines Termins wieder auf die Kalenderübersicht statt der Detailansicht zurückgeleitet zu werden.

##### **Robustheit:**
Ist gegeben, bei dem Test sind keine Systemfehler aufgetreten.

### **Aufgabe 13:** Lösche eine der erfassten Tätigkeiten der ersten Deadline und erledige die Restlichen.

#### **Benutzer*in:**
"Ok ich habe Themengebiet 1 aufgearbeitet und eine Email geschrieben und jetzt löschen ich mal Themengebiet 2 + 3."

#### **Effektivität:**
Nutzer*in hat die Aufgabe erfolgreich gelöst.

#### **Effizienz:**
Es konnten keine überflüssigen Touchgesten oder Screenwechsel festgestellt werden.

##### **Zufriedenheit:**
Nutzer*in ist zufrieden

##### **Robustheit:**
Ist gegeben, bei dem Test sind keine Systemfehler aufgetreten.

### **Aufgabe 14:** Schließe nun die Deadline ab

#### **Benutzer*in:**
"Ja ich möchte die Deadline abschließen. Oh jetzt bin ich zurück auf der Kalenderansicht."

#### **Effektivität:**
Nutzer*in hat die Aufgabe erfolgreich gelöst.

#### **Effizienz:**
Es konnten keine überflüssigen Touchgesten oder Screenwechsel festgestellt werden.

##### **Zufriedenheit:**
Nutzer*in ist zufrieden aber findet es irritierend, dass man plötzlich auf eine andere Übersicht geleitet wurde.

##### **Robustheit:**
Ist nicht vollständig gegeben. Das Zurückkehren in die korrekte Ansicht wäre zu erwarten gewesen.

### **Aufgabe 15:** Schalte die Benachrichtigungen für alle Deadlines aus

#### **Benutzer*in:**
"Ok ich gehe auf Profile, dann auf Settings, dann auf Notifications und dann stelle ich die Deadlines aus"

#### **Effektivität:**
Nutzer*in hat die Aufgabe sehr schnell und erfolgreich gelöst.

#### **Effizienz:**
Es konnten keine überflüssigen Touchgesten oder Screenwechsel festgestellt werden. Die Navigation wurde extrem schnell genutzt.

##### **Zufriedenheit:**
Nutzer*in ist zufrieden.

##### **Robustheit:**
Ist gegeben, bei dem Test sind keine Systemfehler aufgetreten.

### **Aufgabe 16:** Editiere den Namen der zweiten Deadline und ändere den Abgabetyp auf "postalisch / postal"

#### **Benutzer*in:**
"Ok ich gehe auf Deadlines, dann gehe ich auf Mathe, dann gehe ich auf den Stift. Ok dann heißt das ab jetzt nur noch Übung und ändere dann den Submissiontype auf... ah different submissiontype. Ach man kann das sogar eingeben. Das ist ja cool. Und nun speichern."

#### **Effektivität:**
Nutzer*in hat die Aufgabe schnell und erfolgreich gelöst.

#### **Effizienz:**
Es konnten keine überflüssigen Touchgesten oder Screenwechsel festgestellt werden.

##### **Zufriedenheit:**
Nutzer*in ist zufrieden.

##### **Robustheit:**
Ist gegeben, bei dem Test sind keine Systemfehler aufgetreten.

### **Aufgabe 17:** Lösche den zuvor erstellten Termin

#### **Benutzer*in:**
"Ok, dann gehe ich mal auf Kalender, dann muss ich den Tag auswählen dann auf den Termin und ja wo kann ich.. ahja dann auf den Stift und dann kann ich delete auswählen. Den Delete Button hätte ich hier lieber auf der Detailansicht ehrlich gesagt."

#### **Effektivität:**
Nutzer*in hat die Aufgabe erfolgreich gelöst.

#### **Effizienz:**
Es konnte insgesamt eine irrtümliche Touchgeste auf der Detailansicht erfasst werden. Hier wurde der Delete Button zunächst gesucht.

##### **Zufriedenheit:**
Nutzer*in ist zufrieden, würde sich aber den Delete Button eher auf der Detailansicht wünschen.

##### **Robustheit:**
Ist gegeben, bei dem Test sind keine Systemfehler aufgetreten.

### **Aufgabe 18:** Ändere deine Matrikelnummer auf 123456

#### **Benutzer*in:**
"So, dann gehe ich auf Profil, dann gehe ich auf den Stift und dann mache ich aus der aktuellen Nummer eine Andere. Und speichern."

#### **Effektivität:**
Nutzer*in hat die Aufgabe erfolgreich gelöst.

#### **Effizienz:**
Es konnten keine überflüssigen Touchgesten oder Screenwechsel festgestellt werden.

##### **Zufriedenheit:**
Nutzer*in ist zufrieden.

##### **Robustheit:**
Ist gegeben, bei dem Test sind keine Systemfehler aufgetreten.

### **Aufgabe 19:** Lösche das Modul Mathe

#### **Benutzer*in:**
"Ach das ist hier ja direkt bei Profil, da kann man ja direkt auf den Mülleimer klicken. Ja das bestätige ich."

#### **Effektivität:**
Nutzer*in hat die Aufgabe schnell und erfolgreich gelöst.

#### **Effizienz:**
Es konnten keine überflüssigen Touchgesten oder Screenwechsel festgestellt werden.

##### **Zufriedenheit:**
Nutzer*in ist zufrieden.

##### **Robustheit:**
Ist gegeben, bei dem Test sind keine Systemfehler aufgetreten.

### **Aufgabe 20:** Informiere dich wo du Hilfestellung zur App bekommst

#### **Benutzer*in:**
"Ok, das habe ich gesehen. Hierzu muss man bei dem Profil auf die Einstellungen gehen und dann steht hier Help."

#### **Effektivität:**
Nutzer*in hat die Aufgabe schnell und erfolgreich gelöst.

#### **Effizienz:**
Es konnten keine überflüssigen Touchgesten oder Screenwechsel festgestellt werden.

##### **Zufriedenheit:**
Nutzer*in ist zufrieden und hat gelächelt. :)

##### **Robustheit:**
Ist gegeben, bei dem Test sind keine Systemfehler aufgetreten.


## **Testperson 2**
#### Datum 29.01.2021
#### Student*in
#### Ausführung der App in deutscher Sprache und im Light mode

### **Aufgabe 1:** Starte die App, und schließe den Einrichtungsassistenten ab (Pflichtmodul: Mathe)

#### **Benutzer:**
"Ich starte die App. Herzlich Willkommen! Bitte erstelle ein Profil. <br>
Aha, ich kann das auch überspringen. Ich gebe trotzdem mal meine Daten ein... <br>
Jetzt erstelle ich ein Modul Mathe mit Kürzel M und ein Modul Bio mit Kürzel B. <br>
Oh, ich möchte das Modul Bio doch nochmal in Biologie ändern, aber ich kann es nicht mehr bearbeiten.
Ist nicht schlimm. Dann lösche ich das Modul und erstelle es neu: Biologie mit Kürzel Bio. <br>
Ich drücke auf bestätigen und sehe, dass ich noch keine Deadlines in meiner Liste habe."

#### **Effektivität:**
Auf Anhieb erfolgreich.

#### **Effizienz:**
Es konnten keine überflüssigen Touchgesten oder Screenwechsel festgestellt werden. Er/Sie hat seine Daten für das Profil angegeben wodurch mehr Touchgesten entstehen.

##### **Zufriedenheit:**
Der/Die Benutzer*in ist zufrieden.

##### **Robustheit:**
Ist gegeben, bei dem Test sind keine Systemfehler aufgetreten.

### **Aufgabe 2:** Importiere den bestehenden Uni-Stundenplan (Der Link zum Kalender befindet sich bereits im Zwischenspeicher des Smartphones)

#### **Benutzer:**
"Ich gehe auf mein Profil und dann auf die Einstellungen. Jetzt füge ich den Link ein und klicke auf importieren."

#### **Effektivität:**
Auf Anhieb erfolgreich. Anmerkung von ihm/ihr: "Eine anderer Person hätte die Aufgabe vielleicht nicht direkt verstanden."

#### **Effizienz:**
Es konnten keine überflüssigen Touchgesten oder Screenwechsel festgestellt werden.

##### **Zufriedenheit:**
Der/Die Benutzer*in ist zufrieden.

##### **Robustheit:**
Ist gegeben, bei dem Test sind keine Systemfehler aufgetreten.

### **Aufgabe 3:** Informiere dich, wann und wo deine nächste Vorlesung stattfindet

#### **Benutzer:**
"Dann gehe ich zum Kalender, auf den heutigen Tag der schon ausgewählt ist. Um 12 Uhr habe ich Datenbanken"

#### **Effektivität:**
Auf Anhieb erfolgreich.

#### **Effizienz:**
Es konnten keine überflüssigen Touchgesten oder Screenwechsel festgestellt werden.

##### **Zufriedenheit:**
Der/Die Benutzer*in ist zufrieden.

##### **Robustheit:**
Ist gegeben, bei dem Test sind keine Systemfehler aufgetreten.

### **Aufgabe 4:** Falls noch nicht erledigt: Kehre wieder auf den aktuellen Tag zurück

#### **Benutzer:**
"Ist schon erledigt"

#### **Effektivität:**
Auf Anhieb erfolgreich.

#### **Effizienz:**
Es konnten keine überflüssigen Touchgesten oder Screenwechsel festgestellt werden.

##### **Zufriedenheit:**
Da der/die Benutzer*in diese Aufgabe bereits erledigt hatte, ist er/sie zufrieden.

##### **Robustheit:**
Ist gegeben, bei dem Test sind keine Systemfehler aufgetreten.

### **Aufgabe 5:** Erstelle das entsprechende Modul zur genannten Vorlesung

#### **Benutzer:**
"Warum soll ich jetzt für Datenbanken ein neues Modul anlegen? Wurde das nicht automatisch beim Importieren angelegt? <br>
Ok dann gehe ich zum Profil und erzeuge ein neues Modul mit dem Namen Datenbanken und der Abkürzung DB. Wurde hinzugefügt."

#### **Effektivität:**
Nach kurzem Überlegen erfolgreich.

#### **Effizienz:**
Es konnten keine überflüssigen Touchgesten oder Screenwechsel festgestellt werden.

##### **Zufriedenheit:**
Nicht ganz zufrieden, da die Module nicht automatisch beim Imortieren angelegt wurden. 

##### **Robustheit:**
Ist gegeben, bei dem Test sind keine Systemfehler aufgetreten.
Dennoch könnte man an der Stelle das Anlegen von Modulen beim Importieren implementieren.

### **Aufgabe 6:** Erstelle eine neue Deadline mit gleichem Abgabedatum wie der Vorlesung

#### **Benutzer:**
"Die Vorlesung war um 12 Uhr. Dann erstelle ich eine neue Deadline mit dem Modul Datenbanken, das ich gerade erstellt hatte.
Oh moment jetzt hatte ich gar kein Datum angegeben gehabt. Hätte ich das vorher angeben können? Das hatte ich übersehen. Erledigt."

#### **Effektivität:**
Nach kurzem Überlegen erfolgreich.

#### **Effizienz:**
Es gab einen überflüssigen Screenwechsel. Er/Sie hat die neue Deadline gespeichert, aber noch nicht das richtige Datum angegeben. Deshalb musste er/sie nochmal zurück in den Bearbeiten-Modus und die Zeit umstellen.

##### **Zufriedenheit:**
Es war nicht klar erkennbar, ob der/die Nutzer*in zufrieden war

##### **Robustheit:**
Ist gegeben, bei dem Test sind keine Systemfehler aufgetreten.

### **Aufgabe 7:** Vermerke hier, dass du bis dahin noch 3 verschiedene Themengebiete aufarbeiten und eine Email an die lehrende Person senden musst.

#### **Benutzer:**
"Ich erstelle ein ToDo mit 'E-Mail senden' und drei weitere ToDos tg1, tg2, tg3."

#### **Effektivität:**
Auf Anhieb erfolgreich.

#### **Effizienz:**
Es konnten keine überflüssigen Touchgesten oder Screenwechsel festgestellt werden.

##### **Zufriedenheit:**
Der/Die Benutzer*in ist zufrieden.

##### **Robustheit:**
Ist gegeben, bei dem Test sind keine Systemfehler aufgetreten.

### **Aufgabe 8:** Überprüfe die verbleibende Zeit, welche dir für die Erledigung dieser Tätigkeiten noch bleibt

#### **Benutzer:**
"Ich gehe zurück und da steht: Noch eine Stunde und 31 Minuten."

#### **Effektivität:**
Auf Anhieb erfolgreich.

#### **Effizienz:**
Es konnten keine überflüssigen Touchgesten oder Screenwechsel festgestellt werden.

##### **Zufriedenheit:**
Der/Die Benutzer*in ist zufrieden.

##### **Robustheit:**
Ist gegeben, bei dem Test sind keine Systemfehler aufgetreten.

### **Aufgabe 9:** Sieh dir die Deadline im Kalender an

#### **Benutzer:**
"Ich gehe auf den Kalender und da steht die Deadline."

#### **Effektivität:**
Auf Anhieb erfolgreich.

#### **Effizienz:**
Es konnten keine überflüssigen Touchgesten oder Screenwechsel festgestellt werden.

##### **Zufriedenheit:**
Der/Die Benutzer*in ist zufrieden.

##### **Robustheit:**
Ist gegeben, bei dem Test sind keine Systemfehler aufgetreten.

### **Aufgabe 10:** Erstelle eine Deadline für das Mathe-Modul und lasse dich hierfür einen Tag vor dem Abgabedatum erinnern.

#### **Benutzer:**
"Dann gehe ich zu den Deadlines und erzeuge eine neue mit dem Modul Mathe. Erinnerung: einen Tag vorher."

#### **Effektivität:**
Auf Anhieb erfolgreich.

#### **Effizienz:**
Es konnten keine überflüssigen Touchgesten oder Screenwechsel festgestellt werden.

##### **Zufriedenheit:**
Der/Die Benutzer*in ist zufrieden.

##### **Robustheit:**
Ist gegeben, bei dem Test sind keine Systemfehler aufgetreten.

### **Aufgabe 11:** Lasse dir nun in der Deadline-Übersicht nur die Deadline für das Mathe-Modul anzeigen

#### **Benutzer:**
"Dann gehe ich auf den Filter. Das Symbol kennt man ja. Und dort entferne ich die Haken außer bei M. Muss man hier die Kürzel benutzen? Man könnte den Namen vom Modul auch ausschreiben. <br>
Oh plötzlich sind die Deadlines aller Module wieder da."

#### **Effektivität:**
Auf Anhieb erfolgreich.

#### **Effizienz:**
Es konnten keine überflüssigen Touchgesten oder Screenwechsel festgestellt werden.

##### **Zufriedenheit:**
Der/Die Benutzer*in ist zufrieden, dass er/sie einen Systemfehler entdeckt hat.

##### **Robustheit:**
Bei dem Test ist ein Systemfehler aufgetreten. Das System aktualisiert nach ein paar Sekunden die DeadlineView und es werden plötzlich wieder alle Deadlines von jedem Modul angezeigt. Die Einstellungen beim Filtern wurden nicht zurückgesetzt.

### **Aufgabe 12:** Erstelle nun einen neuen Termin für ein Treffen mit Max Mustermann oder Erika Musterfrau zu einem Datum und Uhrzeit deiner Wahl und lasse dich dafür 1 std. zuvor erinnern.

#### **Benutzer:**
"Dann erzeuge ich eine Deadline mit dem Namen 'Treffen mit Erika Musterfrau'. Jetzt muss ich ein Modul auswählen, das ich aber nicht auswählen möchte. <br>
Ah achso ich muss einen neuen Termin erzeugen und keine Deadline. Ich breche ab und gehe in die Kalenderansicht. Dort erzeuge ich einen neuen Termin und lasse mich einer Stunde vorher erinnern."

#### **Effektivität:**
Nicht auf Anhieb erfolgreich, da es eine Verwechslung zwischen Deadline Und Termin gab. Nach kurzem Überlegen hat er/sie es aber verstanden.

#### **Effizienz:**
Es gab überflüssige Touchgesten und Screenwechsel wegen des Missverständnisses.

##### **Zufriedenheit:**
Der/Die Benutzer*in nicht ganz zufrieden.

##### **Robustheit:**
Ist gegeben, bei dem Test sind keine Systemfehler aufgetreten.

### **Aufgabe 13:** Lösche eine der erfassten Tätigkeiten der ersten Deadline und erledige die Restlichen.

#### **Benutzer:**
"Dann lösche ich 'tg3' und erledige die Restlichen."

#### **Effektivität:**
Auf Anhieb erfolgreich.

#### **Effizienz:**
Es konnten keine überflüssigen Touchgesten oder Screenwechsel festgestellt werden.

##### **Zufriedenheit:**
Der/Die Benutzer*in ist zufrieden.

##### **Robustheit:**
Ist gegeben, bei dem Test sind keine Systemfehler aufgetreten.

### **Aufgabe 14:** Schließe nun die Deadline ab

#### **Benutzer:**
"Ich klicke auf erledigen. Erledigt."

#### **Effektivität:**
Auf Anhieb erfolgreich.

#### **Effizienz:**
Es konnten keine überflüssigen Touchgesten oder Screenwechsel festgestellt werden.

##### **Zufriedenheit:**
Der/Die Benutzer*in ist zufrieden.

##### **Robustheit:**
Ist gegeben, bei dem Test sind keine Systemfehler aufgetreten.

### **Aufgabe 15:** Schalte die Benachrichtigungen für alle Deadlines aus

#### **Benutzer:**
"Dann gehe ich zum Profil in die Einstellungen und gehe auf Benachrichtigungen. Da sehe ich Deadlines und ich schalte es aus."

#### **Effektivität:**
Auf Anhieb erfolgreich.

#### **Effizienz:**
Es konnten keine überflüssigen Touchgesten oder Screenwechsel festgestellt werden.

##### **Zufriedenheit:**
Der/Die Benutzer*in ist zufrieden.

##### **Robustheit:**
Ist gegeben, bei dem Test sind keine Systemfehler aufgetreten.

### **Aufgabe 16:** Editiere den Namen der zweiten Deadline und ändere den Abgabetyp auf "postalisch / postal"

#### **Benutzer:**
"Dann gehe ich zu den Deadlines und auf die Deadline, um die es geht. Ich gehe auf Bearbeiten. Ich erstelle einen anderen Abgabetyp 'postalisch' und speichere das."

#### **Effektivität:**
Auf Anhieb erfolgreich.

#### **Effizienz:**
Es konnten keine überflüssigen Touchgesten oder Screenwechsel festgestellt werden.

##### **Zufriedenheit:**
Der/Die Benutzer*in ist zufrieden.

##### **Robustheit:**
Ist gegeben, bei dem Test sind keine Systemfehler aufgetreten.

### **Aufgabe 17:** Lösche den zuvor erstellten Termin

#### **Benutzer:**
"Ich gehe auf den Termin. Leider weiß ich das Datum nicht mehr genau an dem der Termin stattfindet. Hier wäre eine Suchfunktion hilfreich. <br>
Zum Glück konnte ich mir das doch noch merken. Dann gehe ich auf Bearbeiten und löschen."

#### **Effektivität:**
Nach kurzem Überlegen erfolgreich.

#### **Effizienz:**
Es konnten überflüssigen Touchgesten beim Suchen nach dem Termin festgestellt werden.

##### **Zufriedenheit:**
Der/Die Benutzer*in ist nicht ganz zufrieden.

##### **Robustheit:**
Ist gegeben, bei dem Test sind keine Systemfehler aufgetreten.

### **Aufgabe 18:** Ändere deine Matrikelnummer auf 123456

#### **Benutzer:**
"Ich gehe auf mein Profil, Bearbeiten und ändere die Matrikelnummer auf 123456. Speichern."

#### **Effektivität:**
Auf Anhieb erfolgreich.

#### **Effizienz:**
Es konnten keine überflüssigen Touchgesten oder Screenwechsel festgestellt werden.

##### **Zufriedenheit:**
Der/Die Benutzer*in ist zufrieden.

##### **Robustheit:**
Ist gegeben, bei dem Test sind keine Systemfehler aufgetreten.

### **Aufgabe 19:** Lösche das Modul Mathe

#### **Benutzer:**
"Ich klicke neben dem Modul Mathe auf löschen. Oh es ist ein Löschkonflikt aufgetreten. Ja, es sollen alle hinterlegten Deadlines gelöscht werden."

#### **Effektivität:**
Auf Anhieb erfolgreich.

#### **Effizienz:**
Es konnten keine überflüssigen Touchgesten oder Screenwechsel festgestellt werden.

##### **Zufriedenheit:**
Der/Die Benutzer*in ist zufrieden.

##### **Robustheit:**
Ist gegeben, bei dem Test sind keine Systemfehler aufgetreten.

### **Aufgabe 20:** Informiere dich wo du Hilfestellung zur App bekommst

#### **Benutzer:**
"Ich gehe auf die Einstellungen im Profil. Da sehe ich die Hilfe und klicke drauf." ... (Benutzer*in lächelt)

#### **Effektivität:**
Auf Anhieb erfolgreich.

#### **Effizienz:**
Es konnten keine überflüssigen Touchgesten oder Screenwechsel festgestellt werden.

##### **Zufriedenheit:**
Der/Die Benutzer*in ist sehr zufrieden.

##### **Robustheit:**
Ist gegeben, bei dem Test sind keine Systemfehler aufgetreten.

### Wie hat dem/der Nutzer*in der Test gefallen?
"Der Test hat Spaß gemacht und die Aufgaben waren zu bewaltigen."


## **Verbesserungsvorschläge**
- Behebung der Anomalie bei Filterung der Deadlines und zu langem Verweilen in der Übersicht. -> Deadlines tauchen wieder auf.
- Einstellungen wurden zunächst nicht direkt unter Profil vermutet. Die Weiterleitung auf das Profil nach Abschluss des Einrichtungsassistenten hätte womöglich geholfen.
- Verbesserung der Date- & Timepicker Dialoge. Sie sind nicht direkt übersichtlich. "Die alten Dialoge waren besser".
- Ein Indikator für den aktiven Filter wäre gut. Am besten in der Actionbar.
- Importfunktion ist in der Kalenderübersicht möglicherweise besser zu finden.
- Anlegen von Modulen beim Importieren
- Indikator für die Kalenderansicht erstellen, der anzeigt ob an dem jeweiligen Tag Termine stattfinden.
- Der Kalender könnte durch eine Suchfunktion für Termine erweitert werden.
- Tagfarben sollten selbst wählbar sein.
- Einbau einer Autokorrektur beim Schreiben
- Einbau einer Gestensteuerung für den Screenwechsel
- Versetzen des Delete Buttons auf die jeweiligen Detailansichten