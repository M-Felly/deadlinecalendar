# Anwendungsszenarien

## Trainingsszenario: Einführung in die App
Svenja hat sich die App installiert, um eine bessere Organisationsstrategie zu erstellen. Sie erwartet, dass die App ihr dabei hilft. <br>
Die App hat sie bislang nicht gestartet.
1. Svenja öffnet die App.
2. Die App begrüßt die Nutzerin und fragt sie nach ihrer präferierte Organisationsform.
3. Um weniger Stress bei der Terminplanung ihrer Abgaben zu haben, entscheidet sie sich gegen die Organisationsform 'kurzfristig' und wählt stattdessen 'ausgewogen'.
4. Die App bietet die Möglichkeit Termine mittels Abonnement-Link zu importieren. 
5. Da Svenjas Uni anbietet Termine in einen externen Kalender zu exportieren, kopiert sie den Abonnement-Link aus dem Uni-System in das dafür vorgesehene Eingabefeld der App.  
6. Die App importiert alle Termine und fragt wie viele Tage sie für die Termine im Vorhinein erinnert werden möchte 
7. Svenja möchte zwei Tage vorher erinnert werden und wählt dies entsprechend aus.
8. Die App zeigt eine Erfolgsmeldung an, dass die Einführung erfolgreich abgeschlossen ist.
7. Svenja sieht sieht dass alle Termine eingetragen sind.
Sie freut sich, dass sie sich so einfach und schnell neu organisieren konnte und ihr Semester nun strukturierter fortführen kann.

## Visionäres Szenario:
Aus den Interviews hat sich ergeben, dass es für die Persona keinen vorrangigen Bedarf für das [Eintragen von Notizen](https://code.fbi.h-da.de/nzse-praktikum-wise-2021/WiSe20-21_trapp/Mo3x-1/-/commit/643fe54ae0581b4cad49bd9cd008c43b32795193#789c1991d1cb31707c6acaaf891ed3fdf008d15e_15_15) gibt (siehe [interview_results.md](https://code.fbi.h-da.de/nzse-praktikum-wise-2021/WiSe20-21_trapp/Mo3x-1/-/blob/stmafeyl/ux/interviews/interview_results.md)). Deswegen haben wir das Szenario nochmal überarbeitet.<br>
<br>
Sofia benutzt einen Google Kalender. Durch mangelnde Kontinuität der Organisationsstrategie hat sie mit der hinzukommenden Klausurphase nicht mehr alle Abgabetermine im Blick und ist dadurch gestresst.
1. Sofia möchte ihre Organisationsstrategie optimieren, um das Stresslevel kontinuierlich gering zu halten. Hierfür möchte sie eine App verwenden.
2. Ein ausschlaggebenes Kriterium für sie ist, dass die App einen initialen Konfigurationsprozess hat, der ihr eine individuelle Organisationsstrategie erstellt.
3. Sie hat sich für den DeadlineCalendar entschieden und durchläuft den im Trainingsszenario beschriebenen Einführungsprozess.
4. Sofia erwartet, dass sich ihr Stresslevel für die kommenden Klausurphasen reduziert. 
5. Mit dem DeadlineCalender verspricht sie sich ihre neugewonnene Organisationsstrategie langfristig umzusetzen.

## Evaluations Szenario: Abändern von zukünftigen Erinnerungen
Tom ist mitten im Semester und hat bereits alle Erinnerungen in der App eingetragen. 
Nachdem Tom eine Benachrichtung am Dienstag für seinen bereits am Sonntag erledigten Termin bekommen hat, möchte er die zukünftigen Erinnerungen auf den Sonntag verlegen um seine Organisationsstrategie zu optimieren.
1. Tom hat eine Benachrichtigung für seine Erinnerung am Dienstag erhalten.
2. Er wählt in der Pop-up Nachricht 'bereits erledigt' aus.
3. Es öffnet sich die App mit der Möglichkeit die Erinnerung zu verschieben.
4. Tom wählt den Sonntag aus.
5. Das Fenster schließt sich. Tom legt das Handy beiseite und ist froh, dass er seine Organisationsstrategie optimieren konnte.
