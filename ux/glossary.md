# Wichtige Begriffe
*Sie erarbeiten die Begriffe/Methoden/Konzepte in Einzelarbeit oder zu zweit, Ihr Team soll hier kurz und knapp eine Zusammenfassung finden und so sich die Begriffe schnell aneignen können; geben Sie aber nicht einfach die Links an, fassen Sie sinnvoll zusammen, reduzieren Sie auf das Wesentliche; geben Sie bei wörtlichen Zitaten die Quelle an; gehen Sie insbesondere auf Verwendungszweck, Voraussetzungen, Kritik und ggf. den Erstellungsprozess ein. Formulieren Sie Beispiele aus dem Kontext Ihres Projekts*

## Usability vs UX
Bei Usability sowie bei User Experience geht es vorallem um den/die Benutzer\*in im Bezug auf das vom Nutzenden verwendete System. Der Begriff Usabillity ist ein Teil von User Experience und hat dadurch den Begriff Usability praktisch abgelöst.

Usability beschreibt lediglich wie benutzerfreundlich und gebrauchstauglich ein Produkt in einem bestimmten Kontext für eine bestimmte  **[Benutzer*innengruppen](#Benutzer*innengruppen)** ist. User Experience hingegen deckt alle Erwartungen, Wahrnehmungen und Reaktionen eines Nutzers mit ab, welche er während und nach der Nutzung erlebt.


### Usability
Bei Usability geht es vorallem darum dem/der Benutzer\*in einen guten Umgang mit dem System zu ermöglichen. Hierbei ist es wichtig in Erfahrung zu bringen **wann**, **wo** und **unter** **welchen Umständen** er/sie das System benutzt. Nochdazu sollten **Ziele** festgelegt werden, welche sich auf die Bedürfnisse und Erfordernisse des Nutzenden beziehen.

Das System sollte auch die Anforderungen **Effektivität**, **Effizienz** und **Zufriedenstellung** erfüllen.

**Effektivität**: Ist die Vollständigkeit und Genauigkeit der festgelegten Ziele
<br>
**Effizienz**: Bezieht sich auf die benötigten Resourcen
<br>
**Zufriedenstellung**: Hierbei geht es darum, ob der/die Benutzer*in eine positive Einstellung gegenüber dem Produkt hat. Ob alle Ziele erfüllt wurden und ob das Produkt optisch ansprechend ist.

### UX
Bei User Experience ist bei der Nutzung eines Produkts vorallem wichtig, dass das gesamte Nutzungserlebnis abgedeckt ist, welches der Kunde bei der Verwendung eines Produkts erlebt.Dies Ziel soll unteranderem durch die **Geschwindigkeit**, **Gewohnheit**, **Marke** und **Erfahrung** erreicht werden. Hierbei ist es nicht nur wichtig, dass die Nutzer\*in schnell und reibungslos sein/ihr Ziel erreicht, sondern auch dass er/sie positive Gefühle also Spaß und Freude bei der Benutzung hat. Die daraus enstandene Meinung des Kunden ist hierbei sehr entscheident. Aber hier hört die User Experience noch nicht auf, denn die Anbieter versuchen auch nach der Benutzung des Produkts für den/die Nutzer*in weiterhin präsent zu sein.


## User Research 
Bei User Research geht es darum die **Nutzer\*innen eines bestimmten Produkts zu kennen und zu verstehen**, die **Requirements Engineering** Techniken einzusetzen und **Modelle** zu erstellen. 

Für die **Analysierung der Nutzer\*innen** werden zuerst potenzielle bekannte User*innen für ein System in einer **Benutzerliste** aufgeführt.
Anschließend wird anhand der Personen eine allgemeine **Kriterienliste** erstellt.
Um die aufgeführten Kriterien messbar machen zu können werden diese **operationalisiert**.

Wenn dies erfolgt ist wird eine Matrix erstellt, in der die Personen anhand der erstellten Kriterien charakterisiert werden.
Danach wird ein n-dimensionaler Vektorraum (n ist hierbei die Anzahl der Kriterien) erstellt, indem die Personen eingetragen werden.
Anhand dieser können **[Benutzer*innengruppen](#Benutzer*innengruppen)** und die verschiedenen **[Persona](#Persona)** erstellt werden. 
Die Personabeschreibung ist input für die **[Teilstandardisierte Interviews](#Teilstandardisierte-Interviews)**. Die Teilstandardisierten Interviews sind Grundlage für die **[Anwendungsszenarien](#Anwendungsszenarien)** welche wiederum unteranderem die Basis für die **Anforderungsermittlung** ist. Anforderungsermittlung werden systematisch und strukturiert mit den Techniken des **Requirements Engineering** erhoben und verwaltet. Anhand diesen können dann die verschiedenen Modelle erstellt werden.


### Benutzer*innengruppen
Eine Benutzer\*innengruppe beinhaltet verschiedene Benutzer\*innen, die ähnliche oder vergleichbare Eigenschaften besitzen. Dabei kann eine Benutzer\*in mehreren Benutzer\*innengruppen angehören. Wie beim User Research beschrieben werden anhand eines n-dimensionalen Vektorraums die Benutzer\*innengruppen ermittelt. Diese Benutzer\*innengruppen werden gebildet, um einen Problembereich effizient untersuchen zu können und daraus **[Persona](#Persona)** zu erstellen. Die Persona sind die Repräsentanten der Benutzer\*innengruppe, welche zur Überprüfung der Kriterien wichtig sind. Aus Benutzer\*innengruppen können Benutzerrollen für die Software erstellt werden.

### Mentale Modelle
Mithilfe eines Mentalen Modells wandelt der menschliche Verstand komplexe Prozesse und Sachverhalte in weniger komplexe Schlussfolgerungen um und speichert diese im Gedächtnis. Somit ist es einem Menschen möglich, schwierige Abläufe zu abstrahieren und sie in ihrer Ganzheitlichkeit systematisch und dynamisch darzustellen. Ein mentales Modell ist also die eigene Repräsentation der Realität in einer verständlichen Art und Weise, die auf gesammelten Erfahrungen und Erlerntem basieren. 
Mentale Modelle führen also dazu, dass wir bestimmten Vorgehensweisen, Symbolen oder auch Systemen eine Erwartungshaltung zuschreiben, wie sie zu funktionieren haben. Wenn ein Hersteller die mentalen Modelle seiner BenutzerInnen berücksichtigt, kann eine Intuitive Nutzung eines Produkts gewährleistet werden und die Zufriedenheit der NutzerInnen steigt. 
#### Beispiele
- Wenn ein/e AutofahrerIn einen Blinker betätigen möchte, weiß er/sie intuitiv, wo sich dieser befindet und wie man nach links oder rechts blinken kann, da dies von den Herstellern genormt wurde.
- Zum Speichern einer Datei können NutzerInnen den Button mit einer abgebildeten Diskette drücken und wissen intuitiv, dass somit diese Datei gespeichert wird. Das Diskettensymbol wird in verschiedensten Anwendungen mit der "Speichern" - Aktion assoziiert. 
### Hypothesen
Hypothesen sind Annahmen, welche im Kontext des User Research Aussagen über die künftigen Benutzer*innen treffen. Diese Annahmen können jedoch widerlegt werden. Sie sind relevant für die Formulierung der richtigen Fragen und die spätere Charakterisierung der Benutzer*innengruppen. Unter Berücksichtigung der getätigten Annahmen und deren Wahrheitsgehalt kann eine verbesserte User Experience gewährleistet werden.

### Teilstandardisierte Interviews
Teilstandardisierte Interviews arbeiten mit einem **Gesprächsleitfaden**.
Dieser setzt sich aus Themen und Fragevorschlägen zusammen. Dadurch tritt
bei dieser Art von Interviews immer ein ähnlicher, aber nicht komplett gleicher
Gesprächsablauf auf.
Ziel diesert Art von Interview ist es, **Antworten** zu **vergleichen** und über die Möglichkeiten des standardisierten Interviews hinaus, Raum für das Vortragen **individueller Bedürfnisse und Erfahrungen des Interviewenden** zu geben. Realisiert wird dies mittels aktivem Zuhören sowie vorformulierter und offen gestellter Leitfragen. Klassischerweise wird ein teilstandardisiertes Interview mit einer Einleitung, mehrerer Leit- und Folgefragen sowie einem Schlussteil gegliedert. Durch die offen gestellten Fragen können sich ungeahnte Erkenntnisse zeigen, welche funktionale Anforderungen aller Extrema ergänzen können.


### Anwendungsszenarien
Ein Anwendungsszenario ist eine kurze Beschreibung eines Anwendungsfalls.
Ziel ist es einen Workflow des Benutzers zu beschreiben, um einen besseren **Gesamtzusammenhang** darzustellen.
Diesen benötigt man, um in der Designphase einen besseren Systementwurf zu entwickeln.
Es werden **verschiedene Anwendungsszenarien für verschiedene [Persona](#Persona)** erstellt.
Zusätzlich gibt es unterschiedliche Typen von Anwendungsszenarien:
- As-is Szenario → Beschreibung der jetzigen Situation
- Visionäres Szenario → Beschreibung einer zukünftigen Situation
- Evaluationsszenario → Systembewertung durch Benutzer*innen und deren Aufgaben
- Trainingsszenario → Einführung in das System


### Persona
Persona ist eine fiktive Person mit typischen Eigenschaften einer Nutzer*innengruppe.
Diese Nutzer\*innengruppen werden zuvor beim **[User Research](#User-Research)** gebildet. Aus jeder Nutzer\*innengruppen wird anschließend die Beschreibung einer Persona erstellt. Die Persona kann aus einer Person, die am Rand oder in der Mitte der **[Benutzer*innengruppe](#Benutzer*innengruppen)** liegt ermittelt werden. Hierbei ist es wichtig, dass sowohl Personen vom Rand als auch von der Mitte genommen werden, damit keine Bedürfnisse vergessen werden. Personen vom Rand der Benutzer\*innengruppe haben stark ausgeprägte oder außergewöhnliche Eigenschaften.
<br><br>
Die Persona kann mit den folgenden beispielhaften Eigenschaften erstellt werden:
<br>
Name, Foto/Zeichnung/Comic, kurzes Zitat/kurze Einführung, Beruf, Rolle, Aufgabe, Alter, Geschlecht, Fähigkeiten, Kentnisse, Erfahrungen, Vorlieben, Motivation, Hobbys, Ziele, Erwartungen und Wünsche an das Produkt, ...

Anhand der Persona werden die Nutzer\*innenanforderungen ermittelt. Von den Nutzer\*innenanforderungen können die Systemanforderungen für die Entwicklung abgeleitet und anschließend auch getestet werden.

## Anforderungen
### Funktionale Anforderungen
Funktionale Anforderungen beschreiben die vom Kunden/ Stakeholder gewünschten Funktionalitäten, die das System erfüllen soll. Die funktionalen Anforderungen charakterisieren sozusagen die Eigenschaften, Funktionen oder Leistungen eines Systems. Die Kernfrage hierbei lautet: "Was soll das System können?"   
### Nicht-funktionale Anforderungen
Unter nicht-funktionalen Anforderungen versteht man alle Anforderungen, die zwar nicht ausdrücklich vom Kunden/ Stakeholder gewünscht sind, aber dennoch in Bezug auf die Kundenzufriedenheit eine sehr große Rolle spielen. Zu den nicht-funktionalen Anforderungen zählen unter anderem Verfügbarkeit, Ressourcenverbrauch oder Performance.
### Qualitative Anforderung (inkl. Quantifizierung)
Eine qualitative Anforderung sollte mehrere Kriterien erfüllen, um sie als nützlich erachten zu können.
### Anforderungen sollten...
- vollständig und eindeutig sein
- überprüfbar sein
- präzise sein
- realistisch sein
- konsistent sein
- nicht überladen/ nicht doppelt vorhanden sein
- vollständig sein
### Stakeholder
Ein Stakeholder sind alle Personen oder Organisationen, die von der Entwicklung und vom Betrieb eines Systems auf unterschiedliche Arten betroffen sind. Diese haben Anforderungen an das System. Falls hierbei potentielle Stakeholder vergessen werden, werden auch deren Anforderungen nicht bei der Entwicklung des System berücksichtigt. 

Zu den Stakeholdern gehören unteranderem Nutzer\*innen, Entwickler\*innen, Tester\*innen, Auftraggeber\*innen und Betreiber\*in des Systems.

### Satzschablone
Mit einer Satzschablone können Anforderungssätze einheitlich strukturiert werden. Somit können die Anforderungen analysiert werden und ggf. fehlende Anforderungen und irrtümliche Aussagen angepasst werden. Die Satzschablone ist eine Art "Baukasten", der die Struktur der Anforderungssätze festlegt. Durch die Vorgabe dieser Syntax ergibt sich dadurch ein optimaler Anforderungssatz, der im richtigen Kosten-Nutzen Verhältnis steht. 
Die Satzschablone ist jedoch nicht vollständig von von sprachlichen Effekten befreit. Es existieren dennoch andere Wahrnehmungen des Geschriebenen durch die anderen Teammitglieder, sowie das Generalisieren von Informationen, etc. 
#### Regeln zum Verfassen von Anforderungssätzen
- Satzbau im Aktiv (Grammatik)
- Vollständige Sätze
- Anforderungssätze bilden Prozesse mithilfe von Vollverben ab
- Ein Anforderungssatz pro Prozesswort
- Detailierungsmöglichkeiten zur Analyse fehlender Informationen

| Bedingung                                       | Anforderungswort | Subjekt        | Objekt        | Verb           |
|------------------------------------------------:|:----------------:|:--------------:|:-------------:|:--------------:|
|Wenn die CPU-Temperatur den Höchstwert erreicht  | muss             | der Regler     | den Computer  | herunterfahren |

#### Darstellung einer Satzschablone mit optionaler Bedingung
1. **DAS SYSTEM**: Dieser Baustein ist vorgegeben - es ist wichtig, das Subjekt bei Namen zu nennen, um eine leicht Verständliche Anforderung zu formulieren.
2. **MUSS/SOLLTE/WIRD**: Mit diesen Schlüsselworten wird der Anforderung eine Verbindlichkeit zugeschrieben und klar definiert. **MUSS** verpflichtet zu Umsetzung, **SOLLTE** stellt einen Wunsch eines Stakeholders dar, **WIRD** stellt die Absicht eines Stakeholders dar.
3. **DIE MÖGLICHKEIT BIETEN/FÄHIG SEIN/...***: Hiermit wird die Art der Funktionalität festgelegt. Dies können unter anderem selbstständige Systemaktivitäten sein (Automatisches Starten/ Durchführen einer Funktion durch das System), eine Benutzerinteraktion (System stellt Benutzern Funktionalität zu Verfügung, bzw. startet so eine Kommunikation mit ihm) oder eine Schnittstellenanforderung (System soll mit einem Fremdsystem kommunizieren).
4. **OBJEKT**: Das System oder Teile des Systems können als definiert werden. Die Formulierung des Objekts hängt stark vom Prozesswort ab.
5. **PROZESSWORT**: Prozesswörter sind der Kern einer Anforderung und müssen dementsprechend klar und treffend gewählt werden. Eine Konkretisierung kann mithilfe der vier Fragewörter Wann, Wo, Wohin und Woher beantwortet werden.
6. **BEDINGUNG** (optional): Unter Umständen sind Anforderungssätze an Bedingungen geknüpft. Diese können hier ebenfalls formuliert werden.

#### Beispiele Satzschablone funktionale Anforderung (mit Bedingungsbaustein)
|Bedingung                                                 |Anforderungswort|Subjekt   |Objekt                                           |Prozesswort      |
|:--------------------------------------------------------:|:--------------:|:--------:|:-----------------------------------------------:|:---------------:|
|Wenn eine Abgabe-Deadline in weniger als 24 Stunden abläuft|muss            |das System|eine Erinnerung auf dem Display des Mobiltelefons|erscheinen lassen| 
|Wenn der Benutzer eine neue Terminserie erstellt hat      |wird            |das System|die Terminansicht                                |aktualisieren    |

*Merke: Kein Funktionalitäts-Baustein, da selbstauslösende Ereignisse aufgeführt wurden.*


#### Beispiele Satzschablone nicht-funktionale Anforderung
##### Beispiel anhand einer Umgebungs-Satzschablone
|Komponente des Betrachtungsgegenstandes|Anforderungswort|Füllwort               |Bedingung                          |Betrachtungsgegenstand|Eigenschaft     |Füllwort      |
|---------------------------------------|----------------|-----------------------|-----------------------------------|----------------------|----------------|--------------|
|Die App                                |muss            |so gestaltet sein, dass|bei unterschiedlich großen Displays|die Terminansicht     |richtig skaliert|angezeigt wird|

##### Beispiel anhand einer Eigenschafts-Satzschablone
|Bedingung                                |Anforderungswort|Eigenschaft |Betrachtungsgegenstand|Vergleichsoperator|Wert      |Füllwort|
|-----------------------------------------|----------------|------------|----------------------|------------------|----------|--------|
|Wenn die Terminansicht aktualisiert wird,|sollte          |die Dauer   |des Vorgangs          |nicht länger als  |3 Sekunden|betragen|

 
### User Stories
Eine User Story beschreibt die Anforderung eines Kunden an ein Softwareprodukt und dient der Kommunikation zwischen der Kunden\*in und den Entwicklern.
Man benutzt eine User Story, um eine klare Aussage darüber zu haben, was eine Benutzer\*in machen möchte und warum. <br>
Format Beispiel: Als *\<Benutzerrolle\>* möchte ich *\<Funktion\>*, damit *\<Nutzen\>*. <br>
<br>
**Akzeptanzkriterien** <br>
Akzeptanzkriterien legen fest, unter welchen Bedingungen eine User-Story als umgesetzt gilt und ob das System oder die zuentwickelnde Software die Kundenerwartungen erfüllen. <br>
Format Beispiel: *\<Voraussetzungen\>*, wenn *\<Aktionen des/der Benutzers\*in\>*, dann *\<geforderte(n) Reaktion(en) des Systems\>*. <br>
<br>
**Beispiele User Stories im Bezug auf das Projekt:** <br>
Als Nutzer\*in möchte ich eine Liste mit künftigen Terminen bekommen, damit ich die nächsten Termine planen kann und weiß wann ich welche Termine habe. <br>
Als Nutzer\*in möchte ich von der App an meine Abgaben erinnert werden, damit ich weniger Stress habe und sie nicht vergesse. <br>
<br>
**Beispiele Akzeptanzkriterien im Bezug auf das Projekt:**
<br>
Die Nutzer\*in möchte eine Liste mit künftigen Terminen bekommen, wenn er/sie dann den Button für die Listenanzeige drückt, dann zeigt das System die Termine der nächsten paar Tage an. <br>
Die Nutzer\*in möchte für eine Abgabe eine Erinnerung erhalten, wenn er/sie dann einen passenden Termin für die Erinnerung angibt, dann merkt sich das System den Termin und erinnert die Kunden\*in rechtzeitig an die Abgabe.

### Kano-Modell
Mit dem Kano-Modell kann herausgefunden werden, wie zufrieden die Kunden mit den bereitgestellten Anforderungen sind. 
Für die Ermittlung der Kundenzufriedenheit gibt es 3 Klassen von Faktoren **Basisfaktoren**, **Leistungsfaktoren** und **Begeisterungsfaktoren**.

**Basisfaktoren**
<br>
Basisfaktoren beinhalten die Anforderungen, die ein User\*in unterbewusst an das System hat. Diese Anforderungen können durch Beobachtungstechniken und dokumentenzentrierte Technik ermittelt werden und müssen auf jeden Fall bereitstellt werden. 
Ein Beispiel hierfür ist ein funktionierender Kalender indem Termine hinzugefügen kann.

**Leistungsfaktoren** 
<br>
Leistungsfaktoren sind Merkmale im System, welche die User explizit und bewusst äußern. Diese können durch Befragungstechniken ermittelt werden. Die Erfüllung dieser Faktoren erzeugt Zufriedenheit bei den Usern.
Ein Beispiel hierfür ist die automatische Erzeugung einer Erinnerung, welche den/die User\*in an eine Abgabe erinnert.

**Begeisterungsfaktoren** 
<br>
Begeisterungsfaktoren sind Faktoren, die ein Stakeholder noch nicht kennt aber welche er sehr angenehm und nützlich finden wird. Etwas neues und unerwartendes, welches bei längerer Nutzung zu einem Basisfaktor werden kann. Diese können durch Kreativitätstechniken und evaluiertes Prototyping ermittelt werden.
Ein Beispiel hierfür ist ein automatisch generierter Besprechungtermin mit einem Praktikumspartner\*in, welcher auch in dem Kalender des/der Praktikumspartner\*in erscheint.


### Teufelsquadrad Sneed
Weiterentwicklung bzw. andere Darstellungsform des magischen Dreiecks.
Die 3 Zielgrößen Kosten, Zeit und Leistung des magischen Dreiecks wurden nach Harry Sneed nochmals um eine Zielgröße ergänzt. Hierbei wurde die Zielgröße Leistung in Qualität und Inhalt unterteilt, was die Zuordnung der Projektprozesse zu dein einzelnen Zielen erleichtern soll und folglich als anschaulicher wahrgenommen wird.
- Zielgrößen beeinflussen sich untereinander, weswegen es nicht möglich ist, alle zu 100% zu optimieren
- eine Ausblanancierung aller Zielgrößen sorgt für ein optimales Endergebnis des Projekts
- wird eine der Zielgrößen geändert, wirkt sich das auf die anderen Zielgrößen aus und müssen ggf. angepasst werden
#### Einsetzbar für folgende Zwecke:
- grobe Zieldefinierung und Visualisierung (was soll erreicht werden?)
- Steuerungen von Änderungen in der Projektlaufzeit (wie wirken sich Änderungen auf Projektlaufzeit aus?) 
#### Problematik im Projekt
Angenommen, der Kunde hätte gerne sein Produkt schon vor dem eigentlichen Fertigstellungstermin, wird die Zielgröße "Zeit" bevorzugt. Daraus resultiert, dass die Kosten höher werden und Qualität und Inhalt negativ beeinflusst werden. Dies bedeutet, dass alle Zielgrößen voneinander abhängig sind und dies für das Projektteam eine Herausforderung darstellt, die anderen Zielgrößen anzupassen und nicht zu vernachlässigen.
## Visual Design
### Fitt's Law
Fitt's Law repräsentiert den Zusammenhang von Distanz und Zielgröße hinsichtlich der benötigten Transferzeit. Die Erkenntnis zeigt, dass die Transferzeit konstant bleibt, solange das Verhältnis zwischen Distanz und Zielgröße ebenfalls gleich bleibt. Zudem steigt die Transferzeit in Abhängigkeit zur schrumpfenden Zielgröße und/oder steigenden Distanz. Ursprünglich bezogen sich Fitt's Erkenntnisse auf menschliche Interaktionen in der realen Welt. Beim sogenannten "Tapping Task" versuchen Testpersonen schnellstmöglich mit einem Stift die Berührung zweier markierter Ziele abzuwechseln. Aus den berührten Punkten lässt sich anschließend eine Fehlerrate auslesen, welche Bezug auf die o.g. Verhältnismäßigkeit nimmt. Überträgt man diese Gesetzmäßigkeit nun auf Nutzer*inneninteraktionen innerhalb einer Anwendung, lassen sich konkrete Strategien für ein benutzerfreundliches UI herausarbeiten. Anhand folgender Beispiele soll verdeutlicht werden, in welchen Bereichen einer Anwendung Fitt's Law konkrete Auswirkungen auf die UX haben kann.
#### Beispiele (Anwendung auf unser Projekt)

- Generelle Beachtung der Mindestgröße aller Bedien-/ Navigationselemente

- Bottom Navigation mit maximal 3 Elementen und großen Symbolen für die zentrale App-Navigation
    - Gut zu erreichen bei einhändiger Bedienung mit Daumen
    - Schnelle Navigation auch ausgehend von der Bildschirmmitte möglich (Treffsicherheit wird verbessert)

- Kalenderansicht mit geteilter Recycler View in 50:50 Verhältnis für schnelles Wechseln
    - Große Listenelemente zum einfachen Ansteuern

- Floating Action Button (nach Material Design) in rechter, unterer Ecke ergänzend zur Botttom Navigation für Zugriff auf wichtige und oft genutzte Aktivität.

### Hick's Law
Hick's Law besagt, dass die Zeit, bis eine Wahl endgültig getroffen wird, davon abhängt, wievielen Entscheidungsmöglichkeiten Nutzer\*innen gleichzeitig ausgesetzt sind. Zu viele Features führen also zu einer längeren Reaktionszeit seitens des/ der Nutzer\*in und könnten ihn/ sie verwirren oder gar frustrieren. Aus diesem Grund ist es wichtig, Anwender\*innen in einer Anwendung nicht mit Entscheidungsmöglichkeiten zu "überladen" - sonst könnte die Usability und somit die Zufriedenheit der Nutzer\*innen darunter leiden.
#### Beispiele
- Installation einer Software: Möglichkeit, standardmäßige (einfach und kurz) oder fortgeschrittene Installation (mit vielen extra Einstellmöglichkeiten) durchzuführen <br>
- Kategorisieren der Einstellmöglichkeiten einer App: Unterteilung in verschiedene Menüpunkte. Das erhöht das Zurechtfinden und die Navigierbarkeit. Beispiele: <br>
    - Profil bearbeiten
    - Datenschutz
    - Benachrichtigungen
    - Aktivität
    - uvm.
#### Beispiele im eigenen Projekt
- Unterteilung des Einstellungsmenüs in Kategorien, um ein leichteres Navigieren zu ermöglichen <br>
- Nur drei Buttons, mit denen es sich leicht und übersichtlich zu den verschiedenen Ansichten und darin den enthaltenen Aktivitäten navigieren lässt <br>
- Die Auswahl der Konfigurationseinstellungen bei erster Nutzung der Applikation zur Erstellung eines individuellen Profils so gering wie möglich und so detailliert wie nötig halten (d.h. keine Einstellungen, die trivial sind und die "Deadlines" in ihrer Erscheinung dementsprechend auch **nicht** beeinflussen) <br>


### Gestalt-Prinzipien
Die Gestaltungsprinzipien sollen beim Entwurf eines durchdachten Designs helfen. Hierbei geht es vorallem um die Gestaltpsycholgie, welche sich auf den Bereich der menschlichen Wahrnehmung konzentriert. Wie müssen die Elemente angeordnet werden, dass diese stimmig und logisch aussehen? <br> Hierfür gibt es mehrere Prinzipien, welche man befolgen sollte.

1. Figur und Grund:<br>
Hierbei geht es vorallem um die optischen Sinneseindrücke. Die Figur, welche verwendet werden steht im Vordergrund und an diese soll man sich leicht erinnern können. Aus diesem Grund sollten diese eine einfache, kleine und klare Form haben. Am besten sollten diese symmetrisch sein und sich vom Grund abheben. Noch dazu ist eine vertikal oder horizonal Ausrichtung besser, da sie so wahrscheinlicher als Figuren wahrgenommen werden. <br>

    Ein Beispiel für unser Projekt ist, dass ein Kalender die klare rechteckige Form hat und dieser im Vordergrund steht indem der Hintergrund unauffällig ist, damit hier nichts von dem Kalender ablenkt.

2. Symmetrie: <br>
Symmetrische Anordnungen unterstützen die klare Gliederung von Inhalten und ziehen mehr die Aufmerksamkeit auf sich.  Nochdazu scheint das Screendesign ausgewogener zu sein. <br>

    Hierbei geht es z.B. um die Auswahlelemente, welche nicht irgendwie kreuz und quer auf dem Bildschirm anzuordnen sind, sondern zwei links und zwei rechts, sodass hierbei eine symmetrie entsteht.

3. Geschlossenheit und Gruppierung: <br>
Durch Geschlossenheit und Gruppierung können wir erkennen, welche Elemente zusammengehören. Ein Beispiel hierfür sind geschlossene Boxen und Formen, welche zusammengehörde Informationen beinhalten. Aber nicht nur diese zeigen eine optische Gruppierung sondern auch symmetrische Anordnung von Elementen und Formen. Hierfür sollten gleiche Formen beieinander sein. <br>

    Ein Beispiel hier für ist z.B. ein rechteckiger Kasten um jede einzelne Kalendererinnerung. In diesem Kasten stehen dann die wichtigsten Informationen zu dieser Erinnerung. So ist klar, welche Information zu welcher Erinnerung gehört und man sieht auf einen Blick wo eine Kalendereintrag aufhört und wo ein neuer anfängt.

4. Nähe: <br>
Auch die Nähe der Objekte spielen hierbei eine Rolle. Wenn die Elemente nah beieinander sind, bilden diese eine Einheit und man kann erkennen welche Elemente zusammen gehören. Hierbei sollte versucht werden strukturelle Zusammenhänge abzubilden um z.B. die Informationsdichte zu erhöhen. <br> 

    Es sollte sofort erkennbar sein, dass die Beschriftung einer Grafik, welche in der Nähe dieser Grafik platziert wird zu dieser Grafik gehört. 

5. Einheit und Harmonie: <br>
Elemente werden als Einheit erkannt, wenn die Anordnung den Eindruck erwecken und somit über eine visuelle Verbindung verfügen. Hierbei sollte versucht werden ein einheitliches und konsistentes Layout zu erstellen. <br>

    z.B. geht es hierbei um einen Titel, der immer in der oberen linken Ecke steht oder um die Navigationsleiste, die immer unten an der gleichen Stelle ist.

6. Ähnlichkeit: <br>
Gleiche Formen sollten beieinander stehen, denn dies bildet wiederum eine Einheit und eine Harmonie im Layout. Zusammengehörende Elemente sollten in Quer- oder Längsreihen gebildet werden. <br>

    Ein Beispiel hier für ist, dass z.B. auf einer Seite alle Kalendereinträge stehen und auf der nächsten Seite alle ToDos aufgeführt sind und nicht alle vermischt sind.

7. gemeinsame Bewegung: <br>
Wenn Elemente z.B. blinken sollten diese synchron blinken und nicht asynchron.


Die Gestaltprinzipien sollten auch auf unser Projekt angewendet werden. Hierbei sollten wir darauf achten, dass unsere Überschrift immer an der gleichen Position stehen. Genauso sollte die Bottom-Navigation auf den verschiedenen Screens immer an der gleichen Stelle angezeigt werden. Des Weiteren müssen wir bei verwendeten Icon darauf achten, dass diese einfache und klare Form haben. <br>
Um die wichtigsten Informationen auf einen Blick zu sehen, muss darauf geachtet werden, dass z.B. der Kalender im Vordergrund bleibt und nichts etwas im Hintergrund von diesem ablenkt. Termine und Deadlines sollten untereinander in einem eigenen Bereich angeordnet werden damit erkannt werden kann, dass diese zusammen gehören. Alle Informationen zu einer Deadline oder Termin sollten in einem abgegrenzten Bereich stehen (z.B. in einem Kasten).
Zusammengehörende Informationen sollten auf einem Screen sein. Weitere Informationen auf einem neuen Screen. Dies beteutet, dass auf einem Screen der Kalender mit den Terminen pro Tag angezeigt und auf einem weiteren Screen die verschiedenen Deadlines aufgelistet werden sollten und nicht alles auf einem Screen. Die Informationen sollten so angeordnet sein, dass eine symmetrie und somit eine Einheit bzw. Harmonie entsteht.
Die Informationen sollten außerdem logisch getrennt aufgeführt werden. Erst folgen in einem Profil die Informationen zu einer Person, welche Nah bei einander stehen, um die Zusammengehörigkeit zu zeigen und anschließend folgen in einem eigenen Bereich die Module, welche der Benutzer belegt.

### Affordance
Jedes Objekt, ob virtuell oder real, repräsentiert dem/ der Nutzer\*in, wie bzw. wofür es verwendet werden kann. Es **bietet** also eine Interaktion **an**, die sofort ersichtlich ist. Eine gute Affordance wird dann erreicht, wenn die Nutzung so intuitiv gestaltet ist, dass das Handbuch nicht benötigt wird, weil die Interaktionen des Objektes durch den/ die Nutzer\*in selbsterklärend sind. <br>
#### Beispiele
- Ein Fahrstuhlknopf, der gedrückt werden muss, damit ein Aufzug auf der Etage hält. <br>
- Eine Schiebe-Glastür, der man sich nähern muss, damit diese durch einen Sensor vollautomatisch öffnet. <br>
- Einen Laptop, den man aufklappen kann: Ersichtlich durch die Scharniergelenke der auf einen Seite und der gegenüberliegenden "offenen" Seite. <br>
#### Möglichkeiten, Affordances im Projekt einzubauen
- Ein virtueller Schalter kann in der Kalenderansicht getätigt werden, um alle Termine anzeigen zu lassen oder nur die relevanten Abgabetermine (ohne Praktika oder Vorlesungen) anzeigen zu lassen. <br>
- Ein Button mit drei Strichen oder einem Zahnradsymbol bietet den Nutzer\*innen an, das Einstellungsmenü aufzurufen. <br>

### Prototypes und Wireframes
Das Erstellen von Prototypen gilt als essentieller Teil des Entwicklungsprozesses. Ideen werden basierend auf zuvor festgelegten Prinzipien und Anforderungen visuell dargstellt und auf Nutzbarkeit getestet. Mögliche Einschränkungen oder Problemstellungen auf Design und Navigationsebene können somit frühzeitig und schnell identifiziert werden, was im späteren Entwicklungsverlauf Geld sparen kann. In drei verschiedenen Stufen können Prototypen iterativ verbessert werden und später eine erste konkrete UI-Demo darstellen. Angefangen mit dem sogenannten **Paper Prototyping** versucht man hier erste UI-Entwürfe auf Papier durchzuführen, welche für erste Interaktionsbeobachtungen der Nutzer*innen genutzt werden können. Auch in dieser Phase sollten schon MVP, Gestaltungsprinzipien sowie die Heuristiken berücksichtigt werden. Nach Betrachtung und Zusammenführung unterschiedlicher Paper-Prototypen, kann eine daraus resultierende, digitale Version erstellt werden, welche weiter verbessert wird. Oftmals werden Prototypen mit Wireframes verwechselt. Zentrale Unterschiede sind hier die Absichten, sowie der Detailgrad. Während es bei Wireframes um die Verdeutlichung der dargestellten Inhalte auf dem späteren Display sowie um Struktur und Layout der Applikation in niedriger Detailstufe geht, fokussieren sich Prototypen auf die Nutzer\*inneninteraktionen und die Navigation (Stichwort: Affordance) mit hohem Detailgrad bezüglich Inhalt und Gestaltung.

### Material design guidelines
Material design bietet standardisierte best practises für Designs, Icons, Typografie und vieles mehr an.  
Diese standardisierten Komponenten sind von UX/UI-Designer\*innnen in Zusammenarbeit mit Entwickler\*innen als 
open source Lösungen erstellt worden, sodass diese einfach von Entwickler\*innen in ihren Projekten genutzt werden können.
 
#### Checkliste für DeadlineCalendar-Design
- [ ] Globales Theme spezifizieren
    - [ ] Farben auswählen [Tool](https://material.io/resources/color/#!/)
    - [ ] Typografie definieren (Schriftgröße, Schriftart, Zeichenabstand) [Docu](https://material.io/design/typography/the-type-system.html#applying-the-type-scale)
    - [ ] Formen wählen (Eckige / Runde Buttons etc.) [Docu](https://material.io/design/shape/about-shape.html#shaping-material)
    - [ ] Icons auswählen [Liste](https://material.io/resources/icons/?style=baseline)
- [ ] Layouts für Views spezifizieren
    - [ ] Dimensionen wählen [Docu](https://material.io/design/layout/understanding-layout.html#)
    - [ ] Responsive layout (Columns, Margins, Gutters etc.) [Docu](https://material.io/design/layout/responsive-layout-grid.html)
- [ ] Allgemeine Navigationsarchitektur / Hierarchie erstellen [Docu](https://material.io/design/navigation/understanding-navigation.html#types-of-navigation)
    - [ ] Navigationsübergänge definieren [Docu](https://material.io/design/navigation/navigation-transitions.html)
- [ ] Zu verwendende Komponenten auswählen (Buttons, Tables, Cards etc.) [Übersicht](https://material.io/components)
    - [ ] Themes zu den Komponenten spezifizieren (siehe Unterpunkte vom globalem Theme)
- [ ] Sounds auswählen [Download](https://storage.googleapis.com/material-design/downloads/material_product_sounds.zip) 

#### Controls (Steuerelemente)
Controls (zu Deutsch Steuerelemente) sind Elemente bei denen die Benutzer\*innen die Möglichkeit haben von einer Menge an Optionen eine oder mehrere auszuwählen.<br>
Für die verschiedenen Elemente gibt es genaue Anwendungsbereiche: 
- **Switche** werden für das Ein- bzw. Ausschalten **einer** bestimmten Sache verwendet. Die Aktion dauert nicht lange und wird sofort ausgeführt.
    - z.B. Das Ein / Ausschalten von Mobilen Daten
- **Radio buttons** werden für das Auswählen **einer** Option aus **mehreren** Wahlmöglichkeiten für einen Sachverhalt verwendet.
    - z.B. Die Auswahl eines Geschlechts
- **Checkboxes** werden für das Auswählen bzw. Abhaken **mehrerer** Optionen zu einem übergeordneten Sachverhalt verwendet.
    - z.B. Eine To-Do Liste: Die abgehakten Checkboxen repräsentieren die erledigten To-Dos. 


## Usability Evaluation
Beschäftigt sich mit der Analyse der Benutzerfreundlichkeit und den Fragestellungen, wie gut Benutzer*innen mit der Software umgehen können, um ihre Ziele zu erreichen. <br> Im Mittelpunkt stehen Effektivität, Effizienz und die Gesamtzufriedenheit. <br>
### Usability Inspection
Prüfung der Usability einer Software anhand von Richtlinien und Checklisten. Dies wird meist durch die Abteilung der Qualitätssicherung durchgeführt. <br>
Es gibt folgende Methoden, die für die Inspection angewandt werden: <br>
- **Heuristische Auswertung**: erfahrene Inspektor*innen prüfen die Usability anhand einer Checkliste (mit allgemeinen Regeln, Beispielcheckliste auf Folie Nr. 179) <br>
- **Cognitive Walkthrough**: Testpersonengruppe testet anhand typischer Aufgaben, die dem Use Case der Software entsprechen <br>
- **Richtlinienprüfung/ -wertung**: Prüfung der Software-Usabilty anhand eine von detaillierten Regeln und Richtlinien <br>  
### Usability Test
Testen der Usability anhand echter Proband**innen: Unterbewusstes, subjektives Urteilen. Testpersonen sollen im Vorhinein nicht beeinflusst werden!  <br>
**Wichtig**: Nach messbaren Kriterien suchen, um ein bestmögliches Ergebnis zu erzielen. <br>
#### Durchführen eines Usability Tests
1. Erstellen eines Testplans: Es ist wichtig, dass der Testplan Aufgaben enthält, die durch bestimmte Voraussetzungen definiert sind. Außerdem sollten Kriterien definiert werden, wann diese Aufgabe als erfolgreich abgeschlossen gilt. Zusätzlich sollte festgehalten werden, wann diese Aufgabe als fehlerhaft abgeschlossen gilt und eine zeitliche Begrenzung festgelegt werden. <br>
2. Auswählen von Testpersonen <br>
3. Testmaterialien vorbereiten <br>
4. Durchführen eines Pilot Tests: Die gesamte Testprozedur sollte vor dem eigentlichen Test ausgeführt werden, um beispielsweise ungenaue/ unverständliche Instruktionen zu erkennen oder unrealistische AUfgabenstellungen und Zeiteinschätzungen ausfindig zu machen. Nicht entdeckte und beseitigte Fehler können den eigentlichen Usability Test negativ beeinflussen und zu einem Scheitern führen! <br>
5. Ausführen des eigentlichen Tests <br>
6. Analyse und Bericht <br>

## Thinking Aloud
Bei Thinking Aloud geht es darum ein Produkt bzw. eine Software von einem potentiellen User testen zu lassen. <br>
Ein potentieller User ist eine Person, welche\*r in die Zielgruppe des Produktes passt. <br>
*Für unsere App wären dies Studierende, welche Abgaben zu einem bestimmten Zeitpunkt einreichen müssen und an diese erinnert werden wollen.*<br> 

Meist wird erst der fertig programmierte Prototyp von einem User\*in getestet. <br> Um mögliche Fehlplanungen, Fehlkonstruktionen und die dafür verwendete Zeit bzw. Kosten einzusparen, sollten die Konstruktionsideen und der Aufbau der Applikation/Software allerdings schon vor der Entwicklung des Prototypens getestet werden.<br>
Bei der ersten Testung geht es um das Produktverständnis der potentiellen Nutzer. Hierunter zählt das Verständnis für die Verwendung der Navigation sowie die Verwendung und Anordnung der verschiedenen Elemente in den verschiedenen Screens/Activities. Um diese Testungen durchführen zu können, gibt es die Möglichkeit einen Paper-Prototyp zu erstellen. 
<br>
Um diesen Prototyp zu testen, bekommt der Tester verschiedene Aufgaben gestellt, die er mit der Nutzung des Paper-Prototypens erledigen soll. Hierbei kann herausgefunden werden, ob die Navigation, Anordnung und Zweck der App für den Tester verständlich sind oder ob manche Funktionen/Anordnungen in der App noch einmal überarbeitet werden müssen. *Siehe Durchführung [Paper-Prototyp](evaluation/thinking-aloud-paper.md).* <br>

Nach den Testungen kann der finale Paper-Prototyp erstellt und anschließend die App entwickelt werden. Danach sollte eine erneute Testung mit dem entwickelten Prototypen stattfinden.

## Heuristiken
Heuristiken sind grundlegende Regeln bzw. Richtlinien nach denen eine Benutzeroberfläche gestaltet und konzipiert werden soll.
Diese Richtlinien sollen den Benutzern\*innen die bestmögliche [Usability](#Usability) und [User Experience](#UX) bieten damit sie zielgerichtet und effizient die
gewünschte Aktion ausführen können. <br>
Jakob Nielsen hat dies in den folgenden 10 Punkten zusammengefasst, die vor allem als **Jakob's Law** bekannt sind:
1. **Sichtbarkeit des Systemstatus / Feedback** -> Der Benutzer\*inn weiß was das System macht bzw. in welchem Status es sich befindet,
indem es von diesem sofort nach einer Aktion entsprechendes Feedback erhält. Beispiele:
    - Status: Das Batteriesymbol welches anzeigt zu wie viel Prozent der Akku noch geladen ist
    - Feedback: Der Cursor ändert sich nach einem Klick auf einen Button zu einem Ladekreis.
2. **Abbildung der Systemwelt auf die Anwenderwelt** -> Das System soll die Sprache der Anwendenden sprechen.
Begriffe und Aktionen sollten so benannt werden wie sie von den Anwendern\*innen auch bislang in ihrer "natürlichen Umgebung" benannt wurden.
    - Hilft das System und die Funktionen dahinter intuitiv zu verstehen (siehe [Mentale Modelle](#Mentale Modelle))
3. **Kontrolle und Freiheiten** -> Benutzer\*innen fühlen sich in ihrem Handeln freier und sicherer, wenn sie einen Prozess
jederzeit beenden (emergency exit) oder diesen rückgängig machen können.
    - Zum Beispiel durch gut gekennzeichnete Zurück-Buttons
    - Undo / Redo Buttons bzw. Funktionen (STRG + Z)
4. **Konsistente Regeln / Standards** -> Benutzer\*innen erwarten bei gleichem Design immer die gleiche Funktion. Diese
einheitlichen Regeln können entweder "**internal**" für das eine System oder "**external**" weltweit, sowie
 für Systemgruppen (wie z.B. die Google Services (Google Drive, GMail, Google Calendar etc.)) gelten.
5. **Fehlervermeidung** -> Vermeide Fehler bzw Fehlverhalten des Nutzers\*inn durch verständliches Design (z.B. Warn- und 
Informationsmeldungen, wiederholenden Daten siehe Punkt 6). Es wird zwischen zwei Arten von Fehlern unterschieden:
    - **Slips**: Sind unachtsame Fehler wie z.B. das Freilassen eines Pflichtfeldes. (Lösung: Ein Sternsymbol, der ein Eingabefeld als
    Pflichtfeld kennzeichnet)
    - **Mistakes**: Sind unbeabsichtigte Fehler, die durch Inkonsistenz zwischen Design und [mentalem Modell](#Mentale Modelle)
    entstehen. Zum Beispiel durch das unbeabsichtigte Löschen von Dateien da der Benutzer\*inn eine andere Funktion hinter dem 'X'-Button
    verstanden hat. (Lösung: Bestätigungsfrage, dass die Datei gelöscht wird und / oder Undo-Funktion)
6. **Erinnert werden vor Merken** -> Informationen die über mehrere Ansichten hin weg relevant sind sollten auch auf den 
folgenden Ansichten angezeigt werden, damit sich der Benutzer\*inn diese nicht merken oder extra nachschlagen muss.
7. **Flexibles und effizientes Arbeiten** -> Damit der Benutzer\*inn die für sich effizienteste Arbeitsmethode entwickeln 
kann, ist es wichtig ihm diese Möglichkeiten zu bieten. Beispiele:
    - Shortcuts für Aktionen für schnelleres Arbeiten
    - Verschiebare und ausblendbare Ansichten / Fenster für gezieltes Arbeiten
8. **Minimalistisches, ansprechendes und effizientes Design** -> Damit der Benutzer\*inn maximal effizient auf die Elemente
fokusieren kann die wichtig sind, sollte eine Oberfläche so designed werden, dass nur essenzielle Informationen angezeigt werden.
Zusätzlich sollten Elemente, die den Benutzer\*inn durch den jeweiligen Arbeitsablauf leiten, hervorgehoben werden (z.B. ein Weiter-Button).
    - Zum Beispiel hat der Button "Abschicken" die Primärfarbe und signalisiert somit dem Benutzer\*inn wo es weiter geht.
    - Zusammenhängende Elemente werden nicht mehr wie früher umrahmt, sondern lediglich nah beieinander positioniert.
9. **Fehlermeldungen: Mitteilen, Analysieren, Beheben**  -> Fehlermeldungen sollten nicht nur sagen, dass ein Fehler aufgetaucht
ist, sondern diesen auch in **anwenderfreundlicher Sprache** (Keine error codes!) beschreiben und Lösungsvorschläge anzeigen.
    - Beispiel: Der Benutzer\*inn hat in einem Webshop zu viele Filter ausgewählt, sodass kein passendes Produkt gefunden
    wird. Die Oberfläche teilt dies mit und schlägt vor einzelne Filter zu entfernen.
10. **Hilfe und Dokumentation** -> Wenn das System nicht selbsterklärend ist, soll dem Benutzer\*inn an passenden
Stellen gezielte Hilfestellungen angeboten werden, um den gewünschten Prozess Schritt für Schritt durchzuführen.

#### Heuristiken für den DeadlineCalendar
Für den finalen Prototypen werden wir besonders darauf achten alle nötigen, aber nicht zu viele, Informationen für eine Deadline anzuzeigen und gleichzeitig
ein minimalistisches sowie effizientes Design zu garantieren. Dies gilt vor allem für die Übersichtsseiten (List- und Calendar screen).
Konsistente Standards werden durch das Einhalten der [Material design guidelines](#Material design guidelines) über alle Screens hinweg einheitlich umgesetzt. Feedback
bekommt der Benutzer\*inn insbesondere durch die in diesen guidelines enthaltenden Standards (z.B. Farbkontrast ändert sich beim Drücken des
Buttons und ein Click-Sound wird abgespielt). Darüber hinaus werden wir Ladeprozesse (wie z.B. das Importieren von Terminen) durch
anschauliche Animationen überbrücken. Zusätzlich vermeiden wir Slips in Eingabefeldern beim Hinzufügen neuer Deadlines durch Standardwerte sowie Hinweisen zu Pflichtfeldern.
Kontrolle und Freiheiten werden durch ansprechende Zurück-Buttons und Undo-Funktionen gewährleistet.


## Cognitive Walkthrough
Cognitive Walkthrough gehört zu den Usability Inspection. Hierbei spielen kleine Gruppen typische Aufgaben durch.
Im Gegensatz zu den [Heuristiken](#Heuristiken) werden hierbei Expert*innen miteinbezogen. Diese bewerten dann auf Basis von Aufgaben und den Daten der Benutzer/*innen wie gut die App mit 
kognitiven Prozesses des Benutzers/*inn harmoniert.<br>Dies wird zum Beispiel daran bewertet wie lernförderlich die App ist. Eine Überprüfung kann zum Beispiel so aussehen, dass beim Drücken eines
Buttons ein Feedback für den Benutzer/*inn erwartet wird.

## Software-Ergonomie
Software-Ergonomie bezieht sich hauptsächlich auf die bekannten Regeln zum ergonomischen Arbeiten mit Computern(Bildschirmen)

#### Kriterien für Normen allgemein 
Normen sind keine Gesetze. Sie dienen eher als Richtlinien, die individuell angepasst werden. Dies ist auch von den Rahmenbedingungen abhängig.<br>
Normen bieten keine konkrete Lösungen sondern nur Anforderungen an den Soll-Zustand.<br>
Damit Normen von Benutzer/*innnen akzeptiert werden, müssen diese für den Sinn und Zweck der Norm sensibilisiert werden. Vorschriften helfen nur bedingt.

#### Kriterien für Normen zur Software-Ergonomie
Für die Software-Ergonomie gibt es 7 wesentliche Prinzipien. In vielen Punkten überlappen diese sich mit der Jakob's Law aus den [Heuristiken](#Heuristiken). Allerdings werden hier die Punkte noch mehr aus Sicht des Benutzer/*inn betrachtet <br>
Beispiele hierfür sind etwa die Lernförderlichkeit. Ein Programm ist dann Lernförderlich wenn sich Arbeitsschritte mit diesem leicht merken lassen. Ein Arbeitsschritt kann hierbei zum Beispiel eine Tastenkombination sein um die Datei zu speichern (STRG + S) <br>
Zusätzlich zur den Punkten aus den [Heuristiken](#Heuristiken) wird noch die Farb- und Schriftgestaltung mit in Betracht gezogen. Das Prinzip hierzu sieht vor dass es zu Farben allgemeine deutungen gibt. Etwa das Rot Gefhar, Gelb Vorsicht und Grün Sicherheit signalisiert. Es wird auch darauf hingewiesen dass der Benutzer/*inn ein anderes Display mit anderen Einstellungen hat. Farben und Farbtöne können bei ihm / ihr unter Umständen anders dargestellt bzw. wahrgenommen werden. <br>
Gleiches gilt für die Schrift. Durch unterschiedliche Displayauflösungen können Buchstaben näher aneinander rücken, sodass ergonomisches Arbeiten schwierig wird. Des Weiteren sollte darauf geachtet werden eine Schrift zu verwenden bei der der Benutzer/*inn den Unterschied zwischen einem großen 'i' und einem kleinen 'L' sofort erkennen kann(Il). Arial ist hierfür zum Beispiel ungeeignet! 

## Auswertungskriterien
### Definition
Um zu überprüfen ob die Schlüsse die man aus der Analyse und Designphase zu dem richtigen Ergebnis geführt haben, ist es von Vorteil Daten im produktiven Umfeld zu erheben. Dies können Log-Daten der App sein (z.B. wie oft wurde welcher Button angeglickt) oder aber Meta-Daten der Benutzer/*inn sein (z.B. Altersgruppe). Wichtig ist hierbei die Transparenz, Annonymisierung und Einwilligung der Benutzer/*inn. 
### Kriterien
Bei der Auswertung ist es wichtig objektiv messbare Fragen zu formulieren. Zu den allgemeinen Kategorien folgen einige allgemeine Beispielfragen:
1. Effizienz der Interaktion:
    - Wie lange dauert die Durchführung einer Aufgabe?
    - Welche Aktionen treten besonders häufig auf?
    - Wie viele Klicks werden benötigt um eine Aufgabe zu lösen?
2. Auftreten von Bedienfehlern
    - Wie viele Benutzer/*innen können eine Aufgabe erfolgreich lösen? Wie viele nicht?
    - Welche Bedienfehler treten besonders häufig auf?
    - Wie viele ausgeführte Aktionen haben zum gewünschten Ziel geführt? Wie viele nicht?
3. Erlernbarkeit
    - Wie lange braucht der Benutzer/*inn um sich Abläufe zur Durchführung einer Aufgabe merken?
    - Wie ist die Vergessenskurve für die Anwendung nach einer bestimmten Zeit (Tage, Wochen, Monate)?
4. Subjektive Zufriedenheit
    - Standardisiertes Interview um subjektives Empfinden des Benutzer / der Benutzerinn zu erfragen.
        - Wie finden Sie das Design?
        - Benutzen Sie die App gerne?

## Vorgehensmodelle

|Modell          |Zeitpunkt des Einsatzes    |Beteiligte|Phasen|Dauer|Ziel| 
|----------------|---------------------------|----------|------|-----|----|
|Design Sprint   |Projektbeginn              | 5-7 Mitglieder, 5 Kunden Test, Product Owner, Moderator| 5 Phasen: Verstehen und Definition, Sketch, Entscheidung, Prototyp, Evaluation| 5 Tage| Anhand analysierter Probleme Lösungen entwickeln, mit enger Zusammenarbeit mit Kunde|
|Design Thinking |Projektbeginn              | 5-9 Mitglieder (multidisziplinäres, heterogenes Team), Moderator| 6 Phasen: Verstehen und Definition, Beobachten, Definition des Standpunkts, Entwicklung der Ideen, Prototyp, Test| nicht festgelegt; Vorgang gilt erst als abgeschlossen, Produkt/ Idee implementiert ist| Lösungsfinden bei unbekannten und komplexen Problemen|
|Double Diamond  |Projektbeginn/ -begleitend | nicht festgelegt| 4 Phasen: Entdecken, Definition, Entwicklung, Lieferung| nicht festgelegt| kreative Problemlösung vor bzw. während des Projekts|


### Design Sprint
#### Definition
Ein Team aus fünf bis sieben Mitgliedern nehmen an einem fünftätigen Workshop mit dem Ziel teil, ein Produkt oder dessen Weiterentwicklung in einem kurzen Zeitraum zu realisieren. Input hierfür sind andere Teammitglieder, aber auch Testpersonen bzw. Kund\*innen. Der Design Sprint besteht aus fünf oder sechs Phasen. Pro Tag wird meist eine Phase abgeschlossen. Der genaue Vorgang muss im Vorhinein aber geklärt werden, damit der Design Sprint klar strukturiert und eingehalten werden kann. <br>
#### Voraussetzungen
- **Dauer**: Fünf volle Arbeitstage, das Team muss in dieser Zeit vom Tagesgeschäft, Meetings und anderen Verpflichtungen befreit sein. <br>
- **Team**: Fünf bis sieben Mitglieder. Der Product Owner sollte unbedingt im Team vertreten werden. 
- **Moderator**: Ernnenung eines Moderatoren, damit das Team nicht den roten Faden verliert und Prozesse eingehalten werden. 
- **Kunden**: Fünf Kunden werden bei Abschluss des Design Sprints ausführlich interviewt oder mit dem Prototypen konfrontiert.
- **Raum/Material**: Genügend Materialien, damit das Team einen Prototypen entwickeln kann und sich in seiner Kreativität vollstens ausleben kann.
- **Challenge**: Beschreibt das Problem oder die Idee, die zu lösen/ zu entwickeln gilt. Sie enthält folgende Fragestellungen: <br>
        - Was ist das Problem? Von welcher Situation wird ausgegangen? <br>
        - Was ist das Ziel/ wo wollen wir hin? <br>
        - Wer ist der Kunde? <br>
#### Durchführung
- **Tag 1 - Verstehen und Definition** <br>
        Das Team arbeitet zusammen, um eine gemeinsame Auffassung des Problems zu bekommen, damit die Lösungsansätze auf das gleiche Ziel hinarbeiten. Das Ziel des Sprints wird an diesem Tag durch verschiedene Methoden definiert und festgelegt.
- **Tag 2 - Sketch** <br>
        Skizzieren von Lösungsideen. Diese dürfen auch aus einer Inspiration von anderen Wettbewerbern oder anderen Kontexten heraus entstehen. Hierbei wird zu einer Einzelarbeit geraten, da hierbei bessere Ergbebnisse erzielt werden. Die Ideen bzw. Lösungsskizzen werden am Ende des Tages gesammelt, aber noch nicht besprochen.
- **Tag 3 - Entscheidung** <br>
        Bewertung und Besprechung der gesammelten Lösungsskizzen. Hierzu ist es wichtig, den Teammitgliedern ausreichend Zeit zu geben, um sich eine Meinung zu den jeweiligen Lösungen zu bilden. Wenn eine Wahl getroffen wurde, erstellt das Team gemeinsam ein Storyboard, indem sich Prozesse und die Customer Journey wiederspiegeln. Dies ist die Grundlage für den Prototypen.
- **Tag 4 - Prototyp** <br>
        Auf Basis des Storyboards emtwickeln die Teammitglieder Prototypen. Diese können in kleinen Gruppen oder auch einzeln erstellt werden. 
- **Tag 5 - Evaluation** <br>
        Kunden werden mit dem Prototypen konfrontiert und anschließend interviewt, um die Wirkung des Prototypen festzustellen, mögliche Verbesserungsvorschläge zu erhalten oder auch entdeckte Probleme zu bewältigen. <br>
### Design Thinking
#### Definition 
Design Thinking hilft Designern, große, komplizierte oder gar unbekannte Probleme in der Produktentwicklung angehen zu können. Dies ist also ein Prozess/ Framework zur Entwicklung von Lösungen für ein Produkt.
#### Durchführung
- **Phase 1 - Verstehen und Definition** <br>
        Definition der Ausgangssituation und Sicherstellung, dass alle beteiligten Personen die selbe Auffassung des zu lösenden Problems haben.
- **Phase 2 - Beobachten** <br>
        In dieser Phase verfolgen die beteiligten Personen das Ziel, die Bedürfnisse, Wünsche und Prioritäten zu verstehen und zu analysieren. Hierbei ist es wichtig, dem Kunden sehr genau zuzuhören, wie und was er über das Problem äußert.
- **Phase 3 - Definition des Standpunkts** <br>
        Zusammenfassung der Ergebnisse der ersten beiden Phasen: Ziel ist es, einen konzeptionellen Rahmen zu entwickeln, der den Lösungsraum absteckt und ein Kundenprofil erstellt.
- **Phase 4 - Entwicklung der Ideen** <br>
        Aus dem definierten Rahmen aus der dritten Phase werden jetzt Ideen entwickelt. Diese dürfen bzw. sollen ohne Beschränkung gesammelt werden, auch wenn diese in anderen Formen schon bei Konkurrenz oder anderen Branchen bereits existieren. 
- **Phase 5 - Prototyp** <br>
        Entwerfen eines oder mehrerer Prototypen aus den gesammelten Ideen. <br>
- **Phase 6 - Test** <br>
        Präsentation des Prototypen vor dem Kunden. Wie interagiert der Kunde mit der Lösung? Hierbei ist es wichtig, Feedback zu erhalten und ihn nicht von dem Prototypen überzeugen, damit der Wunsch des Kunden zu seiner vollsten Zufriedenheit berücksichtigt wird.<br>

*Der Vorgang gilt erst als abgeschlossen, wenn die Idee materialisiert und implementiert ist.*   
### Double Diamond
#### Definition 
Prozess, der dem Team hilft, gemeinsam in eine Richtung zu arbeiten. Hauptbestandteil ist die Trennung in divergiertes und konvergiertes Denken - im divergierten Denken werden Ideen gesucht, im konvergierten Denken werden die Ideen gesammelt und bewertet. Der erste "Diamant" beinhaltet zwei Phasen und ist der Informationssammlung bzw. dem User Research gewidmet, also dem Fragen, Zuhören und Sortieren. Der zweite "Diamant" und seine zwei Phasen dienen der Informationsverarbeitung bzw. dem iterativen Design-Prozess. 
#### Durchführung
- **Phase 1 - Entdecken** <br>
        Phase des divergentem Denken: Dem Team wird Raum geschaffen, um Ideen und Lösungen zu finden.
- **Phase 2 - Definition** <br>
        Evaluation und Auswählen einer Idee (konvergentes Denken): Ergebnisse werden analysiert, entwickelt und gewinnen an Detailierungsgrad.
- **Phase 3 - Entwicklung** <br>
        Das Team verfeinert eines oder mehrere Konzepte, die in den vorigen Phasen definiert wurden. Dies ist wieder eine Phase des divergenten Denkens, da dieser Vorgang von Prototyping, Brainstorming oder Visualisierung geprägt ist.
- **Phase 4 - Lieferung** <br>
        Während der Lieferphase dreht sich der Prozess um das endgültige Konzept, die Produktion, das finale Testen und den Start des Produkts. 


 
