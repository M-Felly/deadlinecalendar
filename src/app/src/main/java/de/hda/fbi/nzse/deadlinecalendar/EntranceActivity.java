package de.hda.fbi.nzse.deadlinecalendar;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import androidx.appcompat.app.AppCompatActivity;

import de.hda.fbi.nzse.deadlinecalendar.manager.ManagerHolder;
import de.hda.fbi.nzse.deadlinecalendar.manager.NotificationManager;
import de.hda.fbi.nzse.deadlinecalendar.model.Settings;
import de.hda.fbi.nzse.deadlinecalendar.model.repository.SettingsRepository;

public class EntranceActivity extends AppCompatActivity {
    public static final Class<DeadlineViewActivity> MAIN_ACTIVITY = DeadlineViewActivity.class;
    public static final int DISPLAY_LOGO_DURATION = 1000;
    public static final String INITIAL_RUN_KEY = "initialRun";

    private boolean initRun;

    private Handler threadHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entrance);

        this.initRun = false;

        threadHandler = new Handler(Looper.getMainLooper());
        threadHandler.post(this::onStartup);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        threadHandler.postDelayed(this::gotoNextActivity, DISPLAY_LOGO_DURATION);
    }

    /***************************************************************
     *                        Thread tasks
     ***************************************************************/
    private void onStartup() {
        SharedPreferences prefs = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        if (prefs.getBoolean(INITIAL_RUN_KEY, true)) {
            initDatabase();

            this.initRun = true;
        }

        initManager();
    }

    private void gotoNextActivity() {
        Intent intent;

        intent = initRun ?
                new Intent(this, EntranceProfileActivity.class) :
                new Intent(this, DeadlineViewActivity.class);

        startActivity(intent);
    }


    /***************************************************************
     *                      Initialize methods
     ***************************************************************/

    private void initDatabase() {
        SettingsRepository settingsRepository = new SettingsRepository(this);
        Settings settings = new Settings(true, true);

        settingsRepository.insert(settings);
    }

    private void initManager() {
        NotificationManager notificationManager = ManagerHolder.getManager(NotificationManager.class);
        notificationManager.init(this);
    }
}