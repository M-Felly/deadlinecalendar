package de.hda.fbi.nzse.deadlinecalendar.view_elements.extended;

import android.content.Context;
import android.text.SpannableStringBuilder;
import android.text.style.RelativeSizeSpan;
import android.util.AttributeSet;

/**
 * This EditText is specialized for dialogs
 * <p>
 * In dialogs the error message text size is bigger then editText fields on non dialog ui fields
 * To prevent this behaviour the {@link #setError(CharSequence)} method adds a spannable to
 * decrease the font size to {@link #ERROR_TEXT_SCALE_PROPORTION}
 *
 * @author Matthias Feyll
 */
public class DialogEditText extends androidx.appcompat.widget.AppCompatEditText {
    private static final float ERROR_TEXT_SCALE_PROPORTION = 0.7f;

    public DialogEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setError(CharSequence error) {
        SpannableStringBuilder stringBuilder = new SpannableStringBuilder(error);
        stringBuilder.setSpan(new RelativeSizeSpan(ERROR_TEXT_SCALE_PROPORTION), 0, error.length(), 0);

        this.requestFocus();

        super.setError(stringBuilder);
    }
}
