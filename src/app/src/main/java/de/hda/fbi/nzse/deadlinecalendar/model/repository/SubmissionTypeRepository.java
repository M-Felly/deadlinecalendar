package de.hda.fbi.nzse.deadlinecalendar.model.repository;

import android.content.Context;

import java.util.List;

import de.hda.fbi.nzse.deadlinecalendar.model.SubmissionType;

/**
 * SubmissionTypeRepository
 * <p>
 * For more details
 *
 * @author Anna Kilb
 * @see BaseRepository
 */
public class SubmissionTypeRepository extends BaseRepository{

    public static final String TAG = "SubmissionTypeRepository";

    public SubmissionTypeRepository(Context context) {
        super(context);
    }

    /**
     * Get all submission types
     *
     * @param resultCallbackListener that will be passed from the invoking activity {@link de.hda.fbi.nzse.deadlinecalendar.AddDeadlineViewActivity} {@link de.hda.fbi.nzse.deadlinecalendar.EditDeadlineViewActivity}
     */
    public void getAll(ResultCallbackListener<List<SubmissionType>> resultCallbackListener) {
        this.executeDatabaseAction(DatabaseAction.SELECT, appDatabase -> appDatabase.submissionTypeDao().getSubmissionTypes(), resultCallbackListener);
    }

    /**
     * Insert a new Submission type
     *
     * @param submissionType         to be inserted
     * @param resultCallbackListener that will be passed from the invoking activity {@link de.hda.fbi.nzse.deadlinecalendar.AddDeadlineViewActivity} {@link de.hda.fbi.nzse.deadlinecalendar.EditDeadlineViewActivity}
     */
    public void insert(SubmissionType submissionType, ResultCallbackListener<Long> resultCallbackListener) {
        this.executeDatabaseAction(DatabaseAction.INSERT, (appDatabase) -> appDatabase.submissionTypeDao().insert(submissionType), id -> {
            submissionType.submissionTypeID = id;
            resultCallbackListener.onSuccess(id);
        });
    }

    /**
     * Wrapper method for {@link #insert(SubmissionType, ResultCallbackListener)}
     *
     * @param submissionType    to be inserted
     */
    public void insert(SubmissionType submissionType) {
        insert(submissionType, data -> {});
    }

    /**
     * Delete one submission type
     *
     * @param submissionType            to be delete
     * @param resultCallbackListener onDelete lambda
     */
    public void delete(SubmissionType submissionType, ResultCallbackListener<Integer> resultCallbackListener) {
        executeDatabaseAction(DatabaseAction.DELETE, (appDatabase) -> appDatabase.submissionTypeDao().delete(submissionType), resultCallbackListener);
    }

    @Override
    protected String getTag() {
        return TAG;
    }
}
