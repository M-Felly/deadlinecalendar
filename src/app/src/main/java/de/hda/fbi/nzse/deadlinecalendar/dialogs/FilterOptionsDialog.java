package de.hda.fbi.nzse.deadlinecalendar.dialogs;


import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Objects;

import de.hda.fbi.nzse.deadlinecalendar.R;
import de.hda.fbi.nzse.deadlinecalendar.model.repository.StudyModuleRepository;

/**
 * This class handles a dialog where filter options can be specified.
 *
 * The applied filters have impact of the displayed deadlines within the RecyclerView
 *
 * @author Dennis Hilz
 */
public class FilterOptionsDialog extends DialogFragment {
    public final static String ACTION_LISTENER_KEY = "actionListenerKey";
    public final static String FILTER_OPTIONS_STATES_KEY = "filterOptionsStatesKey";

    /**
     * Callback listener to get the resolved attributes.
     *
     * @see FilterOptionsDialog
     */
    @FunctionalInterface
    public interface OnClickListener extends Serializable {
        void onSubmitDialog(String[] filterOptionNames, boolean[] filterOptionStates);
    }

    /* **************************************************************
     *                            Class
     ****************************************************************/
    private OnClickListener dialogListener;
    private final StudyModuleRepository studyModuleRepository;
    private String[] filterOptionName;
    private boolean[] filterOptionState;

    public FilterOptionsDialog() {
        this.studyModuleRepository = new StudyModuleRepository(getContext());
    }


    /**
     * Creates the dialog and set the click listener for back and submit button
     */
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle arguments = getArguments();

        if (arguments == null) {
            throw new Error("Arguments missing");
        }

        this.filterOptionState = (boolean[]) arguments.getSerializable(FILTER_OPTIONS_STATES_KEY);
        this.dialogListener = (OnClickListener) arguments.getSerializable(ACTION_LISTENER_KEY);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_deadline_filter_modules, null);
        builder.setView(dialogView);

        // filter options
        this.studyModuleRepository.getAll(studyModules -> {
            filterOptionName = new String[Objects.requireNonNull(studyModules).size()];
            builder.setTitle(R.string.FilterOptionsialog_title_name);

            for (int i = 0; i < studyModules.size(); i++) {
                filterOptionName[i] = studyModules.get(i).getTag();
            }

            if (filterOptionState == null) {
                this.filterOptionState = new boolean[this.filterOptionName.length];
                Arrays.fill(filterOptionState, true);
            }

            /*
             * Setting up Multichoice-Items -> All created studymodules
             */
            builder.setMultiChoiceItems(filterOptionName, filterOptionState, (dialog, which, isChecked) -> filterOptionState[which] = isChecked);
        });

        // back button
        ImageButton backBtn = dialogView.findViewById(R.id.dialog_deadline_filter_modules_imageButton_back);
        backBtn.setOnClickListener(v -> dismiss());

        // submit button
        ImageButton submitBtn = dialogView.findViewById(R.id.dialog_deadline_filter_modules_imageButton_submit);
        submitBtn.setOnClickListener(submit -> onSubmitBtnClicked());

        return builder.create();
    }

    /* **************************************************************
     *                     Action / Listener
     ****************************************************************/

    /**
     * Get text out of elements and call the listener
     */
    private void onSubmitBtnClicked() {
        this.dialogListener.onSubmitDialog(filterOptionName, filterOptionState);
        dismiss();
    }

}
