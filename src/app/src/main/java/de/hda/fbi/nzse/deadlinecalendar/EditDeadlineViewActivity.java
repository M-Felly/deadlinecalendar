package de.hda.fbi.nzse.deadlinecalendar;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import de.hda.fbi.nzse.deadlinecalendar.adapter.EditDeadline_ToDoAdapter;
import de.hda.fbi.nzse.deadlinecalendar.dialogs.DeadlineDeleteDialog;
import de.hda.fbi.nzse.deadlinecalendar.factory.DialogFragmentFactory;
import de.hda.fbi.nzse.deadlinecalendar.manager.AlarmManager;
import de.hda.fbi.nzse.deadlinecalendar.manager.ManagerHolder;
import de.hda.fbi.nzse.deadlinecalendar.model.Deadline;
import de.hda.fbi.nzse.deadlinecalendar.model.Notification;
import de.hda.fbi.nzse.deadlinecalendar.model.StudyModule;
import de.hda.fbi.nzse.deadlinecalendar.model.SubmissionType;
import de.hda.fbi.nzse.deadlinecalendar.model.ToDo;
import de.hda.fbi.nzse.deadlinecalendar.model.repository.StudyModuleRepository;
import de.hda.fbi.nzse.deadlinecalendar.model.repository.SubmissionTypeRepository;
import de.hda.fbi.nzse.deadlinecalendar.model.typeconverters.GregorianCalendarConverter;
import de.hda.fbi.nzse.deadlinecalendar.model.typeconverters.RemainingTimeConverter;
import de.hda.fbi.nzse.deadlinecalendar.presenter.DeadlineAccessHandler;
import de.hda.fbi.nzse.deadlinecalendar.presenter.SubmissionTypePresenter;

import static de.hda.fbi.nzse.deadlinecalendar.dialogs.DeadlineDeleteDialog.ACTION_LISTENER_KEY;

/**
 * In this activity the user can edit a deadline.
 * He/She is also able to add or delete toDos.
 *
 * Deleting a deadline calls the dialog {@link DeadlineDeleteDialog}
 *
 * @author Anna Kilb
 */
public class EditDeadlineViewActivity extends AppCompatActivity implements ViewTreeObserver.OnPreDrawListener, DeadlineDeleteDialog.OnClickListener {
    private static final String TAG = "EditDeadlineViewActivity";
    public static final int RECYCLER_VIEW_MARGIN_BOTTOM = 370;

    private BottomNavigationView bottomNav;
    private DeadlineAccessHandler deadlineAccessHandler;
    private SubmissionTypeRepository submissionTypeRepository;
    private EditDeadline_ToDoAdapter editDeadlineToDoAdapter;
    private Deadline deadline;

    private EditText deadlineTitle_editText;
    private Spinner studyModule_spinner;
    private Spinner submissionType_spinner;
    private Spinner notification_spinner;
    private Notification.DeadlineNotificationType notificationType;
    private EditText submissionType_editText;
    private TextView date_textView;
    private DatePickerDialog.OnDateSetListener dateSetListener;
    private TextView time_textView;
    private TimePickerDialog.OnTimeSetListener timeSetListener;
    private ConstraintLayout recyclerViewLayout;
    private RecyclerView toDosRecyclerView;
    private ImageButton addToDo_imageButton;
    private EditText newToDoDescription_editText;
    private GregorianCalendar selectedDate;
    private GregorianCalendar date;

    private List<StudyModule> studyModuleList;

    private ParentClass parentClass;

    enum ParentClass {CALENDARVIEWACTIVITY, DEADLINEVIEWACTIVITY}

    /* **************************************************************
     *                     Life cycle hooks
     ****************************************************************/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_deadline_view);
        setTitle(getString(R.string.title_edit));

        // cancel button
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_cancel_icon);

        bottomNav = findViewById(R.id.editDeadlineViewActivity_bottomNav);
        bottomNav.setOnNavigationItemSelectedListener(new BottomNavigation(this));

        this.deadlineTitle_editText = findViewById(R.id.editDeadlineViewActivity_editText_name);
        this.studyModule_spinner = findViewById(R.id.editDeadlineViewActivity_spinner_studyModule);
        this.submissionType_spinner = findViewById(R.id.editDeadlineViewActivity_spinner_submissionType);
        this.submissionType_editText = findViewById(R.id.editDeadlineViewActivity_editText_submissionType);
        this.date_textView = findViewById(R.id.editDeadlineViewActivity_textView_date);
        this.time_textView = findViewById(R.id.editDeadlineViewActivity_textView_time);
        this.notification_spinner = findViewById(R.id.editDeadlineViewActivity_spinner_notification);
        this.recyclerViewLayout = findViewById(R.id.editDeadlineViewActivity_constraintLayout_recyclerViewLayout);
        this.toDosRecyclerView = findViewById(R.id.editDeadlineViewActivity_recyclerView_toDos);
        addToDo_imageButton = findViewById(R.id.editDeadlineViewActivity_imageButton_addToDo);
        this.newToDoDescription_editText = findViewById(R.id.editDeadlineViewActivity_editText_newToDoDescription);
        Button delete_button = findViewById(R.id.editDeadlineViewActivity_button_delete);

        this.deadlineAccessHandler = new DeadlineAccessHandler(this);
        StudyModuleRepository studyModuleRepository = new StudyModuleRepository(this);
        this.submissionTypeRepository = new SubmissionTypeRepository(this);

        Bundle intentExtras = getIntent().getExtras();
        this.deadline = (Deadline) intentExtras.getSerializable("Deadline");

        if (intentExtras.getString("class").equals(CalendarViewActivity.class.toString())) {
            parentClass = ParentClass.CALENDARVIEWACTIVITY;
            selectedDate = (GregorianCalendar) intentExtras.getSerializable("selectedDate");
        } else {
            parentClass = ParentClass.DEADLINEVIEWACTIVITY;
        }
        this.toDosRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        this.editDeadlineToDoAdapter = new EditDeadline_ToDoAdapter(deadlineAccessHandler, deadline);
        this.toDosRecyclerView.setAdapter(editDeadlineToDoAdapter);
        this.toDosRecyclerView.getViewTreeObserver().addOnPreDrawListener(this);

        // title
        deadlineTitle_editText.setText(deadline.getTitle());

        //spinners
        List<String> studyModules = new ArrayList<>();
        studyModuleRepository.getAll(_studyModuleList -> {
            if (_studyModuleList != null) {
                for (StudyModule studyModule : _studyModuleList) {
                    studyModules.add(studyModule.getName());
                }
            }
            this.studyModuleList = _studyModuleList;
        });
        ArrayAdapter<String> studyModule_arrayAdapter = new ArrayAdapter<>(this, R.layout.spinner, studyModules);
        studyModule_arrayAdapter.setDropDownViewResource(R.layout.spinner);
        studyModule_spinner.setAdapter(studyModule_arrayAdapter);
        studyModule_spinner.setSelection(studyModule_arrayAdapter.getPosition(deadlineAccessHandler.getStudyModule(deadline).getName()));

        setSubmissionTypeSpinner();
        setNotificationSpinner();

        // date
        GregorianCalendar date = deadline.getSubmissionDate();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.forLanguageTag(String.valueOf(R.string.languageTag)));
        String dateTemp = dateFormat.format(date.getTime());
        date_textView.setText(dateTemp);
        onDateClicked();

        // time
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm", Locale.forLanguageTag(String.valueOf(R.string.languageTag)));
        String timeTemp = timeFormat.format(date.getTime());
        time_textView.setText(timeTemp);
        onTimeClicked();

        // add ToDo
        newToDoDescription_editText.setOnKeyListener((v, keyCode, event) -> {
            // If the event is a key-down event on the "enter" button
            if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                // Perform action on key press
                onAddToDoBtnClicked(null);
                return true;
            }
            return false;
        });
        addToDo_imageButton.setOnClickListener(this::onAddToDoBtnClicked);

        // delete
        delete_button.setOnClickListener(view -> deleteDeadline());
    }

    @Override
    protected void onResume() {
        super.onResume();
        bottomNav.getMenu().findItem(R.id.bottom_nav_deadlines).setChecked(true);
    }

    /* **************************************************************
     *                         Navigation
     ****************************************************************/

    @Override
    public boolean onSupportNavigateUp() {
        if (parentClass == ParentClass.CALENDARVIEWACTIVITY) {
            startActivity(new Intent(EditDeadlineViewActivity.this, DetailDeadlineViewActivity.class).putExtra("class", CalendarViewActivity.class.toString()).putExtra("selectedDate", selectedDate).putExtra("Deadline", deadline));
        } else {
            startActivity(new Intent(EditDeadlineViewActivity.this, DetailDeadlineViewActivity.class).putExtra("class", EditDeadlineViewActivity.class.toString()).putExtra("Deadline", deadline));
        }
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.edit_view_nav_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.editView_item_done) {
            // Get date
            String date_string = date_textView.getText().toString();
            int dayOfMonth = Integer.parseInt(date_string.substring(0, 2));
            int month = Integer.parseInt(date_string.substring(3, 5)) - 1;
            int year = Integer.parseInt(date_string.substring(6, 10));
            // Get time
            String time = time_textView.getText().toString();
            int hourOfDay = Integer.parseInt(time.substring(0, 2));
            int minute = Integer.parseInt(time.substring(3, 5));
            date = new GregorianCalendar();
            date.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            date.set(Calendar.MONTH, month);
            date.set(Calendar.YEAR, year);
            date.set(Calendar.HOUR_OF_DAY, hourOfDay);
            date.set(Calendar.MINUTE, minute);

            if (checkIfInputIsValid()) {
                deadline.setTitle(deadlineTitle_editText.getText().toString());
                deadline.setSubmissionDate(date);

                // Set Reminder Date (Handling NotificationType)

                GregorianCalendar reminderDateOfDeadline = new GregorianCalendar();
                reminderDateOfDeadline.setTimeInMillis(date.getTimeInMillis());
                switch (notificationType) {

                    case ONE_HOUR: {
                        reminderDateOfDeadline.add(Calendar.HOUR_OF_DAY, -1);
                        break;
                    }
                    case THREE_HOURS: {
                        reminderDateOfDeadline.add(Calendar.HOUR_OF_DAY, -3);
                        break;
                    }
                    case ONE_DAY: {
                        reminderDateOfDeadline.add(Calendar.MONTH, -1);
                        break;
                    }
                    case THREE_DAYS: {
                        reminderDateOfDeadline.add(Calendar.MONTH, -3);
                        break;
                    }
                    case ONE_WEEK: {
                        reminderDateOfDeadline.add(Calendar.WEEK_OF_MONTH, -1);
                        break;
                    }
                }
                deadline.setReminderDate(reminderDateOfDeadline);
                deadline.setNotificationType(notificationType);

                deadlineAccessHandler.updateDeadline(deadline);
                deadlineAccessHandler.setStudyModule(deadline, this.studyModuleList.get(studyModule_spinner.getSelectedItemPosition()));
                setSubmissionType();
                deadlineAccessHandler.updateDeadlinesToDos(deadline, editDeadlineToDoAdapter.getToDos());

                AlarmManager alarmManager = ManagerHolder.getManager(AlarmManager.class);
                alarmManager.update(this, deadline);

                if (parentClass == ParentClass.CALENDARVIEWACTIVITY) {
                    startActivity(new Intent(EditDeadlineViewActivity.this, DetailDeadlineViewActivity.class).putExtra("class", CalendarViewActivity.class.toString()).putExtra("selectedDate", selectedDate).putExtra("Deadline", deadline));
                } else {
                    startActivity(new Intent(EditDeadlineViewActivity.this, DetailDeadlineViewActivity.class).putExtra("class", DeadlineViewActivity.class.toString()).putExtra("Deadline", deadline));
                }
                finish();
            }
            return false;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Checks if all input fields are correctly filled from the user.
     *
     * @return if user input is valid.
     */
    public boolean checkIfInputIsValid() {
        // Checking name field
        if (this.deadlineTitle_editText.getText().toString().isEmpty()) {
            deadlineTitle_editText.setError(getString(R.string.add_edit_appointmentview_error_set_valid_name));
            return false;
        }

        // Check if input date+time
        GregorianCalendar currentTime = GregorianCalendarConverter.dateToCalendar(Calendar.getInstance().getTimeInMillis());
        if (currentTime != null && date.getTimeInMillis() < currentTime.getTimeInMillis() + RemainingTimeConverter.TIME_FACTOR_HOUR/2) {
            date_textView.setError("");
            Toast.makeText(this, getString(R.string.AddDeadlineViewActivity_error_toast_message), Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    /* **************************************************************
     *                      Actions / Listener
     ****************************************************************/

    private void onDateClicked() {
        date_textView.setOnClickListener(view -> {
            String date = date_textView.getText().toString();
            int year = Integer.parseInt(date.substring(6, 10));
            int month = Integer.parseInt(date.substring(3, 5)) - 1;
            int day = Integer.parseInt(date.substring(0, 2));

            DatePickerDialog dialog = new DatePickerDialog(
                    EditDeadlineViewActivity.this,
                    dateSetListener,
                    year, month, day);
            dialog.show();
        });
        dateSetListener = (view, year, month, dayOfMonth) -> {
            Log.d(TAG, "onDateSet: dd.MM.yyyy: " + dayOfMonth + "." + month + "." + year);

            String date1 = "";
            if (dayOfMonth < 10) {
                date1 = date1.concat("0" + dayOfMonth + ".");
            } else
                date1 = date1.concat(dayOfMonth + ".");
            if (month + 1 < 10)
                date1 = date1.concat("0" + (month + 1) + "." + year);
            else
                date1 = date1.concat((month + 1) + "." + year);
            date_textView.setText(date1);
        };
    }

    private void onTimeClicked() {
        time_textView.setOnClickListener(view -> {
            String time = time_textView.getText().toString();
            int hourOfDay = Integer.parseInt(time.substring(0, 2));
            int minute = Integer.parseInt(time.substring(3, 5));

            TimePickerDialog dialog = new TimePickerDialog(
                    EditDeadlineViewActivity.this,
                    timeSetListener,
                    hourOfDay, minute, true);
            dialog.show();
        });
        timeSetListener = (view, hourOfDay, minute) -> {
            Log.d(TAG, "onTimeSet: hh:mm: " + hourOfDay + ":" + minute);

            String time = "";
            if (hourOfDay < 10)
                time = time.concat("0" + hourOfDay + ":");
            else
                time = time.concat(hourOfDay + ":");
            if (minute < 10)
                time = time.concat("0" + minute);
            else
                time = time.concat(Integer.toString(minute));
            time_textView.setText(time);
        };
    }

    private void onAddToDoBtnClicked(View view) {
        String toDoDescription = newToDoDescription_editText.getText().toString();
        if (!toDoDescription.equals("")) {
            ToDo newToDo = new ToDo(toDoDescription);
            editDeadlineToDoAdapter.addToDo(newToDo);
            newToDoDescription_editText.setText("");
            if (toDosRecyclerView.getAdapter() != null)
                toDosRecyclerView.getAdapter().notifyDataSetChanged();
        }
    }

    private void deleteDeadline() {
        Bundle dialogArguments = new Bundle();
        dialogArguments.putSerializable(ACTION_LISTENER_KEY, this);

        DialogFragment deadlineDeleteDialog = DialogFragmentFactory.createInstance(DeadlineDeleteDialog.class, dialogArguments);
        deadlineDeleteDialog.show(getSupportFragmentManager(), TAG);
    }

    @Override
    public void onSubmitDialog() {
        deadlineAccessHandler.deleteDeadline(deadline);
        if (parentClass == ParentClass.CALENDARVIEWACTIVITY) {
            startActivity(new Intent(EditDeadlineViewActivity.this, CalendarViewActivity.class).putExtra("class", DetailCalendarViewActivity.class.toString()).putExtra("selectedDate", selectedDate).putExtra("Deadline", deadline));
        } else {
            startActivity(new Intent(EditDeadlineViewActivity.this, DeadlineViewActivity.class).putExtra("class", DetailDeadlineViewActivity.class.toString()).putExtra("Deadline", deadline));
        }
        finish();
    }

    /* **************************************************************
     *                      Setter
     ****************************************************************/

    private void setSubmissionTypeSpinner() {
        SubmissionTypePresenter submissionTypePresenter = new SubmissionTypePresenter(this, deadlineAccessHandler.getSubmissionType(deadline));
        List<SubmissionType> submissionTypes = new ArrayList<>();
        submissionTypeRepository.getAll(submissionTypes::addAll);
        List<String> allSubmissionTypes = submissionTypePresenter.getPredefinedSubmissionTypes();
        allSubmissionTypes.remove(1);
        for (SubmissionType sT : submissionTypes) {
            if (sT.getCustomText() != null) {
                allSubmissionTypes.add(sT.getCustomText());
            }
        }
        allSubmissionTypes.add(getString(R.string.AddEditDeadlineViewActivity_spinner_otherSubmissionType));
        ArrayAdapter<String> submissionType_arrayAdapter = new ArrayAdapter<>(this, R.layout.spinner, allSubmissionTypes);
        submissionType_arrayAdapter.setDropDownViewResource(R.layout.spinner);
        submissionType_spinner.setAdapter(submissionType_arrayAdapter);
        submissionType_spinner.setSelection(submissionType_arrayAdapter.getPosition(submissionTypePresenter.getSubmissionType()));
        submissionType_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (adapterView.getItemAtPosition(i).toString().equals(getString(R.string.AddEditDeadlineViewActivity_spinner_otherSubmissionType)))
                    submissionType_editText.setVisibility(View.VISIBLE);
                else
                    submissionType_editText.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    private void setNotificationSpinner() {
        notificationType = Notification.DeadlineNotificationType.NO_NOTIFICATION;
        ArrayAdapter<CharSequence> notifications_arrayAdapter = ArrayAdapter.createFromResource
                (this, R.array.deadlineNotifications, R.layout.spinner);

        notifications_arrayAdapter.setDropDownViewResource(R.layout.spinner);
        notification_spinner.setAdapter(notifications_arrayAdapter);
        notification_spinner.setSelection(this.deadline.getNotificationType().getValue());
        notification_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0: {
                        notificationType = Notification.DeadlineNotificationType.NO_NOTIFICATION;
                        break;
                    }
                    case 1: {
                        notificationType = Notification.DeadlineNotificationType.ONE_HOUR;
                        break;
                    }
                    case 2: {
                        notificationType = Notification.DeadlineNotificationType.THREE_HOURS;
                        break;
                    }
                    case 3: {
                        notificationType = Notification.DeadlineNotificationType.ONE_DAY;
                        break;
                    }
                    case 4: {
                        notificationType = Notification.DeadlineNotificationType.THREE_DAYS;
                        break;
                    }
                    case 5: {
                        notificationType = Notification.DeadlineNotificationType.ONE_WEEK;
                        break;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void setSubmissionType() {
        long oldSubmissionTypeID = deadline.relatedSubmissionTypeID;
        String submissionTypeName = submissionType_spinner.getSelectedItem().toString();

        final SubmissionType[] newSubmissionType = {new SubmissionType()};
        SubmissionTypePresenter sTP = new SubmissionTypePresenter(this, newSubmissionType[0]);
        SubmissionTypeRepository submissionTypeRepository = new SubmissionTypeRepository(this);
        submissionTypeRepository.getAll(submissionTypeList -> {
            if (submissionTypeList != null && submissionTypeList.size() != 0) {
                for (SubmissionType sT : submissionTypeList) {
                    if (submissionTypeName.equals(sTP.getStringResourceByName(sT.getPredefinedType().toString()))) {
                        newSubmissionType[0] = sT;
                    } else {
                        if (submissionTypeName.equals(sT.getCustomText()))
                            newSubmissionType[0] = sT;
                    }
                }
            }
            if (submissionTypeName.equals(getString(R.string.AddEditDeadlineViewActivity_spinner_otherSubmissionType))) {
                newSubmissionType[0].setCustomText(submissionType_editText.getText().toString());
                submissionTypeRepository.insert(newSubmissionType[0]);
            }

        });
        deadlineAccessHandler.setSubmissionType(deadline, newSubmissionType[0]);
        deadlineAccessHandler.deleteSubmissionType(oldSubmissionTypeID);
    }

    /* **************************************************************
     *                         Drawing
     ****************************************************************/

    /**
     * Calculate max height for {@link #toDosRecyclerView}
     *
     * @return hook used
     */
    @Override
    public boolean onPreDraw() {
        ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) this.recyclerViewLayout.getLayoutParams();
        layoutParams.matchConstraintMaxHeight = calculateRecyclerViewHeight();
        this.recyclerViewLayout.setLayoutParams(layoutParams);

        this.toDosRecyclerView.getViewTreeObserver().removeOnPreDrawListener(this);

        return true;
    }

    /* **************************************************************
     *                         Calculating
     ****************************************************************/

    /**
     * Calculating height for recycler view. This is necessary make the recycler view wrap content during
     * less height to be displayed but also scrollable with a max height on too much items that
     * have to be displayed.
     *
     * @return calculated height
     */
    private int calculateRecyclerViewHeight() {
        int topBorder = 0, bottomBorder;

        // if orientation is portrait get top height from divider position
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            int[] topBorderLocation = {0, 0};
            View divider = findViewById(R.id.editDeadlineViewActivity_view_horizontalLineRecyclerView);
            divider.getLocationOnScreen(topBorderLocation);
            topBorder = topBorderLocation[1];
        } else {
            // if orientation = landscape set topBorder = actionBar height
            TypedValue tv = new TypedValue();
            if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
                topBorder = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics()) + 50;
            }
        }

        // get the bottomBorder from the absolute bottom nav y position
        int[] bottomBorderLocation = {0, 0};
        bottomNav.getLocationOnScreen(bottomBorderLocation);
        bottomBorder = bottomBorderLocation[1];

        return bottomBorder - topBorder - addToDo_imageButton.getHeight() - RECYCLER_VIEW_MARGIN_BOTTOM;
    }
}