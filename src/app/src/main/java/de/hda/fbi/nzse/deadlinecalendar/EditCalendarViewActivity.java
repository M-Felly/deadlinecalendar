package de.hda.fbi.nzse.deadlinecalendar;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

import de.hda.fbi.nzse.deadlinecalendar.dialogs.AppointmentDeleteDialog;
import de.hda.fbi.nzse.deadlinecalendar.factory.DialogFragmentFactory;
import de.hda.fbi.nzse.deadlinecalendar.manager.AlarmManager;
import de.hda.fbi.nzse.deadlinecalendar.manager.ManagerHolder;
import de.hda.fbi.nzse.deadlinecalendar.model.Appointment;
import de.hda.fbi.nzse.deadlinecalendar.model.Notification;
import de.hda.fbi.nzse.deadlinecalendar.model.repository.AppointmentRepository;

import static de.hda.fbi.nzse.deadlinecalendar.dialogs.DeadlineDeleteDialog.ACTION_LISTENER_KEY;
import static de.hda.fbi.nzse.deadlinecalendar.manager.NotificationManager.NOTIFICATION_APPOINTMENT_INTENT;

/**
 * In this activity, the user can edit his already created appointment.
 */
@SuppressWarnings("ConstantConditions")
public class EditCalendarViewActivity extends AppCompatActivity implements Serializable, AdapterView.OnItemSelectedListener, AppointmentDeleteDialog.OnClickListener {
    private BottomNavigationView bottomNav;
    private EditText appointmentName;
    private TextView appointmentDate;
    private TextView appointmentTimeFrom;
    private TextView appointmentTimeTo;
    private EditText appointmentLocation;
    private Spinner notificationSpinner;
    private EditText appointmentDescription;
    private TextView textViewToTimeError;
    private Notification.AppointmentNotificationType notificationType;
    private Appointment parentAppointment;
    AppointmentRepository appointmentRepository;

    private DatePickerDialog.OnDateSetListener dateSetListener;
    private TimePickerDialog.OnTimeSetListener toTimeSetListener;
    private TimePickerDialog.OnTimeSetListener fromTimeSetListener;

    /**
     * Goes back to DetailAppointmentViewActivity.
     *
     * @return if action was successful.
     */
    @Override
    public boolean onSupportNavigateUp() {
        startActivity(new Intent(EditCalendarViewActivity.this,DetailCalendarViewActivity.class).putExtra("class", this.getClass().toString()).putExtra("Appointment", parentAppointment));
        onBackPressed();
        return true;
    }

    /**
     * On creating the activity, following actions have to be done that the activity works correctly.
     *
     * @param savedInstanceState given saved instance
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //get appointment
        parentAppointment = (Appointment) getIntent().getExtras().getSerializable(NOTIFICATION_APPOINTMENT_INTENT);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_cancel_icon);

        setContentView(R.layout.activity_edit_calendar_view);
        setTitle(getString(R.string.add_edit_appointmentview_edit));

        bottomNav = findViewById(R.id.editCalendarViewActivity_bottomNavigation_bottomNav);
        bottomNav.setOnNavigationItemSelectedListener(new BottomNavigation(this));

        appointmentName = findViewById(R.id.editCalendarView_editText_name);
        appointmentDate = findViewById(R.id.editCalendarView_editText_date);
        appointmentTimeFrom = findViewById(R.id.editCalendarView_editText_from_time);
        appointmentTimeTo = findViewById(R.id.editCalendarView_editText_to_time);
        appointmentLocation = findViewById(R.id.editCalendarView_editText_location);
        notificationSpinner = findViewById(R.id.editAppointmentView_spinner_notificationpicker);
        appointmentDescription = findViewById(R.id.editCalendarView_editText_description);
        Button deleteButton = findViewById(R.id.editCalendarView_buttont_delete);
        textViewToTimeError = findViewById(R.id.editCalendarView_editText_to_time_error);

        //Set notification spinner
        notificationType = Notification.AppointmentNotificationType.NO_NOTIFICATION;
        ArrayAdapter<CharSequence> notificationSpinnerAdapter = ArrayAdapter.createFromResource(
                this, R.array.appointmentNotifications, R.layout.spinner);
        notificationSpinnerAdapter.setDropDownViewResource(R.layout.spinner);
        notificationSpinner.setAdapter(notificationSpinnerAdapter);
        notificationSpinner.setOnItemSelectedListener(this);


        // Repository
        this.appointmentRepository = new AppointmentRepository(this);

        fillView();

        deleteButton.setOnClickListener(view -> deleteAppointment());
    }

    /**
     * show Date Picker and set selected date
     */
    private void onDateClicked() {
        appointmentDate.setOnClickListener(view -> {
            String date = appointmentDate.getText().toString();
            int year = Integer.parseInt(date.substring(6, 10));
            int month = Integer.parseInt(date.substring(3, 5)) - 1;
            int day = Integer.parseInt(date.substring(0, 2));

            DatePickerDialog dialog = new DatePickerDialog(
                    EditCalendarViewActivity.this,
                    dateSetListener,
                    year, month, day);
            dialog.show();
        });
        dateSetListener = (view, year, month, dayOfMonth) -> {
            Log.d("EditCalendarViewActivity", "onDateSet: dd.MM.yyyy: " + dayOfMonth + "." + month + "." + year);

            String date1 = "";
            if (dayOfMonth < 10) {
                date1 = date1.concat("0" + dayOfMonth + ".");
            } else
                date1 = date1.concat(dayOfMonth + ".");
            if (month + 1 < 10)
                date1 = date1.concat("0" + (month + 1) + "." + year);
            else
                date1 = date1.concat((month + 1) + "." + year);
            appointmentDate.setText(date1);
        };
    }

    /**
     * show Time Picker and set selected to time
     */
    private void onTimeToClicked() {
        appointmentTimeTo.setOnClickListener(view -> {
            String time = appointmentTimeTo.getText().toString();
            int hourOfDay = Integer.parseInt(time.substring(0, 2));
            int minute = Integer.parseInt(time.substring(3, 5));

            TimePickerDialog dialog = new TimePickerDialog(
                    EditCalendarViewActivity.this,
                    toTimeSetListener,
                    hourOfDay, minute, true);
            dialog.show();
        });
        toTimeSetListener = (view, hourOfDay, minute) -> {
            Log.d("EditCalendarViewActivity", "onTimeSet: hh:mm: " + hourOfDay + ":" + minute);

            String time = "";
            if (hourOfDay < 10)
                time = time.concat("0" + hourOfDay + ":");
            else
                time = time.concat(hourOfDay + ":");
            if (minute < 10)
                time = time.concat("0" + minute);
            else
                time = time.concat(Integer.toString(minute));
            appointmentTimeTo.setText(time);
        };
    }

    /**
     * show Time Picker and set selected from time
     */
    private void onTimeFromClicked() {
        appointmentTimeFrom.setOnClickListener(view -> {
            String time = appointmentTimeFrom.getText().toString();
            int hourOfDay = Integer.parseInt(time.substring(0, 2));
            int minute = Integer.parseInt(time.substring(3, 5));

            TimePickerDialog dialog = new TimePickerDialog(
                    EditCalendarViewActivity.this,
                    fromTimeSetListener,
                    hourOfDay, minute, true);
            dialog.show();
        });
        fromTimeSetListener = (view, hourOfDay, minute) -> {
            Log.d("EditCalendarViewActivity", "onTimeSet: hh:mm: " + hourOfDay + ":" + minute);

            String time = "";
            String toTime = "";
            if (hourOfDay < 9) {
                time = time.concat("0" + hourOfDay + ":");
                toTime = toTime.concat("0" + (hourOfDay + 1) + ":");
            }
            else if (hourOfDay == 9){
                time = time.concat("0" + hourOfDay + ":");
                toTime = toTime.concat((hourOfDay + 1) + ":");
            }
            else {
                time = time.concat(hourOfDay + ":");
                toTime = toTime.concat((hourOfDay + 1) + ":");
            }

            if (minute < 10) {
                time = time.concat("0" + minute);
                toTime = toTime.concat("0" + minute);
            }
            else {
                time = time.concat(Integer.toString(minute));
                toTime = toTime.concat(Integer.toString(minute));
            }
            appointmentTimeFrom.setText(time);
            appointmentTimeTo.setText(toTime);
        };
    }


    /**
     * Checks the correct icon in bottom navigation bar.
     */
    @Override
    protected void onResume() {
        super.onResume();
        bottomNav.getMenu().findItem(R.id.bottom_nav_calendar).setChecked(true);
    }

    /**
     * Creates/inflates all items of an menu an bind it onto the toolbar.
     *
     * @param menu is the menu that shall be inflated
     * @return if action was successful
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_view_nav_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Implements the action if a specific element is selected.
     * <p>
     * If the user wants to submit his content, all the input will be proven with the method
     * onSubmitListener. Once the content is valid, an object of type Appointment will be
     * updated in the database.
     *
     * @param item is the selected item
     * @return is the selected item
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.editView_item_done) {
            if (onSubmitListener()) {
                getAppointment();
                appointmentRepository.update(parentAppointment, data -> {
                });

                AlarmManager alarmManager = ManagerHolder.getManager(AlarmManager.class);
                alarmManager.update(this, parentAppointment);
                startActivity(new Intent(EditCalendarViewActivity.this, DetailCalendarViewActivity.class).putExtra("class", this.getClass().toString()).putExtra("Appointment", parentAppointment).putExtra(NOTIFICATION_APPOINTMENT_INTENT, parentAppointment));
            }
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Listens users click on the specific item at the toolbar to submit the input.
     *
     * @return if user input is valid.
     */
    private boolean onSubmitListener() {
        return checkIfInputIsValid();
    }

    /**
     * Checks if all input fields are correctly filled from the user.
     *
     * @return if user input is valid.
     */
    public boolean checkIfInputIsValid() {

        boolean validInput = true;

        //Checking name field
        if (this.appointmentName.getText().toString().isEmpty()) {
            appointmentName.setError(getString(R.string.add_edit_appointmentview_error_set_valid_name));
            validInput = false;
        }

        // Checking date field
        if (this.appointmentDate.getText().toString().isEmpty()) {
            appointmentDate.setError(getString(R.string.add_edit_appointmentview_error_set_valid_date));
            validInput = false;
        } else if (this.appointmentDate.getText().toString().length() != 10) {
            appointmentDate.setError(getString(R.string.add_edit_appointmentview_error_setvalid_date_format));
            validInput = false;
        } else if (appointmentDate.getText().toString().charAt(2) != '.' ||
                appointmentDate.getText().toString().charAt(5) != '.') {
            appointmentDate.setError(getString(R.string.add_edit_appointmentview_error_setvalid_date_format));
            validInput = false;
        } else {

            //Check if input date exists
            String dateString = appointmentDate.getText().toString();
            int day = Integer.parseInt(dateString.substring(0, 2));
            int month = Integer.parseInt(dateString.substring(3, 5)) - 1;
            int year = Integer.parseInt(dateString.substring(6, 10));
            GregorianCalendar date = new GregorianCalendar();
            date.set(Calendar.DAY_OF_MONTH, day);
            date.set(Calendar.MONTH, month);
            date.set(Calendar.YEAR, year);
            if (date.get(Calendar.DAY_OF_MONTH) != day) {
                appointmentDate.setError(getString(R.string.add_edit_appointmentview_error_date_doesnt_exist));
                validInput = false;
            }
            if (month <= -1 || month > 11) {
                appointmentDate.setError(getString(R.string.add_edit_appointmentview_error_date_doesnt_exist));
                validInput = false;
            }
        }

        //Checking from time field
        boolean validTimeFrom = true;
        boolean validTimeTo = true;

        if (validTimeInput(appointmentTimeFrom, getString(R.string.add_edit_appointmentview_starttime))) {
            validTimeFrom = false;
            validInput = false;
        }
        //Checking to time field
        if (validTimeInput(appointmentTimeTo, getString(R.string.add_edit_appointmentview_endtime))) {
            validTimeTo = false;
            validInput = false;
        }
        if (validTimeFrom && validTimeTo) {

            if (!startTimeSmallerThanEndTime()) {
                validInput = false;
            }
        }


        //If everything is okay >> return true
        return validInput;

    }

    /**
     * sub-method of checkIfInputIsValid.
     * Checks if the start time is smaller than the end time of an appointment.
     *
     * @return true: start time is smaller than the end time.
     * false: if not.
     */
    private boolean startTimeSmallerThanEndTime() {
        int minutesStartTime = Integer.parseInt(appointmentTimeFrom.getText().toString().substring(3, 5));
        int hoursStartTime = Integer.parseInt(appointmentTimeFrom.getText().toString().substring(0, 2));
        int minutesEndTime = Integer.parseInt(appointmentTimeTo.getText().toString().substring(3, 5));
        int hoursEndTime = Integer.parseInt(appointmentTimeTo.getText().toString().substring(0, 2));

        if (hoursEndTime < hoursStartTime) {
            textViewToTimeError.setError(getString(R.string.add_edit_appointmentview_error_endtime_is_smaller));
            return false;
        } else if (hoursEndTime == hoursStartTime && minutesEndTime < minutesStartTime) {
            textViewToTimeError.setError(getString(R.string.add_edit_appointmentview_error_endtime_is_smaller));
            return false;
        }
        return true;
    }

    /**
     * sub-method of checkIfInputIsValid.
     * Checks if time inputs of different types are valid.
     *
     * @param time     is the given TextView that shall be checked.
     * @param timeType start time or end time to set specific error reports to the user.
     * @return if time input was valid or not.
     */
    private boolean validTimeInput(TextView time, String timeType) {

        if (time.getText().toString().isEmpty()) {
            String errormessage = timeType + " " + getString(R.string.add_edit_appointmentview_error_empty_field);
            time.setError(errormessage);
            return true;
        } else if (time.getText().toString().length() != 5) {
            time.setError(getString(R.string.add_edit_appointmentview_error_invalid_time_format));
            return true;
        } else {
            int hours = Integer.parseInt(time.getText().toString().substring(0, 2));
            int minutes = Integer.parseInt(time.getText().toString().substring(3, 5));
            if (hours > 24 || hours < 0 || minutes > 59 || minutes < 0) {
                String errormessage = timeType + " " + getString(R.string.add_edit_appointmentview_error_invalid_time);
                time.setError(errormessage);
                return true;
            }
        }
        return false;
    }

    /**
     * Fills all the TextViewElements etc. with the attributes of the parentAppointment.
     */
    private void fillView() {

        GregorianCalendar fromDate = parentAppointment.getFromDate();
        GregorianCalendar toDate = parentAppointment.getToDate();

        SimpleDateFormat fromDateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.forLanguageTag(String.valueOf(R.string.languageTag)));
        String fromDateTemp = fromDateFormat.format(fromDate.getTime());

        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm", Locale.forLanguageTag(String.valueOf(R.string.languageTag)));


        String fromTime = timeFormat.format(fromDate.getTime());
        String toTime = timeFormat.format(toDate.getTime());

        appointmentDate.setText(fromDateTemp);
        onDateClicked();

        appointmentTimeFrom.setText(fromTime);
        onTimeFromClicked();

        appointmentTimeTo.setText(toTime);
        onTimeToClicked();

        appointmentName.setText(parentAppointment.getTitle());
        appointmentLocation.setText(parentAppointment.getLocation());
        notificationSpinner.setSelection(parentAppointment.getNotificationType().getValue());
        appointmentDescription.setText(parentAppointment.getNote());

    }

    /**
     * Sets the type of notification, every time the user chooses a new item of the spinner (users
     * can choose if they want to have a reminder for their date using the notification spinner).
     *
     * @param parent   given parent.
     * @param view     given view.
     * @param position given position (on-clicked by user) of spinner element.
     * @param id       given id of spinner element.
     */
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (position){
            case 0: {
                notificationType = Notification.AppointmentNotificationType.NO_NOTIFICATION;
                break;
            }
            case 1: {
                notificationType = Notification.AppointmentNotificationType.FIVE_MINUTES;
                break;
            }
            case 2: {
                notificationType = Notification.AppointmentNotificationType.FIFTEEN_MINUTES;
                break;
            }
            case 3: {
                notificationType = Notification.AppointmentNotificationType.THIRTY_MINUTES;
                break;
            }
            case 4: {
                notificationType = Notification.AppointmentNotificationType.ONE_HOUR;
                break;
            }
            case 5: {
                notificationType = Notification.AppointmentNotificationType.TWO_HOURS;
                break;
            }
            case 6:{
                notificationType = Notification.AppointmentNotificationType.ONE_DAY;
                break;
            }
        }

    }

    /**
     * Processes all data and fill it into a new Object of Type Appointment.
     */
    private void getAppointment() {

        // Get date of the appointment
        String date = appointmentDate.getText().toString();
        int day = Integer.parseInt(date.substring(0, 2));
        int month = Integer.parseInt(date.substring(3, 5)) - 1;
        int year = Integer.parseInt(date.substring(6, 10));
        GregorianCalendar dateOfAppointment_from = new GregorianCalendar();
        dateOfAppointment_from.set(Calendar.DAY_OF_MONTH, day);
        dateOfAppointment_from.set(Calendar.MONTH, month);
        dateOfAppointment_from.set(Calendar.YEAR, year);

        //Get time of the appointment
        String fromTime = appointmentTimeFrom.getText().toString();
        String toTime = appointmentTimeTo.getText().toString();

        //from date
        setMinutesAndHours(dateOfAppointment_from, fromTime);

        //to date
        GregorianCalendar dateOfAppointment_to = new GregorianCalendar();
        dateOfAppointment_to.setTimeInMillis(dateOfAppointment_from.getTimeInMillis());
        setMinutesAndHours(dateOfAppointment_to, toTime);

        //Get reminder date
        GregorianCalendar reminderDateOfAppointment = new GregorianCalendar();
        reminderDateOfAppointment.setTimeInMillis(dateOfAppointment_from.getTimeInMillis());

        switch (notificationType){

            case FIVE_MINUTES:{
                reminderDateOfAppointment.add(Calendar.MINUTE,-5);
                break;
            }
            case FIFTEEN_MINUTES:{
                reminderDateOfAppointment.add(Calendar.MINUTE,-15);
                break;
            }
            case THIRTY_MINUTES:{
                reminderDateOfAppointment.add(Calendar.MINUTE,-30);
                break;
            }
            case ONE_HOUR:{
                reminderDateOfAppointment.add(Calendar.HOUR_OF_DAY,-1);
                break;
            }
            case TWO_HOURS:{
                reminderDateOfAppointment.add(Calendar.HOUR_OF_DAY,-2);
                break;
            }
            case ONE_DAY:{
                reminderDateOfAppointment.add(Calendar.MONTH,-1);
                break;
            }
        }

        parentAppointment.setTitle(appointmentName.getText().toString());
        parentAppointment.setNotificationType(notificationType);
        parentAppointment.setReminderDate(reminderDateOfAppointment);
        parentAppointment.setFromDate(dateOfAppointment_from);
        parentAppointment.setToDate(dateOfAppointment_to);
        parentAppointment.setLocation(appointmentLocation.getText().toString());
        parentAppointment.setNote(appointmentDescription.getText().toString());
    }

    private void setMinutesAndHours(GregorianCalendar cal, String time) {
        int hours = Integer.parseInt(time.substring(0, 2));
        int minutes = Integer.parseInt(time.substring(3, 5));

        cal.set(GregorianCalendar.HOUR_OF_DAY, hours);
        cal.set(GregorianCalendar.MINUTE, minutes);

    }

    /**
     * If the button delete is pressed, the appointment will be deleted in the database and redirects
     * to the CalendarViewActivity.
     */
    private void deleteAppointment() {

        Bundle dialogArguments = new Bundle();
        dialogArguments.putSerializable(ACTION_LISTENER_KEY, this);

        DialogFragment AppointmentDeleteDialog = DialogFragmentFactory.createInstance(AppointmentDeleteDialog.class, dialogArguments);
        AppointmentDeleteDialog.show(getSupportFragmentManager(), "EditCalendarViewActivity");

    }

    /**
     * Does quietly nothing - if nothing is selected :).
     *
     * @param parent given parent.
     */
    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onSubmitDialog() {

        AlarmManager alarmManager = ManagerHolder.getManager(AlarmManager.class);
        alarmManager.unregister(this, parentAppointment);

        appointmentRepository.delete(parentAppointment, data -> {});
        GregorianCalendar selectedDate = (GregorianCalendar) getIntent().getExtras().getSerializable("selectedDate");
        startActivity(new Intent(EditCalendarViewActivity.this, CalendarViewActivity.class).putExtra("class", this.getClass().toString()).putExtra("Appointment", parentAppointment).putExtra("selectedDate", selectedDate));
        finish();
    }
}