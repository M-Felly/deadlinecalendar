package de.hda.fbi.nzse.deadlinecalendar.manager;

import java.util.HashMap;
import java.util.Map;

/**
 * This class holds (manages) all manager as a singleton
 * <p>
 * The main purpose is to have a central manager provider and central singleton instance
 * that contains all manager
 *
 * @author Matthias Feyll
 */
public class ManagerHolder {
    /***************************************************************
     *                     Singleton pattern
     ***************************************************************/
    private static ManagerHolder managerHolder;
    private final Map<Class<? extends AbstractManager>, AbstractManager> managerMap;

    public ManagerHolder() {
        managerMap = new HashMap<>();
        setManager(AlarmManager.class);
        setManager(ServiceManager.class);
        setManager(NotificationManager.class);
    }

    /**
     * Main method to get a holding manager
     *
     * @param managerClass Actual manager class variable
     * @param <C>          Manager class type
     * @return a manager class coming from {@link AbstractManager}
     */
    public static <C extends AbstractManager> C getManager(Class<C> managerClass) {
        AbstractManager abstractManager = getInstance().getManagerMap().get(managerClass);

        if (abstractManager == null) {
            throw new Error("There is no existing manager class to: " + managerClass.toString());
        }

        return managerClass.cast(abstractManager);
    }

    private static ManagerHolder getInstance() {
        if (managerHolder == null) {
            managerHolder = new ManagerHolder();
        }

        return managerHolder;
    }

    /**
     * Adds a new manager
     *
     * @param managerClass a manager class that inherits from {@link AbstractManager}
     */
    private void setManager(Class<? extends AbstractManager> managerClass) {
        try {
            this.managerMap.put(managerClass, managerClass.newInstance());
        } catch (InstantiationException | IllegalAccessException e) {
            throw new Error();
        }
    }


    /***************************************************************
     *                        Helper classes
     ***************************************************************/

    public Map<Class<? extends AbstractManager>, AbstractManager> getManagerMap() {
        return managerMap;
    }

    public static abstract class AbstractManager {
    }
}
