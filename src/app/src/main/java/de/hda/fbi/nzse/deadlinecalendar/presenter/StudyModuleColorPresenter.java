package de.hda.fbi.nzse.deadlinecalendar.presenter;

import android.content.Context;

import de.hda.fbi.nzse.deadlinecalendar.model.StudyModuleColor;


/**
 * This class stands between the StudyModuleColor (model) and a Activity (view) as presenter.
 * It transforms the string value of StudyModuleColor enum to a simple int color value unidirectional.
 *
 * <h2>Usage</h2>
 * Call the class as follows:
 * <code>StudyModuleColorPresenter colorPresenter = new StudyModuleColorPresenter(this, StudyModuleColor.FIRST)</code>
 * <p>
 * To receive the int color value call the method getColor()
 *
 * @see StudyModuleColor
 */
public class StudyModuleColorPresenter {
    private StudyModuleColor studyModuleColor;
    private final Context context;


    public StudyModuleColorPresenter(Context context) {
        this.context = context;
    }

    /**
     * Gets the color resource by the attribute name tag in colors.xml
     *
     * @return color value in a int value format
     * @throws NullPointerException when no studyModuleColor is given. Pass it either on
     *                              constructor call or with the setter setStudyModuleColor(StudyModuleColor)
     */
    public final int getColor() throws NullPointerException {
        if (this.studyModuleColor == null) {
            throw new NullPointerException();
        }

        String packageName = this.context.getPackageName();
        int resourceID = this.context.getResources().getIdentifier(this.studyModuleColor.getAttributeNameTag(), "color", packageName);

        return this.context.getColor(resourceID);
    }

    public void setStudyModuleColor(StudyModuleColor studyModuleColor) {
        this.studyModuleColor = studyModuleColor;
    }
}
