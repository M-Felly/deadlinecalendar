package de.hda.fbi.nzse.deadlinecalendar.model;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * This class represents a specific date that contains a bunch of appointments and deadlines.
 * It is necessary for CalendarElementAdapter class to view all elements on the chosen date in
 * the recycler view of the CalendarViewActivity.
 */
public class AppointmentDeadlineDate {


    private final GregorianCalendar date;
    private final List<Deadline> deadlineList = new ArrayList<>();
    private final List<Appointment> appointmentList = new ArrayList<>();

    /**
     * This Method create this class for one date and add a Deadline to the deadlinelist for this date
     * @param deadline one deadline for one date
     */
    public AppointmentDeadlineDate(Deadline deadline){
        date = deadline.getSubmissionDate();
        deadlineList.add(deadline);
    }

    /**
     * This Method create this class for one date and add an Appointment to the appointmentList for this date
     * @param appointment one appointment for one date
     */
    public AppointmentDeadlineDate(Appointment appointment){
        date = appointment.getFromDate();
        appointmentList.add(appointment);
    }

    public List<Deadline> getDeadlineList() {
        return deadlineList;
    }

    public List<Appointment> getAppointmentList() {
        return appointmentList;
    }

    public GregorianCalendar getDate() {
        return date;
    }

    /**
     * This Method add a Deadline to the deadlinelist for one date
     * @param deadline one deadline for one date
     */
    public void addDeadline(Deadline deadline){
        deadlineList.add(deadline);
    }

    /**
     * This Method add a Deadline to the appointment for one date
     * @param appointment one appointment for one date
     */
    public void addAppointment(Appointment appointment){
        appointmentList.add(appointment);
    }

}
