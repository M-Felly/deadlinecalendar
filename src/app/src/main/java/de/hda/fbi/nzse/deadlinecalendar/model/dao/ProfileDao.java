package de.hda.fbi.nzse.deadlinecalendar.model.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import de.hda.fbi.nzse.deadlinecalendar.model.Profile;

@Dao
public interface ProfileDao {
    @Insert
    long insert(Profile profile);

    @Update
    int update(Profile profile);

    @Query("SELECT * FROM profile LIMIT 1")
    Profile getFirst();
}
