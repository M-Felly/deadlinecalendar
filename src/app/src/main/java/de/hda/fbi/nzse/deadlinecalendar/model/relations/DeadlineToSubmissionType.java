package de.hda.fbi.nzse.deadlinecalendar.model.relations;

import androidx.room.Embedded;
import androidx.room.Ignore;
import androidx.room.Relation;

import de.hda.fbi.nzse.deadlinecalendar.model.Deadline;
import de.hda.fbi.nzse.deadlinecalendar.model.SubmissionType;

/**
 * One deadline to one submission type relation class
 *
 * @author Anna Kilb
 */
public class DeadlineToSubmissionType {
    public DeadlineToSubmissionType() {}
    @Ignore
    public DeadlineToSubmissionType(Deadline deadline, SubmissionType submissionType) {
        this.deadline = deadline;
        this.submissionType = submissionType;
    }

    @Embedded
    public Deadline deadline;

    @Relation(
            parentColumn = "relatedSubmissionTypeID",
            entityColumn = "submissionTypeID"
    )
    public SubmissionType submissionType;
}
