package de.hda.fbi.nzse.deadlinecalendar.model.dao;

import androidx.room.Dao;
import androidx.room.Query;
import androidx.room.Transaction;

import java.util.List;

import de.hda.fbi.nzse.deadlinecalendar.model.relations.DeadlineToStudyModule;

@Dao
public interface DeadlineToStudyModuleDao {

    @Transaction
    @Query("SELECT * FROM Deadline")
    List<DeadlineToStudyModule> getDeadlinesToStudyModule();
}
