package de.hda.fbi.nzse.deadlinecalendar;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CalendarView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import de.hda.fbi.nzse.deadlinecalendar.adapter.CalendarElementsAdapter;
import de.hda.fbi.nzse.deadlinecalendar.adapter.CalendarElementsAdapterInterface;
import de.hda.fbi.nzse.deadlinecalendar.model.Appointment;
import de.hda.fbi.nzse.deadlinecalendar.model.AppointmentDeadlineDate;
import de.hda.fbi.nzse.deadlinecalendar.model.Deadline;
import de.hda.fbi.nzse.deadlinecalendar.model.repository.AppointmentRepository;
import de.hda.fbi.nzse.deadlinecalendar.model.repository.DeadlineRepository;
import de.hda.fbi.nzse.deadlinecalendar.presenter.DeadlineAccessHandler;

import static de.hda.fbi.nzse.deadlinecalendar.manager.NotificationManager.NOTIFICATION_APPOINTMENT_INTENT;
import static de.hda.fbi.nzse.deadlinecalendar.manager.NotificationManager.NOTIFICATION_DEADLINE_INTENT;

/**
 * In this activity, the user is able to see his appointments and deadlines, depending on the date
 * he chooses via tap on the specific calendar date.
 */
public class CalendarViewActivity extends AppCompatActivity implements CalendarElementsAdapterInterface,Serializable {

    private BottomNavigationView bottomNav;
    private TextView textViewSelectedDate;
    private GregorianCalendar selectedDate;
    private List<Appointment> appointments = new ArrayList<>();
    private List<Deadline> deadlines = new ArrayList<>();
    private CalendarElementsAdapter calendarElementsAdapter;
    private final List<AppointmentDeadlineDate> appointmentDeadlineDateList = new ArrayList<>();
    private CalendarView calendarView;



    /**
     * On creating the activity, following actions have to be done that the activity works correctly.
     *
     * @param savedInstanceState given saved instance
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        appointments.clear();
        deadlines.clear();
        appointmentDeadlineDateList.clear();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calenderview);
        setTitle(R.string.title_calender);

        calendarView = findViewById(R.id.calendarViewActivity_calendarView_calendar);
        calendarView.setFirstDayOfWeek(Calendar.MONDAY);

        selectedDate = new GregorianCalendar();

        textViewSelectedDate = findViewById(R.id.calendarViewActivity_textView_showSelectedDate);
        textViewSelectedDate.setText(getCurrentDate());

        getElementsFromDatabase();
        fillAppointmentDeadlineDateList();
        showRightDate();

        //Recycler View
        DeadlineAccessHandler deadlineAccessHandler = new DeadlineAccessHandler(this);
        RecyclerView recyclerViewAppointments = findViewById(R.id.calendarViewActivity_recyclerView_calendarElements);

        calendarElementsAdapter = new CalendarElementsAdapter(selectedDate, appointmentDeadlineDateList , deadlineAccessHandler,this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerViewAppointments.setLayoutManager(linearLayoutManager);
        recyclerViewAppointments.setAdapter(calendarElementsAdapter);

        bottomNav = findViewById(R.id.calendarViewActivity_bottomNavigationView_bottomNav);
        bottomNav.setOnNavigationItemSelectedListener(new BottomNavigation(this));

        FloatingActionButton addAppointmentBtn = findViewById(R.id.calendarViewActivity_floatingBtn_addAppointment);
        addAppointmentBtn.setOnClickListener(this::onFloatingButtonClicked);


        //if one Date is selected the method showList is called and handed over are the variables year, month and year as an int
        calendarView.setOnDateChangeListener((calendarView, year, month, dayOfMonth) -> {
            selectedDate.set(year, month, dayOfMonth);
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.forLanguageTag(String.valueOf(R.string.languageTag)));
            String dateTemp = dateFormat.format(selectedDate.getTime());
            textViewSelectedDate.setText(dateTemp);
            calendarElementsAdapter.fillCalendarElementsList(dateTemp);
            calendarElementsAdapter.notifyDataSetChanged();
        });

    }

    /**
     * Inflates the toolbar and according items.
     *
     * @param menu is the given menu via parameter.
     * @return true if creating was successfull, returns false if not
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.calendar_today_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * If clicked on calendar icon, the date of the CalendarView will be set to the current date.
     * @param item represents the chosen menu item.
     * @return true if creating was successfull, returns false if not
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.calendarview_show_today) {
            calendarView.setDate(Calendar.getInstance().getTimeInMillis(), false, true);
            textViewSelectedDate.setText(getCurrentDate());
            calendarElementsAdapter.fillCalendarElementsList(getCurrentDate());
            calendarElementsAdapter.notifyDataSetChanged();
            selectedDate.setTimeInMillis(Calendar.getInstance().getTimeInMillis());
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Save Database records in Lists
     */
    private void getElementsFromDatabase() {
        //Getting appointment elements of database
        AppointmentRepository appointmentRepository = new AppointmentRepository(this);
        appointmentRepository.getAppointments(appointments -> this.appointments = appointments);

        //Getting deadline elements of database
        DeadlineRepository deadlineRepository = new DeadlineRepository(this);
        deadlineRepository.getDeadlines(deadlines -> this.deadlines = deadlines);

        // Repository
        appointmentRepository = new AppointmentRepository(this);
        appointmentRepository.getAppointments(appointmentList -> this.appointments = appointmentList);

    }

    /**
     * Method set selected Date to the date who was selected before the details was shown
     */
    private void showRightDate(){
        Bundle allExtra = getIntent().getExtras();

        if(allExtra.getString("class").equals(DetailCalendarViewActivity.class.toString())){
            selectedDate = (GregorianCalendar) allExtra.getSerializable("selectedDate");
        }
        else if(allExtra.getString("class").equals(DetailDeadlineViewActivity.class.toString())){
            selectedDate = (GregorianCalendar) allExtra.getSerializable("selectedDate");
        }
        else if(allExtra.getString("class").equals(EditCalendarViewActivity.class.toString())){
            selectedDate = (GregorianCalendar) allExtra.getSerializable("selectedDate");
        }

        calendarView.setDate(selectedDate.getTimeInMillis());
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.forLanguageTag(String.valueOf(R.string.languageTag)));
        String dateTemp = dateFormat.format(selectedDate.getTime());
        textViewSelectedDate.setText(dateTemp);
    }


    /**
     * In this method, all appointments and deadlines will be assigned to a specific AppointmentDeadlineDate.
     * An AppointmentDeadlineDate-Object represents a specific date. All appointments and deadlines
     * with the same date will be sorted to the according AppointmentDeadlineDate.
     */
    private void fillAppointmentDeadlineDateList(){
        // use only deadlines who isn't finished or done
        for(int i=deadlines.size()-1; i >=0  ; i--){
            if(deadlines.get(i).isFinished() || deadlines.get(i).isDone()){
                deadlines.remove(i);
            }
        }

        fillAppointmentDeadlineDateListWithAppointments();
        fillAppointmentDeadlineDateListWithDeadlines();
    }

    /**
     * In this method, all appointments will be assigned to a specific AppointmentDeadlineDate.
     * An AppointmentDeadlineDate-Object represents a specific date. All appointments
     * with the same date will be sorted to the according AppointmentDeadlineDate.
     */
    private void fillAppointmentDeadlineDateListWithAppointments(){
        for(int counterAppointments = 0; counterAppointments < appointments.size(); counterAppointments++) {
            for (int counterAppointmentDeadline = 0; counterAppointmentDeadline <= appointmentDeadlineDateList.size(); counterAppointmentDeadline++) {
                if(counterAppointmentDeadline == appointmentDeadlineDateList.size()) {
                    appointmentDeadlineDateList.add(new AppointmentDeadlineDate(appointments.get(counterAppointments)));
                    break;
                }
                else{
                    SimpleDateFormat fromDateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.forLanguageTag(String.valueOf(R.string.languageTag)));
                    String appointmentDeadlineDate = fromDateFormat.format(appointmentDeadlineDateList.get(counterAppointmentDeadline).getDate().getTime());
                    String appointmentDate = fromDateFormat.format(appointments.get(counterAppointments).getFromDate().getTime());
                    if (appointmentDeadlineDate.equals(appointmentDate)) {
                        appointmentDeadlineDateList.get(counterAppointmentDeadline).addAppointment(appointments.get(counterAppointments));
                        break;
                    }
                }
            }
        }
    }


    /**
     * In this method, all deadlines will be assigned to a specific AppointmentDeadlineDate.
     * An AppointmentDeadlineDate-Object represents a specific date. All deadlines
     * with the same date will be sorted to the according AppointmentDeadlineDate.
     */
    private void fillAppointmentDeadlineDateListWithDeadlines(){
        for(int counterDeadlines = 0; counterDeadlines < deadlines.size(); counterDeadlines++){
            for(int counterAppointmentDeadline = 0; counterAppointmentDeadline <= appointmentDeadlineDateList.size(); counterAppointmentDeadline++){

                if(counterAppointmentDeadline == appointmentDeadlineDateList.size()){
                    appointmentDeadlineDateList.add(new AppointmentDeadlineDate(deadlines.get(counterDeadlines)));
                    break;
                }
                else{
                    SimpleDateFormat fromDateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.forLanguageTag(String.valueOf(R.string.languageTag)));
                    String appointmentDeadlineDate = fromDateFormat.format(appointmentDeadlineDateList.get(counterAppointmentDeadline).getDate().getTime());
                    String deadlineDate = fromDateFormat.format(deadlines.get(counterDeadlines).getSubmissionDate().getTime());
                    if(appointmentDeadlineDate.equals(deadlineDate)){
                        appointmentDeadlineDateList.get(counterAppointmentDeadline).addDeadline(deadlines.get(counterDeadlines));
                        break;
                    }
                }
            }
        }
    }

    /**
     * Returns the current date.
     *
     * @return current date in String format
     */
    private String getCurrentDate() {
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.forLanguageTag(String.valueOf(R.string.languageTag)));
        GregorianCalendar date = new GregorianCalendar();
        return dateFormat.format(date.getTime());
    }

    /**
     * Starts activity AddAppointmentViewActivity
     */
    private void onFloatingButtonClicked(View v) {
        Intent addAppointmentIntent = new Intent(this, AddAppointmentViewActivity.class).putExtra("selectedDate", selectedDate);
        startActivity(addAppointmentIntent);
    }


    /**
     * Checks the correct icon in bottom navigation bar.
     */
    @Override
    protected void onResume() {
        super.onResume();
        appointmentDeadlineDateList.clear();
        appointments.clear();
        deadlines.clear();
        getElementsFromDatabase();
        fillAppointmentDeadlineDateList();
        calendarElementsAdapter.refillCalendarElementsList(selectedDate, appointmentDeadlineDateList);
        bottomNav.getMenu().findItem(R.id.bottom_nav_calendar).setChecked(true);
    }


    /**
     * Starts DetailDeadlineViewActivity, if an element of type Deadline is selected.
     * For getting the specific element, the method returns from appointmentDeadlineDateList (on
     * position positionList) in the deadlineList (on position positionElement) the Deadline-object.
     *
     * @param positionElement position of element in the deadline list of a specific AppointmentDeadlineDate.
     * @param positionList position of the specific object of type AppointmentDeadlineDate in the
     *                     AppointmentDeadlineDate-List.
     */
    @Override
    public void onItemClickDeadline(int positionElement, int positionList) {
        int deadlineOnPosition = calendarElementsAdapter.returnDeadlineOnPosition(positionElement);
        Deadline deadline = appointmentDeadlineDateList.get(positionList).getDeadlineList().get(deadlineOnPosition);
        Intent intent = new Intent(this, DetailDeadlineViewActivity.class).putExtra("class", this.getClass().toString()).putExtra("selectedDate", selectedDate).putExtra(NOTIFICATION_DEADLINE_INTENT,deadline);
        this.startActivity(intent);
    }

    /**
     * Starts DetailAppointmentViewActivity, if an element of type Appointment is selected.
     * For getting the specific element, the method returns from appointmentDeadlineDateList (on
     * position positionList) in the appointmentList (on position positionElement) the Appointment-object.
     *
     * @param positionElement position of element in the appointment list of a specific AppointmentDeadlineDate.
     * @param positionList position of the specific object of type AppointmentDeadlineDate in the
     *                     AppointmentDeadlineDate-List.
     */
    @Override
    public void onItemClickAppointment(int positionElement, int positionList) {
        int appointmentOnPosition = calendarElementsAdapter.returnAppointmentOnPosition(positionElement);
        Appointment appointment =  appointmentDeadlineDateList.get(positionList).getAppointmentList().get(appointmentOnPosition);
        startActivity(new Intent(CalendarViewActivity.this, DetailCalendarViewActivity.class).putExtra("class", this.getClass().toString()).putExtra("selectedDate", selectedDate).putExtra(NOTIFICATION_APPOINTMENT_INTENT, appointment));
    }
}