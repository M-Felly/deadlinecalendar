package de.hda.fbi.nzse.deadlinecalendar.view_elements.custom;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;

import androidx.annotation.NonNull;

import de.hda.fbi.nzse.deadlinecalendar.model.StudyModule;
import de.hda.fbi.nzse.deadlinecalendar.presenter.StudyModuleColorPresenter;

/**
 * This shape creates a ellipse with a inner text.
 * <p>
 * It displays the tag attribute of {@link StudyModule}
 *
 * <h2>Usage</h2>
 * 1. Go to your layout designer and select 'Project' in  Palette. Drag and drop the StudyModuleShape into your layout.
 * 2. Initialize a StudyModuleShape object in your corresponding activity / fragment / where ever you inflate this shape
 * 3. Call the setStudyModule(StudyModule) method and pass the StudyModule to be displayed
 *
 * <h2>Behaviour</h2>
 * The shape has a width of 50dp (see in styles), but is also responsive in width.
 * The height is fixed at all. So if you consider to give the shape a percentage width be aware of a stretched draw ellipse
 * <p>
 * Unless the method setStudyModule(StudyModule) is not called the StudyModuleShape will display nothing but keep the 50dp width
 *
 * @author Matthias Feyll
 */
public class StudyModuleShape extends View {
    /**
     * Fix ellipse height in one direction so the ellipse has a height of 30 * 2 = 60px
     */
    public static final int ELLIPSE_HEIGHT = 30;
    public static final int TEXt_SIZE = 15;

    private StudyModule studyModule;
    private final Paint badgePaint;
    private final Paint ellipsePaint;
    private final StudyModuleColorPresenter colorPresenter;

    public StudyModuleShape(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);

        this.badgePaint = new Paint();
        this.ellipsePaint = new Paint();

        this.colorPresenter = new StudyModuleColorPresenter(context);
    }

    /**
     * Call this method to set the StudyModule to be displayed.
     * As long as there is no studyModule set the shape is invisible but keeps his width
     *
     * @param studyModule to be displayed
     * @see StudyModuleShape
     */
    public void setStudyModule(@NonNull StudyModule studyModule) {
        this.studyModule = studyModule;
        this.colorPresenter.setStudyModuleColor(studyModule.getColor());
    }

    /**
     * Draw the ellipse
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (studyModule == null) {
            return;
        }

        float x = getWidth();
        float y_mid = getHeight() / 2.0f;

        // calculate height for ellipse
        float top = y_mid - ELLIPSE_HEIGHT;
        float bottom = y_mid + ELLIPSE_HEIGHT;


        // draw background
        this.badgePaint.setStyle(Paint.Style.FILL);
        this.badgePaint.setColor(this.colorPresenter.getColor());
        canvas.drawOval(0, top, x, bottom, this.badgePaint);

        // draw border
        this.badgePaint.setStyle(Paint.Style.STROKE);
        this.badgePaint.setColor(Color.BLACK);
        canvas.drawOval(0, top, x, bottom, this.badgePaint);

        // draw text
        this.ellipsePaint.setTextSize(calculatePX());
        this.ellipsePaint.setColor(Color.BLACK);
        this.ellipsePaint.setFakeBoldText(true);

        float textWidth = this.ellipsePaint.measureText(this.studyModule.getTag());
        canvas.drawText(this.studyModule.getTag(), (x - textWidth) / 2, y_mid + ELLIPSE_HEIGHT / 2.0f, this.ellipsePaint);
    }

    private int calculatePX() {
        return (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                StudyModuleShape.TEXt_SIZE,
                getResources().getDisplayMetrics()
        );
    }
}
