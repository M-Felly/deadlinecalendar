package de.hda.fbi.nzse.deadlinecalendar.model.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import de.hda.fbi.nzse.deadlinecalendar.model.Settings;

@Dao
public interface SettingsDao {
    @Query("SELECT * FROM settings LIMIT 1")
    Settings getFirst();

    @Insert
    long insert(Settings settings);

    @Update
    int update(Settings settings);
}
