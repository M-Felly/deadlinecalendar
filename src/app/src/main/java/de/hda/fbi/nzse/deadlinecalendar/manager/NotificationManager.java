package de.hda.fbi.nzse.deadlinecalendar.manager;

import android.app.NotificationChannel;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.StringDef;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import de.hda.fbi.nzse.deadlinecalendar.DetailCalendarViewActivity;
import de.hda.fbi.nzse.deadlinecalendar.DetailDeadlineViewActivity;
import de.hda.fbi.nzse.deadlinecalendar.R;
import de.hda.fbi.nzse.deadlinecalendar.model.Deadline;
import de.hda.fbi.nzse.deadlinecalendar.model.typeconverters.RemainingTimeConverter;
import de.hda.fbi.nzse.deadlinecalendar.receiver.NotificationReceiver;

import static de.hda.fbi.nzse.deadlinecalendar.receiver.NotificationReceiver.BUNDLE_EXTRA;

/**
 * Annotation class for intent key assortment
 */
@StringDef(value = {
        NotificationManager.NOTIFICATION_DEADLINE_INTENT,
        NotificationManager.NOTIFICATION_APPOINTMENT_INTENT,
        NotificationManager.CLASS_INTENT,
        NotificationManager.TARGET_DATE_INTENT
})
@Retention(RetentionPolicy.SOURCE)
@interface INTENT_EXTRA_IDENTIFIER {
}

/**
 * When a alarm gets fired this class creates a notification in its inner {@link Builder} class
 * and displays it.
 * <p>
 * The notification building depends on which whether the notifiable object is a Deadline or an
 * Appointment
 *
 * @author Matthias Feyll
 */
public class NotificationManager extends ManagerHolder.AbstractManager {
    public static final String NOTIFICATION_CHANNEL_ID = "notification_1";
    public static final String NOTIFICATION_CHANNEL_NAME = "notification_channel_1";

    public static final String NOTIFICATION_DEADLINE_INTENT = "Deadline";
    public static final String NOTIFICATION_APPOINTMENT_INTENT = "Appointment";
    public static final String CLASS_INTENT = "class";
    public static final String TARGET_DATE_INTENT = "selectedDate";

    private static int notificationID = 0;

    public void init(Context context) {
        this.createNotificationChannel(context);
    }

    public void notify(Context context, NotificationCompat.Builder notification, NotificationReceiver.Notifiable notifiable) {
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.notify(notificationID++, notification.build());

        AlarmManager alarmManager = ManagerHolder.getManager(AlarmManager.class);
        alarmManager.unregister(context, notifiable);
    }


    /* **************************************************************
     *                        Helper methods
     ***************************************************************/

    /**
     * Create a new notification channel and registers it to the android notification manager
     *
     * @param context context
     */
    private void createNotificationChannel(@NonNull Context context) {
        NotificationChannel channel = new NotificationChannel(
                NOTIFICATION_CHANNEL_ID,
                NOTIFICATION_CHANNEL_NAME,
                android.app.NotificationManager.IMPORTANCE_DEFAULT
        );

        android.app.NotificationManager notificationManager = context.getSystemService(android.app.NotificationManager.class);
        notificationManager.createNotificationChannel(channel);
    }


    /* **************************************************************
     *                           Builder
     ***************************************************************/

    /**
     * This class is a builder and holds the blueprint for a notification
     */
    public static class Builder {
        public static NotificationCompat.Builder createNotification(Context context, NotificationReceiver.Notifiable notifiable) {
            Intent notificationIntent = createIntent(context, notifiable);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, notificationIntent, 0);

            String title, text;

            if (notifiable instanceof Deadline) {
                title = context.getString(R.string.notification_deadline_reminder_title) + ": " + notifiable.getTitle();
                text = RemainingTimeConverter.assembleOutputString(RemainingTimeConverter.parseRemainingTime(context, ((Deadline) notifiable)));
            } else {
                title = context.getString(R.string.notification_appointment_reminder_title) + ": " + notifiable.getTitle();
                text = calendarToString(notifiable.getTargetDate());
            }


            return new NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID)
                    .setSmallIcon(R.drawable.logo_transparent)
                    .setContentTitle(title)
                    .setContentText(text)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)

                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true);
        }

        /**
         * Creates a intent depending on the actual notifiable class
         *
         * @param context    context
         * @param notifiable object the user will be forwarded to by clicking the notification message
         * @return Appendable intent to a notification
         */
        private static Intent createIntent(Context context, NotificationReceiver.Notifiable notifiable) {

            Class<? extends AppCompatActivity> targetActivityClass;
            @INTENT_EXTRA_IDENTIFIER String intentIdentifier;

            if (notifiable instanceof Deadline) {
                targetActivityClass = DetailDeadlineViewActivity.class;
                intentIdentifier = NOTIFICATION_DEADLINE_INTENT;
            } else {
                targetActivityClass = DetailCalendarViewActivity.class;
                intentIdentifier = NOTIFICATION_APPOINTMENT_INTENT;
            }


            Bundle bundle = new Bundle();
            bundle.putSerializable(intentIdentifier, notifiable);

            Intent intent = new Intent(context, targetActivityClass);
            intent.putExtra(BUNDLE_EXTRA, bundle);
            intent.putExtra(CLASS_INTENT, Object.class);
            intent.putExtra(TARGET_DATE_INTENT, notifiable.getTargetDate());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            return intent;
        }

        /**
         * Little presenter method to format the remaining time
         *
         * @param calendar that will be formatted
         * @return formatted output string
         */
        private static String calendarToString(Calendar calendar) {
            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm", Locale.forLanguageTag(String.valueOf(R.string.languageTag)));

            String fromTime = timeFormat.format(calendar.getTime());

            return fromTime + " Uhr";
        }
    }
}
