package de.hda.fbi.nzse.deadlinecalendar.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.io.Serializable;

import de.hda.fbi.nzse.deadlinecalendar.ProfileViewActivity;

/**
 * Profile that represents the User and its data.
 * This is getting used by the {@link ProfileViewActivity}
 */
@Entity
public class Profile implements Serializable {

    @PrimaryKey(autoGenerate = true)
    public long profileID;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "matriculation_number")
    private String matriculationNumber;

    @ColumnInfo(name = "university_name")
    private String universityName;

    @ColumnInfo(name = "study_level")
    private String studyLevel;

    @ColumnInfo(name = "department_level")
    private String departmentName;


    /**
     * Actual constructor after successfully finishing the introduction process
     * <p>
     * All parameter belongs to the users input
     */
    @Ignore
    public Profile(String name, String matriculationNumber, String universityName, String studyLevel, String departmentName) {
        this.name = name;
        this.matriculationNumber = matriculationNumber;
        this.universityName = universityName;
        this.studyLevel = studyLevel;
        this.departmentName = departmentName;
    }

    public Profile() {}


    /***************************************************************
     *                      Getter / Setter
     ***************************************************************/

    public String getName() {
        return name;
    }

    public String getMatriculationNumber() {
        return matriculationNumber;
    }

    public String getUniversityName() {
        return universityName;
    }

    public String getStudyLevel() {
        return studyLevel;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMatriculationNumber(String matriculationNumber) {
        this.matriculationNumber = matriculationNumber;
    }

    public void setUniversityName(String universityName) {
        this.universityName = universityName;
    }

    public void setStudyLevel(String studyLevel) {
        this.studyLevel = studyLevel;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }
}
