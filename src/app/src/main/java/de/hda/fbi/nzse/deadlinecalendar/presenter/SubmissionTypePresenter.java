package de.hda.fbi.nzse.deadlinecalendar.presenter;

import android.content.Context;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hda.fbi.nzse.deadlinecalendar.model.SubmissionType;

/**
 * This class stands between the SubmissionType (model) and a Activity (view) as presenter.
 * It transforms the complex SubmissionType class to a simple String bidirectionally.
 *
 *
 * <h2>Usage</h2>
 * Call the class as follows:
 * <code>SubmissionTypePresenter submissionType = new SubmissionTypePresenter(this, new SubmissionType())</code>
 * <p>
 * Call following methods to handle this class properly:
 * - getSubmissionType()            to get the SubmissionType as String
 * - setSubmissionType(String)      to set the SubmissionType as String
 * - getPredefinedSubmissionTypes() to get all predefined SubmissionTypes as List<String> to insert them into the dropdown list
 *
 *
 * <h2>Behaviour</h2>
 * The class distinguishes between the predefined enum strings and custom strings by users input.
 * On set, the class searches if the string matches to a predefined enum. If not save it as custom text
 * On get, the class firstly checks whether a custom text is set and if so returning it. Otherwise
 * returns the parsed predefined enum value.
 */
public class SubmissionTypePresenter {
    private final Context context;
    private final Map<String, SubmissionType.Predefined> predefinedSubmissionTypesMap;
    private final SubmissionType submissionType;

    /**
     * Initiate presenter class
     *
     * @param context        from the calling activity
     * @param submissionType submissionType that is getting wrapped
     */
    public SubmissionTypePresenter(Context context, SubmissionType submissionType) {
        this.context = context;
        this.predefinedSubmissionTypesMap = this.resolveSubmissionTypes();
        this.submissionType = submissionType;
    }


    /* **************************************************************
     *                      Public methods
     ****************************************************************/

    /**
     * Get SubmissionType as String. This is either a parsed predefined SubmissionType
     * or a custom text inserted by the user
     *
     * @return SubmissionType as String
     */
    public String getSubmissionType() {
        String customText = this.submissionType.getCustomText();

        if (customText != null) {
            return customText;
        }

        return this.getStringResourceByName(this.submissionType.getPredefinedType().toString());
    }

    /**
     * Call this method to get all predefined submission types
     *
     * @return String list with all predefined submission types
     * @see SubmissionType.Predefined
     */
    public List<String> getPredefinedSubmissionTypes() {
        return new ArrayList<>(predefinedSubmissionTypesMap.keySet());
    }


    /* **************************************************************
     *                      Private methods
     ****************************************************************/

    /**
     * Creates a new SubmissionTypes map from all Predefined SubmissionTypes
     *
     * @see SubmissionType.Predefined
     */
    private Map<String, SubmissionType.Predefined> resolveSubmissionTypes() {
        Map<String, SubmissionType.Predefined> newSubmissionTypeMap = new HashMap<>();

        for (SubmissionType.Predefined predefined : SubmissionType.Predefined.values()) {
            newSubmissionTypeMap.put(
                    this.getStringResourceByName(predefined.toString()),
                    predefined
            );
        }

        return newSubmissionTypeMap;
    }

    /**
     * Gets the string resource by the attribute name tag
     *
     * @param nameAttribute of the perspective string value in strings.xml
     * @return the string value
     */
    public String getStringResourceByName(String nameAttribute) {
        String packageName = this.context.getPackageName();
        int resourceID = this.context.getResources().getIdentifier(nameAttribute, "string", packageName);

        return this.context.getString(resourceID);
    }
}
