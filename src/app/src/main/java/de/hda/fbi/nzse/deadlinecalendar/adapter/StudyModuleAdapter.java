package de.hda.fbi.nzse.deadlinecalendar.adapter;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.concurrent.ExecutionException;

import de.hda.fbi.nzse.deadlinecalendar.R;
import de.hda.fbi.nzse.deadlinecalendar.dialogs.ProfileDeleteStudyModuleDialog;
import de.hda.fbi.nzse.deadlinecalendar.factory.DialogFragmentFactory;
import de.hda.fbi.nzse.deadlinecalendar.model.StudyModule;
import de.hda.fbi.nzse.deadlinecalendar.model.relations.StudyModuleWithDeadlines;
import de.hda.fbi.nzse.deadlinecalendar.model.repository.StudyModuleRepository;
import de.hda.fbi.nzse.deadlinecalendar.view_elements.custom.StudyModuleShape;

import static de.hda.fbi.nzse.deadlinecalendar.dialogs.ProfileDeleteStudyModuleDialog.ACTION_LISTENER_KEY;

/**
 * Communication interface with inner ViewHolder
 */
interface StudyModuleViewHolderListener {
    void onRemoveIconClicked(final int position);
}

/**
 * With this adapter you can display a list of {@link StudyModule}. In this list study modules can be
 * deleted with a dependency check for deadlines
 * Calling the method {@link #setStudyModuleList(List)} pass the list to be displayed
 *
 * @author Matthias Feyll
 */
public class StudyModuleAdapter extends RecyclerView.Adapter<StudyModuleAdapter.StudyModuleViewHolder> implements StudyModuleViewHolderListener {

    public static final String TAG = "StudyModuleRepository";

    private List<StudyModule> studyModuleList;
    private final StudyModuleRepository studyModuleRepository;
    private final FragmentManager fragmentManager;


    public StudyModuleAdapter(Activity activity) {
        this.studyModuleRepository = new StudyModuleRepository(activity);
        this.fragmentManager = ((FragmentActivity) activity).getSupportFragmentManager();
    }

    public static class StudyModuleViewHolder extends RecyclerView.ViewHolder {
        private final StudyModuleShape studyModuleShape;
        private final TextView nameTextView;

        public StudyModuleViewHolder(View view, StudyModuleViewHolderListener studyModuleAdapterListener) {
            super(view);

            this.studyModuleShape = view.findViewById(R.id.studyModuleAdapter_tagShape_tag);
            this.nameTextView = view.findViewById(R.id.studyModuleAdapter_textView_name);


            ImageButton removeBtn = view.findViewById(R.id.studyModuleAdapter_imageButton_remove);
            removeBtn.setOnClickListener(v -> studyModuleAdapterListener.onRemoveIconClicked(getAdapterPosition()));
        }

        public StudyModuleShape getStudyModuleShape() {
            return studyModuleShape;
        }

        public TextView getNameTextView() {
            return nameTextView;
        }
    }

    /* **************************************************************
     *                       Action / Listener
     ***************************************************************/

    /**
     * The method checks whether there are existing deadlines to this studyModule and if so opens the
     * {@link ProfileDeleteStudyModuleDialog}
     *
     * @param position clicked position in list
     */
    @Override
    public void onRemoveIconClicked(final int position) {
        try {
            StudyModuleWithDeadlines studyModuleWithDeadlines = studyModuleRepository.getDeadlinesToStudyModule(this.studyModuleList.get(position)).get();
            if (!studyModuleWithDeadlines.deadlineList.isEmpty()) {
                this.openConfirmDeleteModal(position);
                return;
            }

        } catch (ExecutionException | InterruptedException exception) {
            Log.e(TAG, "A critical error occurred during fetching the database: " + exception.getMessage());
            return;
        }

        this.removeStudyModule(position);
    }

    /**
     * Opens the {@link ProfileDeleteStudyModuleDialog} by creating a proper DialogFragment with the {@link DialogFragmentFactory}
     *
     * @param position clicked position in list
     */
    private void openConfirmDeleteModal(final int position) {
        Bundle dialogBundle = new Bundle();
        dialogBundle.putSerializable(ACTION_LISTENER_KEY, (ProfileDeleteStudyModuleDialog.OnClickListener) () -> removeStudyModule(position));

        DialogFragment profileDeleteStudyModuleDialog = DialogFragmentFactory.createInstance(ProfileDeleteStudyModuleDialog.class, dialogBundle);
        profileDeleteStudyModuleDialog.show(this.fragmentManager, TAG);
    }


    /* **************************************************************
     *                         Load / Save
     ***************************************************************/

    /**
     * Removes the study module out of the database
     * @param position clicked position in list
     */
    private void removeStudyModule(final int position) {
        this.studyModuleRepository.delete(this.studyModuleList.get(position), id -> {
            this.studyModuleList.remove(position);
            notifyItemRemoved(position);
        });
    }

    /* **************************************************************
     *                            Adapter
     ****************************************************************/

    @NonNull
    @Override
    public StudyModuleViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_study_module, viewGroup, false);
        return new StudyModuleViewHolder(view, this);
    }

    @Override
    public void onBindViewHolder(StudyModuleViewHolder viewHolder, final int position) {
        viewHolder.getStudyModuleShape().setStudyModule(this.studyModuleList.get(position));
        viewHolder.getNameTextView().setText(this.studyModuleList.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return this.studyModuleList == null ? 0 : this.studyModuleList.size();
    }

    public void setStudyModuleList(List<StudyModule> studyModuleList) {
        this.studyModuleList = studyModuleList;
        notifyDataSetChanged();
    }
}
