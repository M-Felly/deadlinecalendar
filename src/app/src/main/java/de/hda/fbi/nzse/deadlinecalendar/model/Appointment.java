package de.hda.fbi.nzse.deadlinecalendar.model;

import android.content.Context;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.util.Calendar;
import java.util.GregorianCalendar;

import de.hda.fbi.nzse.deadlinecalendar.R;
import de.hda.fbi.nzse.deadlinecalendar.model.typeconverters.GregorianCalendarConverter;
import de.hda.fbi.nzse.deadlinecalendar.model.typeconverters.NotificationConverter;
import de.hda.fbi.nzse.deadlinecalendar.receiver.NotificationReceiver;


/**
 * An Appointment is getting used by the CalendarViewActivity and its subclasses
 */
@Entity
public class Appointment implements NotificationReceiver.Notifiable {

    @PrimaryKey(autoGenerate = true)
    public long appointmentID;

    @TypeConverters(GregorianCalendarConverter.class)
    private GregorianCalendar reminderDate;

    @TypeConverters(GregorianCalendarConverter.class)
    private GregorianCalendar fromDate;

    @TypeConverters(GregorianCalendarConverter.class)
    private GregorianCalendar toDate;

    private String title;
    private String location;
    private String note;
    private boolean isEditable;

    @TypeConverters(NotificationConverter.class)
    Notification.AppointmentNotificationType notificationType;

    @Ignore
    public Appointment(String title, Notification.AppointmentNotificationType notificationType, GregorianCalendar reminderDate, GregorianCalendar fromDate, GregorianCalendar toDate, String location, String note, boolean isEditable) {
        this.title = title;
        this.notificationType = notificationType;
        this.reminderDate = reminderDate;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.location = location;
        this.note = note;
        this.isEditable = isEditable;
    }

    public Appointment() {
    }


    /***************************************************************
     *                      Getter / Setter
     ***************************************************************/

    @Override
    public long getID() {
        return appointmentID;
    }

    @Override
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public GregorianCalendar getReminderDate() {
        return reminderDate;
    }

    @Override
    public GregorianCalendar getNotifiableReminderDate() {
        if(notificationType == Notification.AppointmentNotificationType.NO_NOTIFICATION) {
            return null;
        }

        return reminderDate;
    }

    public void setReminderDate(GregorianCalendar reminderDate) {
        this.reminderDate = reminderDate;
    }

    public  Notification.AppointmentNotificationType getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(Notification.AppointmentNotificationType notificationType) {
        this.notificationType = notificationType;
    }

    public GregorianCalendar getFromDate() {
        return fromDate;
    }

    public void setFromDate(GregorianCalendar fromDate) {
        this.fromDate = fromDate;
    }

    public GregorianCalendar getToDate() {
        return toDate;
    }

    public void setToDate(GregorianCalendar toDate) {
        this.toDate = toDate;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public boolean getIsEditable() {
        return isEditable;
    }

    public void setEditable(boolean editable) {
        isEditable = editable;
    }

    @Override
    public Calendar getTargetDate() {
        return fromDate;
    }

    /***************************************************************
     *                      Static Enum Converters
     ***************************************************************/

    public static String notificationTypeToString(Context context, Notification.AppointmentNotificationType notificationTypeTemp) {

        String type = "";
        switch (notificationTypeTemp) {
            case NO_NOTIFICATION: {
                type = context.getString(R.string.noreminder);
                break;
            }
            case FIVE_MINUTES: {
                type = context.getString(R.string.fiveminutes);
                System.out.println(type);
                break;
            }
            case FIFTEEN_MINUTES: {
                type = context.getString(R.string.fifteenminutes);
                break;
            }
            case THIRTY_MINUTES: {
                type = context.getString(R.string.thirtyminutes);
                break;
            }
            case ONE_HOUR: {
                type = context.getString(R.string.onehour);
                break;
            }
            case TWO_HOURS: {
                type = context.getString(R.string.twohours);
                break;
            }
            case ONE_DAY: {
                type = context.getString(R.string.oneday);
                break;
            }
        }
        return type;
    }
}
