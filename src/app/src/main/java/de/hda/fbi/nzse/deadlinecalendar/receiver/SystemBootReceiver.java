package de.hda.fbi.nzse.deadlinecalendar.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import de.hda.fbi.nzse.deadlinecalendar.manager.AlarmManager;
import de.hda.fbi.nzse.deadlinecalendar.manager.ManagerHolder;
import de.hda.fbi.nzse.deadlinecalendar.model.Appointment;
import de.hda.fbi.nzse.deadlinecalendar.model.Deadline;
import de.hda.fbi.nzse.deadlinecalendar.model.repository.AppointmentRepository;
import de.hda.fbi.nzse.deadlinecalendar.model.repository.DeadlineRepository;

/**
 * This receiver is getting called by the BOOT_COMPLETED action call. It registers all notifiable objects
 * where the user wants to be notified
 *
 * @author Matthias Feyll
 */
public class SystemBootReceiver extends BroadcastReceiver {
    AlarmManager alarmManager;

    /**
     * Register all notifiable deadlines and appointments after boot to the alarmManager
     *
     * @param context context
     * @param intent  receiver intent
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        // to debug execute following command
        // adb shell am broadcast -a android.intent.action.BOOT_COMPLETED -p  de.hda.fbi.nzse.deadlinecalendar

        if (intent.getAction().equalsIgnoreCase(Intent.ACTION_BOOT_COMPLETED)) {
            alarmManager = ManagerHolder.getManager(AlarmManager.class);

            registerDeadlines(context);
            registerAppointments(context);
        }
    }

    /**
     * This method fetches all appointments from the databases and register it to the {@link AlarmManager}
     *
     * @param context context
     */
    private void registerAppointments(Context context) {
        AppointmentRepository appointmentRepository = new AppointmentRepository(context);

        appointmentRepository.getAppointments(appointments -> {
            if (appointments == null) {
                return;
            }


            for (Appointment appointment : appointments) {
                alarmManager.register(context, appointment);
            }
        });
    }

    /**
     * This method fetches all deadlines from the databases and register it to the {@link AlarmManager}
     *
     * @param context context
     */
    private void registerDeadlines(Context context) {
        DeadlineRepository deadlineRepository = new DeadlineRepository(context);

        deadlineRepository.getDeadlines(deadlines -> {
            if (deadlines == null) {
                return;
            }

            for (Deadline deadline : deadlines) {
                alarmManager.register(context, deadline);
            }
        });
    }
}
