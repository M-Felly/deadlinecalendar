package de.hda.fbi.nzse.deadlinecalendar.model.repository;

import android.content.Context;

import androidx.annotation.NonNull;

import java.util.List;
import java.util.concurrent.Future;

import de.hda.fbi.nzse.deadlinecalendar.ProfileViewActivity;
import de.hda.fbi.nzse.deadlinecalendar.model.Profile;
import de.hda.fbi.nzse.deadlinecalendar.model.StudyModule;
import de.hda.fbi.nzse.deadlinecalendar.model.relations.StudyModuleWithDeadlines;

/**
 * Study module repository
 *
 * @author Matthias Feyll
 * @see BaseRepository
 */
public class StudyModuleRepository extends BaseRepository {
    public static final String TAG = "StudyModuleRepository";

    public StudyModuleRepository(Context context) {
        super(context);
    }

    /**
     * Get all study modules
     *
     * @param resultCallbackListener that will be passed from the invoking activity {@link ProfileViewActivity}
     */
    public void getAll(ResultCallbackListener<List<StudyModule>> resultCallbackListener) {
        this.executeDatabaseAction(DatabaseAction.SELECT, appDatabase -> appDatabase.studyModuleDao().getStudyModules(), resultCallbackListener);
    }

    /**
     * Insert a new StudyModule
     *
     * @param studyModule            to be inserted
     * @param relatedProfile         the corresponding profile
     * @param resultCallbackListener that will be passed from the invoking activity {@link ProfileViewActivity}
     */
    public void insert(StudyModule studyModule, Profile relatedProfile, ResultCallbackListener<Long> resultCallbackListener) {
        studyModule.relatedProfileID = relatedProfile.profileID;

        this.executeDatabaseAction(
                DatabaseAction.INSERT,
                (appDatabase) -> appDatabase.profileWithStudyModulesDao().insert(studyModule),
                id -> {
                    studyModule.studyModuleID = id;
                    resultCallbackListener.onSuccess(id);
                }
        );
    }

    /**
     * Wrapper method for {@link #insert(StudyModule, Profile, ResultCallbackListener)}
     *
     * @param studyModule    to be inserted
     * @param relatedProfile the corresponding profile
     */
    public void insert(StudyModule studyModule, Profile relatedProfile) {
        insert(studyModule, relatedProfile, data -> {
        });
    }

    /**
     * Delete one study module
     *
     * @param studyModule            to be delete
     * @param resultCallbackListener onDelete lambda
     */
    public void delete(StudyModule studyModule, ResultCallbackListener<Integer> resultCallbackListener) {
        executeDatabaseAction(DatabaseAction.DELETE, (appDatabase) -> appDatabase.studyModuleDao().delete(studyModule), resultCallbackListener);
    }

    /**
     * Get one study module with the relation class {@link StudyModuleWithDeadlines}
     *
     * @param studyModule related study module
     * @return asynchronous database result
     */
    public Future<StudyModuleWithDeadlines> getDeadlinesToStudyModule(@NonNull StudyModule studyModule) {
        return executeAsyncDatabaseAction(DatabaseAction.SELECT, appDatabase -> appDatabase.studyModuleWithDeadlinesDao().findOneById(studyModule.studyModuleID).get(0));
    }

    public Future<Boolean> studyModuleAlreadyExists(String tag) {
        return executeAsyncDatabaseAction(DatabaseAction.SELECT, appDatabase -> appDatabase.studyModuleDao().studyModuleAlreadyExists(tag));
    }

    @Override
    protected String getTag() {
        return TAG;
    }
}
