package de.hda.fbi.nzse.deadlinecalendar.presenter;

import de.hda.fbi.nzse.deadlinecalendar.R;
import de.hda.fbi.nzse.deadlinecalendar.services.importService.ImportServiceException;

/**
 * The presenter relates to {@link ImportServiceException}.
 *
 * It transforms the error codes and provides string ressources for error title,
 * error description and fix suggestion
 *
 * @author Matthias Feyll
 */
public class ImportServiceExceptionPresenter {
    private final ImportServiceException importServiceException;


    public ImportServiceExceptionPresenter(ImportServiceException importServiceException) {
        this.importServiceException = importServiceException;
    }


    /* **************************************************************
     *                   Error messages output
     ****************************************************************/

    /**
     * Get string resource for error title
     * @return string resource
     */
    public int getErrorTitle() {
        switch (this.importServiceException.getExceptionCategory()) {
            case NETWORK_EXCEPTION:
                return R.string.importService_toast_networkError_title;
            case PARSING_EXCEPTION:
                return R.string.importService_toast_parsingError_title;
            default:
                return R.string.importService_toast_unknownError_title;
        }
    }

    /**
     * Get string resource for error description
     * @return string resource
     */
    public int getErrorDescription() {
        switch (this.importServiceException.getExceptionCategory()) {
            case NETWORK_EXCEPTION:
                return R.string.importService_toast_networkError_description;
            case PARSING_EXCEPTION:
                return R.string.importService_toast_parsingError_description;
            default:
                return R.string.importService_toast_unknownError_description;
        }
    }

    /**
     * Get string resource for fix suggestion
     * @return string resource
     */
    public int getFixSuggestion() {
        switch (this.importServiceException.getExceptionCategory()) {
            case NETWORK_EXCEPTION:
                return R.string.importService_toast_networkError_fixSuggestion;
            case PARSING_EXCEPTION:
                return R.string.importService_toast_parsingError_fixSuggestion;
            default:
                return R.string.importService_toast_unknownError_fixSuggestion;
        }
    }
}
