package de.hda.fbi.nzse.deadlinecalendar.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import de.hda.fbi.nzse.deadlinecalendar.R;
import de.hda.fbi.nzse.deadlinecalendar.model.Appointment;
import de.hda.fbi.nzse.deadlinecalendar.model.AppointmentDeadlineDate;
import de.hda.fbi.nzse.deadlinecalendar.model.CalendarElement;
import de.hda.fbi.nzse.deadlinecalendar.model.Deadline;
import de.hda.fbi.nzse.deadlinecalendar.presenter.DeadlineAccessHandler;
import de.hda.fbi.nzse.deadlinecalendar.view_elements.custom.StudyModuleShape;

/**
 * In this Adapter class, appointments and deadlines will be bound to the recycler view of the
 * CalendarViewActivity.
 */
public class CalendarElementsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Serializable {

    private final List<Appointment> appointments = new ArrayList<>();
    private final List<Deadline> deadlines = new ArrayList<>();
    private final List<CalendarElement> calendarElements  = new ArrayList<>();
    private final DeadlineAccessHandler deadlineAccessHandler;
    private final CalendarElementsAdapterInterface calendarElementsAdapterInterface;
    private List<AppointmentDeadlineDate> appointmentDeadlineDateList;
    private int listElement;
    public CalendarElementsAdapter(GregorianCalendar selectedDate, List<AppointmentDeadlineDate> appointmentsDeadlineDateList, DeadlineAccessHandler deadlineAccessHandler, CalendarElementsAdapterInterface calendarElementsAdapterInterface) {
        this.deadlineAccessHandler = deadlineAccessHandler;
        this.calendarElementsAdapterInterface = calendarElementsAdapterInterface;
        this.appointmentDeadlineDateList = appointmentsDeadlineDateList;

        SimpleDateFormat fromDateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.forLanguageTag(String.valueOf(R.string.languageTag)));
        String appointmentDeadlineDate = fromDateFormat.format(selectedDate.getTime());
        fillCalendarElementsList(appointmentDeadlineDate);
        Collections.reverse(calendarElements);

    }

    /**
     * All appointments and deadlines that match with the date of the given parameter will be filled
     * into the List of CalendarElements. In the same step, appointments and deadlines will parsed
     * into the object type CalendarElement, so they are comparable and able to bind to the same
     * recycler view.
     *
     * @param date is the selected Date in Calendar as a String
     */
    public void fillCalendarElementsList(String date){
        calendarElements.clear();
        appointments.clear();
        deadlines.clear();
        listElement = -1;

        boolean firstTime = true;
        for(int counterAppointmentDeadlineDate = 0; counterAppointmentDeadlineDate < appointmentDeadlineDateList.size(); counterAppointmentDeadlineDate++){
            SimpleDateFormat fromDateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.forLanguageTag(String.valueOf(R.string.languageTag)));
            String appointmentDeadlineDate = fromDateFormat.format(appointmentDeadlineDateList.get(counterAppointmentDeadlineDate).getDate().getTime());
            if( appointmentDeadlineDate.equals(date)){
                if(firstTime){
                    fillAppointments(counterAppointmentDeadlineDate);
                    fillDeadline(counterAppointmentDeadlineDate);
                    listElement = counterAppointmentDeadlineDate;
                    firstTime = false;
                }
                for (int i = 0; i < appointments.size(); i++){
                    calendarElements.add(CalendarElement.toCalendarElement(appointments.get(i), i));
                }
                for (int i = 0; i < deadlines.size(); i++){
                    calendarElements.add(CalendarElement.toCalendarElement(deadlines.get(i), i));
                }
            }
        }

        Collections.sort(calendarElements);

    }

    /**
     * Updates the RecyclerView. If a specific date is chosen, the recycler view shall only show
     * elements of this date.
     *
     * @param selectedDate represents the preferred date that shall be shown in recycler view.
     * @param appointmentsDeadlineDateList list of all appointments and deadlines of a specific date.
     */
    public void refillCalendarElementsList(GregorianCalendar selectedDate, List<AppointmentDeadlineDate> appointmentsDeadlineDateList){
        this.appointmentDeadlineDateList = appointmentsDeadlineDateList;
        SimpleDateFormat selectedDateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.forLanguageTag(String.valueOf(R.string.languageTag)));
        String date = selectedDateFormat.format(selectedDate.getTime());

        calendarElements.clear();
        appointments.clear();
        deadlines.clear();
        listElement = -1;

        boolean firstTime = true;
        for(int counterAppointmentDeadlineDate = 0; counterAppointmentDeadlineDate < appointmentDeadlineDateList.size(); counterAppointmentDeadlineDate++){
            SimpleDateFormat fromDateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.forLanguageTag(String.valueOf(R.string.languageTag)));
            String appointmentDeadlineDate = fromDateFormat.format(appointmentDeadlineDateList.get(counterAppointmentDeadlineDate).getDate().getTime());
            if( appointmentDeadlineDate.equals(date)){
                if(firstTime){
                    fillAppointments(counterAppointmentDeadlineDate);
                    fillDeadline(counterAppointmentDeadlineDate);
                    listElement = counterAppointmentDeadlineDate;
                    firstTime = false;
                }
                for (int i = 0; i < appointments.size(); i++){
                    calendarElements.add(CalendarElement.toCalendarElement(appointments.get(i), i));
                }
                for (int i = 0; i < deadlines.size(); i++){
                    calendarElements.add(CalendarElement.toCalendarElement(deadlines.get(i), i));
                }
            }
        }

        Collections.sort(calendarElements);

    }

    /**
     * Fills all appointments into a specific date (realized by using data type
     * AppointmentDeadlineDate).
     *
     * @param counter represents the index, where the appointments will be inserted into the
     * AppointmentDeadlineDate List.
     */
    private void fillAppointments(int counter){
        appointments.addAll(appointmentDeadlineDateList.get(counter).getAppointmentList());
    }

    /**
     * Fills all deadlines into a specific date (realized by using data type
     * AppointmentDeadlineDate).
     *
     * @param counter represents the index, where the deadlines will be inserted into the
     * AppointmentDeadlineDate List.
     */
    private void fillDeadline(int counter){
        deadlines.addAll(appointmentDeadlineDateList.get(counter).getDeadlineList());
    }

    /**
     * Returns position of an appointment in the appointments List.
     *
     * @param position of the Calendar Element in the recycler view.
     * @return position of the Appointment in the List appointments.
     */
    public int returnAppointmentOnPosition(int position) {
        if (calendarElements.size()!= 0){
            if (calendarElements.get(position).getType()== CalendarElement.Type.APPOINTMENT
                    && appointments.size() > calendarElements.get(position).getPosition() ){
                return calendarElements.get(position).getPosition();
            }
            return 0;
        }
        return -1;
    }

    /**
     * Returns position of a deadline in the deadlines List.
     *
     * @param position  of the Calendar Element in the recycler view.
     * @return position of the Deadline in the List deadlines.
     */
    public int returnDeadlineOnPosition(int position) {

        if (calendarElements.size()!= 0){
            if (calendarElements.get(position).getType()== CalendarElement.Type.DEADLINE
                    && deadlines.size() > calendarElements.get(position).getPosition() ){
                return calendarElements.get(position).getPosition();
            }
            return 0;
        }
        return -1;
    }

    /**
     * Returns the type of the CalendarElement, so the adapter can invoke the corresponding
     * ViewHolder class.
     *
     * @param position of the CalendarElement in List.
     * @return type of the CalendarElement (0 = APPOINTMENT, 1 = DEADLINE).
     */
    @Override
    public int getItemViewType(int position) {
        if(calendarElements.size() != 0){
            int type = 0; //Type is APPOINTMENT
            if (calendarElements.get(position).getType()==CalendarElement.Type.DEADLINE){
                type = 1;
            }
            return type;
        }
        return -1;
    }

    /**
     * Creates the Viewholder, according to the element type.
     *
     * @param parent is the given parent.
     * @param viewType, 0 = APPOINTMENT, 1 = DEADLINE.
     * @return the DeadlineViewHolder
     */
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view;

        if (viewType == 0){ //if it is an appointment

            view = layoutInflater.inflate(R.layout.adapter_calendarview_appointment, parent,false );
            return new AppointmentViewHolder(view);
        }

            view = layoutInflater.inflate(R.layout.adapter_calendarview_deadline, parent,false );
            return new DeadlineViewholder(view);

    }

    /**
     * Binds the CalendarElement to the ViewHolder.
     * If type = 0: APPOINTMENT.
     * If type = 1: DEADLINE.
     *
     * @param holder is the given ViewHolder.
     * @param position of the CalendarElement in the RecyclerView.
     */
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if(calendarElements.size() != 0){
            if (calendarElements.get(position).getType() == CalendarElement.Type.APPOINTMENT){
                AppointmentViewHolder appointmentViewHolder = (AppointmentViewHolder) holder;
                CalendarElement calendarElement = calendarElements.get(position);
                Appointment appointment = appointments.get(calendarElement.getPosition());
                appointmentViewHolder.title.setText(appointment.getTitle());

                //Getting time of appointment
                SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm", Locale.forLanguageTag(String.valueOf(R.string.languageTag)));
                String fromTime = timeFormat.format(appointment.getFromDate().getTime());
                String toTime = timeFormat.format(appointment.getToDate().getTime());
                String totalTime = fromTime + " - " + toTime + " Uhr";
                appointmentViewHolder.time.setText(totalTime);

                appointmentViewHolder.location.setText(appointment.getLocation());
            }
            else {
                DeadlineViewholder deadlineViewholder = (DeadlineViewholder) holder;
                CalendarElement calendarElement = calendarElements.get(position);
                Deadline deadline = deadlines.get(calendarElement.getPosition());
                deadlineViewholder.title.setText(deadline.getTitle());

                //Getting time of deadline
                SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm", Locale.forLanguageTag(String.valueOf(R.string.languageTag)));
                String submissionTime = timeFormat.format(deadline.getReminderDate().getTime());
                submissionTime = submissionTime + " Uhr";
                deadlineViewholder.time.setText(submissionTime);

                deadlineViewholder.studyModuleShape.setStudyModule(deadlineAccessHandler.getStudyModule(deadline));

            }
        }

    }

    /**
     * @return sum of appointments and deadlines.
     */
    @Override
    public int getItemCount() {
        return appointments.size()+deadlines.size();
    }

    /**
     * This class represents the ViewHolder for handling appointments in the RecyclerView.
     */
    class AppointmentViewHolder extends RecyclerView.ViewHolder{

        public final TextView title;
        public final TextView time;
        public final TextView location;

        public AppointmentViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.calendarViewActivity_appointment_textView_description);
            time = itemView.findViewById(R.id.calendarViewActivity_appointment_textView_time);
            location = itemView.findViewById(R.id.calendarViewActivity_appointment_textView_location);

            itemView.setOnClickListener(v -> calendarElementsAdapterInterface.onItemClickAppointment(getAdapterPosition(),listElement));
        }
    }

    /**
     * This class represents the ViewHolder for handling deadlines in the RecyclerView.
     */
    class DeadlineViewholder extends RecyclerView.ViewHolder{

        private final TextView title;
        private final TextView time;
        private final StudyModuleShape studyModuleShape;

        public DeadlineViewholder(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(v -> calendarElementsAdapterInterface.onItemClickDeadline(getAdapterPosition(),listElement));

            this.studyModuleShape = itemView.findViewById(R.id.calendarViewActivity_deadline_studyModuleShape);
            this.title = itemView.findViewById(R.id.calendarViewActivity_deadline_textView_description);
            this.time = itemView.findViewById(R.id.calendarViewActivity_deadline_TextView_time);

           }

    }


}
