package de.hda.fbi.nzse.deadlinecalendar;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.List;

import de.hda.fbi.nzse.deadlinecalendar.adapter.StudyModuleAdapter;
import de.hda.fbi.nzse.deadlinecalendar.dialogs.ProfileAddStudyModuleDialog;
import de.hda.fbi.nzse.deadlinecalendar.dialogs.ProfileDeleteStudyModuleDialog;
import de.hda.fbi.nzse.deadlinecalendar.factory.DialogFragmentFactory;
import de.hda.fbi.nzse.deadlinecalendar.model.Profile;
import de.hda.fbi.nzse.deadlinecalendar.model.StudyModule;
import de.hda.fbi.nzse.deadlinecalendar.model.StudyModuleColor;
import de.hda.fbi.nzse.deadlinecalendar.model.repository.ProfileRepository;
import de.hda.fbi.nzse.deadlinecalendar.model.repository.StudyModuleRepository;

import static de.hda.fbi.nzse.deadlinecalendar.dialogs.ProfileAddStudyModuleDialog.ACTION_LISTENER_KEY;
import static de.hda.fbi.nzse.deadlinecalendar.dialogs.ProfileAddStudyModuleDialog.STUDY_MODULE_COLOR_KEY;


/**
 * In this activity the user can change the profile.
 * He/She is also able to add or delete study modules.
 * <p>
 * Adding new study modules call {@link ProfileAddStudyModuleDialog}
 * Confirm delete conflicts calls the dialog {@link ProfileDeleteStudyModuleDialog}
 *
 * @author Matthias Feyll
 */
public class ProfileViewActivity extends AppCompatActivity implements ProfileAddStudyModuleDialog.OnClickListener, ViewTreeObserver.OnPreDrawListener {
    private static final String ADD_STUDY_MODULE_DIALOG_TAG = "dialogs";
    public static final int RECYCLER_VIEW_MARGIN_BOTTOM = 50;

    private BottomNavigationView bottomNav;

    private TextView nameTextView;
    private TextView matriculationNumberTextView;
    private TextView universityNameTextView;
    private TextView studyLevelTextView;
    private TextView departmentNameTextView;

    private RecyclerView studyModuleRecyclerView;
    private StudyModuleAdapter studyModuleAdapter;

    private Profile profile;
    private List<StudyModule> studyModuleList;

    private StudyModuleRepository studyModuleRepository;
    private ProfileRepository profileRepository;

    private ConstraintLayout recyclerViewLayout;

    ImageButton addStudyModulesBtn;


    /* **************************************************************
     *                     Life cycle hooks
     ****************************************************************/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_view);
        setTitle(R.string.title_profile);

        bottomNav = findViewById(R.id.profileViewActivity_bottomNav);
        bottomNav.setOnNavigationItemSelectedListener(new BottomNavigation(this));

        // input elements
        this.nameTextView = findViewById(R.id.profileViewActivity_textView_name);
        this.matriculationNumberTextView = findViewById(R.id.profileViewActivity_textView_matriculationNumber);
        this.universityNameTextView = findViewById(R.id.profileViewActivity_textView_universityName);
        this.studyLevelTextView = findViewById(R.id.profileViewActivity_textView_studyLevel);
        this.departmentNameTextView = findViewById(R.id.profileViewActivity_textView_departmentName);

        // set on click listener for add study module
        addStudyModulesBtn = findViewById(R.id.profileViewActivity_imageButton_addStudyModule);
        TextView addStudyModuleText = findViewById(R.id.profileViewActivity_textView_studyModuleName);
        addStudyModulesBtn.setOnClickListener(v -> this.onAddStudyModuleBtnClicked());
        addStudyModuleText.setOnClickListener(v -> this.onAddStudyModuleBtnClicked());

        // recycler view
        this.studyModuleRecyclerView = findViewById(R.id.profileViewActivity_recyclerView_studyModules);
        this.studyModuleRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        this.studyModuleAdapter = new StudyModuleAdapter(this);
        this.studyModuleRecyclerView.setAdapter(this.studyModuleAdapter);
        this.studyModuleRecyclerView.getViewTreeObserver().addOnPreDrawListener(this);
        this.recyclerViewLayout = findViewById(R.id.editProfileViewActivity_constraintLayout_recyclerViewLayout);

        // repository
        this.studyModuleRepository = new StudyModuleRepository(this);
        this.profileRepository = new ProfileRepository(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        bottomNav.getMenu().findItem(R.id.bottom_nav_profile).setChecked(true);
        loadProfile();
    }


    /* **************************************************************
     *                         Load / Save
     ****************************************************************/

    private void loadProfile() {
        this.profileRepository.getProfileWithStudyModules(profileWithStudyModules -> {
            if (profileWithStudyModules == null) {
                return;
            }

            this.profile = profileWithStudyModules.profile;
            this.studyModuleList = profileWithStudyModules.studyModules;
            displayProfile();
        });
    }

    private void saveStudyModule(StudyModule studyModule) {
        this.studyModuleRepository.insert(studyModule, profile, id -> this.displayProfile());
    }

    /* **************************************************************
     *                          Display
     ****************************************************************/

    /**
     * Display all attributes of the class profile inclusive the study module list
     */
    private void displayProfile() {
        this.nameTextView.setText(this.profile.getName());
        this.matriculationNumberTextView.setText(this.profile.getMatriculationNumber());
        this.universityNameTextView.setText(this.profile.getUniversityName());
        this.studyLevelTextView.setText(this.profile.getStudyLevel());
        this.departmentNameTextView.setText(this.profile.getDepartmentName());

        this.studyModuleAdapter.setStudyModuleList(this.studyModuleList);
    }

    /* **************************************************************
     *                      Actions / Listener
     ****************************************************************/

    /**
     * Open dialog {@link ProfileAddStudyModuleDialog}
     */
    private void onAddStudyModuleBtnClicked() {
        Bundle dialogArguments = new Bundle();
        dialogArguments.putSerializable(ACTION_LISTENER_KEY, this);
        dialogArguments.putSerializable(STUDY_MODULE_COLOR_KEY, resolveNextStudyModuleColor());

        DialogFragment addStudyModuleDialog = DialogFragmentFactory.createInstance(ProfileAddStudyModuleDialog.class, dialogArguments);
        addStudyModuleDialog.show(getSupportFragmentManager(), ADD_STUDY_MODULE_DIALOG_TAG);
    }

    /**
     * Add new study module to the list and save it in database
     *
     * @param studyModule new object
     */
    @Override
    public void onSubmitDialog(StudyModule studyModule) {
        studyModuleList.add(studyModule);
        this.studyModuleAdapter.notifyItemInserted(studyModuleList.size() - 1);

        this.saveStudyModule(studyModule);
    }

    /* **************************************************************
     *                            Drawing
     ****************************************************************/

    /**
     * Calculate max height for {@link #studyModuleRecyclerView}
     *
     * @return hook used
     */
    @Override
    public boolean onPreDraw() {
        ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) this.recyclerViewLayout.getLayoutParams();
        layoutParams.matchConstraintMaxHeight = calculateRecyclerViewHeight();
        this.recyclerViewLayout.setLayoutParams(layoutParams);

        this.studyModuleRecyclerView.getViewTreeObserver().removeOnPreDrawListener(this);

        return true;
    }

    /* **************************************************************
     *                         Calculating
     ****************************************************************/

    /**
     * Resolving the next study module color depending by list size
     *
     * @return resolved next study module color
     */
    private StudyModuleColor resolveNextStudyModuleColor() {
        StudyModuleColor[] studyModuleColors = StudyModuleColor.values();

        if (!studyModuleList.isEmpty()) {
            StudyModuleColor currentLastIndexColor = studyModuleList.get(studyModuleList.size() -1).getColor();

            for (int i = 0; i < studyModuleColors.length; i++) {
                if (studyModuleColors[i] == currentLastIndexColor) {
                    return studyModuleColors[(i + 1) % (studyModuleColors.length)];
                }
            }
        }

        return studyModuleColors[0];
    }

    /**
     * Calculating height for recycler view. This is necessary make the recycler view wrap content during
     * less height to be displayed but also scrollable with a max height on too much items that
     * have to be displayed.
     *
     * @return calculated height
     */
    private int calculateRecyclerViewHeight() {
        int topBorder = 0, bottomBorder;

        // if orientation is portrait get top height from divider position
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            int[] topBorderLocation = {0, 0};
            View divider = findViewById(R.id.profileViewActivity_divider);
            divider.getLocationOnScreen(topBorderLocation);
            topBorder = topBorderLocation[1];
        }else {
            // if orientation = landscape set topBorder = actionBar height
            TypedValue tv = new TypedValue();
            if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
                topBorder = TypedValue.complexToDimensionPixelSize(tv.data,getResources().getDisplayMetrics()) + 50;
            }
        }

        // get the bottomBorder from the absolute bottom nav y position
        int[] bottomBorderLocation  = {0, 0};
        bottomNav.getLocationOnScreen(bottomBorderLocation);
        bottomBorder = bottomBorderLocation[1];

        return bottomBorder - topBorder - addStudyModulesBtn.getHeight() - RECYCLER_VIEW_MARGIN_BOTTOM;
    }

    /* **************************************************************
     *                         Navigation
     ****************************************************************/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.profile_view_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.profileView_item_edit) {
            startActivity(new Intent(this, EditProfileViewActivity.class));
            return true;
        }

        if (item.getItemId() == R.id.profileView_item_settings) {
            startActivity(new Intent(this, SettingsViewActivity.class));
            return true;
        }

        return false;
    }
}