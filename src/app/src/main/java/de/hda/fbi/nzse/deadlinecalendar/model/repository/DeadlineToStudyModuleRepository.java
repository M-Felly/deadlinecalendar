package de.hda.fbi.nzse.deadlinecalendar.model.repository;

import android.content.Context;

import java.util.List;

import de.hda.fbi.nzse.deadlinecalendar.DeadlineViewActivity;
import de.hda.fbi.nzse.deadlinecalendar.model.Deadline;
import de.hda.fbi.nzse.deadlinecalendar.model.StudyModule;
import de.hda.fbi.nzse.deadlinecalendar.model.relations.DeadlineToStudyModule;

/**
 * Deadline to study module repository
 *
 * @author Dennis Hilz, Anna Kilb
 * @see BaseRepository
 */
public class DeadlineToStudyModuleRepository extends BaseRepository {

    public static final String TAG = "DeadlineToStudyModuleRepository";

    public DeadlineToStudyModuleRepository(Context context) {
        super(context);
    }

    /**
     * Updating an existing StudyModule
     *
     * @param resultCallbackListener that will be passed from the invoking activity {@link DeadlineViewActivity}
     */
    public void getAll(BaseRepository.ResultCallbackListener<List<DeadlineToStudyModule>> resultCallbackListener) {
        this.executeDatabaseAction(BaseRepository.DatabaseAction.SELECT, appDatabase -> appDatabase.deadlineToStudyModuleDao().getDeadlinesToStudyModule(), resultCallbackListener);
    }

    /**
     * Updating an existing StudyModule
     *
     * @param studyModule to be updated
     * @param deadline corresponding to studyModule
     */
    public void changeStudyModule(Deadline deadline, StudyModule studyModule) {
        deadline.relatedStudyModuleID = studyModule.studyModuleID;

        this.executeDatabaseAction(
                DatabaseAction.UPDATE,
                (appDatabase) -> appDatabase.deadlineDao().update(deadline));
    }

    @Override
    protected String getTag() {
        return TAG;
    }
}
