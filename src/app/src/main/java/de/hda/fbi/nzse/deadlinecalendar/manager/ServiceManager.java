package de.hda.fbi.nzse.deadlinecalendar.manager;

import androidx.annotation.NonNull;

import java.util.Observable;
import java.util.Observer;

/**
 * The service manager manages all services tasks which runs in this app.
 *
 * @author Matthias Feyll
 */
public class ServiceManager extends ManagerHolder.AbstractManager {
    /**
     * Wraps the service into a {@link ServiceHolder}
     *
     * @param serviceClass the created service inheriting the {@link AbstractService}
     * @return A service holder containing the service and corresponding thread
     */
    @NonNull
    public ServiceHolder buildService(AbstractService serviceClass) {
        ServiceHolder serviceHolder;
        serviceHolder = new ServiceHolder(serviceClass);

        return serviceHolder;
    }

    /**
     * Call this method if you want to parse a observer to the service
     *
     * @param serviceClass the created service inheriting the {@link AbstractService}
     * @param observer     a optional observer that is getting attached to the service to communicate with
     *                     the service
     * @return A service holder containing the service and corresponding thread
     */
    @NonNull
    public ServiceHolder buildService(AbstractService serviceClass, Observer observer) {
        ServiceHolder serviceHolder = buildService(serviceClass);
        serviceHolder.addObserver(observer);

        return serviceHolder;
    }


    /***************************************************************
     *                        Service Manager
     ***************************************************************/

    public abstract static class AbstractService extends Observable implements Runnable {
    }

    /**
     * Wrapper class for AbstractServices and the related thread
     */
    public static class ServiceHolder {
        private final AbstractService abstractService;
        private final Thread thread;

        public ServiceHolder(AbstractService abstractService) {
            this.abstractService = abstractService;
            this.thread = new Thread(abstractService);
        }

        public void start() {
            this.thread.start();
        }

        public void addObserver(Observer observer) {
            this.abstractService.addObserver(observer);
        }
    }
}
