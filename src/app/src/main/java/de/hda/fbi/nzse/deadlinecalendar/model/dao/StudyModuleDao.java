package de.hda.fbi.nzse.deadlinecalendar.model.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Query;
import androidx.room.Transaction;

import java.util.List;

import de.hda.fbi.nzse.deadlinecalendar.model.StudyModule;


@Dao
public interface StudyModuleDao {

    @Transaction
    @Query("SELECT * FROM studymodule")
    List<StudyModule> getStudyModules();

    @Delete
    int delete(StudyModule studyModule);

    @Transaction
    @Query("SELECT COUNT(studyModuleID) FROM studymodule WHERE tag = :tag")
    boolean studyModuleAlreadyExists(String tag);
}
