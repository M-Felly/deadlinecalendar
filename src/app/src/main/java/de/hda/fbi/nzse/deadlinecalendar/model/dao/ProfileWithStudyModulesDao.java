package de.hda.fbi.nzse.deadlinecalendar.model.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;

import de.hda.fbi.nzse.deadlinecalendar.model.StudyModule;
import de.hda.fbi.nzse.deadlinecalendar.model.relations.ProfileWithStudyModules;

@Dao
public interface ProfileWithStudyModulesDao {
    @Transaction
    @Query("SELECT * FROM profile LIMIT 1")
    ProfileWithStudyModules getProfileWithStudyModules();


    @Insert
    long insert(StudyModule studyModule);
}
