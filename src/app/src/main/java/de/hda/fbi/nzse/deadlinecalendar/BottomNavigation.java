package de.hda.fbi.nzse.deadlinecalendar;

import android.content.Context;
import android.content.Intent;
import android.view.MenuItem;

import androidx.annotation.NonNull;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class BottomNavigation implements BottomNavigationView.OnNavigationItemSelectedListener {
    final Context parent;

    public BottomNavigation(Context parent) {
        this.parent = parent;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.bottom_nav_calendar) {
            Intent intent = new Intent(parent, CalendarViewActivity.class).putExtra("class", this.getClass().toString());
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_NO_HISTORY);
            parent.startActivity(intent);
            return true;
        }
        if (id == R.id.bottom_nav_profile) {
            Intent intent = new Intent(parent, ProfileViewActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_NO_HISTORY );
            parent.startActivity(intent);
            return true;
        }
        if (id == R.id.bottom_nav_deadlines) {
            Intent intent = new Intent(parent, DeadlineViewActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_NO_HISTORY);
            parent.startActivity(intent);
            return true;
        }
        return false;
    }
}