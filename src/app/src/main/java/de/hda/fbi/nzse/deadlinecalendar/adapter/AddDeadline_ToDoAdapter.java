package de.hda.fbi.nzse.deadlinecalendar.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import de.hda.fbi.nzse.deadlinecalendar.R;
import de.hda.fbi.nzse.deadlinecalendar.model.ToDo;

/**
 * Communication interface with inner ViewHolder
 */
interface AddDeadlineToDoViewHolderListener {
    void onRemoveIconClicked(final int position);

    void onCheckBoxClicked(AddDeadline_ToDoAdapter.ToDoViewHolder toDoViewHolder, final int position);
}

/**
 * With this adapter you can display a list of {@link ToDo}. In this list toDos can be
 * checked and deleted.
 *
 * @author Anna Kilb
 */
public class AddDeadline_ToDoAdapter extends RecyclerView.Adapter<AddDeadline_ToDoAdapter.ToDoViewHolder> implements AddDeadlineToDoViewHolderListener {
    private final List<ToDo> toDos;

    public AddDeadline_ToDoAdapter() {
        this.toDos = new ArrayList<>();
    }

    public static class ToDoViewHolder extends RecyclerView.ViewHolder {
        private final CheckBox toDoIsDone_checkBox;
        private final TextView name_textView;

        public ToDoViewHolder(View view, AddDeadlineToDoViewHolderListener toDoViewHolderListener) {
            super(view);
            this.toDoIsDone_checkBox = view.findViewById(R.id.editDeadlineView_toDoAdapter_checkBox_toDoIsDone);
            this.name_textView = view.findViewById(R.id.editDeadlineView_toDoAdapter_textView_name);
            ImageButton remove_imageButton = view.findViewById(R.id.editDeadlineView_toDoAdapter_imageButton_remove);

            toDoIsDone_checkBox.setOnCheckedChangeListener((buttonView, isChecked) ->
                    toDoViewHolderListener.onCheckBoxClicked(ToDoViewHolder.this, ToDoViewHolder.this.getAdapterPosition()));

            remove_imageButton.setOnClickListener(v -> toDoViewHolderListener.onRemoveIconClicked(getAdapterPosition()));
        }

        public CheckBox getCheckBox_toDoIsDone() {
            return toDoIsDone_checkBox;
        }

        public TextView getTextView_name() { return name_textView; }
    }

    public List<ToDo> getToDos() {
        return this.toDos;
    }

    public void addToDo(ToDo toDo) {
        this.toDos.add(toDo);
    }

    /* **************************************************************
     *                            Adapter
     ****************************************************************/

    @NonNull
    @Override
    public AddDeadline_ToDoAdapter.ToDoViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_editdeadlineview_todo, viewGroup, false);
        return new AddDeadline_ToDoAdapter.ToDoViewHolder(view, this);
    }

    @Override
    public void onBindViewHolder(AddDeadline_ToDoAdapter.ToDoViewHolder viewHolder, int position) {
        viewHolder.getCheckBox_toDoIsDone().setChecked(this.toDos.get(position).getIsDone());
        viewHolder.getTextView_name().setText(this.toDos.get(position).getDescription());
    }

    @Override
    public int getItemCount() {
        return this.toDos.size();
    }

    /* **************************************************************
     *                       Action / Listener
     ****************************************************************/

    @Override
    public void onRemoveIconClicked(final int position) {
        this.toDos.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public void onCheckBoxClicked(AddDeadline_ToDoAdapter.ToDoViewHolder viewHolder, final int position) {
        toDos.get(position).setIsDone(viewHolder.toDoIsDone_checkBox.isChecked());
    }
}