package de.hda.fbi.nzse.deadlinecalendar.services.importService;

import android.util.Log;

import androidx.annotation.NonNull;

import java.io.IOException;

/**
 * This exception class holds custom error codes thrown during the import process to determine the
 * exact error.
 *
 * @author Matthias Feyll
 */
public class ImportServiceException extends IOException {
    public static final String TAG = "ImportServiceException";

    /***************************************************************
     *                       ExceptionCategory
     ***************************************************************/
    public enum ExceptionCategory {
        NETWORK_EXCEPTION(1000),
        PARSING_EXCEPTION(2000);

        private final int scopeCode;

        ExceptionCategory(int scopeCode) {
            this.scopeCode = scopeCode;
        }

        public boolean isInScope(int errorCode) {
            return Math.round((float) errorCode / this.scopeCode) == 1;
        }

        @NonNull
        @Override
        public String toString() {
            return String.valueOf(this.scopeCode);
        }
    }

    /***************************************************************
     *                      Custom error codes
     ***************************************************************/

    public static final int HTTP_NOT_CONNECTED = 1001;
    public static final int GENERAL_NETWORK_ERROR = 1003;
    public static final int PARSER_FAILURE = 2001;

    /***************************************************************
     *                  ImportServiceExceptionClass
     ***************************************************************/

    private final int errorCode;

    public ImportServiceException(int errorCode) {
        this.errorCode = errorCode;
    }

    public ImportServiceException(int errorCode, String message) {
        super(message);

        this.errorCode = errorCode;
        this.logError();
    }

    public ExceptionCategory getExceptionCategory() {
        for (ExceptionCategory category : ExceptionCategory.values()) {
            if (category.isInScope(this.errorCode)) {
                return category;
            }
        }

        throw new Error("Exception with error code: " + errorCode + " can not be specified");
    }

    private void logError() {
        Log.e(TAG, getMessage());
    }
}
