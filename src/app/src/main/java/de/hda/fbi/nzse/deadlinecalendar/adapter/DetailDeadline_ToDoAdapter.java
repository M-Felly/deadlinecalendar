package de.hda.fbi.nzse.deadlinecalendar.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import de.hda.fbi.nzse.deadlinecalendar.DetailDeadlineViewActivity;
import de.hda.fbi.nzse.deadlinecalendar.R;
import de.hda.fbi.nzse.deadlinecalendar.model.Deadline;
import de.hda.fbi.nzse.deadlinecalendar.model.ToDo;
import de.hda.fbi.nzse.deadlinecalendar.presenter.DeadlineAccessHandler;

/**
 * Communication interface with inner ViewHolder
 */
interface DetailDeadlineToDoViewHolderListener {
    void onRemoveIconClicked(final int position);
    void onCheckBoxClicked(DetailDeadline_ToDoAdapter.ToDoViewHolder toDoViewHolder, final int position);
}

/**
 * With this adapter you can display a list of {@link ToDo}. In this list toDos can be
 * checked and deleted.
 *
 * @author Dennis Hilz, Anna Kilb
 */
public class DetailDeadline_ToDoAdapter extends RecyclerView.Adapter<DetailDeadline_ToDoAdapter.ToDoViewHolder> implements DetailDeadlineToDoViewHolderListener{
    private final Deadline deadline;
    private final List<ToDo> toDos;
    private final Context context;
    final DeadlineAccessHandler deadlineAccessHandler;

    public DetailDeadline_ToDoAdapter(DeadlineAccessHandler deadlineAccessHandler, Deadline deadline, Context context) {
        this.deadlineAccessHandler = deadlineAccessHandler;
        this.context = context;
        this.deadline = deadline;
        this.toDos = deadlineAccessHandler.getToDos(deadline);
    }

    public static class ToDoViewHolder extends RecyclerView.ViewHolder {
        private final CheckBox toDoIsDone_checkBox;
        private final TextView name_textView;

        public ToDoViewHolder(View view, DetailDeadlineToDoViewHolderListener toDoViewHolderListener) {
            super(view);
            this.toDoIsDone_checkBox = view.findViewById(R.id.detailDeadlineView_toDoAdapter_checkBox_toDoIsDone);
            this.name_textView = view.findViewById(R.id.detailDeadlineView_toDoAdapter_textView_name);
            ImageButton remove_imageButton = view.findViewById(R.id.detailDeadlineView_toDoAdapter_imageButton_remove);

            toDoIsDone_checkBox.setOnCheckedChangeListener((buttonView, isChecked) -> toDoViewHolderListener.onCheckBoxClicked(ToDoViewHolder.this, ToDoViewHolder.this.getAdapterPosition()));
            remove_imageButton.setOnClickListener(v -> toDoViewHolderListener.onRemoveIconClicked(getAdapterPosition()));
        }

        public CheckBox getCheckBox_toDoIsDone() {return toDoIsDone_checkBox;}

        public TextView getTextView_name() {return name_textView;}
    }

    public void addToDo(ToDo toDo) {
        this.toDos.add(toDo);
    }

    private int getCheckedToDos() {
        int checkedToDos = 0;
        for (ToDo todo : this.toDos) {
            if (todo.getIsDone())
                checkedToDos++;
        }
        return checkedToDos;
    }

    /* **************************************************************
     *                            Adapter
     ****************************************************************/

    @NonNull
    @Override
    public ToDoViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_detaildeadlineview_todo, viewGroup, false);
        return new DetailDeadline_ToDoAdapter.ToDoViewHolder(view, this);
    }

    @Override
    public void onBindViewHolder(ToDoViewHolder viewHolder, int position) {
        viewHolder.getCheckBox_toDoIsDone().setChecked(this.toDos.get(position).getIsDone());
        viewHolder.getTextView_name().setText(this.toDos.get(position).getDescription());
    }

    @Override
    public int getItemCount() {
        return this.toDos == null ? 0 : this.toDos.size();
    }

    /* **************************************************************
     *                       Action / Listener
     ****************************************************************/

    @Override
    public void onRemoveIconClicked(final int position)  {
        this.deadlineAccessHandler.deleteToDo(deadline, this.toDos.get(position));
        this.toDos.remove(position);
        notifyItemRemoved(position);
        /*
         * Calls updateCompletionState method of DetailDeadlineActivity through passed context within the adapter to refresh
         * the textView within the activity.
         */
        if (this.context instanceof DetailDeadlineViewActivity) {
            ((DetailDeadlineViewActivity)this.context).updateCompletionState(getCheckedToDos(), this.toDos.size());
        }
    }

    @Override
    public void onCheckBoxClicked(ToDoViewHolder viewHolder, final int position) {
        this.deadlineAccessHandler.checkToDo(this.toDos.get(position), viewHolder.getCheckBox_toDoIsDone().isChecked());

        if (this.context instanceof DetailDeadlineViewActivity) {
            ((DetailDeadlineViewActivity)this.context).updateCompletionState(getCheckedToDos(), this.toDos.size());
        }
    }
}
