package de.hda.fbi.nzse.deadlinecalendar.manager;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import de.hda.fbi.nzse.deadlinecalendar.R;
import de.hda.fbi.nzse.deadlinecalendar.receiver.NotificationReceiver;
import de.hda.fbi.nzse.deadlinecalendar.receiver.NotificationReceiver.Notifiable;

import static de.hda.fbi.nzse.deadlinecalendar.receiver.NotificationReceiver.BUNDLE_EXTRA;
import static de.hda.fbi.nzse.deadlinecalendar.receiver.NotificationReceiver.NOTIFIABLE_INTENT_EXTRA;

/**
 * This class manages the alarms of deadlines and appointments represented as {@link Notifiable}
 * Register / unregister or update notifiable objects to the android alarm system.
 *
 * @author Matthias Feyll
 */
public class AlarmManager extends ManagerHolder.AbstractManager {
    public static final String TAG = "AlarmManager";
    private final Map<Integer, PendingIntent> intentMap;
    private Context context;
    private int id;
    public AlarmManager() {
        id = (int) System.currentTimeMillis(); // unique id for alarms
        intentMap = new HashMap<>();
    }

    /**
     * Register a notifiable to remind the user by notifications for the
     * {@link Notifiable#getNotifiableReminderDate()} and {@link Notifiable#getTargetDate()}
     *
     * @param context    required context
     * @param notifiable notifiable to remind the user for the respective dates
     */
    public void register(Context context, @NonNull NotificationReceiver.Notifiable notifiable) {
        this.context = context;

        setAlarm(notifiable.getNotifiableReminderDate(), notifiable, DateType.REMINDER_DATE);
        setAlarm(notifiable.getTargetDate(), notifiable, DateType.TARGET_DATE);
    }

    /**
     * Unregister a registered notifiable. If the notifiable is not registered the method will do nothing.
     * Usually you call this method if the corresponding object gets deleted by a user action
     *
     * @param context    required context
     * @param notifiable notifiable that will be unregistered
     */
    public void unregister(Context context, @NonNull NotificationReceiver.Notifiable notifiable) {
        this.context = context;

        unsetAlarm(notifiable.getNotifiableReminderDate(), notifiable, DateType.REMINDER_DATE);
        unsetAlarm(notifiable.getTargetDate(), notifiable, DateType.TARGET_DATE);
    }

    /* **************************************************************
     *                     Register / Unregister
     ****************************************************************/

    /**
     * Call this method when the corresponding object is getting updated.
     *
     * @param context    required context
     * @param notifiable notifiable that will be unregistered
     */
    public void update(Context context, @NonNull NotificationReceiver.Notifiable notifiable) {
        this.unregister(context, notifiable);
        this.register(context, notifiable);
    }

    /**
     * Set an alarm for a exact calendar date
     *
     * @param calendar date when the alarm is getting fire
     */
    private void setAlarm(Calendar calendar, Notifiable notifiable, DateType dateType) {
        if (calendar == null || calendar.getTimeInMillis() <= System.currentTimeMillis()) {
            return;
        }

        Intent intent = createIntent(notifiable);

        PendingIntent alarmIntent = PendingIntent.getBroadcast(context, ++id, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        intentMap.put(AlarmIdentifier.createHash(notifiable, dateType), alarmIntent);
        getSystemAlarmManager().set(android.app.AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), alarmIntent);

        logging(true, notifiable, calendar);
    }

    /**
     * Unset an alarm for a exact calendar date
     *
     * @param calendar   exact calendar date
     * @param notifiable exact notifiable
     */
    private void unsetAlarm(Calendar calendar, Notifiable notifiable, DateType dateType) {
        int identifier = AlarmIdentifier.createHash(notifiable, dateType);
        PendingIntent intent = this.intentMap.get(identifier);

        if (intent == null) {
            return;
        }

        getSystemAlarmManager().cancel(intent);
        this.intentMap.remove(identifier);

        logging(false, notifiable, calendar);
    }

    /* **************************************************************
     *                      Set Alarm methods
     ****************************************************************/

    /**
     * Creates a new intent object.
     * It bundles the given notifiable
     *
     * @param notifiable that is getting bundled into the intent
     * @return created intent
     */
    @NonNull
    private Intent createIntent(Notifiable notifiable) {
        Intent intent = new Intent(context, NotificationReceiver.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(NOTIFIABLE_INTENT_EXTRA, notifiable);
        intent.putExtra(BUNDLE_EXTRA, bundle);

        return intent;
    }

    /**
     * Return the actual {@link android.app.AlarmManager}
     *
     * @return the system alarm manger
     */
    private android.app.AlarmManager getSystemAlarmManager() {
        return (android.app.AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
    }

    /* **************************************************************
     *                            Helper
     ****************************************************************/

    /**
     * Logs a set or unset activity
     *
     * @param setAlarm   alarm get set or unset
     * @param notifiable notifiable that will be set / unset
     */
    private void logging(boolean setAlarm, Notifiable notifiable, Calendar calendar) {
        String message = setAlarm ? "Register alarm: " : "Unregister alarm: ";

        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss dd.MM.yyyy", Locale.forLanguageTag(String.valueOf(R.string.languageTag)));
        Log.i(TAG, message + notifiable.getTitle() + " for " + format.format(calendar.getTime()));
    }

    private enum DateType {
        REMINDER_DATE, TARGET_DATE
    }

    /**
     * Internal alarm identifier to get a unique id from the class and id
     */
    private static final class AlarmIdentifier {
        private static int createHash(Notifiable notifiable, DateType dateType) {
            return Objects.hash(notifiable.getClass(), notifiable.getID(), dateType);
        }
    }
}
