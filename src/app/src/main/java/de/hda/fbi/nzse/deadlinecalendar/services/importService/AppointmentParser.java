package de.hda.fbi.nzse.deadlinecalendar.services.importService;

import android.content.Context;

import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.hda.fbi.nzse.deadlinecalendar.manager.AlarmManager;
import de.hda.fbi.nzse.deadlinecalendar.manager.ManagerHolder;
import de.hda.fbi.nzse.deadlinecalendar.model.Appointment;
import de.hda.fbi.nzse.deadlinecalendar.model.Notification;
import de.hda.fbi.nzse.deadlinecalendar.model.repository.AppointmentRepository;

import static de.hda.fbi.nzse.deadlinecalendar.services.importService.ImportServiceException.PARSER_FAILURE;

/**
 * This class provides methods to read, process and persist calendar-data from an ics file obtained
 * by the OBS-Calendar Subscription-Link. The data is split into separate appointments and their
 * attributes by iterating through the file while using RegEx for matching specific strings to obtain
 * the corresponding values.

 * <h2>Usage</h2>
 * Instantiate the class as follows:
 * <code>AppointmentParser appointmentParser = new AppointmentParser(icsResponse, appointmentRepository)</code>
 * *
 * <h2>Notes</h2>
 * The parameter "icsResponse" is considered a single string containing all calendar-data separated
 * by specific keywords.
 *
 */
public class AppointmentParser {
    private final String icsResponse;
    private final AppointmentRepository appointmentRepository;

    private final Context context;

    public AppointmentParser(String icsResponse, Context context) {
        this.icsResponse = icsResponse;
        this.appointmentRepository = new AppointmentRepository(context);
        this.context = context;
    }

    public void run() throws ImportServiceException {
        appointmentRepository.getOBSAppointments(obsAppointments -> {
            if (!Objects.requireNonNull(obsAppointments).isEmpty()) {
                AlarmManager alarmManager = ManagerHolder.getManager(AlarmManager.class);
                obsAppointments.forEach(appointment -> alarmManager.unregister(this.context, appointment));

                appointmentRepository.purgeOBSCalendarData();
            }
        });
        /*
         * Specifying all patterns to match for the extraction of every attribute
         */
        Pattern patternAppointment = Pattern.compile("BEGIN:VEVENT(.*?)END:VEVENT", Pattern.DOTALL);
        Pattern patternModule = Pattern.compile("SUMMARY;LANGUAGE=de-DE:(.*?)\r");
        Pattern patternDesc = Pattern.compile("DESCRIPTION;LANGUAGE=de-DE:(.*?)LOCATION;", Pattern.DOTALL);
        Pattern patternLocation = Pattern.compile("LOCATION;LANGUAGE=de-DE:(.*?)\r");
        Pattern patternStart = Pattern.compile("DTSTART:(.*?)\r");
        Pattern patternEnd = Pattern.compile("DTEND:(.*?)\r");

        List<String> appointmentInfos = new LinkedList<>();
        Matcher appointmentMatcher = patternAppointment.matcher(icsResponse);
        while (appointmentMatcher.find()) {
            appointmentInfos.add(appointmentMatcher.group());
        }
        if (appointmentInfos.size() < 1) {throw new ImportServiceException(PARSER_FAILURE);}

        String module = "";
        String notes = "";
        String location = "";
        String start = "";
        String end = "";


        for (String appointmentInfo : appointmentInfos) {
            Matcher matcher = patternModule.matcher(appointmentInfo);
            if (matcher.find()) { module = matcher.group(1); }
            matcher = patternDesc.matcher(appointmentInfo);
            if (matcher.find()) { notes = matcher.group(1); }
            matcher = patternLocation.matcher(appointmentInfo);
            if (matcher.find()) { location = matcher.group(1); }
            matcher = patternStart.matcher(appointmentInfo);
            if (matcher.find()) { start = matcher.group(1); }
            matcher = patternEnd.matcher(appointmentInfo);
            if (matcher.find()) { end = matcher.group(1); }

            Appointment appointment = new Appointment(
                    module,
                    Notification.AppointmentNotificationType.NO_NOTIFICATION,
                    parseICSDateString(Objects.requireNonNull(start)),
                    parseICSDateString(start),
                    parseICSDateString(Objects.requireNonNull(end)),
                    location,
                    notes,
                    false
            );

            AlarmManager alarmManager = ManagerHolder.getManager(AlarmManager.class);
            alarmManager.register(this.context, appointment);

            appointmentRepository.insert(appointment, id ->{});
        }
    }

    /**
     * This method provides a simple way to parse the date-string provided by the ics-file
     * (ex: 20210204T073000Z)
     *
     * @param icsDateString as single string of calendar-data
     * @return GregorianCalendar as Calendar-object to persist with the respective appointment
     */
    private GregorianCalendar parseICSDateString (String icsDateString) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.set(
                Integer.parseInt(icsDateString.substring(0,4)),   // year
                Integer.parseInt(icsDateString.substring(4,6))-1,   // month
                Integer.parseInt(icsDateString.substring(6,8)),   // day
                Integer.parseInt(icsDateString.substring(9,11))+1,  // hours
                Integer.parseInt(icsDateString.substring(11,13)), // minutes
                Integer.parseInt(icsDateString.substring(13,15))  // seconds
        );
        return gregorianCalendar;
    }

}