package de.hda.fbi.nzse.deadlinecalendar.model.repository;

import android.content.Context;

import java.util.List;

import de.hda.fbi.nzse.deadlinecalendar.model.Deadline;
import de.hda.fbi.nzse.deadlinecalendar.model.SubmissionType;
import de.hda.fbi.nzse.deadlinecalendar.model.relations.DeadlineToSubmissionType;

/**
 * Deadline to submission type repository
 *
 * @author Anna Kilb
 * @see BaseRepository
 */
public class DeadlineToSubmissionTypeRepository extends BaseRepository {

    public static final String TAG = "DeadlineToSubmissionType";

    public DeadlineToSubmissionTypeRepository(Context context) {
        super(context);
    }

    public void getAll(BaseRepository.ResultCallbackListener<List<DeadlineToSubmissionType>> resultCallbackListener) {
        this.executeDatabaseAction(BaseRepository.DatabaseAction.SELECT, appDatabase -> appDatabase.deadlineToSubmissionTypeDao().getDeadlinesToSubmissionType(), resultCallbackListener);
    }

    /**
     * Updating an existing SubmissionType
     *
     * @param submissionType            to be updated
     */
    public void changeSubmissionType(Deadline deadline, SubmissionType submissionType) {
        deadline.relatedSubmissionTypeID = submissionType.submissionTypeID;

        this.executeDatabaseAction(
                DatabaseAction.UPDATE,
                (appDatabase) -> appDatabase.deadlineDao().update(deadline));
    }

    @Override
    protected String getTag() {
        return TAG;
    }
}