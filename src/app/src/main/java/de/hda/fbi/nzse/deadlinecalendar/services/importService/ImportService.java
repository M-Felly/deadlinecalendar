package de.hda.fbi.nzse.deadlinecalendar.services.importService;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import androidx.annotation.Nullable;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import de.hda.fbi.nzse.deadlinecalendar.manager.ServiceManager;
import de.hda.fbi.nzse.deadlinecalendar.model.Settings;
import de.hda.fbi.nzse.deadlinecalendar.model.repository.SettingsRepository;
import de.hda.fbi.nzse.deadlinecalendar.presenter.ImportServicePresenter;

/**
 * Inner communication interface to handle volley response properly
 */
interface OnPullICSFileHandler {
    void onError(VolleyError error);

    void onSuccess(String icsResponse);
}

/**
 * This service handles the import process.
 * Firstly it pulls the ICS file from the given url and then parse it to the {@link AppointmentParser}
 * <p>
 * Every possible outcome (success, error, exception) will be handled here
 * Its an subject that notifies their observer by state change
 *
 * @author Matthias Feyll
 * @see ImportServiceException
 * @see ImportServicePresenter
 */
public class ImportService extends ServiceManager.AbstractService implements OnPullICSFileHandler {
    public static final String TAG = "ImportService";
    private final SettingsRepository settingsRepository;
    private final Activity activity;

    public enum State {
        VALIDATE, PULL, PARSING, COMPLETE, ERROR, ABORTED
    }

    /**
     * Outer communication interface
     */
    public interface ImportServiceHandler {
        void onFailure(ImportServiceException importServiceException);
    }

    private final ImportServiceHandler handler;

    public ImportService(Activity activity) {
        this.settingsRepository = new SettingsRepository(activity);
        this.activity = activity;
        this.handler = (ImportServiceHandler) activity;
    }


    @Override
    public void run() {
        notifyObservers(State.VALIDATE);
        if (!isConnected()) {
            handler.onFailure(new ImportServiceException(ImportServiceException.HTTP_NOT_CONNECTED));
            return;
        }

        notifyObservers(State.PULL);
        try {
            Settings settings = getSettings().get();
            this.pullICSFile(settings, this);
        } catch (ExecutionException | InterruptedException e) {
            Log.e(TAG, "Something went wrong during the database fetch process: " + e.getMessage());
            this.handler.onFailure(new ImportServiceException(1));
        }
    }

    /***************************************************************
     *                            Actions
     ***************************************************************/
    private void pullICSFile(Settings settings, OnPullICSFileHandler handler) {
        RequestQueue queue = Volley.newRequestQueue(this.activity);
        String url = settings.getUrl().toString();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                handler::onSuccess,
                handler::onError
        );

        queue.add(stringRequest);
    }

    /***************************************************************
     *                       Pull action handler
     ***************************************************************/
    @Override
    public void onError(VolleyError error) {
        notifyObservers(State.ERROR);
        if (error.networkResponse != null) {
            handler.onFailure(new ImportServiceException(error.networkResponse.statusCode));

            return;
        }

        if (error.getCause() instanceof IOException) {
            handler.onFailure(new ImportServiceException(ImportServiceException.GENERAL_NETWORK_ERROR, error.getCause().getMessage()));
        }
    }

    @Override
    public void onSuccess(String icsResponse) {
        notifyObservers(State.PARSING);
        AppointmentParser appointmentParser = new AppointmentParser(icsResponse, this.activity);
        try {
            appointmentParser.run();
            notifyObservers(State.COMPLETE);
        } catch (ImportServiceException exception) {
            notifyObservers(State.ERROR);
            handler.onFailure(exception);
        }
    }


    /***************************************************************
     *                        Helper methods
     ***************************************************************/
    private boolean isConnected() {
        ConnectivityManager connectivityManager = ((ConnectivityManager) this.activity.getSystemService(Context.CONNECTIVITY_SERVICE));

        @Nullable
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();

        return activeNetwork != null && activeNetwork.isConnected();
    }

    private Future<Settings> getSettings() {
        CompletableFuture<Settings> settingsFuture = new CompletableFuture<>();
        this.settingsRepository.getSettings(settingsFuture::complete);

        return settingsFuture;
    }

    @Override
    public void notifyObservers(Object state) {
        setChanged();
        super.notifyObservers(state);
    }
}
