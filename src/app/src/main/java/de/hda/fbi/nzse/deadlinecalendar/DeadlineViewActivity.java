package de.hda.fbi.nzse.deadlinecalendar;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Timer;
import java.util.TimerTask;

import de.hda.fbi.nzse.deadlinecalendar.adapter.DeadlineAdapter;
import de.hda.fbi.nzse.deadlinecalendar.dialogs.AddDeadlineDialog;
import de.hda.fbi.nzse.deadlinecalendar.dialogs.FilterOptionsDialog;
import de.hda.fbi.nzse.deadlinecalendar.factory.DialogFragmentFactory;
import de.hda.fbi.nzse.deadlinecalendar.model.Deadline;
import de.hda.fbi.nzse.deadlinecalendar.model.SubmissionType;
import de.hda.fbi.nzse.deadlinecalendar.model.repository.StudyModuleRepository;
import de.hda.fbi.nzse.deadlinecalendar.model.repository.SubmissionTypeRepository;
import de.hda.fbi.nzse.deadlinecalendar.model.typeconverters.RemainingTimeConverter;
import de.hda.fbi.nzse.deadlinecalendar.presenter.DeadlineAccessHandler;

import static de.hda.fbi.nzse.deadlinecalendar.dialogs.FilterOptionsDialog.ACTION_LISTENER_KEY;
import static de.hda.fbi.nzse.deadlinecalendar.dialogs.FilterOptionsDialog.FILTER_OPTIONS_STATES_KEY;

/**
 * In this activity, the user is able to see his deadlines.
 *
 * Adding new deadline without existing study modules calls the dialog {@link AddDeadlineDialog}
 *
 * @author Dennis Hilz, Anna Kilb
 */
public class DeadlineViewActivity extends AppCompatActivity implements DeadlineAdapter.SelectedDeadline, FilterOptionsDialog.OnClickListener{
    private static final String TAG = "DeadlineViewActivity";
    private BottomNavigationView bottomNav;
    private StudyModuleRepository studyModuleRepository;
    private DeadlineAdapter deadlineAdapter;
    private DeadlineAccessHandler deadlineAccessHandler;
    boolean[] filterOptionStates;
    private static final String FILTER_DEADLINES_DIALOG_TAG = "dialogs";
    public static final int DETAIL_TODO_VIEW_ACTIVITY_REQUEST_CODE = 1;

    /* **************************************************************
     *                     Life cycle hooks
     ****************************************************************/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deadlineview);
        setTitle(R.string.title_deadlineView);

        this.deadlineAccessHandler = new DeadlineAccessHandler(this);

        if (deadlineAccessHandler.getAllSubmissionTypes().size() == 0) {
            addAllPredefinedTypes();
        }

        bottomNav = findViewById(R.id.deadlineViewActivity_bottomNav);
        bottomNav.setOnNavigationItemSelectedListener(new BottomNavigation(this));

        FloatingActionButton addDeadlineBtn = findViewById(R.id.deadlineView_floatingBtn_addDeadline);
        addDeadlineBtn.setOnClickListener(v -> this.onAddDeadlineButtonClicked());

        FloatingActionButton filterDeadlinesBtn = findViewById(R.id.deadlineView_floatingBtn_filterDeadlines);
        filterDeadlinesBtn.setOnClickListener(v -> this.onFilterDeadlinesButtonClicked());

        // recycler view
        RecyclerView deadlinesRecyclerView = findViewById(R.id.deadlineViewActivity_recyclerView_deadlines);
        deadlinesRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        this.studyModuleRepository = new StudyModuleRepository(this);
        this.deadlineAdapter = new DeadlineAdapter(this, deadlineAccessHandler, this);
        deadlinesRecyclerView.setAdapter(deadlineAdapter);

        /*
         * Providing a Timer- and TimerTask-object to periodically refresh the RecyclerView for live time-updates
         * This needs to run on the UiThread!
         */
        Timer refreshTimer = new Timer();
        TimerTask refreshTask = new TimerTask () {

            @Override
            public void run () {
                runOnUiThread(() -> deadlineAdapter.setDeadlineList(deadlineAccessHandler.getDeadlines()));
            }
        };
        refreshTimer.schedule (refreshTask, 0L, RemainingTimeConverter.TIME_FACTOR_MINUTE/2);
    }

    @Override
    protected void onResume() {
        super.onResume();
        deadlineAdapter.setDeadlineList(deadlineAccessHandler.getDeadlines());
        bottomNav.getMenu().findItem(R.id.bottom_nav_deadlines).setChecked(true);
    }

    /**
     * Result handler for finished Deadline from the {@link DetailDeadlineViewActivity}
     *
     * Updates the DeadlineViewAdapter via notifyDataSetChanged {@link DeadlineAdapter}
     *
     * @param requestCode request code is {@link #DETAIL_TODO_VIEW_ACTIVITY_REQUEST_CODE}
     * @param resultCode result code is {@link #RESULT_OK}
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        deadlineAdapter.setDeadlineList(deadlineAccessHandler.getDeadlines());
    }

    /* **************************************************************
     *                      Actions / Listener
     ****************************************************************/

    /**
     * Start AddDeadlineViewActivity
     */
    private void onAddDeadlineButtonClicked() {
        studyModuleRepository.getAll(list -> {
            if (list != null) {
                if (list.size() == 0) {
                    FragmentManager fragmentManager;
                    fragmentManager = (this).getSupportFragmentManager();
                    AddDeadlineDialog addDeadlineDialog = new AddDeadlineDialog();
                    addDeadlineDialog.show(fragmentManager, TAG);
                } else {
                    Intent addDeadlineIntent = new Intent(this, AddDeadlineViewActivity.class);
                    startActivity(addDeadlineIntent);
                }
            }
        });
    }

    /**
     * If one Deadline is selected the Activity DetailDeadlineView is called
     */
    @Override
    public void onDeadlineIsSelected(Deadline deadline) {
        Intent intent = new Intent(this, DetailDeadlineViewActivity.class).putExtra("class", this.getClass().toString()).putExtra("Deadline", deadline);
        this.startActivityForResult(intent, DETAIL_TODO_VIEW_ACTIVITY_REQUEST_CODE);
    }

    public void onFilterDeadlinesButtonClicked() {
        Bundle dialogArguments = new Bundle();
        dialogArguments.putSerializable(ACTION_LISTENER_KEY, this);
        dialogArguments.putSerializable(FILTER_OPTIONS_STATES_KEY, this.filterOptionStates);

        DialogFragment filterOptionsDialog = DialogFragmentFactory.createInstance(FilterOptionsDialog.class, dialogArguments);
        filterOptionsDialog.show(getSupportFragmentManager(), FILTER_DEADLINES_DIALOG_TAG );
    }

    private void addAllPredefinedTypes() {
        SubmissionType sTMoodle = new SubmissionType();
        sTMoodle.setPredefinedType(SubmissionType.Predefined.Moodle);
        SubmissionType sTPresent = new SubmissionType();
        sTPresent.setPredefinedType(SubmissionType.Predefined.Present);
        SubmissionType sTOnline = new SubmissionType();
        sTOnline.setPredefinedType(SubmissionType.Predefined.Online);
        SubmissionTypeRepository submissionTypeRepository = new SubmissionTypeRepository(this);
        submissionTypeRepository.insert(sTMoodle);
        submissionTypeRepository.insert(sTPresent);
        submissionTypeRepository.insert(sTOnline);
    }

    /**
     * This method is calls after submitting the filterOptionsDialog and calls methods within the
     * DeadlineAdapter to apply the filterOptions to the RecyclerView
     *
     * @param filterOptionNames Array of all Names for the filterOptions (studyModules)
     * @param filterOptionStates Array of all checkedStates for the filterOptions (studyModules)
     *
     * Notes: Attributes are named as filterOptions because future methods could provide more filtering
     * options, than just the studyModules.
     */
    @Override
    public void onSubmitDialog(String[] filterOptionNames, boolean[] filterOptionStates) {
        this.filterOptionStates = filterOptionStates;

        StringBuilder filterString = new StringBuilder();
        for (int i = 0; i < filterOptionNames.length; i++) {
            if (filterOptionStates[i])
                filterString.append(filterOptionNames[i]).append(";");
        }
        if (filterString.toString().equals(""))
            deadlineAdapter.nonSelectedFilter();

        deadlineAdapter.getFilter().filter(filterString.toString());
    }
}