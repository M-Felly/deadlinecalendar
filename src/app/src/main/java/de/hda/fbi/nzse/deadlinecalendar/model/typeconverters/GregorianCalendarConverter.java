package de.hda.fbi.nzse.deadlinecalendar.model.typeconverters;

import androidx.room.TypeConverter;

import java.util.GregorianCalendar;

public class GregorianCalendarConverter {
    @TypeConverter
    public static GregorianCalendar dateToCalendar(long date) {
        if (date == 0){
            return null;
        }
        else{
            GregorianCalendar cal= new GregorianCalendar();
            cal.setTimeInMillis(date);
            return cal;
        }
    }

    @TypeConverter
    public static long calendarToLong(GregorianCalendar date) {

        if (date == null){
            return 0;
        }
        else{
            return date.getTimeInMillis();
        }
    }

}