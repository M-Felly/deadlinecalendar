package de.hda.fbi.nzse.deadlinecalendar.model;

/**
 * CalendarElements are used for generalize different items in the CalendarElementsAdapter.
 * Deadlines and Appointments are different classes and are unable to have a common parent class,
 * so these two types will be parsed to an calendar element.
 *
 * The advantage is that all different elements are now comparable and can be shown in the same
 * recycler view.
 */
public class CalendarElement implements Comparable<CalendarElement>{
    
    private final int position;
    final long time;
    final Type type;

    @Override
    public int compareTo(CalendarElement calendarElement) {
        return Long.compare(this.time, calendarElement.time);
    }

    public enum Type{
        APPOINTMENT,DEADLINE
    }
    
    public CalendarElement(int position, long time, Type type){
        this.position = position;
        this.time = time;
        this.type = type;
    }
    
    public static CalendarElement toCalendarElement(Appointment appointment, int position){
        
        return new CalendarElement(position,appointment.getFromDate().getTimeInMillis(),Type.APPOINTMENT);
    }
    
    public static CalendarElement toCalendarElement(Deadline deadline, int position){
        
        return new CalendarElement(position,deadline.getSubmissionDate().getTimeInMillis(),Type.DEADLINE);
    }
    
    
    public int getPosition() {
        return position;
    }

    public Type getType() {
        return type;
    }

}
