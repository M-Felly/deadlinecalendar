package de.hda.fbi.nzse.deadlinecalendar.model.repository;

import android.content.Context;

import java.util.concurrent.Future;

import de.hda.fbi.nzse.deadlinecalendar.model.Settings;

/**
 * Settings repository
 *
 * @author Matthias Feyll
 * @see BaseRepository
 */
public class SettingsRepository extends BaseRepository {
    public static final String TAG = "SettingsRepository";

    public SettingsRepository(Context context) {
        super(context);
    }

    /**
     * Get the settings object
     *
     * @param resultCallbackListener onGet lambda
     */
    public void getSettings(ResultCallbackListener<Settings> resultCallbackListener) {
        executeDatabaseAction(DatabaseAction.SELECT, (appDatabase) -> appDatabase.settingsDao().getFirst(), resultCallbackListener);
    }

    /**
     * Get the settings object asynchronously
     *
     * @return future object containing the settings
     */
    public Future<Settings> getSettingsAsync() {
        return executeAsyncDatabaseAction(DatabaseAction.SELECT, (appDatabase) -> appDatabase.settingsDao().getFirst());
    }

    /**
     * Update the settings
     *
     * @param settings the settings object to be updated
     */
    public void update(Settings settings) {
        executeDatabaseAction(DatabaseAction.UPDATE, appDatabase -> appDatabase.settingsDao().update(settings));
    }

    /**
     * Insert a new Settings object (this will be actually only called ones)
     *
     * @param settings to be inserted
     */
    public void insert(Settings settings) {
        executeDatabaseAction(DatabaseAction.INSERT, appDatabase -> appDatabase.settingsDao().insert(settings), id -> settings.id = id);
    }

    @Override
    protected String getTag() {
        return TAG;
    }
}
