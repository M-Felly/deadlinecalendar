package de.hda.fbi.nzse.deadlinecalendar.model;

import androidx.annotation.NonNull;
import androidx.room.TypeConverter;

import java.io.Serializable;

/**
 * Enum with attribute name tag from colors.xml for {@link StudyModule}
 */
public enum StudyModuleColor implements Serializable {
    FIRST ("study_module_color_first"),
    SECOND ("study_module_color_second"),
    THIRD ("study_module_color_third"),
    FOURTH ("study_module_color_fourth"),
    FIFTH ("study_module_color_fifth"),
    SIXTH ("study_module_color_sixth"),
    SEVENTH ("study_module_color_seventh");

    /**
     * Attribute name tag of colors.xml
     */
    private final String attributeNameTag;

    StudyModuleColor(String attributeNameTag) {
        this.attributeNameTag = attributeNameTag;
    }

    public String getAttributeNameTag() {
        return attributeNameTag;
    }

    @NonNull
    @Override
    public String toString() {
        return this.attributeNameTag;
    }

    // converter
    public static class Converter {
        @TypeConverter
        public String enumToString(StudyModuleColor studyModuleColor) {
            return studyModuleColor.toString();
        }

        @TypeConverter
        public StudyModuleColor stringToEnum(String attributeNameTag) {

            for (StudyModuleColor studyModuleColor : StudyModuleColor.values()) {
                if (studyModuleColor.toString().equals(attributeNameTag)) {
                    return studyModuleColor;
                }
            }

            throw new NullPointerException();
        }
    }
}
