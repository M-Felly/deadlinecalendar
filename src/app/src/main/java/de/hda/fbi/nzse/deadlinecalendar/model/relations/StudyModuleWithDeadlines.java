package de.hda.fbi.nzse.deadlinecalendar.model.relations;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

import de.hda.fbi.nzse.deadlinecalendar.model.Deadline;
import de.hda.fbi.nzse.deadlinecalendar.model.StudyModule;

/**
 * One study module to many deadlines relation class
 *
 * @author Matthias Feyll
 */
public class StudyModuleWithDeadlines {
    @Embedded
    public StudyModule studyModule;

    @Relation(
            parentColumn = "studyModuleID",
            entityColumn = "relatedStudyModuleID"
    )
    public List<Deadline> deadlineList;
}
