package de.hda.fbi.nzse.deadlinecalendar.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import de.hda.fbi.nzse.deadlinecalendar.R;

/**
 * This dialog is getting display from {@link de.hda.fbi.nzse.deadlinecalendar.DeadlineViewActivity}
 * when a you want to add a deadline but there are no study modules.
 *
 * @author Anna Kilb
 */
public class AddDeadlineDialog extends DialogFragment {

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_adddeadline_nostudymodules, null);
        builder.setView(dialogView);

        // ok button
        Button okBtn = dialogView.findViewById(R.id.addDeadlineDialog_button_ok);
        okBtn.setOnClickListener(v -> dismiss());

        return builder.create();
    }
}
