package de.hda.fbi.nzse.deadlinecalendar.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import de.hda.fbi.nzse.deadlinecalendar.R;
import de.hda.fbi.nzse.deadlinecalendar.presenter.ImportServiceExceptionPresenter;
import de.hda.fbi.nzse.deadlinecalendar.services.importService.ImportServiceException;

public class ImportServiceFeedbackDialog extends DialogFragment {
    public final static String IMPORT_SERVICE_EXCEPTION_KEY = "importServiceExceptionKey";

    private ImportServiceException importServiceException;

    private TextView errorTitle;
    private TextView errorDescription;
    private TextView fixSuggestion;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Bundle arguments = getArguments();

        if (arguments == null) {
            throw new Error("Arguments missing");
        }

        this.importServiceException = (ImportServiceException) arguments.getSerializable(IMPORT_SERVICE_EXCEPTION_KEY);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_importservice_feedback, null);
        builder.setView(dialogView);

        // elements
        this.errorTitle = dialogView.findViewById(R.id.importServiceFeedbackDialog_textView_errorTitle);
        this.errorDescription = dialogView.findViewById(R.id.importServiceFeedbackDialog_textView_errorDescription);
        this.fixSuggestion = dialogView.findViewById(R.id.importServiceFeedbackDialog_textView_fixSuggestion);

        // submit button
        ImageButton submitBtn = dialogView.findViewById(R.id.importServiceFeedbackDialog_imageButton_submit);
        submitBtn.setOnClickListener(v -> dismiss());

        this.displayErrorMessages();

        return builder.create();
    }

    /* **************************************************************
     *                          Display
     ****************************************************************/
    public void displayErrorMessages() {
        ImportServiceExceptionPresenter importExceptionPresenter = new ImportServiceExceptionPresenter(importServiceException);

        this.errorTitle.setText(this.getString(importExceptionPresenter.getErrorTitle()));
        this.errorDescription.setText(this.getString(importExceptionPresenter.getErrorDescription()));
        this.fixSuggestion.setText(this.getString(importExceptionPresenter.getFixSuggestion()));
    }
}
