package de.hda.fbi.nzse.deadlinecalendar;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.Spannable;
import android.text.style.StyleSpan;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.Locale;

import de.hda.fbi.nzse.deadlinecalendar.adapter.DetailDeadline_ToDoAdapter;
import de.hda.fbi.nzse.deadlinecalendar.dialogs.DeadlineCompleteDialog;
import de.hda.fbi.nzse.deadlinecalendar.factory.DialogFragmentFactory;
import de.hda.fbi.nzse.deadlinecalendar.model.Deadline;
import de.hda.fbi.nzse.deadlinecalendar.model.ToDo;
import de.hda.fbi.nzse.deadlinecalendar.model.typeconverters.RemainingTimeConverter;
import de.hda.fbi.nzse.deadlinecalendar.presenter.DeadlineAccessHandler;
import de.hda.fbi.nzse.deadlinecalendar.presenter.SubmissionTypePresenter;
import de.hda.fbi.nzse.deadlinecalendar.view_elements.custom.StudyModuleShape;

import static de.hda.fbi.nzse.deadlinecalendar.dialogs.DeadlineDeleteDialog.ACTION_LISTENER_KEY;
import static de.hda.fbi.nzse.deadlinecalendar.manager.NotificationManager.NOTIFICATION_DEADLINE_INTENT;
import static de.hda.fbi.nzse.deadlinecalendar.receiver.NotificationReceiver.BUNDLE_EXTRA;

/**
 * In this activity the user can view details of a deadline.
 * He/She is also able to add or delete toDos.
 *
 * @author Dennis Hilz, Anna Kilb
 */
@SuppressWarnings("ConstantConditions")
public class DetailDeadlineViewActivity extends AppCompatActivity implements ViewTreeObserver.OnPreDrawListener, Serializable, DeadlineCompleteDialog.OnClickListener {
    public static final int RECYCLER_VIEW_MARGIN_BOTTOM = 500;
    private static final String TAG = "DetailDeadlineViewActivity";

    private BottomNavigationView bottomNav;
    private DeadlineAccessHandler deadlineAccessHandler;
    private DetailDeadline_ToDoAdapter detailDeadlineToDoAdapter;
    private Deadline deadline;

    private TextView completion_textView;
    private ConstraintLayout recyclerViewLayout;
    private RecyclerView toDosRecyclerView;
    private ImageButton addToDo_imageButton;
    private EditText newToDoDescription_editText;
    private GregorianCalendar selectedDate;
    private ParentClass callingParentClass;

    enum ParentClass {CALENDARVIEWACTIVITY, DEADLINEVIEWACTIVITY}

    /* **************************************************************
     *                     Life cycle hooks
     ****************************************************************/

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_deadline_view);
        setTitle("Details");

        //back button
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        bottomNav = findViewById(R.id.detailDeadlineViewActivity_bottomNav);
        bottomNav.setOnNavigationItemSelectedListener(new BottomNavigation(this));

        StudyModuleShape studyModuleShape = findViewById(R.id.detailDeadlineViewActivity_tagShape_tag);
        TextView deadlineTitle_textView = findViewById(R.id.detailDeadlineViewActivity_textView_name);
        TextView date_textView = findViewById(R.id.detailDeadlineViewActivity_textView_date);
        TextView time_textView = findViewById(R.id.detailDeadlineViewActivity_textView_time);
        TextView counter_textView = findViewById(R.id.detailDeadlineViewActivity_textView_counter);
        this.completion_textView = findViewById(R.id.detailDeadlineViewActivity_textView_completion);
        TextView reminder_textView = findViewById(R.id.detailDeadlineViewActivity_textView_reminderDate);
        this.toDosRecyclerView = findViewById(R.id.detailDeadlineViewActivity_recyclerView_toDos);
        this.recyclerViewLayout = findViewById(R.id.detailDeadlineViewActivity_constraintLayout_recyclerViewLayout);
        this.addToDo_imageButton = findViewById(R.id.detailDeadlineViewActivity_imageButton_addToDo);
        this.newToDoDescription_editText = findViewById(R.id.detailDeadlineViewActivity_editText_newToDoDescription);
        TextView submissionType_textView = findViewById(R.id.detailDeadlineViewActivity_textView_submissionType);
        Button finishDeadlineButton = findViewById(R.id.detailDeadlineViewActivity_button_done);

        finishDeadlineButton.setOnClickListener(this::onFinishDeadlineBtnClicked);

        this.deadlineAccessHandler = new DeadlineAccessHandler(this);
        this.deadline = getDeadlineFromIntent();
        this.toDosRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        this.detailDeadlineToDoAdapter = new DetailDeadline_ToDoAdapter(deadlineAccessHandler, deadline, this);
        this.toDosRecyclerView.setAdapter(detailDeadlineToDoAdapter);
        this.toDosRecyclerView.getViewTreeObserver().addOnPreDrawListener(this);

        // StudyModule Deadline
        studyModuleShape.setStudyModule(deadlineAccessHandler.getStudyModule(deadline));

        // Title Deadline
        deadlineTitle_textView.setText(deadline.getTitle());

        // Date
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.forLanguageTag(String.valueOf(R.string.languageTag)));
        date_textView.setText(dateFormat.format(deadline.getSubmissionDate().getTime()));

        // Time
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm", Locale.forLanguageTag(String.valueOf(R.string.languageTag)));
        time_textView.setText(timeFormat.format(deadline.getSubmissionDate().getTime()));

        // Counter Time
        counter_textView.setText(RemainingTimeConverter.assembleOutputStringDetail(RemainingTimeConverter.parseRemainingTime(this, deadline), this));

        // Completion ToDos
        int finishedToDos = 0;
        for (ToDo toDo : deadlineAccessHandler.getToDos(deadline)) {
            if (toDo.getIsDone())
                finishedToDos++;
        }
        String completionState = finishedToDos + "/" + deadlineAccessHandler.getToDos(deadline).size() + " ToDos";
        completion_textView.setText(completionState);

        // Reminder
        reminder_textView.setText(Deadline.notificationTypeToString(this, deadline.getNotificationType()));

        // add ToDo
        newToDoDescription_editText.setOnKeyListener((v, keyCode, event) -> {
            // If the event is a key-down event on the "enter" button
            if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                // Perform action on key press
                onAddToDoBtnClicked(null);
                return true;
            }
            return false;
        });
        addToDo_imageButton.setOnClickListener(this::onAddToDoBtnClicked);

        // SubmissionType
        CharSequence text_SubmissionType = submissionType_textView.getText();
        SubmissionTypePresenter submissionTypePresenter = new SubmissionTypePresenter(this,deadlineAccessHandler.getSubmissionType(deadline));
        submissionType_textView.setText(text_SubmissionType + " " + submissionTypePresenter.getSubmissionType(), TextView.BufferType.SPANNABLE);
        Spannable s = (Spannable) submissionType_textView.getText();
        s.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), text_SubmissionType.length(), submissionType_textView.getText().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE); // Bold text
    }

    @Override
    protected void onResume() {
        super.onResume();
        bottomNav.getMenu().findItem(R.id.bottom_nav_deadlines).setChecked(true);
    }

    /* **************************************************************
     *                         Navigation
     ****************************************************************/

    @Override
    public boolean onSupportNavigateUp() {

        if(callingParentClass == ParentClass.CALENDARVIEWACTIVITY){
            startActivity(new Intent(DetailDeadlineViewActivity.this, CalendarViewActivity.class).putExtra("class", this.getClass().toString()).putExtra("selectedDate", selectedDate));
        }
        else{
            startActivity(new Intent(DetailDeadlineViewActivity.this, DeadlineViewActivity.class).putExtra("class", this.getClass().toString()));
        }

        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.detail_view_nav_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.actionBarNext_forwardArrow) {
            if (callingParentClass == ParentClass.CALENDARVIEWACTIVITY) {
                startActivity(new Intent(DetailDeadlineViewActivity.this, EditDeadlineViewActivity.class).putExtra("class", CalendarViewActivity.class.toString()).putExtra("Deadline", deadline).putExtra("selectedDate", selectedDate));
            } else {
                startActivity(new Intent(DetailDeadlineViewActivity.this, EditDeadlineViewActivity.class).putExtra("class", DeadlineViewActivity.class.toString()).putExtra("Deadline", deadline));
            }

            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    /* **************************************************************
     *                      Actions / Listener
     ****************************************************************/


    private void onFinishDeadlineBtnClicked(View view) {
        Bundle dialogArguments = new Bundle();
        dialogArguments.putSerializable(ACTION_LISTENER_KEY, this);

        DialogFragment deadlineCompleteDialog = DialogFragmentFactory.createInstance(DeadlineCompleteDialog.class, dialogArguments);
        deadlineCompleteDialog.show(getSupportFragmentManager(), TAG);
    }

    private void onAddToDoBtnClicked(View view) {
        String toDoDescription = newToDoDescription_editText.getText().toString();
        if (!toDoDescription.equals("")) {
            ToDo newToDo = new ToDo(toDoDescription);
            this.deadlineAccessHandler.addToDo(deadline, newToDo);
            detailDeadlineToDoAdapter.addToDo(newToDo);
            newToDoDescription_editText.setText("");
            toDosRecyclerView.getAdapter().notifyDataSetChanged();

            // Completion ToDos
            int finishedToDos = 0;
            for (ToDo toDo : deadlineAccessHandler.getToDos(deadline)) {
                if (toDo.getIsDone())
                    finishedToDos++;
            }
            updateCompletionState(finishedToDos, deadlineAccessHandler.getToDos(deadline).size());
        }
    }

    @Override
    public void onSubmitDialog() {
        this.deadline.setDone(true);
        this.deadlineAccessHandler.updateDeadline(this.deadline);
        Intent intent = new Intent();
        if(callingParentClass == ParentClass.CALENDARVIEWACTIVITY){
            startActivity(new Intent(DetailDeadlineViewActivity.this, CalendarViewActivity.class).putExtra("class", this.getClass().toString()).putExtra("selectedDate", selectedDate));
        }
        else{
            setResult(RESULT_OK, intent);
        }

        finish();
    }

    public void updateCompletionState(int finishedToDos, int allToDos) {
        String completionState = finishedToDos + "/" + allToDos + " ToDos";
        completion_textView.setText(completionState);
    }

    /* **************************************************************
     *                            Drawing
     ****************************************************************/

    /**
     * Calculate max height for {@link #toDosRecyclerView}
     * @return hook used
     */
    @Override
    public boolean onPreDraw() {
        ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) this.recyclerViewLayout.getLayoutParams();
        layoutParams.matchConstraintMaxHeight = calculateRecyclerViewHeight();
        this.recyclerViewLayout.setLayoutParams(layoutParams);

        this.toDosRecyclerView.getViewTreeObserver().removeOnPreDrawListener(this);

        return true;
    }

    /* **************************************************************
     *                         Calculating
     ****************************************************************/

    /**
     * Calculating height for recycler view. This is necessary make the recycler view wrap content during
     * less height to be displayed but also scrollable with a max height on too much items that
     * have to be displayed.
     *
     * @return calculated height
     */
    private int calculateRecyclerViewHeight() {
        int topBorder = 0, bottomBorder;

        // if orientation is portrait get top height from divider position
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            int[] topBorderLocation = {0, 0};
            View divider = findViewById(R.id.detailDeadlineViewActivity_view_horizontalLineRecyclerView);
            divider.getLocationOnScreen(topBorderLocation);
            topBorder = topBorderLocation[1];
        }else {
            // if orientation = landscape set topBorder = actionBar height
            TypedValue tv = new TypedValue();
            if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
                topBorder = TypedValue.complexToDimensionPixelSize(tv.data,getResources().getDisplayMetrics()) + 50;
            }
        }

        // get the bottomBorder from the absolute bottom nav y position
        int[] bottomBorderLocation  = {0, 0};
        bottomNav.getLocationOnScreen(bottomBorderLocation);
        bottomBorder = bottomBorderLocation[1];

        return bottomBorder - topBorder - addToDo_imageButton.getHeight() - RECYCLER_VIEW_MARGIN_BOTTOM;
    }


    /* **************************************************************
     *                        Intent resolver
     ****************************************************************/

    private Deadline getDeadlineFromIntent() {

        Bundle bundle = getIntent().getBundleExtra(BUNDLE_EXTRA);
        if (bundle != null) {
            deadline = (Deadline) bundle.getSerializable(NOTIFICATION_DEADLINE_INTENT);

            if (deadline != null) {
                callingParentClass = ParentClass.DEADLINEVIEWACTIVITY;
                return deadline;
            }
        }

        Bundle allExtra = getIntent().getExtras();

        if(allExtra.getString("class").equals(CalendarViewActivity.class.toString())){
            callingParentClass = ParentClass.CALENDARVIEWACTIVITY;
            selectedDate = (GregorianCalendar) allExtra.getSerializable("selectedDate");
        }
        else{
            callingParentClass = ParentClass.DEADLINEVIEWACTIVITY;
        }
        Deadline deadline;
        deadline = (Deadline) allExtra.getSerializable(NOTIFICATION_DEADLINE_INTENT);

        if (deadline != null) {
            return deadline;
        }


        throw new Error("Missing extra in intent");
    }
}