package de.hda.fbi.nzse.deadlinecalendar.model.database;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import de.hda.fbi.nzse.deadlinecalendar.model.Appointment;
import de.hda.fbi.nzse.deadlinecalendar.model.Deadline;
import de.hda.fbi.nzse.deadlinecalendar.model.Profile;
import de.hda.fbi.nzse.deadlinecalendar.model.Settings;
import de.hda.fbi.nzse.deadlinecalendar.model.StudyModule;
import de.hda.fbi.nzse.deadlinecalendar.model.SubmissionType;
import de.hda.fbi.nzse.deadlinecalendar.model.ToDo;
import de.hda.fbi.nzse.deadlinecalendar.model.dao.AppointmentDao;
import de.hda.fbi.nzse.deadlinecalendar.model.dao.DeadlineDao;
import de.hda.fbi.nzse.deadlinecalendar.model.dao.DeadlineToStudyModuleDao;
import de.hda.fbi.nzse.deadlinecalendar.model.dao.DeadlineToSubmissionTypeDao;
import de.hda.fbi.nzse.deadlinecalendar.model.dao.DeadlineWithToDosDao;
import de.hda.fbi.nzse.deadlinecalendar.model.dao.ProfileDao;
import de.hda.fbi.nzse.deadlinecalendar.model.dao.ProfileWithStudyModulesDao;
import de.hda.fbi.nzse.deadlinecalendar.model.dao.SettingsDao;
import de.hda.fbi.nzse.deadlinecalendar.model.dao.StudyModuleDao;
import de.hda.fbi.nzse.deadlinecalendar.model.dao.StudyModuleWithDeadlinesDao;
import de.hda.fbi.nzse.deadlinecalendar.model.dao.SubmissionTypeDao;

@Database(entities = {Appointment.class, Deadline.class,Profile.class, Settings.class, StudyModule.class, SubmissionType.class, ToDo.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract AppointmentDao appointmentDao();
    public abstract ProfileDao profileDao();
    public abstract ProfileWithStudyModulesDao profileWithStudyModulesDao();
    public abstract SettingsDao settingsDao();
    public abstract DeadlineDao deadlineDao();
    public abstract DeadlineToStudyModuleDao deadlineToStudyModuleDao();
    public abstract DeadlineToSubmissionTypeDao deadlineToSubmissionTypeDao();
    public abstract DeadlineWithToDosDao deadlineWithToDosDao();
    public abstract SubmissionTypeDao submissionTypeDao();
    public abstract StudyModuleDao studyModuleDao();
    public abstract StudyModuleWithDeadlinesDao studyModuleWithDeadlinesDao();

    public static final String DATABASE_NAME = "deadlinecalendar_db";
    private static volatile AppDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 4;
    public static final ExecutorService databaseExecutor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    static final Migration MIGRATION_1_2 = new Migration(1,2){

        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
        }
    };

    public static AppDatabase getDatabase(final Context context) {
        if (INSTANCE == null){
            synchronized (AppDatabase.class){
                if (INSTANCE == null){
                    INSTANCE = Room.databaseBuilder
                            (context.getApplicationContext(),AppDatabase.class, DATABASE_NAME).
                            addMigrations(MIGRATION_1_2).build();
                }
                
            }
        }
        return INSTANCE;
    }

}


