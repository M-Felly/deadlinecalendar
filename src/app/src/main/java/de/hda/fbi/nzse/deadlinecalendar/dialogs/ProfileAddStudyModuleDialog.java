package de.hda.fbi.nzse.deadlinecalendar.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import java.io.Serializable;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import de.hda.fbi.nzse.deadlinecalendar.R;
import de.hda.fbi.nzse.deadlinecalendar.model.StudyModule;
import de.hda.fbi.nzse.deadlinecalendar.model.StudyModuleColor;
import de.hda.fbi.nzse.deadlinecalendar.model.repository.StudyModuleRepository;
import de.hda.fbi.nzse.deadlinecalendar.view_elements.extended.DialogEditText;

/**
 * This class handles a dialog to with input fields to add a new StudyModule.
 * <p>
 * Due to the complex process creating the next StudyModuleColor the class does not create the
 * StudyModule object itself but return the tag and name attribute as listener
 *
 * @author Matthias Feyll
 * @see OnClickListener
 */
public class ProfileAddStudyModuleDialog extends DialogFragment {
    public final static String ACTION_LISTENER_KEY = "actionListener";
    public final static String STUDY_MODULE_COLOR_KEY = "studyModuleColor";
    public static final String TAG = "ProfileAddStudyModuleDialog";

    /**
     * Callback listener to get the resolved attributes.
     *
     * @see ProfileAddStudyModuleDialog
     */
    public interface OnClickListener extends Serializable {
        void onSubmitDialog(StudyModule studyModule);
    }

    /* **************************************************************
     *                            Class
     ****************************************************************/
    private OnClickListener actionListener;
    private StudyModuleColor studyModuleColor;
    private DialogEditText tagEditText;
    private DialogEditText nameEditText;

    /**
     * Creates the dialog and set the click listener for back and submit button
     */
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle arguments = getArguments();

        if (arguments == null) {
            throw new Error("Arguments missing");
        }

        this.actionListener = (OnClickListener) arguments.getSerializable(ACTION_LISTENER_KEY);
        this.studyModuleColor = (StudyModuleColor) arguments.getSerializable(STUDY_MODULE_COLOR_KEY);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_profile_add_study_module, null);
        builder.setView(dialogView);

        // elements
        this.tagEditText = dialogView.findViewById(R.id.profileAddStudyModuleDialog_editText_tag);
        this.nameEditText = dialogView.findViewById(R.id.profileAddStudyModuleDialog_editText_name);

        // back button
        ImageButton backBtn = dialogView.findViewById(R.id.profileAddStudyModuleDialog_imageButton_back);
        backBtn.setOnClickListener(v -> dismiss());

        // submit button
        ImageButton submitBtn = dialogView.findViewById(R.id.profileAddStudyModuleDialog_imageButton_submit);
        submitBtn.setOnClickListener(v -> onSubmitBtnClicked());

        return builder.create();
    }

    /* **************************************************************
     *                     Action / Listener
     ****************************************************************/

    /**
     * Get text out of elements and call the listener
     */
    private void onSubmitBtnClicked() {
        if (!this.isFormValid()) {
            return;
        }

        String tag = Objects.requireNonNull(this.tagEditText.getText()).toString();
        String name = Objects.requireNonNull(this.nameEditText.getText()).toString();

        StudyModule studyModule = new StudyModule(tag, name, this.studyModuleColor);

        this.actionListener.onSubmitDialog(studyModule);
        dismiss();
    }

    /* **************************************************************
     *                          Validity
     ****************************************************************/

    /**
     * Check if form is valid.
     * <p>
     * Tag name must not have a length greater then {@link StudyModule#MAX_TAG_LENGTH}
     *
     * @return boolean whether the form is valid or not
     */
    private boolean isFormValid() {
        if (Objects.requireNonNull(this.tagEditText.getText()).length() > StudyModule.MAX_TAG_LENGTH || this.tagEditText.getText().toString().isEmpty()) {
            this.tagEditText.setError(getString(R.string.ProfileAddStudyModuleDialog_editText_tag_invalid));

            return false;
        }

        if (Objects.requireNonNull(this.nameEditText.getText()).toString().isEmpty()) {
            this.nameEditText.setError(getString(R.string.ProfileAddStudyModuleDialog_editText_name_invalid));

            return false;
        }

        StudyModuleRepository studyModuleRepository = new StudyModuleRepository(getContext());
        Future<Boolean> response = studyModuleRepository.studyModuleAlreadyExists(this.tagEditText.getText().toString());


        try {
            if (response.get()) {
                this.tagEditText.setError(getString(R.string.ProfileAddStudyModuleDialog_editText_tag_exists));

                return false;
            }
        } catch (ExecutionException | InterruptedException e) {
            Log.e(TAG, "Something went wrong during the database fetch process: " + e.getMessage());
        }

        return true;
    }
}
