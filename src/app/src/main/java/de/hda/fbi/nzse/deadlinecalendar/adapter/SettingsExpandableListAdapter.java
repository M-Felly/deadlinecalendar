package de.hda.fbi.nzse.deadlinecalendar.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Observer;

import de.hda.fbi.nzse.deadlinecalendar.R;
import de.hda.fbi.nzse.deadlinecalendar.adapter.settingsViewHolder.HelperViewHolder;
import de.hda.fbi.nzse.deadlinecalendar.adapter.settingsViewHolder.ImportServiceViewHolder;
import de.hda.fbi.nzse.deadlinecalendar.adapter.settingsViewHolder.NotificationViewHolder;
import de.hda.fbi.nzse.deadlinecalendar.model.Settings;

/**
 * This is a adapter for the expandable list view in the settings view.
 *
 * It creates expandable bodies based on the inheriting ChildViewHolders
 * @author Matthias Feyll
 */
public class SettingsExpandableListAdapter extends BaseExpandableListAdapter {
    /**
     * Listener interface for inflating component
     */
    public interface ExpandableListItemListener {
        void onImportBtnClicked(String importUrl);
    }

    /**
     * Declare hooks during child view create
     */
    private interface ChildViewHook {
        void onCreateChildView(View v);
    }

    private final Activity activity;
    private final List<ChildViewHolder> childViewHolderList;
    private final ExpandableListItemListener actionListener;

    public SettingsExpandableListAdapter(Activity activity) {
        this.activity = activity;
        this.actionListener = (ExpandableListItemListener) activity;
        this.childViewHolderList = collectViewHolder();
    }

    /* **************************************************************
     *                     ChildViewHolder
     ****************************************************************/

    /**
     * Abstract class to define a ChildViewHolder
     */
    public abstract static class ChildViewHolder implements ChildViewHook, Observer {
        private final int layoutID;
        private final int titleID;

        public ChildViewHolder(int layoutID, int titleID) {
            this.layoutID = layoutID;
            this.titleID = titleID;
        }

        public int getLayoutID() {
            return layoutID;
        }

        public int getTitleID() {
            return titleID;
        }
    }


    /**
     * Creates a list with all child view holder extended by {@link ChildViewHolder}
     *
     * @return list with collected view holder
     */
    public List<ChildViewHolder> collectViewHolder() {
        List<ChildViewHolder> list = new ArrayList<>();
        list.add(new ImportServiceViewHolder(actionListener, this.activity));
        list.add(new NotificationViewHolder(this.activity));
        list.add(new HelperViewHolder());

        return list;
    }


    /* **************************************************************
     *                       Create views
     ****************************************************************/

    /**
     * Create expandable head view
     * @return view element
     */
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        // take the already build group view
        if (convertView == null) {
            LayoutInflater groupInflater = LayoutInflater.from(parent.getContext());
            convertView = groupInflater.inflate(R.layout.adapter_settings_group_head_view, parent, false);
        }

        // set title text
        TextView title = convertView.findViewById(R.id.adapterSettingsGroupListView_textView_title);
        title.setText(this.activity.getString(this.childViewHolderList.get(groupPosition).getTitleID()));

        return convertView;
    }

    /**
     * Create expandable body view
     * @return view element
     */
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        LayoutInflater childInflater = LayoutInflater.from(parent.getContext());
        View view = childInflater.inflate(this.childViewHolderList.get(groupPosition).getLayoutID(), null);

        childViewHolderList.get(groupPosition).onCreateChildView(view);

        return view;
    }

    /***************************************************************
     *                        Setter
     ***************************************************************/
    public void setSettings(Settings settings) {
        ImportServiceViewHolder importServiceViewHolder = (ImportServiceViewHolder) this.childViewHolderList.get(0);
        NotificationViewHolder notificationViewHolder = (NotificationViewHolder) this.childViewHolderList.get(1);

        importServiceViewHolder.setSettings(settings);
        notificationViewHolder.setSettings(settings);

        notifyDataSetChanged();
    }



    /***************************************************************
     *                         Getter
     ***************************************************************/

    @Override
    public Object getGroup(int groupPosition) {
        return this.childViewHolderList.get(groupPosition);
    }

    /**
     * Unused due to the fact that we don't have child elements
     */
    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public int getGroupCount() {
        return this.childViewHolderList.size();
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public <ReturnType> ReturnType getChildViewHolderList(Class<ReturnType> childViewHolderClass) {
        for (ChildViewHolder childViewHolder : this.childViewHolderList) {
            if (childViewHolder.getClass().equals(childViewHolderClass)) {
                return childViewHolderClass.cast(childViewHolder);
            }
        }

        throw new Error(childViewHolderClass.toString() + " not found");
    }
}
