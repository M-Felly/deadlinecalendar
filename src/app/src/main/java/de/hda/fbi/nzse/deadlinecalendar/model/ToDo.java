package de.hda.fbi.nzse.deadlinecalendar.model;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.io.Serializable;

/**
 * A ToDo_ represents a single Task included in a Deadline.
 *
 * By default a ToDo_ is not done.
 */

@Entity
public class ToDo implements Serializable {

    @PrimaryKey(autoGenerate = true)
    public long toDoID;

    public long relatedDeadlineID;

    private String description;
    private boolean isDone;

    @Ignore
    public ToDo(String description) {
        this.description = description;
        this.isDone = false;
    }

    public ToDo(){}

    /* **************************************************************
     *                      Getter / Setter
     ****************************************************************/
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getIsDone() {
        return isDone;
    }

    public void setIsDone(boolean done) {
        isDone = done;
    }
}
