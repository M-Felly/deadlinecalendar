package de.hda.fbi.nzse.deadlinecalendar.model.repository;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import java.util.Collection;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import de.hda.fbi.nzse.deadlinecalendar.model.database.AppDatabase;


/**
 * Oh boy a lot of code. But don't worry. There just a few things to know:
 * <p>
 * The main purpose of this class is to make the database more talkative to you by logging (Logcat) all
 * operations. Furthermore the database throws an error if you accidentally delete or update NON! (not interchanged)
 * existing objects in database. It can save you a lot of pain :)
 *
 * <h2>General explanation</h2>
 * A repository links between the DAOs and an Activity that requires data from the database.
 * It helps you to outsource database calls from your activity. Another afford is that you don't
 * repeat yourself by coding the whole database fetch process for multiple activities that need the same data objects.
 *
 * <h2>Usage</h2>
 * 1. Extending this class to your new Repository class
 * 2. Create a TAG -> public static final TAG = "MyClassRepository"
 * 3. Return the TAG in the getTag() method.
 * 4. Create public methods how your designated class can be modified (e.g. insert(MyClass myclass), update(MyClass myclass), etc.)
 * 5. In your respective method call the executeDatabaseAction() method with the following two / three parameters:
 * 5.1 The needed {@link DatabaseAction} eg. INSERT or UPDATE
 * 5.2 Implement the lambda function by calling the actual DAO method that matches your current action
 * (the method names do not have to be equal but it would make sense to)
 * 5.3 (Optional) pass the {@link ResultCallbackListener} as parameter from your created method to
 * make a callback to your invoking method (mostly your Activity)
 * 5.3.1 Set the correct DatabaseResultType see {@link ResultCallbackListener} for more information
 * <p>
 * <p>
 * Example repository:
 *
 * @author Matthias Feyll
 * @see StudyModuleRepository
 */
public abstract class BaseRepository {
    private static final String TAG = "BaseRepository";

    /**
     * This interface getting implemented in the Activity. The lambda function contains the received
     * data from the database
     *
     *
     * <h2>Generic (DatabaseResultType)</h2>
     * The generic type is just required for {@link DatabaseAction#SELECT} actions.
     * Take the following types respectively for each {@link DatabaseAction} type
     * - UPDATE = Integer
     * - DELETE = Integer
     * - INSERT = Long
     * - SELECT = <your expected object that will be returned
     * from database (e.g. List<Deadline> or DeadlineWithToDos etc.)>
     *
     * @param <DatabaseResultType> the expected result type. (see description above)
     */
    @FunctionalInterface
    public interface ResultCallbackListener<DatabaseResultType> {
        void onSuccess(DatabaseResultType data);
    }

    /**
     * Internal interface that will be created in the inherited repositories to execute
     * the designated DAO
     *
     * @param <DatabaseResultType> the expected result type. Will be set implicitly (so you don't have to worry about this)
     */
    @FunctionalInterface
    protected interface DatabaseHandler<DatabaseResultType> {
        DatabaseResultType execute(@NonNull AppDatabase appDatabase);
    }

    private final Context context;

    public BaseRepository(Context context) {
        this.context = context;
    }

    /***************************************************************
     *                      DatabaseAction Enum
     ***************************************************************/
    public enum DatabaseAction {
        INSERT("INSERT"),
        UPDATE("UPDATE"),
        DELETE("DELETE"),
        SELECT("SELECT"),
        ;

        private final String action;

        DatabaseAction(String action) {
            this.action = action;
        }

        @NonNull
        @Override
        public String toString() {
            return this.action;
        }
    }


    /* **************************************************************
     *                   Database request executor
     ****************************************************************/

    /**
     * This is the control center of a database call.
     * The method executes the database action by calling the given lambda with the databaseHandler.
     * The return value going to be checked according to the executed database action.
     * Before return the database execution is getting logged.
     * <p>
     * See the following access methods for the actual repos
     *
     * @param action               action type that will be executed
     * @param databaseHandler      which provides a lambda function with the actual dao method call (yes this could be more generic by parsing the exact dao reflection class and refer to the desired method to be executed. But I'm also just a human and need time for sleep lol)
     * @param <DatabaseResultType> the expected database result type
     * @return database return value (either the SELECTED value or a status code on UPDATE, DELETE or INSERT)
     * @see #executeDatabaseAction(DatabaseAction, DatabaseHandler)
     * @see #executeDatabaseAction(DatabaseAction, DatabaseHandler, ResultCallbackListener)
     * @see #executeAsyncDatabaseAction(DatabaseAction, DatabaseHandler)
     */
    @NonNull
    private <DatabaseResultType> Future<DatabaseResultType> _executeDatabaseAction(DatabaseAction action, DatabaseHandler<DatabaseResultType> databaseHandler) {
        CompletableFuture<DatabaseResultType> databasePromise = new CompletableFuture<>();
        AppDatabase.databaseExecutor.execute(() -> {
            DatabaseResultType databaseResult = databaseHandler.execute(AppDatabase.getDatabase(context));

            if (action != DatabaseAction.SELECT) {
                this.resultHandler(databaseResult, action);
            } else {
                Log.i(this.getTag(), action.toString() + " | Returning: " + (databaseResult == null ? "null" : databaseResult.toString()));
            }

            databasePromise.complete(databaseResult);
        });

        return databasePromise;
    }

    /**
     * @param action               action type that will be executed
     * @param databaseHandler      which provides a lambda function with the actual dao method call
     * @param <DatabaseResultType> the expected database result type
     */
    protected <DatabaseResultType> void executeDatabaseAction(DatabaseAction action, DatabaseHandler<DatabaseResultType> databaseHandler, @NonNull ResultCallbackListener<DatabaseResultType> resultCallbackListener) {
        Future<DatabaseResultType> databasePromise = this._executeDatabaseAction(action, databaseHandler);

        try {
            DatabaseResultType databaseResult = databasePromise.get();
            resultCallbackListener.onSuccess(databaseResult);
        } catch (InterruptedException | ExecutionException exception) {
            Log.e(TAG, "Critical: Something went wrong during the database execute process: " + exception.getMessage());
        }
    }

    /**
     * This is a wrapper method for database actions who doesn't need a callback (e.g. UPDATE)
     *
     * @param action               action type that will be executed
     * @param databaseHandler      which provides a lambda function with the actual dao method call
     * @param <DatabaseResultType> the expected database result type
     * @see #executeDatabaseAction(DatabaseAction, DatabaseHandler, ResultCallbackListener)
     */
    protected <DatabaseResultType> void executeDatabaseAction(DatabaseAction action, DatabaseHandler<DatabaseResultType> databaseHandler) {
        this.executeDatabaseAction(action, databaseHandler, unimportant -> {
        });
    }

    /**
     * Execute the database action asynchronously by returning a Future object with the expected
     * database return value
     *
     * @param action          action type that will be executed
     * @param databaseHandler which provides a lambda function with the actual dao method call
     * @return database       return value (either the SELECTED value or a status code on UPDATE, DELETE or INSERT)
     */
    @SuppressWarnings("SameParameterValue")
    protected <DatabaseResultType> Future<DatabaseResultType> executeAsyncDatabaseAction(DatabaseAction action, DatabaseHandler<DatabaseResultType> databaseHandler) {
        return this._executeDatabaseAction(action, databaseHandler);
    }

    /* **************************************************************
     *                  Database request handler
     ****************************************************************/

    /**
     * Checks whether the database return value is valid according to the database action.
     * <p>
     * If the action was to insert a list of objects the return value is a collection. Each item
     * will be checked with
     *
     * @param result         database result value
     * @param databaseAction database action
     */
    private void resultHandler(long result, DatabaseAction databaseAction) {
        if (this.isOperationSuccessfully(result, databaseAction)) {
            writeInfoLog(result, databaseAction);
        } else {
            writeErrorLog(result, databaseAction);
            throw new Error("A database operation fails! See logcat for more information");
        }
    }

    /**
     * This is the generic method which will calling the {@link #resultHandler(long, DatabaseAction)}
     * by converting the database result type to a Long
     *
     * @param result               database result value
     * @param databaseAction       database action
     * @param <DatabaseResultType> database result type
     */
    @SuppressWarnings("unchecked")
    private <DatabaseResultType> void resultHandler(DatabaseResultType result, DatabaseAction databaseAction) {
        if (result instanceof Collection) {
            for (long _result : (Collection<Long>) result) {
                resultHandler(_result, databaseAction);
            }

            return;
        }

        resultHandler(Long.parseLong(result.toString()), databaseAction);
    }

    /**
     * This method decide whether the database result is an error or not
     *
     * @param result         database result
     * @param databaseAction respected database action
     * @return whether the database return value is an error or not
     */
    private boolean isOperationSuccessfully(long result, DatabaseAction databaseAction) {
        switch (databaseAction) {
            case UPDATE:
            case DELETE:
                return result > 0;
            case INSERT:
                return result > -1;
            /*
             * This is actually dead code cause the resultHandler only is getting called != SELECT.
             * Its still there to fix bugs by adding DatabaseActions but forget to extend the
             * switch case here and blame missing 'SELECT' for it
             */
            case SELECT:
                return true;
        }

        throw new IllegalArgumentException("Action does not match to any values in isOperationSuccessfully. Action: " + databaseAction.toString());
    }


    /* **************************************************************
     *                           Logger
     ****************************************************************/

    /**
     * Write a error log
     *
     * @param result         database result value
     * @param databaseAction database action
     */
    private void writeErrorLog(long result, DatabaseAction databaseAction) {
        Log.e(this.getTag(), databaseAction.toString() + " fails: Database returns " + result);
    }

    /**
     * Write a info log decide whether the database result code is a id or not
     *
     * @param result         database result value
     * @param databaseAction database action
     */
    private void writeInfoLog(long result, DatabaseAction databaseAction) {
        String resultType = "Result (not id!)";

        if (databaseAction == DatabaseAction.INSERT) {
            resultType = "ID";
        }

        Log.i(this.getTag(), databaseAction.toString() + " successfully! " + resultType + ": " + result);
    }

    /**
     * Get the log from the specific repository
     *
     * @return the tag which will the specific repo will be called in the logging process
     */
    protected abstract String getTag();
}
