package de.hda.fbi.nzse.deadlinecalendar.adapter.settingsViewHolder;

import android.content.Context;
import android.content.res.ColorStateList;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.Observable;

import de.hda.fbi.nzse.deadlinecalendar.R;
import de.hda.fbi.nzse.deadlinecalendar.adapter.SettingsExpandableListAdapter;
import de.hda.fbi.nzse.deadlinecalendar.adapter.SettingsExpandableListAdapter.ExpandableListItemListener;
import de.hda.fbi.nzse.deadlinecalendar.model.Settings;
import de.hda.fbi.nzse.deadlinecalendar.presenter.ImportServicePresenter;
import de.hda.fbi.nzse.deadlinecalendar.services.importService.ImportService;


/**
 * Expandable list item that provides the ui for the import service
 *
 * @author Matthias Feyll
 * @see SettingsExpandableListAdapter
 * @see ImportService
 */
public class ImportServiceViewHolder extends SettingsExpandableListAdapter.ChildViewHolder {
    @FunctionalInterface
    public interface NotifyActions {
        void onInvalidURL(EditText editText);
    }

    private final ExpandableListItemListener actionListener;
    private final ImportServicePresenter presenter;

    private Settings settings;
    private EditText urlEditText;
    private ProgressBar progressBar;
    private TextView importStatusText;
    private Button importButton;


    public ImportServiceViewHolder(ExpandableListItemListener actionListener, Context context) {
        super(R.layout.adapter_settings_import_group_view, R.string.adapterSettingsImportGroupView_textView_title);

        this.actionListener = actionListener;
        this.presenter = new ImportServicePresenter(context);
    }

    public void setSettings(Settings settings) {
        this.settings = settings;
    }


    @Override
    public void onCreateChildView(View v) {
        urlEditText = v.findViewById(R.id.adapterSettingsImportListView_editText_url);
        progressBar = v.findViewById(R.id.adapterSettingsImportListView_progressBar_import);
        importStatusText = v.findViewById(R.id.adapterSettingsImportListView_textView_importStatus);
        importButton = v.findViewById(R.id.adapterSettingsImportListView_button_import);

        if (settings.getUrl() != null) {
            urlEditText.setText(this.settings.getUrl().toString());
        }

        this.importButton.setText(R.string.adapterSettingsImportListView_button_import_text);
        this.importButton.setOnClickListener(_v -> onImportClicked());
        this.setImportBtn2Import();
    }

    public void onError(@NonNull NotifyActions notifyActions) {
        notifyActions.onInvalidURL(this.urlEditText);
    }

    /***************************************************************
     *                        Import status
     ***************************************************************/

    @Override
    public void update(Observable o, Object state) {
        int progressValue;
        int color;
        String text;

        switch ((ImportService.State) state) {
            case VALIDATE:
                this.setImportBtn2Cancel();
                progressValue = 20;
                color = this.presenter.getColor("grey_light");
                text = this.presenter.getString("importCalendarViewHolder_importStatus_validate");
                break;
            case PULL:
                progressValue = 40;
                color = this.presenter.getColor("grey_light");
                text = this.presenter.getString("importCalendarViewHolder_importStatus_pull");
                break;
            case PARSING:
                progressValue = 60;
                color = this.presenter.getColor("grey_light");
                text = this.presenter.getString("importCalendarViewHolder_importStatus_parsing");
                break;
            case COMPLETE:
                this.setImportBtn2Import();
                progressValue = 100;
                color = this.presenter.getColor("green");
                text = this.presenter.getString("importCalendarViewHolder_importStatus_complete");
                break;
            case ERROR:
                this.setImportBtn2Import();
                progressValue = 100;
                color = this.presenter.getColor("red");
                text = this.presenter.getString("importCalendarViewHolder_importStatus_error");
                break;
            case ABORTED:
                progressValue = 100;
                color = this.presenter.getColor("grey_light");
                text = this.presenter.getString("importCalendarViewHolder_importStatus_abort");
                break;

            default:
                throw new Error("No matching enum value. Got: " + state.toString());
        }

        int finalProgressValue = progressValue;
        String finalText = text;
        int finalColor = color;

        progressBar.post(() -> {
            progressBar.setProgress(finalProgressValue);
            importStatusText.setText(finalText);

            progressBar.setProgressTintList(ColorStateList.valueOf(finalColor));
        });
    }

    /***************************************************************
     *                     Set import button action
     ***************************************************************/

    private void setImportBtn2Cancel() {
        this.importButton.post(() -> this.importButton.setEnabled(false));
    }

    private void setImportBtn2Import() {
        this.importButton.post(() -> this.importButton.setEnabled(true));
    }


    /***************************************************************
     *                    Import Button listener
     ***************************************************************/

    private void onImportClicked() {
        // TODO close key board
        this.actionListener.onImportBtnClicked(urlEditText.getText().toString());
    }

}


