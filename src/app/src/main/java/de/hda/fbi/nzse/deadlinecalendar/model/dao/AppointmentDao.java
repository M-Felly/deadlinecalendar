package de.hda.fbi.nzse.deadlinecalendar.model.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import de.hda.fbi.nzse.deadlinecalendar.model.Appointment;

@Dao
public interface AppointmentDao {
    @Query ("SELECT * FROM appointment")
    List<Appointment> getAll();

    @Query ("SELECT * FROM appointment WHERE isEditable = 0")
    List<Appointment> getOBSAppointments();

    @Query ("DELETE FROM appointment WHERE isEditable = 0")
    int purgeOBSCalendarData();

    @Insert
    long insert(Appointment appointment);

    @Delete
    int delete(Appointment appointment);

    @Update
    int update(Appointment appointment);
}
