package de.hda.fbi.nzse.deadlinecalendar.presenter;

import android.content.Context;

import de.hda.fbi.nzse.deadlinecalendar.services.importService.ImportService;

/**
 * This presenter resolves string and color values for the status about the {@link ImportService}
 *
 * @author Matthias Feyll
 */
public class ImportServicePresenter {

    private final Context context;

    public ImportServicePresenter(Context context) {
        this.context = context;
    }

    /**
     * Resolve status color by given tagColorName
     *
     * @param tagColorName color resource
     * @return hex color value
     */
    public final int getColor(final String tagColorName) {
        String packageName = this.context.getPackageName();
        int resourceID = this.context.getResources().getIdentifier(tagColorName, "color", packageName);

        return this.context.getColor(resourceID);
    }


    /**
     * Resolve status string with given tagStringName
     *
     * @param tagStringName string resource
     * @return resolved string
     */
    public final String getString(final String tagStringName) {
        String packageName = this.context.getPackageName();
        int resourceID = this.context.getResources().getIdentifier(tagStringName, "string", packageName);

        return this.context.getString(resourceID);
    }
}