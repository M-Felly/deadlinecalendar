package de.hda.fbi.nzse.deadlinecalendar.model.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import de.hda.fbi.nzse.deadlinecalendar.model.Deadline;

@Dao
public interface DeadlineDao {

    @Query("SELECT * FROM deadline")
    List<Deadline> getAll();

    @Insert
    long insert(Deadline deadline);

    @Delete
    int delete(Deadline deadline);

    @Update
    int update(Deadline deadline);
}
