package de.hda.fbi.nzse.deadlinecalendar.model.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import java.util.List;

import de.hda.fbi.nzse.deadlinecalendar.model.ToDo;
import de.hda.fbi.nzse.deadlinecalendar.model.relations.DeadlineWithToDos;

@Dao
public interface DeadlineWithToDosDao {

    @Transaction
    @Query("SELECT * FROM deadline")
    List<DeadlineWithToDos> getDeadlinesWithToDos();

    @Insert
    long insert(ToDo todo);

    @Delete
    int delete(ToDo todo);

    @Update
    int update(ToDo todo);
}
