package de.hda.fbi.nzse.deadlinecalendar.model.relations;

import androidx.room.Embedded;
import androidx.room.Ignore;
import androidx.room.Relation;

import de.hda.fbi.nzse.deadlinecalendar.model.Deadline;
import de.hda.fbi.nzse.deadlinecalendar.model.StudyModule;

/**
 * One Deadline to one study module relation class
 *
 * @author Dennis Hilz, Anna Kilb
 */
public class DeadlineToStudyModule {
    public DeadlineToStudyModule() {}
    @Ignore
    public DeadlineToStudyModule(Deadline deadline, StudyModule studyModule) {
        this.deadline = deadline;
        this.studyModule = studyModule;
    }

    @Embedded
    public Deadline deadline;

    @Relation(
            parentColumn = "relatedStudyModuleID",
            entityColumn = "studyModuleID"
    )
    public StudyModule studyModule;

}
