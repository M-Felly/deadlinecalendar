package de.hda.fbi.nzse.deadlinecalendar.model.typeconverters;

import androidx.room.TypeConverter;

import de.hda.fbi.nzse.deadlinecalendar.model.Notification;

public class NotificationConverter {


    /**
     * Parses an Enum of type Notification into a String.
     *
     * @param notificationEnum given Enum that shall be converted into a String.
     * @return String that matches to the corresponding Enum Notification Type.
     */
    @TypeConverter
    public static String AppointmentEnumToString(Notification.AppointmentNotificationType notificationEnum){

        return notificationEnum.toString();

    }

    /**
     * Parses an Enum of type Notification into a String.
     *
     * @param notificationEnum given Enum that shall be converted into a String.
     * @return String that matches to the corresponding Enum Notification Type.
     */
    @TypeConverter
    public static String DeadlineEnumToString(Notification.DeadlineNotificationType notificationEnum){

        return notificationEnum.toString();

    }

    /**
     * Parses a String into an Enum of Type Notification Type.
     *
     * @param string given String that shall be converted into an Enum.
     * @return Enum Notification Type that matches to the corresponding String.
     */
    @TypeConverter
    public static Notification.AppointmentNotificationType AppointmentstringToEnum(String string){

      for (Notification.AppointmentNotificationType notificationType : Notification.AppointmentNotificationType.values()){
          if (notificationType.toString().equals(string)){
              return notificationType;
          }
      }
      throw new NullPointerException();
    }

    /**
     * Parses a String into an Enum of Type Notification Type.
     *
     * @param string given String that shall be converted into an Enum.
     * @return Enum Notification Type that matches to the corresponding String.
     */
    @TypeConverter
    public static Notification.DeadlineNotificationType DeadlineStringToEnum(String string){

        for (Notification.DeadlineNotificationType notificationType : Notification.DeadlineNotificationType.values()){
            if (notificationType.toString().equals(string)) {
                return notificationType;
            }
        }
        throw new NullPointerException();
    }
}
