package de.hda.fbi.nzse.deadlinecalendar.model.typeconverters;

import android.content.Context;

import androidx.room.TypeConverter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import de.hda.fbi.nzse.deadlinecalendar.R;
import de.hda.fbi.nzse.deadlinecalendar.model.Deadline;

/**
 * @author Dennis Hilz
 */
public class RemainingTimeConverter {
    public static final long TIME_FACTOR_WEEK = 604800000L;
    public static final long TIME_FACTOR_DAY = 86400000L;
    public static final long TIME_FACTOR_HOUR = 3600000L;
    public static final long TIME_FACTOR_MINUTE = 60000L;

    public static String assembleOutputStringDetail(List<String> timeDisplay, Context context) {
        String output = timeDisplay.get(0);
        if (timeDisplay.size() == 2)
            output += " " + context.getString(R.string.remainingTimeConverter_and)  + " " + timeDisplay.get(1);
        output += " " + context.getString(R.string.remainingTimeConverter_remaining);
        return output;
    }

    public static String assembleOutputString(List<String> timeDisplay) {
        String output = timeDisplay.get(0);
        if (timeDisplay.size() == 2)
            output += "\n" + timeDisplay.get(1);
        return output;
    }

    @TypeConverter
    public static long getRemainingTime(Deadline deadline){
        GregorianCalendar currentTime = GregorianCalendarConverter.dateToCalendar(Calendar.getInstance().getTimeInMillis());
        return (deadline.getSubmissionDate().getTimeInMillis() - GregorianCalendarConverter.calendarToLong(currentTime));
    }

    @TypeConverter
    public static List<String> parseRemainingTime(Context context, Deadline deadline) {
        long remainingTimeMs = getRemainingTime(deadline);


        List<String> timeDisplay = new ArrayList<>();

        if (remainingTimeMs / TIME_FACTOR_WEEK >= 1) {       // At least one week to display
            if (remainingTimeMs / TIME_FACTOR_WEEK > 52) {   // More than 52 weeks displayed as > 52
                timeDisplay.add("> 52" +  " " + context.getString(R.string.DeadlineViewActivity_displayedTimeWeeks));
                return timeDisplay;
            }
            else if (remainingTimeMs / TIME_FACTOR_WEEK < 2) // Handle singular
                timeDisplay.add(remainingTimeMs / TIME_FACTOR_WEEK + " " + context.getString(R.string.DeadlineViewActivity_displayedTimeWeek));
            else  // Handle plural
                timeDisplay.add(remainingTimeMs / TIME_FACTOR_WEEK + " " + context.getString(R.string.DeadlineViewActivity_displayedTimeWeeks));
            long newRemainingTme = remainingTimeMs % TIME_FACTOR_WEEK;
            if (newRemainingTme / TIME_FACTOR_DAY >= 1) {       // At least one day to display
                if (newRemainingTme / TIME_FACTOR_DAY == 1)
                    timeDisplay.add(newRemainingTme / TIME_FACTOR_DAY + " " + context.getString(R.string.DeadlineViewActivity_displayedTimeDay));
                else
                    timeDisplay.add(newRemainingTme / TIME_FACTOR_DAY + " " + context.getString(R.string.DeadlineViewActivity_displayedTimeDays));
            }
            } else {
            if (remainingTimeMs / TIME_FACTOR_DAY >= 1) {        // At least one day to display
                if (remainingTimeMs / TIME_FACTOR_DAY < 2)      // Handle singular
                    timeDisplay.add(remainingTimeMs / TIME_FACTOR_DAY + " " + context.getString(R.string.DeadlineViewActivity_displayedTimeDay));
                else                                            // Handle plural
                    timeDisplay.add(remainingTimeMs / TIME_FACTOR_DAY + " " + context.getString(R.string.DeadlineViewActivity_displayedTimeDays));
                long newRemainingTme = remainingTimeMs % TIME_FACTOR_DAY;
                if (newRemainingTme / TIME_FACTOR_HOUR >= 1)       // At least one hour to display
                    timeDisplay.add(newRemainingTme / TIME_FACTOR_HOUR + " " + context.getString(R.string.DeadlineViewActivity_displayedTimeHours));
            } else {
                if (remainingTimeMs / TIME_FACTOR_HOUR >= 1) {   // At least one hour to display
                    if (remainingTimeMs / TIME_FACTOR_HOUR < 2)      // Handle singular
                        timeDisplay.add(remainingTimeMs / TIME_FACTOR_HOUR + " " + context.getString(R.string.DeadlineViewActivity_displayedTimeHour));
                    else                                            // Handle plural
                        timeDisplay.add(remainingTimeMs / TIME_FACTOR_HOUR + " " + context.getString(R.string.DeadlineViewActivity_displayedTimeHours));
                    long newRemainingTime = remainingTimeMs % TIME_FACTOR_HOUR;
                    if (newRemainingTime / TIME_FACTOR_MINUTE >= 1)       // At least one minute to display
                        timeDisplay.add(newRemainingTime / TIME_FACTOR_MINUTE + " " + context.getString(R.string.DeadlineViewActivity_displayedTimeMinutes));
                } else                                          // Handle minutes
                    timeDisplay.add(remainingTimeMs / TIME_FACTOR_MINUTE + " " + context.getString(R.string.DeadlineViewActivity_displayedTimeMinutes));
            }
        }
        return timeDisplay;
    }
}
