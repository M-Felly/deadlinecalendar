package de.hda.fbi.nzse.deadlinecalendar.factory;

import android.os.Bundle;

import androidx.fragment.app.DialogFragment;

import org.jetbrains.annotations.Nullable;

/**
 * Factory builder for dialog fragments
 * Use this creator when your dialog needs parameter passed in the constructor.
 * <p>
 * You are not allowed to parse parameter in the constructor directly due to the fact that the view is
 * getting recreated on orientation change
 *
 * @author Matthias Feyll
 */
public class DialogFragmentFactory {
    /**
     * Creates a new DialogFragment and parse parameter.
     * <p>
     * Catch the parameter by calling the getArguments() in the onCreateDialog() extract the serializable
     * bundle
     *
     * @param fragmentClass the specific dialog class which inherits DialogFragment
     * @param parameter     you wanna parse in the constructor
     * @return A proper
     */
    public static DialogFragment createInstance(Class<? extends DialogFragment> fragmentClass, @Nullable Bundle parameter) {
        DialogFragment fragment;
        try {
            fragment = fragmentClass.newInstance();
        } catch (IllegalAccessException | InstantiationException e) {
            throw new Error(e.getMessage());
        }

        if (parameter != null) {
            fragment.setArguments(parameter);
        }

        return fragment;
    }
}
