package de.hda.fbi.nzse.deadlinecalendar.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import org.jetbrains.annotations.Nullable;

import java.io.Serializable;
import java.util.Calendar;
import java.util.concurrent.ExecutionException;

import de.hda.fbi.nzse.deadlinecalendar.manager.ManagerHolder;
import de.hda.fbi.nzse.deadlinecalendar.manager.NotificationManager;
import de.hda.fbi.nzse.deadlinecalendar.model.Settings;
import de.hda.fbi.nzse.deadlinecalendar.model.repository.SettingsRepository;

/**
 * When a registered alarm is getting fired this receiver is getting called. It creates a notification
 * by the blueprint from the {@link NotificationManager}
 *
 * @author Matthias Feyll
 */
public class NotificationReceiver extends BroadcastReceiver {
    public final static String NOTIFIABLE_INTENT_EXTRA = "notifiable";
    public final static String BUNDLE_EXTRA = "bundle";
    public static final String TAG = "NotificationReceiver";

    /**
     * Implement this interface to classes which should be able to fire notifications.
     * The class must have at least one calendar for getTargetDate() and a text that is getting
     * displayed in the notification.
     */
    public interface Notifiable extends Serializable {
        long getID();
        String getTitle();
        @Nullable Calendar getNotifiableReminderDate();
        Calendar getTargetDate();
    }

    /**
     * Build a notification with the NotificationManager
     * The given intent requires a {@link Notifiable} object with the {@link #NOTIFIABLE_INTENT_EXTRA} key
     *
     * @param context context
     * @param intent intent with required notifiable
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle =  intent.getBundleExtra(BUNDLE_EXTRA);
        if (bundle == null) {
            throw new NullPointerException("Bundle is null. Consider to make a byte stream instead of a serializing entity");
        }

        Notifiable notifiable = (Notifiable) bundle.getSerializable(NOTIFIABLE_INTENT_EXTRA);

        if (!isNotificationEnabled(context, notifiable)) {
            return;
        }

        NotificationManager notificationManager = ManagerHolder.getManager(NotificationManager.class);
        NotificationCompat.Builder notification = NotificationManager.Builder.createNotification(context, notifiable);

        notificationManager.notify(context, notification, notifiable);
    }


    /**
     * Method that checks if notifications for this type of notifiable object is enabled by the settings
     *
     * @param context context
     * @param notifiable notifiable object
     * @return whether notification are enabled for this type of notifiable
     */
    private boolean isNotificationEnabled(Context context, Notifiable notifiable) {
        SettingsRepository settingsRepository = new SettingsRepository(context);

        Settings settings;
        try {
             settings = settingsRepository.getSettingsAsync().get();
        } catch (InterruptedException | ExecutionException exception) {
            Log.w(TAG, "Fetching settings from database failed, aborted: " + exception.getMessage());

            return false;
        }

        return settings.isNotificationsEnabled(notifiable.getClass());
    }
}