package de.hda.fbi.nzse.deadlinecalendar.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.io.Serializable;

import de.hda.fbi.nzse.deadlinecalendar.dialogs.ProfileAddStudyModuleDialog;
import de.hda.fbi.nzse.deadlinecalendar.presenter.StudyModuleColorPresenter;
import de.hda.fbi.nzse.deadlinecalendar.view_elements.custom.StudyModuleShape;

/**
 * This class represents a module displayed as badge
 *
 *  DON`T use the getter / setter of this class directly! Always take the presenter class
 *  StudyModuleColorPresenter to get the color as int value
 *
 * @see StudyModuleColorPresenter
 */
@Entity
public class StudyModule implements Serializable {
    /**
     * The max tag length depends on the width of the {@link StudyModuleShape}
     *
     * By changing the value be aware that you also have to change it in the
     * {@link ProfileAddStudyModuleDialog dialog layout} at android:maxLength
     */
    public static final int MAX_TAG_LENGTH = 5;

    @PrimaryKey(autoGenerate = true)
    public long studyModuleID;

    public long relatedProfileID;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "tag")
    private String tag;

    @TypeConverters(StudyModuleColor.Converter.class)
    private StudyModuleColor color;


    @Ignore
    public StudyModule(String tag, String name, StudyModuleColor color) {
        this.tag = tag;
        this.name = name;
        this.color = color;
    }

    public StudyModule() {}

    /***************************************************************
     *                      Getter / Setter
     ***************************************************************/
    public String getTag() {
        return tag;
    }

    public String getName() {
        return name;
    }

    public StudyModuleColor getColor() {
        return color;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public void setColor(StudyModuleColor color) {
        this.color = color;
    }
}
