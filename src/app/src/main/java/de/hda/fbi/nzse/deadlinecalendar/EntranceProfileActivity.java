package de.hda.fbi.nzse.deadlinecalendar;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.concurrent.ExecutionException;

import de.hda.fbi.nzse.deadlinecalendar.model.Profile;
import de.hda.fbi.nzse.deadlinecalendar.model.repository.ProfileRepository;

import static de.hda.fbi.nzse.deadlinecalendar.EntranceActivity.INITIAL_RUN_KEY;

public class EntranceProfileActivity extends AppCompatActivity {
    public static final String PROFILE_INTENT = "profile";
    public static final String TAG = "EntranceProfileActivity";

    private EditText nameEditText;
    private EditText matriculationNumberEditText;
    private EditText universityNameEditText;
    private EditText studyLevelEditText;
    private EditText departmentNameEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_entrance);

        checkExistingProfile();

        Button nextButton = findViewById(R.id.profileEntranceViewActivity_button_next);
        nextButton.setOnClickListener(v -> onNextBtnClicked());

        // elements
        nameEditText = findViewById(R.id.profileEntranceViewActivity_textView_name);
        matriculationNumberEditText = findViewById(R.id.profileEntranceViewActivity_textView_matriculationNumber);
        universityNameEditText = findViewById(R.id.profileEntranceViewActivity_textView_universityName);
        studyLevelEditText = findViewById(R.id.profileEntranceViewActivity_textView_studyLevel);
        departmentNameEditText = findViewById(R.id.profileEntranceViewActivity_textView_departmentName);
    }

    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences prefs = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        if (!prefs.getBoolean(INITIAL_RUN_KEY, true)) {
            finish();
        }
    }

    /***************************************************************
     *                           Save
     ***************************************************************/
    private void saveProfile(Profile profile) {
        ProfileRepository profileRepository = new ProfileRepository(this);
        profileRepository.insert(profile);
    }
    
    /***************************************************************
     *                        Display / Validity
     ***************************************************************/
    private Profile getProfileFromUser() {
        Profile profile = new Profile();

        profile.setName(nameEditText.getText().toString());
        profile.setMatriculationNumber(matriculationNumberEditText.getText().toString());
        profile.setUniversityName(universityNameEditText.getText().toString());
        profile.setStudyLevel(studyLevelEditText.getText().toString());
        profile.setDepartmentName(departmentNameEditText.getText().toString());

        return profile;
    }

    private void checkExistingProfile() {
        Profile existingProfile = getExistingProfile();

        if (existingProfile != null) {
            goToNextEntrance(existingProfile);
        }
    }

    @Nullable
    private Profile getExistingProfile(){
        ProfileRepository profileRepository = new ProfileRepository(this);

        Profile profile = null;

        try {
            profile = profileRepository.getProfileAsync().get();
        }catch (InterruptedException | ExecutionException e) {
            Log.e(TAG, "Something went wrong during the database fetch process: " + e.getMessage());
        }

        return profile;
    }

    /**
     * Check users input and set error message to the fields respectively
     *
     * @return whether the user input is valid in all fields or invalid in one
     */
    private boolean isUsersInputValid() {
        // name
        if (this.nameEditText.getText().length() <= 0) {
            this.nameEditText.setError(getString(R.string.ProfileAddStudyModuleDialog_editText_profileName_invalid));
            return false;
        }

        return true;
    }

    /***************************************************************
     *                          Listener
     ***************************************************************/
    
    private void onNextBtnClicked() {
        if (!isUsersInputValid()) {
            return;
        }

        Profile profile = getProfileFromUser();

        saveProfile(profile);
        goToNextEntrance(profile);
    }

    private void onSkipBtnClicked() {
        Profile emptyProfile = new Profile();
        saveProfile(emptyProfile);

        goToNextEntrance(emptyProfile);
    }

    /***************************************************************
     *                         Navigation
     ***************************************************************/
    private void goToNextEntrance(Profile profile) {
        Intent intent = new Intent(this, EntranceStudyModuleActivity.class);
        intent.putExtra(PROFILE_INTENT, profile);

        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_bar_skip, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.actionBarNext_forwardArrow) {
            onSkipBtnClicked();

            return true;
        }

        return false;
    }
}