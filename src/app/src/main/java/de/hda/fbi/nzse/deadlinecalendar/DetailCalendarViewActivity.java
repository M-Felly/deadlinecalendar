package de.hda.fbi.nzse.deadlinecalendar;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.Locale;

import de.hda.fbi.nzse.deadlinecalendar.model.Appointment;

import static de.hda.fbi.nzse.deadlinecalendar.manager.NotificationManager.NOTIFICATION_APPOINTMENT_INTENT;
import static de.hda.fbi.nzse.deadlinecalendar.receiver.NotificationReceiver.BUNDLE_EXTRA;

/**
 * In this activity, the user is able to see his created appointment in a detailed view. This activity
 * starts, if the user chooses the specific appointment in the recycler view.
 */
@SuppressWarnings("ConstantConditions")
public class DetailCalendarViewActivity extends AppCompatActivity implements Serializable {

    private BottomNavigationView bottomNav;
    private Appointment parentAppointment;

    /**
     * Goes back to CalendarViewActivity.
     *
     * @return if action was successful.
     */
    @Override
    public boolean onSupportNavigateUp() {
        String sClass = getIntent().getExtras().getString("class");
        if(sClass != null && sClass.equals(CalendarViewActivity.class.toString())) {
            GregorianCalendar selectedDate = (GregorianCalendar) getIntent().getExtras().getSerializable("selectedDate");
            startActivity(new Intent(this, CalendarViewActivity.class).putExtra("class", this.getClass().toString()).putExtra("selectedDate", selectedDate));
        }
        else{
            startActivity(new Intent(this, CalendarViewActivity.class).putExtra("class", this.getClass().toString()).putExtra("selectedDate", parentAppointment.getFromDate()));
        }
        onBackPressed();
        return true;
    }

    /**
    * On creating the activity, following actions have to be done that the activity works correctly.
    *
    * @param savedInstanceState given saved instance
    */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_calendar_view);
        setTitle("Details");

        TextView textViewTitle = findViewById(R.id.detailCalendarViewActivity_textView_name);
        TextView textViewDate = findViewById(R.id.detailCalendarViewActivity_textView_date);
        TextView textViewFromTime = findViewById(R.id.detailCalendarViewActivity_textView_from_time);
        TextView textViewToTime = findViewById(R.id.detailCalendarViewActivity_textView_to_time);
        TextView textViewLocation = findViewById(R.id.detailCalendarViewActivity_textView_location);
        TextView textViewNotification = findViewById(R.id.detailCalendarViewActivity_textView_notification);
        TextView textViewDescription = findViewById(R.id.detailCalendarViewActivity_textView_description);


        //back button
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //get appointment
        parentAppointment = getAppointmentFromIntent();

        GregorianCalendar fromDate = parentAppointment.getFromDate();
        GregorianCalendar toDate = parentAppointment.getToDate();

        SimpleDateFormat fromDateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.forLanguageTag(String.valueOf(R.string.languageTag)));
        String fromDateTemp = fromDateFormat.format(fromDate.getTime());

        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm", Locale.forLanguageTag(String.valueOf(R.string.languageTag)));


        String fromTime = timeFormat.format(fromDate.getTime());
        String toTime = timeFormat.format(toDate.getTime());

        String temp = parentAppointment.getTitle();
        textViewTitle.setText(temp);
        textViewDate.setText(fromDateTemp);
        textViewFromTime.setText(fromTime);
        textViewToTime.setText(toTime);
        textViewLocation.setText(parentAppointment.getLocation());
        textViewNotification.setText(Appointment.notificationTypeToString(this, parentAppointment.getNotificationType()));

        //check if the note is empty. If the note is empty field isn't show
        if (parentAppointment.getNote().isEmpty()){
            textViewDescription.setVisibility(View.GONE);
        }
        else{
            textViewDescription.setText(parentAppointment.getNote());
        }

        bottomNav = findViewById(R.id.detailCalendarViewActivity_bottomNavigation_bottomNav);
        bottomNav.setOnNavigationItemSelectedListener(new BottomNavigation(this));
    }


    /**
     * Checks the correct icon in bottom navigation bar.
     */
    @Override
    protected void onResume() {
        super.onResume();
        bottomNav.getMenu().findItem(R.id.bottom_nav_calendar).setChecked(true);
    }

    /**
     * Implements the action if a specific element is selected.
     *
     * If the user taps the edit button on the toolbar to edit his content, he will redirected to
     * the EditAppointmentViewActivity by creating a new intent. This also redirects the selected
     * appointment to the EditAppointmentViewActivity.
     *
     * @param item is the selected item
     * @return if action was successful
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.actionBarNext_forwardArrow) {
            GregorianCalendar selectedDate = (GregorianCalendar) getIntent().getExtras().getSerializable("selectedDate");
            startActivity(new Intent(DetailCalendarViewActivity.this, EditCalendarViewActivity.class).putExtra("class", this.getClass().toString()).putExtra(NOTIFICATION_APPOINTMENT_INTENT, parentAppointment).putExtra("selectedDate", selectedDate));
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Creates/inflates all items of an menu an bind it onto the toolbar.
     *
     * @param menu is the menu that shall be inflated
     * @return if action was successful
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(parentAppointment.getIsEditable())
        {
            getMenuInflater().inflate(R.menu.detail_view_nav_menu, menu);
        }

        return super.onCreateOptionsMenu(menu);
    }

    /* **************************************************************
     *                        Intent resolver
     ****************************************************************/

    /**
     * This method handles the intent with the given appointment.
     *
     * @return the appointment of the intent.
     */
    private Appointment getAppointmentFromIntent() {
        Appointment appointment;
        appointment = (Appointment) getIntent().getExtras().getSerializable(NOTIFICATION_APPOINTMENT_INTENT);

        if (appointment != null) {
            return appointment;
        }

        Bundle bundle = getIntent().getBundleExtra(BUNDLE_EXTRA);
        appointment = (Appointment) bundle.getSerializable(NOTIFICATION_APPOINTMENT_INTENT);

        if (appointment != null) {
            return appointment;
        }

        throw new Error("Missing extra in intent");
    }
}
