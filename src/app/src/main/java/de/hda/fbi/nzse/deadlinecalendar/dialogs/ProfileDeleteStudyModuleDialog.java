package de.hda.fbi.nzse.deadlinecalendar.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import java.io.Serializable;

import de.hda.fbi.nzse.deadlinecalendar.R;
import de.hda.fbi.nzse.deadlinecalendar.adapter.StudyModuleAdapter;

/**
 * This dialog is getting display from {@link StudyModuleAdapter} when a study module
 * that should going to be deleted has dependencies to deadlines. The dialog asks the
 * user whether he wants to delete these deadlines, too
 *
 * @author Matthias Feyll
 */
public class ProfileDeleteStudyModuleDialog extends DialogFragment {
    public static final String ACTION_LISTENER_KEY = "actionListener";

    /**
     * Callback listener to get the resolved attributes.
     *
     * @see ProfileAddStudyModuleDialog
     */
    @FunctionalInterface
    public interface OnClickListener extends Serializable {
        void onSubmitDialog();
    }

    private OnClickListener actionListener;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle arguments = getArguments();

        if (arguments == null) {
            throw new Error("Arguments missing");
        }

        this.actionListener = (OnClickListener) arguments.getSerializable(ACTION_LISTENER_KEY);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_profile_delete_study_module, null);
        builder.setView(dialogView);

        // back button
        ImageButton backBtn = dialogView.findViewById(R.id.profileDeleteStudyModuleDialog_imageButton_back);
        backBtn.setOnClickListener(v -> dismiss());

        // submit button
        ImageButton submitBtn = dialogView.findViewById(R.id.profileDeleteStudyModuleDialog_imageButton_submit);
        submitBtn.setOnClickListener(v -> {
            this.actionListener.onSubmitDialog();
            dismiss();
        });

        return builder.create();
    }
}