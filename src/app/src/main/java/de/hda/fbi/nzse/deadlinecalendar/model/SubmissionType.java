package de.hda.fbi.nzse.deadlinecalendar.model;


import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverter;
import androidx.room.TypeConverters;

import java.io.Serializable;

import de.hda.fbi.nzse.deadlinecalendar.presenter.SubmissionTypePresenter;

/**
 * This class holds the data structure for submission types eg online or via moodle
 * It also can be set to a custom text by users input.
 * <p>
 * DON`T use the getter / setter of this class directly! Always take the presenter class
 * SubmissionTypePresenter to get and set submission types as strings
 *
 * @see SubmissionTypePresenter
 */
@Entity
public class SubmissionType implements Serializable {
    @PrimaryKey(autoGenerate = true)
    public long submissionTypeID;

    private String customText;

    @TypeConverters(SubmissionType.Predefined.Converter.class)
    private Predefined predefinedType;

    public SubmissionType() {
    }

    public String getCustomText() {
        return customText;
    }

    public void setCustomText(String customText) {
        this.customText = customText;
        this.predefinedType = Predefined.Unused;
    }

    public Predefined getPredefinedType() {
        return predefinedType;
    }

    public void setPredefinedType(Predefined predefinedType) {
        this.predefinedType = predefinedType;
    }

    /* **************************************************************
     *                         Predefined
     ****************************************************************/

    public enum Predefined implements Serializable {
        Present("submissionType_predefined_present"),
        Online("submissionType_predefined_online"),
        Moodle("submissionType_predefined_moodle"),
        Unused("submissionType_predefined_unused");

        private final String resourceNameAttribute;

        Predefined(String resourceNameAttribute) {
            this.resourceNameAttribute = resourceNameAttribute;
        }

        @NonNull
        @Override
        public String toString() {
            return resourceNameAttribute;
        }

        // converter
        public static class Converter {
            @TypeConverter
            public String enumToString(Predefined predefined) {
                return predefined.toString();
            }

            @TypeConverter
            public Predefined stringToEnum(String attributeNameTag) {

                for (Predefined predefined : Predefined.values()) {
                    if (predefined.toString().equals(attributeNameTag)) {
                        return predefined;
                    }
                }
                throw new NullPointerException();
            }
        }
    }
}


