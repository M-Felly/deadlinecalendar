package de.hda.fbi.nzse.deadlinecalendar.model;

import java.io.Serializable;
public class Notification {

    /**
     * Notification types are necessary for setting the correct reminder date of a deadline.
     */
    public enum DeadlineNotificationType implements Serializable {
        NO_NOTIFICATION(0), ONE_HOUR(1), THREE_HOURS(2), ONE_DAY(3), THREE_DAYS(4), ONE_WEEK(5);

        private final int value;

        public int getValue() {
            return value;
        }

        DeadlineNotificationType(int value) {
            this.value = value;
        }
    }

    /**
     * Notification types are necessary for setting the correct reminder date of an appointment.
     */
    public enum AppointmentNotificationType implements Serializable {
        NO_NOTIFICATION(0), FIVE_MINUTES(1), FIFTEEN_MINUTES(2),THIRTY_MINUTES(3), ONE_HOUR(4) ,TWO_HOURS(5),ONE_DAY(6);

        private final int value;

        public int getValue() {
            return value;
        }

        AppointmentNotificationType(int value) {
            this.value = value;
        }
    }

}
