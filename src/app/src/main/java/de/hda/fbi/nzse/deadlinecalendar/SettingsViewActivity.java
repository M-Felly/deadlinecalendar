package de.hda.fbi.nzse.deadlinecalendar;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ExpandableListView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;
import java.util.Observer;

import de.hda.fbi.nzse.deadlinecalendar.adapter.SettingsExpandableListAdapter;
import de.hda.fbi.nzse.deadlinecalendar.adapter.settingsViewHolder.ImportServiceViewHolder;
import de.hda.fbi.nzse.deadlinecalendar.dialogs.ImportServiceFeedbackDialog;
import de.hda.fbi.nzse.deadlinecalendar.factory.DialogFragmentFactory;
import de.hda.fbi.nzse.deadlinecalendar.manager.ManagerHolder;
import de.hda.fbi.nzse.deadlinecalendar.manager.ServiceManager;
import de.hda.fbi.nzse.deadlinecalendar.manager.ServiceManager.ServiceHolder;
import de.hda.fbi.nzse.deadlinecalendar.model.Settings;
import de.hda.fbi.nzse.deadlinecalendar.model.repository.SettingsRepository;
import de.hda.fbi.nzse.deadlinecalendar.services.importService.ImportService;
import de.hda.fbi.nzse.deadlinecalendar.services.importService.ImportServiceException;

import static de.hda.fbi.nzse.deadlinecalendar.dialogs.ImportServiceFeedbackDialog.IMPORT_SERVICE_EXCEPTION_KEY;


public class SettingsViewActivity extends AppCompatActivity implements SettingsExpandableListAdapter.ExpandableListItemListener, ImportService.ImportServiceHandler {
    public static final String TAG = "SettingsViewActivity";
    private BottomNavigationView bottomNav;

    private ExpandableListView expandableList;
    private SettingsExpandableListAdapter expandableListAdapter;

    private Settings settings;
    private SettingsRepository settingsRepository;

    /***************************************************************
     *                     Life cycle hooks
     ***************************************************************/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_view);
        setTitle(getString(R.string.settingsView_title_settingsHeader));
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        bottomNav = findViewById(R.id.SettingsViewActivity_bottomNav);
        bottomNav.setOnNavigationItemSelectedListener(new BottomNavigation(this));

        // expandable list view
        expandableList = findViewById(R.id.expandableListView);
        this.expandableListAdapter = new SettingsExpandableListAdapter(this);
        expandableList.setAdapter(this.expandableListAdapter);
        expandableList.setOnGroupClickListener((parent, v, groupPosition, id) -> onGroupClick(groupPosition));
        onGroupClick(0);

        this.settingsRepository = new SettingsRepository(this);
    }


    @Override
    protected void onResume() {
        super.onResume();
        bottomNav.getMenu().findItem(R.id.bottom_nav_profile).setChecked(true);
        loadSettings();
    }

    /***************************************************************
     *                        Load / Save
     ***************************************************************/

    private void loadSettings() {
        this.settingsRepository.getSettings(settings -> {
            this.settings = settings;
            displaySettings();
        });
    }


    /***************************************************************
     *                    Display / Validity
     ***************************************************************/

    private boolean isURLValid(String url) {
        if (url.length() <= 0) {
            return false;
        }

        try {
            new URL(url);
        }catch (MalformedURLException e) {
            return false;
        }

        return true;
    }

    private void displaySettings() {
        this.expandableListAdapter.setSettings(settings);
    }

    private void showURLError() {
        this.expandableListAdapter.getChildViewHolderList(ImportServiceViewHolder.class).onError(
                editText -> editText.setError(getString(R.string.adapterSettingsImportListView_editText_url_error))
        );
    }

    /***************************************************************
     *                    Import action listener
     ***************************************************************/

    @Override
    public void onImportBtnClicked(String importUrl) {
        if (!this.isURLValid(importUrl)) {
            this.showURLError();
            return;
        }

        try {
            this.settings.setUrl(new URL(importUrl));
        }catch (MalformedURLException e) {
            this.showURLError();
            return;
        }

        this.settingsRepository.update(settings);
        this.startImport();
    }

    @Override
    public void onFailure(ImportServiceException importServiceException) {
        Bundle dialogArguments = new Bundle();
        dialogArguments.putSerializable(IMPORT_SERVICE_EXCEPTION_KEY, importServiceException);

        DialogFragment feedbackDialog = DialogFragmentFactory.createInstance(ImportServiceFeedbackDialog.class, dialogArguments);
        feedbackDialog.show(this.getSupportFragmentManager(), TAG);
    }


    /***************************************************************
     *                     Expandable list listener
     ***************************************************************/

    private int previousExpandedGroup = -1;
    @SuppressWarnings("SameReturnValue")
    public boolean onGroupClick(final int position) {
        if (this.expandableList.isGroupExpanded(position)) {
            this.expandableList.collapseGroup(position);

            this.previousExpandedGroup = -1;
            return true;
        }

        this.expandableList.expandGroup(position);
        if (this.previousExpandedGroup != -1) {
            this.expandableList.collapseGroup(this.previousExpandedGroup);
        }

        this.previousExpandedGroup = position;

        return true;
    }

    /***************************************************************
     *                        ImportService
     ***************************************************************/
    private void startImport() {
        ServiceManager serviceManager = ManagerHolder.getManager(ServiceManager.class);

        Observer importServiceObserver = this.expandableListAdapter.getChildViewHolderList(ImportServiceViewHolder.class);
        ServiceHolder serviceHolder = serviceManager.buildService(new ImportService(this), importServiceObserver);

        serviceHolder.start();
    }


    /***************************************************************
     *                         Navigation
     ***************************************************************/

    @Override
    public void onBackPressed() {
        this.gotoProfileViewActivity();
    }

    public boolean onSupportNavigateUp() {
        startActivity(new Intent(this, ProfileViewActivity.class));
        finish();
        onBackPressed();

        return true;
    }

    private void gotoProfileViewActivity() {
        startActivity(new Intent(this, ProfileViewActivity.class));
        finish();
    }
}