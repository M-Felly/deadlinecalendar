package de.hda.fbi.nzse.deadlinecalendar.model.repository;

import android.content.Context;

import java.util.List;

import de.hda.fbi.nzse.deadlinecalendar.model.Appointment;

public class AppointmentRepository extends BaseRepository {

    public static final String TAG = "AppointmentRepository";

    public AppointmentRepository(Context context) { super(context); }
    
    @Override
    protected String getTag() {
        return TAG;
    }

    List<Appointment> appointmentList;
    /** Get all Appointments of the database.
     *
     * @param resultCallbackListener will be passed to invoking activity
     */
    public void getAppointments (ResultCallbackListener<List<Appointment>> resultCallbackListener){
        executeDatabaseAction(DatabaseAction.SELECT,
                                (appDatabase) -> {
                                    appointmentList = appDatabase.appointmentDao().getAll();
                                    return appointmentList;
                                },
                                resultCallbackListener);
    }

    /** Get all read-only Appointments which were imported from OBS
     *
     * @param resultCallbackListener will be passed to invoking activity
     */
    public void getOBSAppointments (ResultCallbackListener<List<Appointment>> resultCallbackListener){
        executeDatabaseAction(DatabaseAction.SELECT,
                (appDatabase) -> appDatabase.appointmentDao().getOBSAppointments(),
                resultCallbackListener);
    }

    /**
     * Inserts an object of type appointment into the database.
     *
     * @param appointment which will be inserted.
     * @param resultCallbackListener will be passed to invoking activity.
     */
    public void insert (Appointment appointment, ResultCallbackListener<Long> resultCallbackListener){
        this.executeDatabaseAction(DatabaseAction.INSERT,
                (appDatabase) -> appDatabase.appointmentDao().insert(appointment),
                id -> {
                    if (id != null){
                        appointment.appointmentID = id;
                        resultCallbackListener.onSuccess(id);
                    }

                });
    }

    /**
     * Deletes given appointment out of database.
     *
     * @param appointment object that will be deleted.
     * @param resultCallbackListener will be passed to invoking activity.
     */
    public void delete (Appointment appointment, ResultCallbackListener <Integer> resultCallbackListener){
        executeDatabaseAction(DatabaseAction.DELETE,
                (appDatabase) -> appDatabase.appointmentDao().delete(appointment),
                resultCallbackListener
                );
    }

    /**
     * Refreshes appointment that even exists in database.
     *
     * @param appointment object that will be updated.
     * @param resultCallbackListener will be passed to invoking activity.
     */
    public void update(Appointment appointment, ResultCallbackListener<Integer> resultCallbackListener){
        executeDatabaseAction(DatabaseAction.UPDATE,
                (appDatabase) -> appDatabase.appointmentDao().update(appointment),
                resultCallbackListener);
    }

    /**
     * Deletes all imported Appointment objects from OBS calendar import
     */
    public void purgeOBSCalendarData() {
        executeDatabaseAction(DatabaseAction.DELETE,
                (appDatabase) -> appDatabase.appointmentDao().purgeOBSCalendarData());
    }

}
