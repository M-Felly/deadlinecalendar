package de.hda.fbi.nzse.deadlinecalendar;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.List;
import java.util.Objects;

import de.hda.fbi.nzse.deadlinecalendar.adapter.StudyModuleAdapter;
import de.hda.fbi.nzse.deadlinecalendar.dialogs.ProfileAddStudyModuleDialog;
import de.hda.fbi.nzse.deadlinecalendar.dialogs.ProfileDeleteStudyModuleDialog;
import de.hda.fbi.nzse.deadlinecalendar.factory.DialogFragmentFactory;
import de.hda.fbi.nzse.deadlinecalendar.model.Profile;
import de.hda.fbi.nzse.deadlinecalendar.model.StudyModule;
import de.hda.fbi.nzse.deadlinecalendar.model.StudyModuleColor;
import de.hda.fbi.nzse.deadlinecalendar.model.repository.ProfileRepository;
import de.hda.fbi.nzse.deadlinecalendar.model.repository.StudyModuleRepository;

import static de.hda.fbi.nzse.deadlinecalendar.dialogs.ProfileAddStudyModuleDialog.ACTION_LISTENER_KEY;
import static de.hda.fbi.nzse.deadlinecalendar.dialogs.ProfileAddStudyModuleDialog.STUDY_MODULE_COLOR_KEY;

/**
 * In this activity the user can change the profile.
 * He/She is also able to add or delete study modules.
 * <p>
 * Adding new study modules call {@link ProfileAddStudyModuleDialog}
 * Confirm delete conflicts calls the dialog {@link ProfileDeleteStudyModuleDialog}
 *
 * @author Matthias Feyll
 */
public class EditProfileViewActivity extends AppCompatActivity implements ProfileAddStudyModuleDialog.OnClickListener, ViewTreeObserver.OnPreDrawListener {
    private static final String ADD_STUDY_MODULE_DIALOG_TAG = "ProfileAddStudyModuleDialog";
    public static final int RECYCLER_VIEW_MARGIN_BOTTOM = 50;

    private BottomNavigationView bottomNav;

    private TextView nameEditText;
    private TextView matriculationNumberEditText;
    private TextView universityNameEditText;
    private TextView studyLevelEditText;
    private TextView departmentNameEditText;

    private RecyclerView studyModuleRecyclerView;
    private StudyModuleAdapter studyModuleAdapter;

    private StudyModuleRepository studyModuleRepository;
    private ProfileRepository profileRepository;

    private ConstraintLayout recyclerViewLayout;

    private Profile profile;
    private List<StudyModule> studyModuleList;

    private ImageButton addStudyModulesBtn;

    /* **************************************************************
     *                     Life cycle hooks
     ****************************************************************/

    /**
     * Initialising views and repos, sets listener
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile_view);
        setTitle(getString(R.string.title_edit));
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_cancel_icon);

        bottomNav = findViewById(R.id.editProfileViewActivity_bottomNav);
        bottomNav.setOnNavigationItemSelectedListener(new BottomNavigation(this));

        // elements
        this.nameEditText = findViewById(R.id.editProfileViewActivity_editText_name);
        this.matriculationNumberEditText = findViewById(R.id.editProfileViewActivity_editText_matriculationNumber);
        this.universityNameEditText = findViewById(R.id.editProfileViewActivity_editText_universityName);
        this.studyLevelEditText = findViewById(R.id.editProfileViewActivity_editText_studyLevel);
        this.departmentNameEditText = findViewById(R.id.editProfileViewActivity_editText_departmentName);

        // set on click listener for add study module
        addStudyModulesBtn = findViewById(R.id.editProfileViewActivity_imageButton_addStudyModule);
        TextView addStudyModulesLayout = findViewById(R.id.editProfileViewActivity_textView_studyModuleName);
        addStudyModulesBtn.setOnClickListener(v -> this.onAddStudyModuleBtnClicked());
        addStudyModulesLayout.setOnClickListener(v -> this.onAddStudyModuleBtnClicked());

        // recycler view
        this.studyModuleRecyclerView = findViewById(R.id.editProfileViewActivity_recyclerView_studyModules);
        this.studyModuleRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        this.studyModuleRecyclerView.getViewTreeObserver().addOnPreDrawListener(this);
        this.recyclerViewLayout = findViewById(R.id.editProfileViewActivity_constraintLayout_recyclerViewLayout);

        // repository
        this.studyModuleRepository = new StudyModuleRepository(this);
        this.profileRepository = new ProfileRepository(this);
        this.studyModuleAdapter = new StudyModuleAdapter(this);
        this.studyModuleRecyclerView.setAdapter(this.studyModuleAdapter);
    }

    /**
     * Load profile
     */
    @Override
    protected void onResume() {
        super.onResume();
        bottomNav.getMenu().findItem(R.id.bottom_nav_profile).setChecked(true);
        loadProfile();
    }

    /* **************************************************************
     *                        Load / Save
     ****************************************************************/

    /**
     * Loads the profile from repo and displays it
     *
     * @see EditProfileViewActivity#display()
     */
    private void loadProfile() {
        this.profileRepository.getProfileWithStudyModules(profileWithStudyModules -> {
            if (profileWithStudyModules == null) {
                return;
            }

            this.profile = profileWithStudyModules.profile;
            this.studyModuleList = profileWithStudyModules.studyModules;

            this.display();
        });
    }

    /**
     * Pass the current values in the input fields to the locale profile. After that save the
     * profile in the database
     */
    private void saveProfile() {
        // map user input to profile attributes
        this.profile.setName(this.nameEditText.getText().toString());
        this.profile.setMatriculationNumber(this.matriculationNumberEditText.getText().toString());
        this.profile.setUniversityName(this.universityNameEditText.getText().toString());
        this.profile.setStudyLevel(this.studyLevelEditText.getText().toString());
        this.profile.setDepartmentName(this.departmentNameEditText.getText().toString());

        this.profileRepository.update(this.profile);
    }

    /**
     * Save the given study module in database with the current profile
     *
     * @param studyModule given study module to be saved
     */
    private void saveStudyModule(StudyModule studyModule) {
        this.studyModuleRepository.insert(studyModule, profile);
    }

    /* **************************************************************
     *                            Drawing
     ****************************************************************/

    /**
     * Calculate max height for {@link #studyModuleRecyclerView}.
     * Change the max height on the parent constraint layout
     *
     * @return hook used
     */
    @Override
    public boolean onPreDraw() {
        ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) this.recyclerViewLayout.getLayoutParams();
        layoutParams.matchConstraintMaxHeight = calculateRecyclerViewHeight();
        this.recyclerViewLayout.setLayoutParams(layoutParams);

        this.studyModuleRecyclerView.getViewTreeObserver().removeOnPreDrawListener(this);

        return true;
    }

    /* **************************************************************
     *                       Display / Validity
     ****************************************************************/

    /**
     * Display all attributes of the class profile inclusive the study module list
     */
    private void display() {
        this.nameEditText.setText(this.profile.getName());
        this.matriculationNumberEditText.setText(this.profile.getMatriculationNumber());
        this.universityNameEditText.setText(this.profile.getUniversityName());
        this.studyLevelEditText.setText(this.profile.getStudyLevel());
        this.departmentNameEditText.setText(this.profile.getDepartmentName());

        this.studyModuleAdapter.setStudyModuleList(this.studyModuleList);
    }

    /* **************************************************************
     *                      Actions / Listener
     ****************************************************************/

    /**
     * Open add study module dialog {@link ProfileAddStudyModuleDialog}
     */
    private void onAddStudyModuleBtnClicked() {
        Bundle dialogArguments = new Bundle();
        dialogArguments.putSerializable(ACTION_LISTENER_KEY, this);
        dialogArguments.putSerializable(STUDY_MODULE_COLOR_KEY, resolveNextStudyModuleColor());

        DialogFragment addStudyModuleDialog = DialogFragmentFactory.createInstance(ProfileAddStudyModuleDialog.class, dialogArguments);
        addStudyModuleDialog.show(getSupportFragmentManager(), ADD_STUDY_MODULE_DIALOG_TAG);
    }

    /**
     * Add new study module to the list and save it in database
     *
     * @param studyModule new object
     */
    @Override
    public void onSubmitDialog(StudyModule studyModule) {
        studyModuleList.add(studyModule);
        this.studyModuleAdapter.notifyItemInserted(studyModuleList.size() - 1);

        this.saveStudyModule(studyModule);
    }

    /* **************************************************************
     *                         Calculating
     ****************************************************************/

    /**
     * Resolving the next study module color depending by list size
     *
     * @return resolved next study module color
     */
    private StudyModuleColor resolveNextStudyModuleColor() {
        StudyModuleColor[] studyModuleColors = StudyModuleColor.values();

        if (!studyModuleList.isEmpty()) {
            StudyModuleColor currentLastIndexColor = studyModuleList.get(studyModuleList.size() -1).getColor();

            for (int i = 0; i < studyModuleColors.length; i++) {
                if (studyModuleColors[i] == currentLastIndexColor) {
                    return studyModuleColors[(i + 1) % (studyModuleColors.length)];
                }
            }
        }

        return studyModuleColors[0];
    }

    /**
     * Calculating height for recycler view. This is necessary make the recycler view wrap content during
     * less height to be displayed but also scrollable with a max height on too much items that
     * have to be displayed.
     *
     * @return calculated height
     */
    private int calculateRecyclerViewHeight() {
        int topBorder = 0, bottomBorder;

        // if orientation is portrait get top height from divider position
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            int[] topBorderLocation = {0, 0};
            View divider = findViewById(R.id.editProfileViewActivity_divider);
            divider.getLocationOnScreen(topBorderLocation);
            topBorder = topBorderLocation[1];
        }else {
            // if orientation = landscape set topBorder = actionBar height
            TypedValue tv = new TypedValue();
            if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
                topBorder = TypedValue.complexToDimensionPixelSize(tv.data,getResources().getDisplayMetrics()) + 50;
            }
        }

        // get the bottomBorder from the absolute bottom nav y position
        int[] bottomBorderLocation  = {0, 0};
        bottomNav.getLocationOnScreen(bottomBorderLocation);
        bottomBorder = bottomBorderLocation[1];

        return bottomBorder - topBorder - addStudyModulesBtn.getHeight() - RECYCLER_VIEW_MARGIN_BOTTOM;
    }

    /* **************************************************************
     *                    Navigation / Action bar
     ****************************************************************/

    /**
     * Display bottom navigation
     *
     * @param menu menu ressource
     * @return hook was consumed by returning true
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.save_menu, menu);
        return true;
    }

    /**
     * @return consumed hook
     * @see #gotoProfileViewActivity()
     */
    @Override
    public boolean onSupportNavigateUp() {
        this.gotoProfileViewActivity();
        return true;
    }

    /**
     * @see #gotoProfileViewActivity()
     */
    @Override
    public void onBackPressed() {
        this.gotoProfileViewActivity();
    }

    /**
     * Save the profile and {@link this#gotoProfileViewActivity() go back}
     *
     * @param item clicked item
     * @return consumed hook by clicking the confirm button
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.save_menu_item_save) {
            this.saveProfile();
            this.gotoProfileViewActivity();

            return true;
        }

        return false;
    }

    /**
     * Go back to {@link ProfileViewActivity}
     */
    private void gotoProfileViewActivity() {
        startActivity(new Intent(this, ProfileViewActivity.class));
        finish();
    }
}