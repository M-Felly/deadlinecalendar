package de.hda.fbi.nzse.deadlinecalendar.presenter;

import android.app.Activity;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import de.hda.fbi.nzse.deadlinecalendar.model.Deadline;
import de.hda.fbi.nzse.deadlinecalendar.model.StudyModule;
import de.hda.fbi.nzse.deadlinecalendar.model.SubmissionType;
import de.hda.fbi.nzse.deadlinecalendar.model.ToDo;
import de.hda.fbi.nzse.deadlinecalendar.model.relations.DeadlineToStudyModule;
import de.hda.fbi.nzse.deadlinecalendar.model.relations.DeadlineToSubmissionType;
import de.hda.fbi.nzse.deadlinecalendar.model.relations.DeadlineWithToDos;
import de.hda.fbi.nzse.deadlinecalendar.model.repository.DeadlineRepository;
import de.hda.fbi.nzse.deadlinecalendar.model.repository.DeadlineToStudyModuleRepository;
import de.hda.fbi.nzse.deadlinecalendar.model.repository.DeadlineToSubmissionTypeRepository;
import de.hda.fbi.nzse.deadlinecalendar.model.repository.DeadlineWithToDosRepository;
import de.hda.fbi.nzse.deadlinecalendar.model.repository.SubmissionTypeRepository;

/**
 * This class stands between the respective Entity Repository and the data access within an activity.
 * It provides all functions to retrieve and persist data of a Deadline throughout
 * the activity while handling the corresponding relations of this deadline for the database.

 * <h2>Usage</h2>
 * Instantiate the class as follows:
 * <code>DeadlineAccessHandler deadlineAccessHandler = new DeadlineAccessHandler(this)</code>
 *
 * Use the provided class methods to handle all data exchange between the database and the objects
 * in memory.
 *
 * <h2>Notes</h2>
 * Read-Only methods (ex: getter) will retrieve the requested data from the corresponding list
 * which are managed by this class without requesting data from the database. Methods which provide
 * write-access, always write into the corresponding list and into the database to keep both sides
 * persistent.
 *
 * @author Dennis Hilz, Anna Kilb
 */
public class DeadlineAccessHandler {
    private List<Deadline> deadlineList;
    private List<DeadlineWithToDos> deadlineWithToDosList;
    private List<DeadlineToStudyModule> deadlineToStudyModulesList;
    private List<DeadlineToSubmissionType> deadlineToSubmissionTypesList;

    private final DeadlineRepository deadlineRepository;
    private final DeadlineWithToDosRepository deadlineWithToDosRepository;
    private final DeadlineToStudyModuleRepository deadlineToStudyModuleRepository;
    private final DeadlineToSubmissionTypeRepository deadlineToSubmissionTypeRepository;
    private final SubmissionTypeRepository submissionTypeRepository;
    private final Activity activity;

    /* *
     * Standard Constructor for DeadlineAccessHandler class
     *
     */
    public DeadlineAccessHandler(Activity activity) {
        this.activity = activity;
        deadlineList = new ArrayList<>();
        deadlineWithToDosList = new ArrayList<>();
        deadlineToStudyModulesList = new ArrayList<>();
        deadlineToSubmissionTypesList = new ArrayList<>();

        deadlineRepository = new DeadlineRepository(this.activity);
        deadlineWithToDosRepository = new DeadlineWithToDosRepository(this.activity);
        deadlineToStudyModuleRepository = new DeadlineToStudyModuleRepository(this.activity);
        deadlineToSubmissionTypeRepository = new DeadlineToSubmissionTypeRepository(this.activity);
        submissionTypeRepository = new SubmissionTypeRepository(this.activity);

        /*
         * Fill lists with current data from the database using the respective repos.
         */
        this.deadlineRepository.getDeadlines(deadlines -> this.deadlineList = deadlines);

        this.deadlineWithToDosRepository.getAll(deadlinewithtodos -> this.deadlineWithToDosList = deadlinewithtodos);

        this.deadlineToStudyModuleRepository.getAll(deadlinestostudymodule -> this.deadlineToStudyModulesList = deadlinestostudymodule);

        this.deadlineToSubmissionTypeRepository.getAll(deadlinestosubmissionType -> this.deadlineToSubmissionTypesList = deadlinestosubmissionType);

        this.submissionTypeRepository.getAll(submissionType -> {});
    }

    public List<Deadline> getDeadlines() {
        deadlineRepository.getDeadlines(deadlines -> this.deadlineList = deadlines);
        return deadlineList;
    }

    public void addDeadline(Deadline deadline, List<ToDo> toDos, StudyModule studyModule, SubmissionType submissionType) {
        DeadlineWithToDos dwt = new DeadlineWithToDos(deadline, toDos);
        DeadlineToStudyModule dts = new DeadlineToStudyModule(deadline, studyModule);
        DeadlineToSubmissionType dtst = new DeadlineToSubmissionType(deadline, submissionType);

        deadlineRepository.insert(deadline, studyModule, submissionType, id -> {
            deadlineList.add(deadline);
            deadlineToStudyModulesList.add(dts);
            deadlineToSubmissionTypesList.add(dtst);
        });
        deadlineWithToDosRepository.insert(toDos, deadline, id -> deadlineWithToDosList.add(dwt));
    }

    public void updateDeadline(Deadline deadline) {
        deadlineRepository.update(deadline);
        deadlineList = this.getDeadlines();
    }

    public void deleteDeadline(Deadline deadline) {
        long submissionTypeID = deadline.relatedSubmissionTypeID;

        // delete todos
        for (ToDo toDo : getToDos(deadline)) {
            deadlineWithToDosRepository.delete(toDo, id -> {});
        }

        // delete deadline
        deadlineRepository.delete(deadline, id -> deadlineList.remove(deadline));

        deleteSubmissionType(submissionTypeID);
    }

    public List<ToDo> getToDos(Deadline deadline) {
        AtomicReference<List<DeadlineWithToDos>> dwtList = new AtomicReference<>();
        deadlineWithToDosRepository.getAll(dwtList::set);
        for (DeadlineWithToDos dwt : dwtList.get()) {
            if (dwt.deadline != null && dwt.deadline.equals(deadline))
                return dwt.toDos;
        }
        return null;
    }

    public void addToDo(Deadline deadline, ToDo todo) {
        List<ToDo> currToDos = getToDos(deadline);
        currToDos.add(todo);
        DeadlineWithToDos deadlineWithToDos = new DeadlineWithToDos(deadline, currToDos);

        deadlineWithToDosRepository.insert(todo, deadline, id -> deadlineWithToDosList.add(deadlineWithToDosList.size(), deadlineWithToDos));
    }

    public void deleteToDo(Deadline deadline, ToDo todo) {
        deadlineWithToDosRepository.delete(todo, id -> {
            int i = 0;
            for (DeadlineWithToDos dWT : deadlineWithToDosList) {
                if (dWT.deadline.equals(deadline))
                    deadlineWithToDosList.get(i).toDos.remove(todo);
                i++;
            }
        });
    }

    public void checkToDo(ToDo todo, boolean checked) {
        todo.setIsDone(checked);
        deadlineWithToDosRepository.update(todo);
    }

    public void updateDeadlinesToDos(Deadline deadline, List<ToDo> toDos) {
        for (DeadlineWithToDos dwt : deadlineWithToDosList) {
            if (dwt.deadline.equals(deadline)) {
                for (int i = 0; i < dwt.toDos.size(); i++) {
                    boolean delete = true;
                    for (int k = 0; k < toDos.size(); k++) {
                        if (dwt.toDos.get(i).equals(toDos.get(k))) {
                            deadlineWithToDosRepository.update(toDos.get(k));
                            delete = false;
                            toDos.remove(k);
                            break;
                        }
                    }
                    if (delete)
                        deadlineWithToDosRepository.delete(dwt.toDos.get(i), id -> {});
                }
                for (int i = 0; i < toDos.size(); i++) {
                    deadlineWithToDosRepository.insert(toDos.get(i), deadline, id -> {});
                }
                dwt.toDos = toDos;
            }
        }
    }

    public StudyModule getStudyModule (Deadline deadline) {
        for (DeadlineToStudyModule dts : deadlineToStudyModulesList) {
            if (dts.deadline != null && dts.deadline.equals(deadline))
                return dts.studyModule;
        }
        return null;
    }

    public void setStudyModule (Deadline deadline, StudyModule studyModule) {
        deadlineToStudyModuleRepository.changeStudyModule(deadline, studyModule);
        for (DeadlineToStudyModule dts: deadlineToStudyModulesList) {
            if (dts.deadline != null && dts.deadline.equals(deadline)) {
                dts.studyModule = studyModule;
                dts.deadline.relatedStudyModuleID = studyModule.studyModuleID;
            }
        }
        for (Deadline d: deadlineList) {
            if (d.equals(deadline))
                d.relatedStudyModuleID = studyModule.studyModuleID;
        }
    }

    public SubmissionType getSubmissionType(Deadline deadline) {
        for (DeadlineToSubmissionType dts : deadlineToSubmissionTypesList) {
            if (dts.deadline != null && dts.deadline.equals(deadline))
                return dts.submissionType;
        }
        return null;
    }

    public void setSubmissionType(Deadline deadline, SubmissionType submissionType) {
        deadlineToSubmissionTypeRepository.changeSubmissionType(deadline,submissionType);
        for (DeadlineToSubmissionType dts : deadlineToSubmissionTypesList) {
            if (dts.deadline != null && dts.deadline.equals(deadline)) {
                dts.submissionType = submissionType;
                dts.deadline.relatedSubmissionTypeID = submissionType.submissionTypeID;
            }
        }
        for (Deadline d: deadlineList) {
            if (d.equals(deadline))
                d.relatedSubmissionTypeID = submissionType.submissionTypeID;
        }
    }

    /**
     * deletes SubmissionType if it is unused
     * @param submissionTypeID submissionType id
     */
    public void deleteSubmissionType(long submissionTypeID) {
        if(submissionTypeID > 3) {
            final boolean[] deleteSubmissionType = {true};
            final SubmissionType[] submissionType = new SubmissionType[1];
            submissionTypeRepository.getAll(list -> {
                if (list != null) {
                    for (int i = 3; i < list.size(); i++) {
                        if (list.get(i).submissionTypeID == submissionTypeID)
                            submissionType[0] = list.get(i);
                    }
                }
            });

            deadlineToSubmissionTypeRepository.getAll(list -> {
                if (list != null) {
                    for (DeadlineToSubmissionType dtst : list) {
                        if (dtst.submissionType.submissionTypeID == submissionType[0].submissionTypeID) {
                            deleteSubmissionType[0] = false;
                            break;
                        }
                    }
                }
            });
            if (deleteSubmissionType[0])
                submissionTypeRepository.delete(submissionType[0], id -> {});
        }
    }

    public List<SubmissionType> getAllSubmissionTypes() {
        SubmissionTypeRepository submissionTypeRepository = new SubmissionTypeRepository(this.activity);
        List<SubmissionType> submissionTypeList = new ArrayList<>();
        submissionTypeRepository.getAll(submissionTypeList::addAll);
        return submissionTypeList;
    }
}