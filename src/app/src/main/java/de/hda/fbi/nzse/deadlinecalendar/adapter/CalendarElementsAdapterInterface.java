package de.hda.fbi.nzse.deadlinecalendar.adapter;

/**
 * This interface implements listener for clicking on items of a recycler view.
 */
public interface CalendarElementsAdapterInterface {
        void onItemClickDeadline(int positionElement, int positionList);
        void onItemClickAppointment(int positionElement, int positionList);
}
