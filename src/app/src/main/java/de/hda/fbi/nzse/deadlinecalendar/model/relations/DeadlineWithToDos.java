package de.hda.fbi.nzse.deadlinecalendar.model.relations;

import androidx.room.Embedded;
import androidx.room.Ignore;
import androidx.room.Relation;

import java.util.List;

import de.hda.fbi.nzse.deadlinecalendar.model.Deadline;
import de.hda.fbi.nzse.deadlinecalendar.model.ToDo;

/**
 * One deadline to many toDos relation class
 *
 * @author Dennis Hilz, Anna Kilb
 */
public class DeadlineWithToDos {
    public DeadlineWithToDos() {}
    @Ignore
    public DeadlineWithToDos(Deadline deadline, List<ToDo> toDos) {
        this.deadline = deadline;
        this.toDos = toDos;
    }
    @Embedded public Deadline deadline;
    @Relation(
            parentColumn = "deadlineID",
            entityColumn = "relatedDeadlineID"
    )
    public List<ToDo> toDos;
}
