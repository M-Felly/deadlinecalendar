package de.hda.fbi.nzse.deadlinecalendar.model.repository;

import android.content.Context;

import java.util.List;

import de.hda.fbi.nzse.deadlinecalendar.DeadlineViewActivity;
import de.hda.fbi.nzse.deadlinecalendar.model.Deadline;
import de.hda.fbi.nzse.deadlinecalendar.model.StudyModule;
import de.hda.fbi.nzse.deadlinecalendar.model.SubmissionType;

/**
 *  DeadlineRepository
 *
 *  For more details
 * @author Dennis Hilz, Anna Kilb
 * @see BaseRepository
 */
public class DeadlineRepository extends BaseRepository{
    public static final String TAG = "DeadlineRepository";

    public DeadlineRepository(Context context) { super(context); }

    /**
     * Retrieving all Deadlines
     * @param resultCallbackListener that will be passed from the invoking activity {@link DeadlineViewActivity}
     */
    public void getDeadlines(ResultCallbackListener<List<Deadline>> resultCallbackListener) {
        executeDatabaseAction(DatabaseAction.SELECT, (appDatabase) -> appDatabase.deadlineDao().getAll(), resultCallbackListener);
    }

    /**
     * Insert a new Deadline
     * @param deadline to be inserted
     * @param resultCallbackListener that will be passed from the invoking activity {@link DeadlineViewActivity}
     */
    public void insert(Deadline deadline, StudyModule studyModule, SubmissionType submissionType, ResultCallbackListener<Long> resultCallbackListener) {
        deadline.relatedStudyModuleID = studyModule.studyModuleID;
        deadline.relatedSubmissionTypeID = submissionType.submissionTypeID;

        this.executeDatabaseAction(
                DatabaseAction.INSERT,
                (appDatabase) -> appDatabase.deadlineDao().insert(deadline),
                id -> {deadline.deadlineID = id; resultCallbackListener.onSuccess(id);}
        );
    }

    /**
     * Delete a Deadline
     * @param deadline to be deleted
     * @param resultCallbackListener that will be passed from the invoking activity {@link DeadlineViewActivity}
     */
    public void delete(Deadline deadline, ResultCallbackListener<Integer> resultCallbackListener) {
        executeDatabaseAction(DatabaseAction.DELETE, (appDatabase) -> appDatabase.deadlineDao().delete(deadline), resultCallbackListener);
    }

    /**
     * Update a Deadline
     * @param deadline to be updated
     */
    public void update(Deadline deadline) {
        executeDatabaseAction(DatabaseAction.UPDATE, appDatabase -> appDatabase.deadlineDao().update(deadline));
    }

    @Override
    protected String getTag() {
        return TAG;
    }
}
