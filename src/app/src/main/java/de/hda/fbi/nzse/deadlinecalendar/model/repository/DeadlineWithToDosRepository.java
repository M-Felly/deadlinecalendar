package de.hda.fbi.nzse.deadlinecalendar.model.repository;

import android.content.Context;

import java.util.List;

import de.hda.fbi.nzse.deadlinecalendar.DeadlineViewActivity;
import de.hda.fbi.nzse.deadlinecalendar.model.Deadline;
import de.hda.fbi.nzse.deadlinecalendar.model.ToDo;
import de.hda.fbi.nzse.deadlinecalendar.model.relations.DeadlineWithToDos;

/**
 * Deadline with toDos repository
 *
 * @author Dennis Hilz, Anna Kilb
 * @see BaseRepository
 */
public class DeadlineWithToDosRepository extends BaseRepository {
    public static final String TAG = "DeadlineWithToDosRepository";

    public DeadlineWithToDosRepository(Context context) {
        super(context);
    }

    public void getAll(ResultCallbackListener<List<DeadlineWithToDos>> resultCallbackListener) {
        this.executeDatabaseAction(DatabaseAction.SELECT, appDatabase -> appDatabase.deadlineWithToDosDao().getDeadlinesWithToDos(), resultCallbackListener);
    }

    /**
     * Insert a new List of ToDos
     *
     * @param toDos            to be inserted
     * @param resultCallbackListener that will be passed from the invoking activity {@link DeadlineViewActivity}
     */
    public void insert(List<ToDo> toDos, Deadline relatedDeadline, ResultCallbackListener<Long> resultCallbackListener) {
        for (ToDo toDo : toDos) {
            toDo.relatedDeadlineID = relatedDeadline.deadlineID;
            this.executeDatabaseAction(
                    DatabaseAction.INSERT,
                    (appDatabase) -> appDatabase.deadlineWithToDosDao().insert(toDo),
                    id -> {
                        toDo.toDoID = id;
                        resultCallbackListener.onSuccess(id);
                    }
            );
        }
    }

    /**
     * Insert a new ToDo
     *
     * @param toDo            to be inserted
     * @param resultCallbackListener that will be passed from the invoking activity {@link DeadlineViewActivity}
     */
    public void insert(ToDo toDo, Deadline relatedDeadline, ResultCallbackListener<Long> resultCallbackListener) {
        toDo.relatedDeadlineID = relatedDeadline.deadlineID;
        this.executeDatabaseAction(
                DatabaseAction.INSERT,
                (appDatabase) -> appDatabase.deadlineWithToDosDao().insert(toDo),
                id -> {
                    toDo.toDoID = id;
                    resultCallbackListener.onSuccess(id);
                }
        );
    }

    /**
     * Updating an existing ToDo
     *
     * @param toDo            to be updated
     */
    public void update(ToDo toDo) {
        this.executeDatabaseAction(
                DatabaseAction.UPDATE,
                (appDatabase) -> appDatabase.deadlineWithToDosDao().update(toDo));
    }

    /**
     * Delete a ToDo
     *
     * @param toDo            to be deleted
     * @param resultCallbackListener that will be passed from the invoking activity {@link DeadlineViewActivity}
     */
    public void delete(ToDo toDo, ResultCallbackListener<Integer> resultCallbackListener) {
        executeDatabaseAction(DatabaseAction.DELETE, (appDatabase) -> appDatabase.deadlineWithToDosDao().delete(toDo), resultCallbackListener);
    }

    @Override
    protected String getTag() {
        return TAG;
    }
}
