package de.hda.fbi.nzse.deadlinecalendar.model;

import android.content.Context;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Objects;

import de.hda.fbi.nzse.deadlinecalendar.DeadlineViewActivity;
import de.hda.fbi.nzse.deadlinecalendar.R;
import de.hda.fbi.nzse.deadlinecalendar.model.typeconverters.GregorianCalendarConverter;
import de.hda.fbi.nzse.deadlinecalendar.model.typeconverters.NotificationConverter;
import de.hda.fbi.nzse.deadlinecalendar.receiver.NotificationReceiver;

import static androidx.room.ForeignKey.CASCADE;

/**
 * A Deadline is getting displayed by {@link DeadlineViewActivity}.
 */

@Entity(foreignKeys = {
        @ForeignKey(entity = StudyModule.class,
                parentColumns = "studyModuleID",
                childColumns = "relatedStudyModuleID",
                onDelete = CASCADE
        ),
    },
    indices = {
        @Index("relatedStudyModuleID")
    }
)

/*
 * A Deadline represents a specific point in time towards which certain toDos have to be fulfilled {@Link ToDo}
 * and submitted in a specific way (postal, digital e.g.) {@Link SubmissionType}
 * This is mainly getting used by the {@link DeadlineViewActivity}
 */
public class Deadline implements NotificationReceiver.Notifiable, Comparable<Deadline>{

    @PrimaryKey(autoGenerate = true)
    public long deadlineID;

    public long relatedStudyModuleID;

    public long relatedSubmissionTypeID;

    @TypeConverters(GregorianCalendarConverter.class)
    private GregorianCalendar submissionDate;

    @TypeConverters(GregorianCalendarConverter.class)
    private GregorianCalendar reminderDate;

    private boolean isFinished;
    private boolean isDone;
    private String title;

    @TypeConverters(NotificationConverter.class)
    private Notification.DeadlineNotificationType notificationType;

    /**
     * Notification types are necessary for setting the correct reminder date of an deadline.
     * A deadline has different notification intervals than an appointment.
     */
    @Ignore
    public Deadline(String title, GregorianCalendar submissionDate, GregorianCalendar reminderDate, boolean isFinished) {
        this.title = title;
        this.submissionDate = submissionDate;
        this.reminderDate = reminderDate;
        this.isFinished = isFinished;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Deadline deadline = (Deadline) o;
        return deadlineID == deadline.deadlineID;
    }

    @Override
    public int hashCode() {
        return Objects.hash(deadlineID);
    }

    public Deadline() {
    }

    /* **************************************************************
     *                      Getter / Setter
     ****************************************************************/

    @Override
    public long getID() {
        return deadlineID;
    }

    @Override
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public GregorianCalendar getSubmissionDate() {
        return submissionDate;
    }

    public void setSubmissionDate(GregorianCalendar submissionDate) {
        this.submissionDate = submissionDate;
    }

    public GregorianCalendar getReminderDate() {
        return reminderDate;
    }

    public void setNotificationType(Notification.DeadlineNotificationType notificationType) {
        this.notificationType = notificationType;
    }

    public Notification.DeadlineNotificationType getNotificationType() {
        return notificationType;
    }

    @Override
    public GregorianCalendar getNotifiableReminderDate() {
        if (notificationType == Notification.DeadlineNotificationType.NO_NOTIFICATION) {
            return null;
        }

        return reminderDate;
    }

    public void setReminderDate(GregorianCalendar reminderDate) {
        this.reminderDate = reminderDate;
    }

    public boolean submissionDue() {
        GregorianCalendar currentTime = GregorianCalendarConverter.dateToCalendar(Calendar.getInstance().getTimeInMillis());
        long remainingTimeMs = (this.getSubmissionDate().getTimeInMillis() - GregorianCalendarConverter.calendarToLong(currentTime));
        if (remainingTimeMs < 0) {
            setFinished(true);
            return true;
        }
        return false;
    }

    public boolean isFinished() {return isFinished;}
    public boolean isDone() {return isDone;}

    public void setFinished(boolean finished) { isFinished = finished; }
    public void setDone(boolean done) {isDone = done;}

    @Override
    public Calendar getTargetDate() {
        return submissionDate;
    }

    @Override
    public int compareTo(Deadline dl) {
        return this.getSubmissionDate().compareTo(dl.getSubmissionDate());
    }

    /* **************************************************************
     *                      Static Enum Converters
     ****************************************************************/

    public static String notificationTypeToString(Context context, Notification.DeadlineNotificationType notificationTypeTemp) {

        String type = "";
        switch (notificationTypeTemp) {
            case NO_NOTIFICATION: {
                type = context.getString(R.string.noreminder);
                break;
            }
            case ONE_HOUR: {
                type = context.getString(R.string.onehour);
                System.out.println(type);
                break;
            }
            case THREE_HOURS: {
                type = context.getString(R.string.threehours);
                break;
            }
            case ONE_DAY: {
                type = context.getString(R.string.oneday);
                break;
            }
            case THREE_DAYS: {
                type = context.getString(R.string.threedays);
                break;
            }
            case ONE_WEEK: {
                type = context.getString(R.string.oneweek);
                break;
            }
        }
        return type;
    }
}
