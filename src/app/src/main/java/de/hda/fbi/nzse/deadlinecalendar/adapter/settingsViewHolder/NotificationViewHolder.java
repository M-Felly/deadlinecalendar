package de.hda.fbi.nzse.deadlinecalendar.adapter.settingsViewHolder;

import android.content.Context;
import android.view.View;

import com.google.android.material.switchmaterial.SwitchMaterial;

import java.util.Observable;

import de.hda.fbi.nzse.deadlinecalendar.R;
import de.hda.fbi.nzse.deadlinecalendar.adapter.SettingsExpandableListAdapter;
import de.hda.fbi.nzse.deadlinecalendar.model.Appointment;
import de.hda.fbi.nzse.deadlinecalendar.model.Deadline;
import de.hda.fbi.nzse.deadlinecalendar.model.Settings;
import de.hda.fbi.nzse.deadlinecalendar.model.repository.SettingsRepository;
import de.hda.fbi.nzse.deadlinecalendar.receiver.NotificationReceiver;

/**
 * Expandable list item for displaying the on/off switches for notification enable / disable
 *
 * @author Matthias Feyll
 * @see SettingsExpandableListAdapter
 */
public class NotificationViewHolder extends SettingsExpandableListAdapter.ChildViewHolder {

    private final SettingsRepository settingsRepository;
    private Settings settings;

    public NotificationViewHolder(Context context) {
        super(R.layout.adapter_settings_notification_list_view, R.string.adapterSettingsAppearanceGroupView_textView_title);

        settingsRepository = new SettingsRepository(context);
    }

    @Override
    public void onCreateChildView(View v) {
        SwitchMaterial deadlineSwitch = v.findViewById(R.id.adapterSettingsNotificationGroupView_switch_deadlines);
        SwitchMaterial appointmentSwitch = v.findViewById(R.id.adapterSettingsNotificationGroupView_switch_appointments);

        deadlineSwitch.setChecked(settings.isDeadlineNotificationEnabled());
        appointmentSwitch.setChecked(settings.isAppointmentNotificationEnabled());

        deadlineSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> onSwitchToggle(isChecked, Deadline.class));
        appointmentSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> onSwitchToggle(isChecked, Appointment.class));
    }

    /***************************************************************
     *                          Listener
     ***************************************************************/

    private void onSwitchToggle(boolean isChecked, Class<? extends NotificationReceiver.Notifiable> notifiableClass) {
        if (settings == null) {
            return;
        }

        settings.setNotificationEnabled(isChecked, notifiableClass);
        settingsRepository.update(settings);
    }

    @Override
    public void update(Observable o, Object arg) {
    }

    public void setSettings(Settings settings) {
        this.settings = settings;
    }
}



