package de.hda.fbi.nzse.deadlinecalendar;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import de.hda.fbi.nzse.deadlinecalendar.adapter.StudyModuleAdapter;
import de.hda.fbi.nzse.deadlinecalendar.dialogs.ProfileAddStudyModuleDialog;
import de.hda.fbi.nzse.deadlinecalendar.factory.DialogFragmentFactory;
import de.hda.fbi.nzse.deadlinecalendar.model.Profile;
import de.hda.fbi.nzse.deadlinecalendar.model.StudyModule;
import de.hda.fbi.nzse.deadlinecalendar.model.StudyModuleColor;
import de.hda.fbi.nzse.deadlinecalendar.model.repository.ProfileRepository;
import de.hda.fbi.nzse.deadlinecalendar.model.repository.StudyModuleRepository;

import static de.hda.fbi.nzse.deadlinecalendar.EntranceActivity.INITIAL_RUN_KEY;
import static de.hda.fbi.nzse.deadlinecalendar.dialogs.ProfileAddStudyModuleDialog.ACTION_LISTENER_KEY;
import static de.hda.fbi.nzse.deadlinecalendar.dialogs.ProfileAddStudyModuleDialog.STUDY_MODULE_COLOR_KEY;

/**
 * This activity expects a intent with a {@link EntranceProfileActivity#PROFILE_INTENT}
 *
 * @author Matthias Feyll
 */
public class EntranceStudyModuleActivity extends AppCompatActivity implements ProfileAddStudyModuleDialog.OnClickListener, ViewTreeObserver.OnPreDrawListener {
    private static final String ADD_STUDY_MODULE_DIALOG_TAG = "dialogs";
    private static final float RECYCLER_VIEW_PERCENTAGE_MAX_HEIGHT_ORIENTATION = 0.7f;
    private static final float RECYCLER_VIEW_PERCENTAGE_MAX_HEIGHT_LANDSCAPE = 0.3f;

    List<StudyModule> studyModuleList;
    Profile profile;

    StudyModuleAdapter studyModuleAdapter;
    ConstraintLayout recyclerViewParentView;
    ConstraintLayout rootLayout;
    RecyclerView studyModulesRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_study_module_entrance);

        // elements
        recyclerViewParentView = findViewById(R.id.stuyModuleEntranceActivity_constraintLayout_recyclerViewLayout);
        rootLayout = findViewById(R.id.studyModuleEntranceActivity_rootLayout);

        // recycler view
        studyModulesRecyclerView = findViewById(R.id.stuyModuleEntranceActivity_recyclerView_studyModules);
        studyModulesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        this.studyModuleAdapter = new StudyModuleAdapter(this);
        studyModulesRecyclerView.setAdapter(studyModuleAdapter);
        studyModulesRecyclerView.getViewTreeObserver().addOnPreDrawListener(this);


        Button addButton = findViewById(R.id.stuyModuleEntranceActivity_button_addStudyModule);
        addButton.setOnClickListener(v -> openDialog());

        this.profile = (Profile) getIntent().getSerializableExtra(EntranceProfileActivity.PROFILE_INTENT);
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences prefs = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        if (!prefs.getBoolean(INITIAL_RUN_KEY, true)) {
            finish();
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        loadStudyModule();
    }

    /***************************************************************
     *                        Load / Save
     ***************************************************************/

    private void loadStudyModule() {
        ProfileRepository profileRepository = new ProfileRepository(this);

        profileRepository.getProfileWithStudyModules(profileWithStudyModules -> {
            this.profile = profileWithStudyModules.profile;
            this.studyModuleList = profileWithStudyModules.studyModules;

            this.studyModuleAdapter.setStudyModuleList(studyModuleList);
        });
    }

    private void saveStudyModule(StudyModule studyModule) {
        StudyModuleRepository studyModuleRepository = new StudyModuleRepository(this);
        studyModuleRepository.insert(studyModule, profile);
    }

    /***************************************************************
     *                      Actions / Listener
     ***************************************************************/
    
    @Override
    public void onSubmitDialog(StudyModule studyModule) {
        studyModuleList.add(studyModule);
        this.studyModuleAdapter.notifyItemInserted(studyModuleList.size() - 1);

        this.saveStudyModule(studyModule);
    }

    private void openDialog() {
        Bundle dialogArguments = new Bundle();
        dialogArguments.putSerializable(ACTION_LISTENER_KEY, this);
        dialogArguments.putSerializable(STUDY_MODULE_COLOR_KEY, resolveNextStudyModuleColor());

        DialogFragment addStudyModuleDialog = DialogFragmentFactory.createInstance(ProfileAddStudyModuleDialog.class, dialogArguments);
        addStudyModuleDialog.show(getSupportFragmentManager(), ADD_STUDY_MODULE_DIALOG_TAG);
    }

    /* **************************************************************
     *                        Calculating
     ****************************************************************/
    /**
     * Resolving the next study module color depending by list size
     *
     * @return resolved next study module color
     */
    private StudyModuleColor resolveNextStudyModuleColor() {
        StudyModuleColor[] studyModuleColors = StudyModuleColor.values();

        if (!studyModuleList.isEmpty()) {
            StudyModuleColor currentLastIndexColor = studyModuleList.get(studyModuleList.size() -1).getColor();

            for (int i = 0; i < studyModuleColors.length; i++) {
                if (studyModuleColors[i] == currentLastIndexColor) {
                    return studyModuleColors[(i + 1) % (studyModuleColors.length)];
                }
            }
        }

        return studyModuleColors[0];
    }

    private int calculateRecyclerViewHeight() {
        float maxHeight = getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT ?
                RECYCLER_VIEW_PERCENTAGE_MAX_HEIGHT_ORIENTATION : RECYCLER_VIEW_PERCENTAGE_MAX_HEIGHT_LANDSCAPE;

        return (int) (this.rootLayout.getHeight() * maxHeight);
    }


    /***************************************************************
     *                          Drawable
     ***************************************************************/
    @Override
    public boolean onPreDraw() {
        ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) this.recyclerViewParentView.getLayoutParams();
        layoutParams.matchConstraintMaxHeight = calculateRecyclerViewHeight();
        this.recyclerViewParentView.setLayoutParams(layoutParams);

        this.studyModulesRecyclerView.getViewTreeObserver().removeOnPreDrawListener(this);

        return true;
    }


    /***************************************************************
     *                         Navigation
     ***************************************************************/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.edit_view_nav_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.editView_item_done) {
            if (studyModuleList.isEmpty()) {
                Toast.makeText(this, R.string.studyModulesEntranceViewActivity_toast_noStudyModules, Toast.LENGTH_LONG).show();

                return true;
            }
            
            startActualAppActivity();
            return true;
        }

        return false;
    }

    /**
     * Set initial run flag to false and start activity
     */
    private void startActualAppActivity() {
        SharedPreferences prefs = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        prefs.edit().putBoolean(INITIAL_RUN_KEY, false).apply();

        startActivity(new Intent(this, EntranceActivity.MAIN_ACTIVITY));
    }
}