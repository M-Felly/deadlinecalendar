package de.hda.fbi.nzse.deadlinecalendar.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.io.Serializable;
import java.net.URL;

import de.hda.fbi.nzse.deadlinecalendar.model.typeconverters.URLConverter;
import de.hda.fbi.nzse.deadlinecalendar.receiver.NotificationReceiver;

@Entity
public class Settings implements Serializable {
    @PrimaryKey(autoGenerate = true)
    public long id;

    @ColumnInfo(name = "url")
    @TypeConverters(URLConverter.class)
    private URL url;

    private boolean deadlineNotificationEnabled;
    private boolean appointmentNotificationEnabled;

    @Ignore
    public Settings(boolean deadlineNotificationEnabled, boolean appointmentNotificationEnabled) {
        this.url = null;
        this.deadlineNotificationEnabled = deadlineNotificationEnabled;
        this.appointmentNotificationEnabled = appointmentNotificationEnabled;
    }

    public Settings() {}

    public URL getUrl() {
        return url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

    public boolean isDeadlineNotificationEnabled() {
        return deadlineNotificationEnabled;
    }

    public void setDeadlineNotificationEnabled(boolean deadlineNotificationEnabled) {
        this.deadlineNotificationEnabled = deadlineNotificationEnabled;
    }

    public boolean isAppointmentNotificationEnabled() {
        return appointmentNotificationEnabled;
    }

    public void setAppointmentNotificationEnabled(boolean appointmentNotificationEnabled) {
        this.appointmentNotificationEnabled = appointmentNotificationEnabled;
    }

    public boolean isNotificationsEnabled(Class<? extends NotificationReceiver.Notifiable> notifiableClass) {
        return notifiableClass.equals(Deadline.class) ? deadlineNotificationEnabled : appointmentNotificationEnabled;
    }

    public void setNotificationEnabled(boolean value, Class<? extends NotificationReceiver.Notifiable> notifiableClass) {
        if (notifiableClass.equals(Deadline.class)) {
            this.deadlineNotificationEnabled = value;
        }else {
            this.appointmentNotificationEnabled = value;
        }
    }
}
