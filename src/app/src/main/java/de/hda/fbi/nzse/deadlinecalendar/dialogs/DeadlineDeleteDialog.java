package de.hda.fbi.nzse.deadlinecalendar.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import java.io.Serializable;

import de.hda.fbi.nzse.deadlinecalendar.R;

/**
 * This dialog is getting display from {@link de.hda.fbi.nzse.deadlinecalendar.EditDeadlineViewActivity} when a deadline
 * should going to be deleted. The dialog asks the user whether he wants to delete this deadline.
 *
 * @author Anna Kilb
 */
public class DeadlineDeleteDialog extends DialogFragment {
    public final static String ACTION_LISTENER_KEY = "actionListenerKey";

    /**
     * Callback listener to get the resolved attributes.
     *
     * @see DeadlineDeleteDialog
     */
    @FunctionalInterface
    public interface OnClickListener extends Serializable {
        void onSubmitDialog();
    }

    private DeadlineDeleteDialog.OnClickListener actionListener;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle arguments = getArguments();

        if (arguments == null) {
            throw new Error("Arguments missing");
        }

        this.actionListener = (OnClickListener) arguments.getSerializable(ACTION_LISTENER_KEY);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_deadline_delete, null);
        builder.setView(dialogView);

        // back button
        ImageButton backBtn = dialogView.findViewById(R.id.deadlineDeleteDialog_imageButton_back);
        backBtn.setOnClickListener(v -> dismiss());

        // submit button
        ImageButton submitBtn = dialogView.findViewById(R.id.deadlineDeleteDialog_imageButton_submit);
        submitBtn.setOnClickListener(v -> {
            this.actionListener.onSubmitDialog();
            dismiss();
        });

        return builder.create();
    }
}
