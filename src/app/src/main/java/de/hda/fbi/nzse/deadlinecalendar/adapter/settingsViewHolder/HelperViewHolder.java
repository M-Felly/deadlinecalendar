package de.hda.fbi.nzse.deadlinecalendar.adapter.settingsViewHolder;

import android.view.View;

import java.util.Observable;

import de.hda.fbi.nzse.deadlinecalendar.R;
import de.hda.fbi.nzse.deadlinecalendar.adapter.SettingsExpandableListAdapter;

/**
 * Expandable list item that displays the best and most helpful solution in the world
 *
 * @author Matthias Feyll
 * @see SettingsExpandableListAdapter
 */
public class HelperViewHolder extends SettingsExpandableListAdapter.ChildViewHolder {
    public HelperViewHolder() {
        super(R.layout.adapter_settings_help_list_view, R.string.adapterSettingsHelpGroupView_textView_title);
    }

    @Override
    public void onCreateChildView(View v) {

    }

    @Override
    public void update(Observable o, Object arg) {

    }
}