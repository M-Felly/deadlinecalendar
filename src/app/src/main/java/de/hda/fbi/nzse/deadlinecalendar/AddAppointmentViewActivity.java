package de.hda.fbi.nzse.deadlinecalendar;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

import de.hda.fbi.nzse.deadlinecalendar.manager.AlarmManager;
import de.hda.fbi.nzse.deadlinecalendar.manager.ManagerHolder;
import de.hda.fbi.nzse.deadlinecalendar.model.Appointment;
import de.hda.fbi.nzse.deadlinecalendar.model.Notification;
import de.hda.fbi.nzse.deadlinecalendar.model.repository.AppointmentRepository;

import static de.hda.fbi.nzse.deadlinecalendar.manager.NotificationManager.NOTIFICATION_APPOINTMENT_INTENT;

/**
 * In this activity, the user is able to create a new appointment that will be shown in CalendarViewActivity.
 */

@SuppressWarnings("ConstantConditions")
public class AddAppointmentViewActivity extends AppCompatActivity implements Serializable, AdapterView.OnItemSelectedListener {


    private BottomNavigationView bottomNavigation;
    private TextView textViewName;
    private TextView textViewDate;
    private TextView textViewFromTime;
    private TextView textViewToTime;
    private TextView textViewLocation;
    private TextView textViewDescription;
    private TextView textViewToTimeError;
    private Notification.AppointmentNotificationType notificationType;
    private AppointmentRepository appointmentRepository;
    private GregorianCalendar selectedDate;
    private DatePickerDialog.OnDateSetListener dateSetListener;
    private TimePickerDialog.OnTimeSetListener toTimeSetListener;
    private TimePickerDialog.OnTimeSetListener fromTimeSetListener;

    /**
     * On creating the activity, following actions have to be done that the activity works correctly.
     *
     * @param savedInstanceState given saved instance
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_appointment_view);

        // Set bottom navigation
        this.bottomNavigation = findViewById(R.id.addAppointmentViewActivity_bottomNavigation_bottomNav);
        this.bottomNavigation.setOnNavigationItemSelectedListener(new BottomNavigation(this));

        // Set toolbar elements
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_cancel_icon);
        setTitle(getString(R.string.title_newAppointment));

        // Initialize all view elements
        textViewName = findViewById(R.id.addAppointmentView_editText_name);
        textViewDate = findViewById(R.id.addAppointmentView_editText_date);
        textViewFromTime = findViewById(R.id.addAppointmentView_editText_from_time);
        textViewToTime = findViewById(R.id.addAppointmentView_editText_to_time);
        textViewLocation = findViewById(R.id.addAppointmentView_editText_location);
        textViewDescription = findViewById(R.id.addAppointmentView_editText_description);
        textViewToTimeError = findViewById(R.id.addAppointmentView_editText_to_time_error);
        Spinner notificationSpinner = findViewById(R.id.addAppointmentView_spinner_notificationpicker);

        //Set notification spinner
        notificationType = Notification.AppointmentNotificationType.NO_NOTIFICATION;
        ArrayAdapter<CharSequence> notificationSpinnerAdapter = ArrayAdapter.createFromResource(
                this, R.array.appointmentNotifications, R.layout.spinner);
        notificationSpinnerAdapter.setDropDownViewResource(R.layout.spinner);
        notificationSpinner.setAdapter(notificationSpinnerAdapter);
        notificationSpinner.setOnItemSelectedListener(this);

        selectedDate = (GregorianCalendar) getIntent().getExtras().getSerializable("selectedDate");

        SimpleDateFormat fromDateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.forLanguageTag(String.valueOf(R.string.languageTag)));
        textViewDate.setText(fromDateFormat.format(selectedDate.getTime()));

        onDateClicked();


        GregorianCalendar dateFromActualTime = new GregorianCalendar();
        GregorianCalendar dateToActualTime = new GregorianCalendar();

        SimpleDateFormat  timeFormat = new SimpleDateFormat("HH:mm", Locale.forLanguageTag(String.valueOf(R.string.languageTag)));

        //calculate to the actual time one hour
        int setHours = Integer.parseInt(timeFormat.format(dateToActualTime.getTime()).substring(0,2))+1;
        dateToActualTime.set(Calendar.HOUR_OF_DAY , setHours);

        String fromTime = timeFormat.format(dateFromActualTime.getTime());
        String toTime = timeFormat.format(dateToActualTime.getTime());


        textViewFromTime.setText(fromTime);
        onTimeFromClicked();

        textViewToTime.setText(toTime);
        onTimeToClicked();


        // Repository
        this.appointmentRepository = new AppointmentRepository(this);

    }

    /**
     * Method to show date picker and to select date
     */
    private void onDateClicked() {
        textViewDate.setOnClickListener(view -> {
            String date = textViewDate.getText().toString();
            int year = Integer.parseInt(date.substring(6, 10));
            int month = Integer.parseInt(date.substring(3, 5)) - 1;
            int day = Integer.parseInt(date.substring(0, 2));

            DatePickerDialog dialog = new DatePickerDialog(
                    AddAppointmentViewActivity.this,
                    dateSetListener,
                    year, month, day);
            dialog.show();
        });
        dateSetListener = (view, year, month, dayOfMonth) -> {
            Log.d("AddAppointmentViewActivity", "onDateSet: dd.MM.yyyy: " + dayOfMonth + "." + month + "." + year);

            String date1 = "";
            if (dayOfMonth < 10) {
                date1 = date1.concat("0" + dayOfMonth + ".");
            } else
                date1 = date1.concat(dayOfMonth + ".");
            if (month + 1 < 10)
                date1 = date1.concat("0" + (month + 1) + "." + year);
            else
                date1 = date1.concat((month + 1) + "." + year);
            textViewDate.setText(date1);
        };
    }

    /**
     * Method to show time picker and to select to time
     */
    private void onTimeToClicked() {
        textViewToTime.setOnClickListener(view -> {
            String time = textViewToTime.getText().toString();
            int hourOfDay = Integer.parseInt(time.substring(0, 2));
            int minute = Integer.parseInt(time.substring(3, 5));

            TimePickerDialog dialog = new TimePickerDialog(
                    AddAppointmentViewActivity.this,
                    toTimeSetListener,
                    hourOfDay, minute, true);
            dialog.show();
        });
        toTimeSetListener = (view, hourOfDay, minute) -> {
            Log.d("AddAppointmentViewActivity", "onTimeSet: hh:mm: " + hourOfDay + ":" + minute);

            String time = "";
            if (hourOfDay < 10)
                time = time.concat("0" + hourOfDay + ":");
            else
                time = time.concat(hourOfDay + ":");
            if (minute < 10)
                time = time.concat("0" + minute);
            else
                time = time.concat(Integer.toString(minute));
            textViewToTime.setText(time);
        };
      }

    /**
     * Method to show time picker and to select from time
     */
    private void onTimeFromClicked() {
        textViewFromTime.setOnClickListener(view -> {
            String time = textViewFromTime.getText().toString();
            int hourOfDay = Integer.parseInt(time.substring(0, 2));
            int minute = Integer.parseInt(time.substring(3, 5));

            TimePickerDialog dialog = new TimePickerDialog(
                    AddAppointmentViewActivity.this,
                    fromTimeSetListener,
                    hourOfDay, minute, true);
            dialog.show();
        });
        fromTimeSetListener = (view, hourOfDay, minute) -> {
            Log.d("AddAppointmentViewActivity", "onTimeSet: hh:mm: " + hourOfDay + ":" + minute);

            String time = "";
            String toTime = "";
            if (hourOfDay < 9) {
                time = time.concat("0" + hourOfDay + ":");
                toTime = toTime.concat("0" + (hourOfDay + 1) + ":");
            }
            else if (hourOfDay == 9){
                time = time.concat("0" + hourOfDay + ":");
                toTime = toTime.concat((hourOfDay + 1) + ":");
            }
            else {
                time = time.concat(hourOfDay + ":");
                toTime = toTime.concat((hourOfDay + 1) + ":");
            }

            if (minute < 10) {
                time = time.concat("0" + minute);
                toTime = toTime.concat("0" + minute);
            }
            else {
                time = time.concat(Integer.toString(minute));
                toTime = toTime.concat(Integer.toString(minute));
            }
            textViewFromTime.setText(time);
            textViewToTime.setText(toTime);
        };
    }

    /**
     * Checks the correct icon in bottom navigation bar.
     */
    @Override
    protected void onResume() {
        super.onResume();
        this.bottomNavigation.getMenu().findItem(R.id.bottom_nav_calendar).setChecked(true);
    }

    /**
     * Goes back to CalendarViewActivity.
     *
     * @return if action was successful.
     */
    @Override
    public boolean onSupportNavigateUp() {
        startActivity(new Intent(AddAppointmentViewActivity.this,CalendarViewActivity.class).putExtra("class", this.getClass().toString()));
        onBackPressed();
        return true;
    }


    /**
     * Creates/inflates all items of an menu an bind it onto the toolbar.
     *
     * @param menu is the menu that shall be inflated
     * @return if action was successful
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_view_nav_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /***
     * Implements the action if a specific element is selected.
     *
     * If the user wants to submit his content, all the input will be proven with the method
     * onSubmitListener. Once the content is valid, an object of type Appointment will be created
     * and inserted into the database.
     *
     * @param item is the selected item
     * @return is the selected item
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.editView_item_done) {
            if (onSubmitListener()) {
                Appointment appointment = getAppointment();
                appointmentRepository.insert(appointment, id -> {
                });

                AlarmManager alarmManager = ManagerHolder.getManager(AlarmManager.class);
                alarmManager.register(this, appointment);

                startActivity(new Intent(AddAppointmentViewActivity.this, DetailCalendarViewActivity.class).putExtra("class", this.getClass().toString()).putExtra(NOTIFICATION_APPOINTMENT_INTENT, appointment).putExtra("selectedDate",selectedDate));
            }
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Listens users click on the specific item at the toolbar to submit the input.
     *
     * @return if user input is valid.
     */
   private boolean onSubmitListener() {
       return checkIfInputIsValid();
   }

    /**
     * Checks if all input fields are correctly filled from the user.
     *
     * @return if user input is valid.
     */
    public boolean checkIfInputIsValid(){

        boolean validInput = true;

        //Checking name field
        if (this.textViewName.getText().toString().isEmpty()) {
            textViewName.setError(getString(R.string.add_edit_appointmentview_error_set_valid_name));
            validInput = false;
        }

        // Checking date field
        if (this.textViewDate.getText().toString().isEmpty()) {
            textViewDate.setError(getString(R.string.add_edit_appointmentview_error_set_valid_date));
            validInput = false;
        }
        else if (this.textViewDate.getText().toString().length() != 10){
            textViewDate.setError(getString(R.string.add_edit_appointmentview_error_setvalid_date_format));
            validInput = false;
        }
        else if (textViewDate.getText().toString().charAt(2) != '.' ||
                 textViewDate.getText().toString().charAt(5) != '.'    ){
            textViewDate.setError(getString(R.string.add_edit_appointmentview_error_setvalid_date_format));
            validInput = false;
        }
        else {

            //Check if input date exists
            String dateString = textViewDate.getText().toString();
            int day = Integer.parseInt(dateString.substring(0,2));
            int month = Integer.parseInt(dateString.substring(3,5)) -1;
            int year = Integer.parseInt(dateString.substring(6,10));
            GregorianCalendar date = new GregorianCalendar();
            date.set(Calendar.DAY_OF_MONTH, day);
            date.set(Calendar.MONTH, month);
            date.set(Calendar.YEAR, year);
            if (date.get(Calendar.DAY_OF_MONTH) != day){
                textViewDate.setError(getString(R.string.add_edit_appointmentview_error_date_doesnt_exist));
                validInput = false;
            }
            if (month <= -1 || month > 11){
                textViewDate.setError(getString(R.string.add_edit_appointmentview_error_date_doesnt_exist));
                validInput = false;
            }
        }


        //Checking from time field
        boolean validTimeFrom = true;
        boolean validTimeTo = true;

      if (validTimeInput(textViewFromTime, getString(R.string.add_edit_appointmentview_starttime))){
          validTimeFrom = false;
          validInput = false;
      }

      //Checking to time field
       if(validTimeInput(textViewToTime, getString(R.string.add_edit_appointmentview_endtime))){
          validTimeTo = false;
          validInput = false;
      }
        if (validTimeFrom && validTimeTo){

            if (!startTimeSmallerThanEndTime()){
                validInput = false;
            }
        }

        //If everything is okay >> return true
        return validInput;


    }

    /**
     * sub-method of checkIfInputIsValid.
     * Checks if the start time is smaller than the end time of an appointment.
     *
     * @return true: start time is smaller than the end time.
     *         false: if not.
     */
    private boolean startTimeSmallerThanEndTime(){
        int minutesStartTime = Integer.parseInt(textViewFromTime.getText().toString().substring(3,5));
        int hoursStartTime = Integer.parseInt(textViewFromTime.getText().toString().substring(0,2));
        int minutesEndTime = Integer.parseInt(textViewToTime.getText().toString().substring(3,5));
        int hoursEndTime = Integer.parseInt(textViewToTime.getText().toString().substring(0,2));

        if (hoursEndTime < hoursStartTime){
            textViewToTimeError.setError(getString(R.string.add_edit_appointmentview_error_endtime_is_smaller));
            return false;
        }
        else if (hoursEndTime == hoursStartTime && minutesEndTime < minutesStartTime){
            textViewToTimeError.setError(getString(R.string.add_edit_appointmentview_error_endtime_is_smaller));
            return false;
        }
        return true;
    }

    /**
     * sub-method of checkIfInputIsValid.
     * Checks if time inputs of different types are valid.
     *
     * @param time is the given TextView that shall be checked.
     * @param timeType start time or end time to set specific error reports to the user.
     * @return if time input was valid or not.
     */
    private boolean validTimeInput(TextView time, String timeType){

        if (time.getText().toString().isEmpty()) {
            String errormessage = timeType + " " + getString(R.string.add_edit_appointmentview_error_empty_field);
            time.setError(errormessage);
            return true;
        }
        else if (time.getText().toString().length() != 5){
            time.setError(getString(R.string.add_edit_appointmentview_error_invalid_time_format));
            return true;
        }
        else {
            int hours = Integer.parseInt(time.getText().toString().substring(0,2));
            int minutes = Integer.parseInt(time.getText().toString().substring(3,5));
            if (hours > 24 || hours < 0 || minutes > 59 || minutes < 0){
                String errormessage2 = timeType + " " + getString(R.string.add_edit_appointmentview_error_invalid_time);
                time.setError(errormessage2);
                return true;
            }
        }
        return false;
    }


    /**
     * Processes all data and fill it into a new Object of Type Appointment.
     *
     * @return the processed appointment.
     */
    private Appointment getAppointment(){

        // Get date of the appointment
        String date = textViewDate.getText().toString();
        int day = Integer.parseInt(date.substring(0,2));
        int month = Integer.parseInt(date.substring(3,5)) -1;
        int year = Integer.parseInt(date.substring(6,10));
        GregorianCalendar dateOfAppointment_from = new GregorianCalendar();
        dateOfAppointment_from.set(Calendar.DAY_OF_MONTH, day);
        dateOfAppointment_from.set(Calendar.MONTH, month);
        dateOfAppointment_from.set(Calendar.YEAR, year);

        //Get time of the appointment
        String fromTime = textViewFromTime.getText().toString();
        String toTime = textViewToTime.getText().toString();
        if (toTime.equals("24:00")){
            toTime = "23:59";
        }

        //from date
        setMinutesAndHours(dateOfAppointment_from, fromTime);

        //to date
        GregorianCalendar dateOfAppointment_to = new GregorianCalendar();
        dateOfAppointment_to.setTimeInMillis(dateOfAppointment_from.getTimeInMillis());
        setMinutesAndHours(dateOfAppointment_to, toTime);


        //Get reminder date
        GregorianCalendar reminderDateOfAppointment = new GregorianCalendar();
        reminderDateOfAppointment.setTimeInMillis(dateOfAppointment_from.getTimeInMillis());

        switch (notificationType){

            case FIVE_MINUTES:{
                reminderDateOfAppointment.add(Calendar.MINUTE,-5);
                break;
            }
            case FIFTEEN_MINUTES:{
                reminderDateOfAppointment.add(Calendar.MINUTE,-15);
                break;
            }
            case THIRTY_MINUTES:{
                reminderDateOfAppointment.add(Calendar.MINUTE,-30);
                break;
            }
            case ONE_HOUR:{
                reminderDateOfAppointment.add(Calendar.HOUR_OF_DAY,-1);
                break;
            }
            case TWO_HOURS:{
                reminderDateOfAppointment.add(Calendar.HOUR_OF_DAY,-2);
                break;
            }
            case ONE_DAY:{
                reminderDateOfAppointment.add(Calendar.MONTH,-1);
                break;
            }
        }

        //Check if appointment is empty
        if (this.textViewLocation.getText().toString().isEmpty()){
            textViewLocation.setText(getString(R.string.add_edit_appointmentview_no_location_set));
        }

        return new Appointment(textViewName.getText().toString(),notificationType, reminderDateOfAppointment,dateOfAppointment_from,dateOfAppointment_to ,textViewLocation.getText().toString(), textViewDescription.getText().toString(), true);
    }

    /**
     * Sets the time of an GregorianCalendar correctly out of an given string.
     *
     * @param cal is the calendar whose time shall be set.
     * @param time is the time of type String that shall be processed, so the method of GregorianCalendar
     *             can be used for setting the correct time.
     */
    private void setMinutesAndHours(GregorianCalendar cal, String time){
        int hours = Integer.parseInt(time.substring(0,2));
        int minutes = Integer.parseInt(time.substring(3,5));

        cal.set(GregorianCalendar.HOUR_OF_DAY, hours);
        cal.set(GregorianCalendar.MINUTE, minutes);

    }

    /**
     * Sets the type of notification, every time the user chooses a new item of the spinner (users
     * can choose if they want to have a reminder for their date using the notification spinner).
     *
     * @param parent given parent.
     * @param view given view.
     * @param position given position (on-clicked by user) of spinner element.
     * @param id given id of spinner element.
     */
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (position){
            case 0: {
                notificationType = Notification.AppointmentNotificationType.NO_NOTIFICATION;
                break;
            }
            case 1: {
                notificationType = Notification.AppointmentNotificationType.FIVE_MINUTES;
                break;
            }
            case 2: {
                notificationType = Notification.AppointmentNotificationType.FIFTEEN_MINUTES;
                break;
            }
            case 3: {
                notificationType = Notification.AppointmentNotificationType.THIRTY_MINUTES;
                break;
            }
            case 4: {
                notificationType = Notification.AppointmentNotificationType.ONE_HOUR;
                break;
            }
            case 5: {
                notificationType = Notification.AppointmentNotificationType.TWO_HOURS;
                break;
            }
            case 6:{
               notificationType = Notification.AppointmentNotificationType.ONE_DAY;
               break;
            }
        }

    }

    /**
     * Does quietly nothing - if nothing is selected :).
     *
     * @param parent given parent.
     */
    @Override
    public void onNothingSelected(AdapterView<?> parent) { }
}