package de.hda.fbi.nzse.deadlinecalendar.model.relations;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

import de.hda.fbi.nzse.deadlinecalendar.model.Profile;
import de.hda.fbi.nzse.deadlinecalendar.model.StudyModule;

/**
 * One profile to many study modules relation class
 *
 * @author Matthias Feyll
 */
public class ProfileWithStudyModules {
    @Embedded public Profile profile;
    @Relation(
            parentColumn = "profileID",
            entityColumn = "relatedProfileID"
    )
    public List<StudyModule> studyModules;
}
