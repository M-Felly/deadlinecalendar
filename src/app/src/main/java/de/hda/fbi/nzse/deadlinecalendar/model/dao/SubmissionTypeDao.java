package de.hda.fbi.nzse.deadlinecalendar.model.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;

import java.util.List;

import de.hda.fbi.nzse.deadlinecalendar.model.SubmissionType;

@Dao
public interface SubmissionTypeDao {

    @Transaction
    @Query("SELECT * FROM submissiontype")
    List<SubmissionType> getSubmissionTypes();

    @Insert
    long insert(SubmissionType submissionType);

    @Delete
    int delete(SubmissionType submissionType);
}
