package de.hda.fbi.nzse.deadlinecalendar.model.dao;

import androidx.room.Dao;
import androidx.room.Query;
import androidx.room.Transaction;

import java.util.List;

import de.hda.fbi.nzse.deadlinecalendar.model.relations.DeadlineToSubmissionType;

@Dao
public interface DeadlineToSubmissionTypeDao {

    @Transaction
    @Query("SELECT * FROM deadline")
    List<DeadlineToSubmissionType> getDeadlinesToSubmissionType();
}
