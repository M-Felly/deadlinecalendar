package de.hda.fbi.nzse.deadlinecalendar.model.typeconverters;

import androidx.annotation.Nullable;
import androidx.room.TypeConverter;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Converter to convert a url to a string and reverse
 *
 * @author Matthias Feyll
 */
public class URLConverter {
    /**
     * Converts a string to a url or null object if the url is malformed
     *
     * @param _url to be converted
     * @return url object
     */
    @TypeConverter
    @Nullable
    public static URL stringToUrl(String _url) {
        URL url;
        try {
            url = new URL(_url);
        }catch (MalformedURLException e) {
            return null;
        }
        return url ;
    }

    /**
     * URL object that will be saved in database
     * If its null a empty string will be saved
     *
     * @param url to be converted to a string
     * @return string that will be saved in database
     */
    @TypeConverter
    public static String urlToString(URL url) {
        if (url == null) {
            return "";
        }

        return url.toString();
    }
}
