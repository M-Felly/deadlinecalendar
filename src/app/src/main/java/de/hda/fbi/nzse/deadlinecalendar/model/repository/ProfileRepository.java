package de.hda.fbi.nzse.deadlinecalendar.model.repository;

import android.content.Context;
import android.util.Log;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import de.hda.fbi.nzse.deadlinecalendar.model.Profile;
import de.hda.fbi.nzse.deadlinecalendar.model.relations.ProfileWithStudyModules;

/**
 * Profile repository
 *
 * - add the profile                       {@link #insert(Profile)}
 * - update the profile                    {@link #update(Profile)}
 * - get the profile with study modules    {@link #getProfileWithStudyModules(ResultCallbackListener)}
 *
 * @author Matthias Feyll
 * @see BaseRepository
 */
public class ProfileRepository extends BaseRepository {
    public final String TAG = "ProfileRepository";

    public ProfileRepository(Context context) {
        super(context);
    }

    public void insert(Profile profile) {
        try {
            Profile existingProfile = getProfileAsync().get();
            if (existingProfile != null){
                profile.profileID = existingProfile.profileID;
                this.executeDatabaseAction(DatabaseAction.UPDATE, appDatabase -> appDatabase.profileDao().update(profile));
            }else {
                this.executeDatabaseAction(DatabaseAction.INSERT, appDatabase -> appDatabase.profileDao().insert(profile), id -> profile.profileID = id);
            }
        } catch (ExecutionException | InterruptedException e) {
            Log.e(TAG, "Something went wrong during the database fetch process: " + e.getMessage());
        }

    }

    public void update(Profile profile) {
        this.executeDatabaseAction(DatabaseAction.UPDATE, (appDatabase) -> appDatabase.profileDao().update(profile));
    }

    public void getProfileWithStudyModules(ResultCallbackListener<ProfileWithStudyModules> resultCallbackListener) {
        this.executeDatabaseAction(DatabaseAction.SELECT, (appDatabase) -> appDatabase.profileWithStudyModulesDao().getProfileWithStudyModules(), resultCallbackListener);
    }

    public Future<Profile> getProfileAsync() {
        return this.executeAsyncDatabaseAction(DatabaseAction.SELECT, appDatabase -> appDatabase.profileDao().getFirst());
    }

    @Override
    protected String getTag() {
        return TAG;
    }
}
