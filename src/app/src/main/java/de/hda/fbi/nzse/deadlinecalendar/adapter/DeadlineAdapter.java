package de.hda.fbi.nzse.deadlinecalendar.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import de.hda.fbi.nzse.deadlinecalendar.R;
import de.hda.fbi.nzse.deadlinecalendar.model.Deadline;
import de.hda.fbi.nzse.deadlinecalendar.model.ToDo;
import de.hda.fbi.nzse.deadlinecalendar.model.typeconverters.RemainingTimeConverter;
import de.hda.fbi.nzse.deadlinecalendar.presenter.DeadlineAccessHandler;
import de.hda.fbi.nzse.deadlinecalendar.view_elements.custom.StudyModuleShape;

/**
 * With this adapter you can display a list of {@link Deadline}.
 *
 * @author Dennis Hilz, Anna Kilb
 */
public class DeadlineAdapter extends RecyclerView.Adapter<DeadlineAdapter.DeadlineViewHolder> implements Filterable {
    private final List<Deadline> deadlineList = new ArrayList<>();
    private final List<Deadline> deadlineListFull = new ArrayList<>();
    private final List<Deadline> deadlineListFinished = new ArrayList<>();
    private final List<Deadline> deadlineListDone = new ArrayList<>();
    private final Context context;
    final DeadlineAccessHandler deadlineAccessHandler;
    private final SelectedDeadline selectedDeadline;

    public DeadlineAdapter(Context context, DeadlineAccessHandler deadlineAccessHandler, SelectedDeadline selectedDeadline) {
        this.context = context;
        this.deadlineAccessHandler = deadlineAccessHandler;
        setDeadlineList(deadlineAccessHandler.getDeadlines());
        this.selectedDeadline = selectedDeadline;
    }

    /**
     * <p>Returns a filter that can be used to constrain data with a filtering
     * pattern.</p>
     *
     * <p>This method is usually implemented by {@link Adapter}
     * classes.</p>
     *
     * @return a filter used to constrain data
     */
    @Override
    public Filter getFilter() {
        return new Filter() {

            /**
             * <p>Invoked in a worker thread to filter the data according to the
             * constraint. Subclasses must implement this method to perform the
             * filtering operation. Results computed by the filtering operation
             * must be returned as a {@link FilterResults} that
             * will then be published in the UI thread through
             * {@link #publishResults(CharSequence,
             * FilterResults)}.</p>
             *
             * <p><strong>Contract:</strong> When the constraint is null, the original
             * data must be restored.</p>
             *
             * @param constraint the constraint used to filter the data
             * @return the results of the filtering operation
             * @see #filter(CharSequence, FilterListener)
             * @see #publishResults(CharSequence, FilterResults)
             * @see FilterResults
             */
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                List<Deadline> filteredDeadlines = new ArrayList<>();
                if (constraint == null || constraint.length() == 0) {
                    filteredDeadlines.addAll(deadlineList);
                } else {

                    String[] filterPattern = constraint.toString().split(";");

                    for (Deadline deadline : deadlineListFull) {
                        for (String s : filterPattern) {
                            if (deadlineAccessHandler.getStudyModule(deadline).getTag().equals(s))
                                filteredDeadlines.add(deadline);
                        }
                    }
                }
                FilterResults results = new FilterResults();
                results.values = filteredDeadlines;
                return results;
            }

            /**
             * <p>Invoked in the UI thread to publish the filtering results in the
             * user interface. Subclasses must implement this method to display the
             * results computed in {@link #performFiltering}.</p>
             *
             * @param constraint the constraint used to filter the data
             * @param results    the results of the filtering operation
             * @see #filter(CharSequence, FilterListener)
             * @see #performFiltering(CharSequence)
             * @see FilterResults
             */
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                List<Deadline> tempList = (List<Deadline>) results.values;

                deadlineList.clear();
                deadlineList.addAll(tempList);
                notifyDataSetChanged();
            }
        };
    }

    /**
     * Called if no filter options are selected -> Empty RecyclerView
     */
    public void nonSelectedFilter() {
        deadlineList.clear();
        notifyDataSetChanged();
    }

    /**
     * Interface for selecting a specific Deadline.
     */
    public interface SelectedDeadline {
        void onDeadlineIsSelected(Deadline deadline);
    }

    public class DeadlineViewHolder extends RecyclerView.ViewHolder {

        private final TextView nameTextView;
        private final TextView counterTextView;
        private final TextView completionTextView;
        private final ImageView completionCheckImage;
        private final StudyModuleShape studyModuleShape;

        public DeadlineViewHolder(View view) {
            super(view);
            this.studyModuleShape = view.findViewById(R.id.deadlineAdapter_tagShape_tag);
            this.nameTextView = view.findViewById(R.id.deadlineAdapter_textView_name);
            this.counterTextView = view.findViewById(R.id.deadlineAdapter_textView_counter);
            this.completionTextView = view.findViewById(R.id.deadlineAdapter_textView_completion);
            this.completionCheckImage = view.findViewById(R.id.deadlineAdapter_imageView_completionCheck);
            ConstraintLayout constraintLayout = view.findViewById(R.id.deadlineAdapter_constraintLayout);

            // Select one specific item of the recycler view
            constraintLayout.setOnClickListener(v -> selectedDeadline.onDeadlineIsSelected(deadlineList.get(getAdapterPosition())));
        }

        public TextView getNameTextView() {
            return nameTextView;
        }

        public TextView getCounterTextView() {
            return counterTextView;
        }

        public TextView getCompletionTextView() {
            return completionTextView;
        }

        public StudyModuleShape getStudyModuleShape() {
            return studyModuleShape;
        }


    }

    @NonNull
    @Override
    public DeadlineViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_deadline, viewGroup, false);
        return new DeadlineViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DeadlineViewHolder viewHolder, int position) {
        viewHolder.getStudyModuleShape().setStudyModule(deadlineAccessHandler.getStudyModule(this.deadlineList.get(position)));
        viewHolder.getNameTextView().setText(this.deadlineList.get(position).getTitle());
        viewHolder.getCounterTextView().setText(RemainingTimeConverter.assembleOutputString(RemainingTimeConverter.parseRemainingTime(context, this.deadlineList.get(position))));

        int finishedToDos = 0;
        for (ToDo toDo : deadlineAccessHandler.getToDos(this.deadlineList.get(position))) {
            if (toDo.getIsDone())
                finishedToDos++;
        }
        String completionState = finishedToDos + "/" + deadlineAccessHandler.getToDos(this.deadlineList.get(position)).size() + "\nToDos";
        viewHolder.getCompletionTextView().setText(completionState);


        /*
         * If every todo within the respective deadline has been checked/fulfilled, a small arrow-circle symbol appears
         * to indicate completion of this deadline
         */
        if (finishedToDos == deadlineAccessHandler.getToDos(this.deadlineList.get(position)).size()) {
            viewHolder.completionTextView.setVisibility(View.INVISIBLE);
            viewHolder.completionCheckImage.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return this.deadlineList.size();
    }

    public void setDeadlineList(List<Deadline> pDeadlineList) {
        this.deadlineList.clear();
        this.deadlineListFull.clear();

        /*
         * The lines stating @deadlineListDone and @deadlineListFinished are written for possible features in the future :)
         */
        this.deadlineListDone.clear();
        this.deadlineListFinished.clear();

        for (Deadline deadline : pDeadlineList) {
            if (!(deadline.submissionDue()) && !(deadline.isDone())) {
                this.deadlineList.add(deadline);
                this.deadlineListFull.add(deadline);
            }
            if (deadline.isDone())
                this.deadlineListDone.add(deadline);
            if (deadline.isFinished())
                this.deadlineListFinished.add(deadline);
        }
        this.deadlineList.sort(Deadline::compareTo);
        this.deadlineListFull.sort(Deadline::compareTo);
        notifyDataSetChanged();
    }
}
