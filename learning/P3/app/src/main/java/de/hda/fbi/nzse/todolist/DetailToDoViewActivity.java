package de.hda.fbi.nzse.todolist;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Vector;

import de.hda.fbi.nzse.todolist.adapter.TasksAdapter;
import de.hda.fbi.nzse.todolist.model.Task;
import de.hda.fbi.nzse.todolist.model.ToDo;

/**
 * You can create new Tasks here.
 *
 * This class returns a Intent with updated todo item on success.
 * On back pressed the class just finishes without Intent
 */
public class DetailToDoViewActivity extends AppCompatActivity implements TasksAdapter.SelectedTask {

    private RecyclerView recyclerViewTasks;
    private EditText editTextToDoName;
    private EditText editTextToDoDesc;
    private TasksAdapter tasksAdapter;
    private FloatingActionButton addTaskButton;
    private FloatingActionButton saveToDoButton;
    private ToDo parentToDo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_to_do_view);

        // Inflation of all used elements
        editTextToDoName = findViewById(R.id.detailToDoViewActivity_editText_todoName);
        editTextToDoDesc = findViewById(R.id.detailToDoViewActivity_editText_todoDescription);
        recyclerViewTasks = findViewById(R.id.detailToDoActivity_recyclerView_tasks);
        addTaskButton = findViewById(R.id.detailToDoActivity_btn_add);
        saveToDoButton = findViewById(R.id.detailToDoActivity_btn_save);

        // Registration of OnClickListeners
        addTaskButton.setOnClickListener(this::onAddTaskButtonClicked);
        saveToDoButton.setOnClickListener(this::onSaveToDoButtonClicked);

        parentToDo = (ToDo) getIntent().getExtras().getSerializable("ToDo");

        tasksAdapter = new TasksAdapter(this, parentToDo.getTasks(), this );

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(parentToDo.getName());
        editTextToDoName.setText(parentToDo.getName());
        editTextToDoDesc.setText(parentToDo.getDescription());

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerViewTasks.setLayoutManager(linearLayoutManager);
        recyclerViewTasks.setAdapter(tasksAdapter);
    }

    /**This function is called if the floating action button addTaskButton is clicked.
     * @param v given View of invoking function.
     */
    private void onAddTaskButtonClicked(View v) {
        // Create an alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Task Name");
        // Set the dialog layout
        final View dialogLayout = getLayoutInflater().inflate(R.layout.activity_detail_to_do_view_dialog, null);
        builder.setView(dialogLayout);

        // Add the dialog confirm button
        builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Send data from the dialog back to the activity
                EditText editText = dialogLayout.findViewById(R.id.detailToDoViewActivity_editText_dialogTaskName);
                sendDialogDataToActivity(editText.getText().toString());}
        });
        // Create and show the dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**This function is called if the floating action button saveToDoButton is clicked.
     * @param v given View of invoking function.
     */
    private void onSaveToDoButtonClicked(View v) {
        parentToDo.setName(editTextToDoName.getText().toString());
        parentToDo.setDescription(editTextToDoDesc.getText().toString());
        Intent resultIntent = new Intent();

        resultIntent.putExtra("ToDo", parentToDo);
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }

    /**
     * Called on back pressed and ends the activity without any Intent.
     */
    @Override
    public boolean onSupportNavigateUp() {
        super.onBackPressed();
        finish();

        return true;
    }

    /**
     * This function processes the returned string for the new task from the called dialog
     * and notifies tasksAdapter, that new item has been inserted.
     *
     * @param taskName represents the new Task to be created and added to vector
     */
    private void sendDialogDataToActivity(String taskName) {
        parentToDo.addTask(new Task(taskName));
        tasksAdapter.notifyItemInserted(parentToDo.getTasks().size()-1);
    }

    /**
     * Interface for selecting a specific Task.
     */
    @Override
    public void selectedTask(Task task) {

    }
}