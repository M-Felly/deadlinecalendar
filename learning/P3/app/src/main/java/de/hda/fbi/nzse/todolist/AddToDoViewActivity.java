package de.hda.fbi.nzse.todolist;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import de.hda.fbi.nzse.todolist.model.ToDo;

/**
 * You can create new Todos here
 *
 * This class returns a Intent with extended data (key: ToDo) on success.
 * On back pressed the class just finishes without Intent
 */
public class AddToDoViewActivity extends AppCompatActivity {

    private EditText nameEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_to_do_view);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.addToDoViewActivity_actionBar_title);

        // elements
        nameEditText = findViewById(R.id.addToDoViewActivity_editText_name);
    }

    /**
     * Main handler on submit.
     * If the user input is valid the method will execute the completion process.
     */
    private void onSubmitListener() {
        if (!checkInputElementsAreValid()) {
            return;
        }

        Intent resultIntent = new Intent();
        resultIntent.putExtra("ToDo", getToDo());
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }

    /**
     * Checks if the nameEditText input element contains text
     *
     * @return truth value whether the user input is valid
     */
    private boolean checkInputElementsAreValid() {
        if (this.nameEditText.getText().toString().isEmpty()) {
            Toast.makeText(this, getString(R.string.addToDoViewActivity_toast_text_noValidName), Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }


    /**
     * Creates a ToDo_ out of the input elements
     * @return created todo_ object
     */
    private ToDo getToDo() {
        return new ToDo(this.nameEditText.getText().toString());
    }


    /**
     * Calls the submit listener by clicking the done icon
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.media_route_menu_item) {
            onSubmitListener();
            return true;
        }

        return false;
    }

    /**
     * Called on back pressed and ends the activity without any Intent
     */
    @Override
    public boolean onSupportNavigateUp() {
        super.onBackPressed();
        finish();

        return true;
    }


    /**
     * Adds the done icon to the action bar
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.submit_icon_action_bar, menu);
        return true;
    }
}