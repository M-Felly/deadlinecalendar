package de.hda.fbi.nzse.todolist.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Vector;

import de.hda.fbi.nzse.todolist.R;
import de.hda.fbi.nzse.todolist.model.ToDo;

/**This class is a specialized adapter for the recycler view. It combines the recycler view with
 * the xml items and the ToDo class values.
 *
 */
public class ToDosAdapter extends RecyclerView.Adapter<ToDosAdapter.ViewholderToDos> {

    private Vector<ToDo> todos;
    private Context context;
    private SelectedToDo selectedToDo;

    /**Constructor of ToDosAdapter.
     *
     * @param context gets the Context.
     * @param todos all created todos, given by ToDoOverviewActivity.
     * @param selectedToDo defines the actual ToDo.
     */
    public ToDosAdapter(Context context, Vector<ToDo> todos, SelectedToDo selectedToDo) {
        this.context = context;
        this.todos = todos;
        this.selectedToDo = selectedToDo;
    }

    /**Interface for selecting a specific ToDo.
     *
     */
    public interface SelectedToDo{
        void selectedToDo(ToDo todo);
    }

    /**This class contains all objects of an item of the recycler view.
     *
     */
    public class ViewholderToDos extends RecyclerView.ViewHolder {
        public CheckBox toDoCheckBox;
        public ImageView detailView;

        /**Constructor of ViewholderToDos
         *
         * @param itemView given view from invoking function.
         */
        public ViewholderToDos(View itemView) {
            super(itemView);
            toDoCheckBox = itemView.findViewById(R.id.toDoOverviewActivity_btn_toDoCheckBox);
            detailView = itemView.findViewById(R.id.toDoOverviewActivity_imageView_detail);

            /**Select one specific item of the recycler view (for pressing the details button
             * of a specific item).
             * Contains an animation, because imageView detail doesn't contains any while pressing it.
             */
            detailView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.startAnimation(AnimationUtils.loadAnimation(context, R.anim.anim_item_onclick));
                    selectedToDo.selectedToDo(todos.get(getAdapterPosition()));
                }
            });
        }
    }

    /**Inflates items of the xml file for the recycler view.
     *@return inflated items
     */
    @NonNull
    @Override
    public ViewholderToDos onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.viewholder_todos, parent, false);
        return new ViewholderToDos(view);
    }

    /**Binds an item on a position of the recycler view.
     * @param holder given viewholder that manages the items.
     * @param position current position in recycler view.
     */
    @Override
    public void onBindViewHolder(@NonNull ViewholderToDos holder, int position) {
        ToDo todo = todos.get(position);
        holder.toDoCheckBox.setText(todo.getName());
    }

    /**
     * @return size of current todos in vector.
     */
    @Override
    public int getItemCount() {
        return todos.size();
    }
}
