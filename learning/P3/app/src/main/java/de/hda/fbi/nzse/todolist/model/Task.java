package de.hda.fbi.nzse.todolist.model;

import java.io.Serializable;

/**
 * A simple Task_ class which contains a name and the status (checked / unchecked) by boolean
 */
public class Task implements Serializable {
    private String name;
    private boolean checked;

    public Task(String name) {
        this.name = name;
        this.checked = false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
