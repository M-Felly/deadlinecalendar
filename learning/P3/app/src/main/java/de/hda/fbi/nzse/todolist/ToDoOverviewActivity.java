package de.hda.fbi.nzse.todolist;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Vector;

import de.hda.fbi.nzse.todolist.adapter.ToDosAdapter;
import de.hda.fbi.nzse.todolist.model.Task;
import de.hda.fbi.nzse.todolist.model.ToDo;

public class ToDoOverviewActivity extends AppCompatActivity implements ToDosAdapter.SelectedToDo{
    private static int ACTIVITY_RESULT = 42;
    private FloatingActionButton addToDoButton;
    private Vector<ToDo> todos;
    ToDosAdapter todosAdapter;
    RecyclerView recyclerViewToDos;

    public static final int ADD_TODO_VIEW_ACTIVITY_REQUEST_CODE = 1;
    public static final int DETAIL_TODO_VIEW_ACTIVITY_REQUEST_CODE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_to_do_overview);

        // Floating Action Button for adding a new ToDo
        addToDoButton = findViewById(R.id.toDoOverview_btn_add);
        addToDoButton.setOnClickListener(this::onFloatingButtonClicked);

        // init vector and todos
        todos = new Vector<>();
        todos.add(new ToDo("Mathe Hausaufgaben"));
        todos.add(new ToDo("NZSE App fertig stellen"));
        todos.add(new ToDo("E-mail an Urs schreiben"));

        todosAdapter = new ToDosAdapter(this, todos, this);
        recyclerViewToDos = findViewById(R.id.toDoOverviewActivity_recyclerView_toDos);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerViewToDos.setLayoutManager(linearLayoutManager);
        recyclerViewToDos.setAdapter(todosAdapter);
    }

    /**This function is called if the floating action button is clicked. It will start the process
     * of creating a new ToDo.
     *
     * @param v given View of invoking function.
     */
    private void onFloatingButtonClicked(View v) {
        Intent addToDoIntent = new Intent(this, AddToDoViewActivity.class);
        this.startActivityForResult(addToDoIntent, ADD_TODO_VIEW_ACTIVITY_REQUEST_CODE);

    }

    /**This function is implemented by an interface of ToDosAdapter. It starts the DetailToDoViewActivity
     * if the detail ImageView of an item of the recycler view is selected/pressed.
     *
     * @param todo contains the todo of associated ImageView detail "button"
     */
    @Override
    public void selectedToDo(ToDo todo) {
        Intent addToDoIntent = new Intent(ToDoOverviewActivity.this, DetailToDoViewActivity.class).putExtra("ToDo", todo);
        this.startActivityForResult(addToDoIntent, DETAIL_TODO_VIEW_ACTIVITY_REQUEST_CODE);

    }

    /**If a todo in AddToDoView is created and valid, it will be passed back to this funtion.
     * If this data and todo aren't null, the todo will be added in todos vector and the recycler
     * view will be updated, so the user can see the new created todo.
     *
     * @param requestCode given request code of invoking function.
     * @param resultCode given result code of invoking function.
     * @param data given intent with created to do (created in AddToDoViewActivity).
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == ADD_TODO_VIEW_ACTIVITY_REQUEST_CODE){
            if (data != null) {
                ToDo todo = (ToDo) data.getSerializableExtra("ToDo");
                if (todo != null) {
                    todos.add(todo);
                    todosAdapter.notifyItemInserted(todos.size()-1);
                }

            }
        }
        else if(requestCode == DETAIL_TODO_VIEW_ACTIVITY_REQUEST_CODE){
            if (data != null) {
                ToDo todo = (ToDo) data.getSerializableExtra("ToDo");
                for(ToDo tempToDo : todos){
                    if(tempToDo.getId() == todo.getId()){
                        tempToDo.setName(todo.getName());
                        tempToDo.setDescription(todo.getDescription());
                        tempToDo.setTasks(todo.getTasks());
                    }
                }
                todosAdapter.notifyDataSetChanged();
            }
        }
    }
}