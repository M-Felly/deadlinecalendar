package de.hda.fbi.nzse.todolist.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.Vector;

import de.hda.fbi.nzse.todolist.R;
import de.hda.fbi.nzse.todolist.model.Task;

/**This class is a specialized adapter for the recycler view. It combines the recycler view with
 * the xml items and the Task class values.
 *
 */
public class TasksAdapter extends RecyclerView.Adapter<TasksAdapter.ViewholderTasks>  {

    private Vector<Task> tasks;
    private Context context;
    private SelectedTask selectedTask;

    /**Constructor of TasksAdapter.
     *
     * @param context gets the Context.
     * @param tasks all created tasks, given by DetailToDoViewActivity.
     * @param selectedTask defines the actual Task.
     */
    public TasksAdapter(Context context, Vector<Task> tasks, SelectedTask selectedTask) {
        this.context = context;
        this.tasks = tasks;
        this.selectedTask = selectedTask;
    }

    /**Interface for selecting a specific Task.
     *
     */
    public interface SelectedTask{
        void selectedTask(Task task);
    }

    public class ViewholderTasks extends RecyclerView.ViewHolder {
        public CheckBox taskCheckBox;

        /**
         * Constructor of ViewholderTask
         *
         * @param itemView given view from invoking function.
         */
        public ViewholderTasks(View itemView) {
            super(itemView);
            taskCheckBox = itemView.findViewById(R.id.detailToDoActivity_btn_taskCheckBox);
        }
    }

    /**Inflates items of the xml file for the recycler view.
     *@return inflated items
     */
    @NonNull
    @Override
    public ViewholderTasks onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.viewholder_tasks, parent, false);

        return new ViewholderTasks(view);
    }

    /**Binds an item on a position of the recycler view.
     * @param holder given viewholder that manages the items.
     * @param position current position in recycler view.
     */
    @Override
    public void onBindViewHolder(@NonNull ViewholderTasks holder, int position) {
        Task task = tasks.get(position);
        holder.taskCheckBox.setText(task.getName());
        holder.taskCheckBox.setTag(position);

        if (task.isChecked())
            holder.taskCheckBox.setChecked(true);

        holder.taskCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Integer pos = (Integer) holder.taskCheckBox.getTag();

                if (isChecked)
                    tasks.get(pos).setChecked(true);
                else if (!isChecked)
                    tasks.get(pos).setChecked(false);
            }
        });
    }

    /**
     * @return size of current tasks in vector.
     */
    @Override
    public int getItemCount() {
        return tasks.size();
    }
}
