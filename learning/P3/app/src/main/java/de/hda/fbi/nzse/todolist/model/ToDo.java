package de.hda.fbi.nzse.todolist.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * A simple Todo_ class which contains a name and the status (checked / unchecked) by boolean
 */
public class ToDo implements Serializable {
    private static int counter = 0;

    private int id;
    private String name;
    private boolean checked;
    private String description;
    private Vector<Task> tasks;

    public ToDo(String name) {
        this.name = name;
        this.description = "";
        this.checked = false;
        this.tasks = new Vector<>();
        this.id = counter;
        counter++;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() { return description; }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public Vector<Task> getTasks(){
        return this.tasks;
    }

    public void setTasks(Vector<Task> pTasks) {
        this.tasks = pTasks;
    }

    public void addTask (Task task) {
        this.tasks.add(task);
    }
}
