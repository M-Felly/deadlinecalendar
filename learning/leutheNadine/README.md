# Praktikum1 - Nadine Leuthe

## Grundlagen Java
Ich habe bereits in den Basics von Java Erfahrung sammeln können, um eine strukturierte Übersicht zu haben, habe ich mir hier nochmal die unterschiede sowie neue Informationen aufgeschrieben.

- 1 Klasse pro Datei
- Es gibt keine Default-Parameter -> Methoden überlagern
- keine Operatorüberladung/Default-Parameter 

### Klasse 
    public class Klassenname extends BaseClass implements Interface1, Interface2{...}

### Vererbung:
einfach Vererbung

    extends Klassenname

#### protected: 
erbende Klassen und Package

### Konstruktor
erste Zeile: super(...) -> ruft den Konstruktor der Basisklasse auf

### Package 
Ordner im Dateisystem -> klein geschrieben

    package name;

### Import
    import java.util.Vector;    -> eine Klasse
    import java.util.*;         -> alle Klassen

### Interface implementieren:
    implements name

### Deklaration 
eine Instanz muss erzeugt werden

    MyClass x; 
    x = new MyClass();

### Konstanten
final (nicht mehr const)

    private static final int SIZE = 10;


### Objekte
    Klasse Objektreferenz = new Klasse(Parameter);
- immer dynamisch
- Objekt existiert solange, wie eine Referenz darauf existiert
- delete existiert nicht
- kein destruktor

### String
- in Java großgeschrieben
- vergleiche mit equals

#### Stringverkettung in Schleifen: 
    StringBuilder

### Iteration über eine Liste:
    for(String name:names)

### Methode der Basisklasse überschreiben/ Implementierung Interface:
    @Override

### Aufruf Basisklasse:
    super(...) --> wenn nicht abstract

### Wrapperklassen
Integer, Double, Long (großgeschrieben)

    Integer o = new Integer(i);

### Anonyme Klassen

    dialogBuilder.setPositiveButton(
        new AnonymeKlasse implements OnClickListner(){
            public void onClick(DialogInterface dialog, int  whichButton){
                //Nutzinhalt des Handlers
            }
        }
    );


### Lambdas
    (parameter1, parameter2)->{code block}

### Interfaces
beschreibt Außenansicht (Schnittstelle) - enthält keine Implementation, nur Signatur & Konstanten

        class Klasse implements Interface 

### Generics 
    List<Car>

### Konvention 
    private static final String TAG = "MainActivity";

### Liste 
- global: private List < Student > students;
- lokal: students = new ArrayList<>();

### Exception: 
- checked: erkennt Compiler -> catch / throw
- unchecked: Laufzeit error -> runtimeexception

~ try, catch, finally -> Finally ist optional


### Streams/Datenströme
- Stream öffnen - lesen/schreiben - Stream schließen
    - Byte-Stream: Audio, Video, Bilder, Executables -> Input/Outputstream
    - Character-Streams: Reader, Writer -> Text
- BufferedInput/OutputStream -> Byte
- BufferedRead/Write -> Character

### Try with Ressourcen:
    try(BufferedReader br = new BufferedReader(new FileReader(path))){
        return br.readLine();
    }

### Scanner: 
zerlegt Text in einfachen Datentypen & Strings anhand von regulären Ausdrücken

    Scanner s = new Scanner(new BufferedReader(new FileReader("input.txt")));

### Seriablizable
"ok für Serialisierung"
    
    class MeineKlasse implements Serializable

### Java Docs Tags
- @author
- @version
- @param
- @return
- @exception(@throws synonym)
    - @ throws 10 Exception if an input or output exception occurred
- @see

#### Checkliste/Achtung/typische Fehler
- equals benutzen statt ==
- String statt string
- final statt const
- Vererbung: extends BaseClass
- import statt include

#### CheatSheet
- Lambda
- super()
- equals()
- String verkettung: StringBuilder


#
## Android Studio 
Ich habe bisher noch keine Erfahrungen mit der Entwicklung von Apps.
### Erstellung des Projekts:
1. Tools: SDK Manager
2. Template: Empty Activity
3. Name: aussagekräftig für das Projekt
4. Packagename: de.h_da.fbi.name
5. Sprache: Java
6. Minimum SDK: z.B. 7.0 (Nougat)

Nach der Erstellung:
- App: Anwendung
- Gradle Scripts: zum bauen der Anwendung
    - build.gradle (Module:app): version
- Designer: activit.main.xml

Unterteilung:
- manifest: xml (Name, Icon, Activity angelegt)
- res: icon, layout, string, style
- java: eigentliche Implementation + 2 Testordner


### Basisfunktionalitäten:
    AppCompatActivity
#### Methode der Basisklasse anschauen:
rechte Maustaste -> go to -> Declaration or Usages

#### Im nachhinein in eine Methode Auslagern:
rechte Maustaste -> Refactor -> Extract Method

#### Hardcoded Strings
Text markieren -> Glühbirne -> Extract string Ressource

#### Code Beispiel:
    private EditText editText;
    edit Weight = findViewByID(R.id.editTextWeight);

#### Formatangaben:
Formatangabe: String.format 
Angabe 1 Nachkommastelle: %.1f

#### Umwandlung:
    int size = Integer.parseInt(editSize.getText().toString());
    
    float sizeM = size / 100.0f;

#### Button
    EventHandler: 
    Button.setOnClickListner(view->{});

### Android Application Model:
1 Activity -> Repräsentiert 1 Task / 1 Screen -> Repräsentiert durch eine Java-Klasse -> AppCompactActivity
- onCreate -> erster Start
- onStart -> start
- onResume
- onPause
- onStop

#### Hollywood Prinzip:
"we call you"

### Intents
Activity kommunizieren untereinander über intents

Intent wird vom Laufzeitsystem abgearbeitet

1. explicit intents -> explizit was wird als nächstes gestartet/aufgerufen
2. implicit intents -> intent Filter -> flexibilität


#
## Grundlagen XML
Ich habe bisher noch nicht mit XML gearbeitet
### Erste Zeile:
    <?xml version="1.0! encoding="utf-8"?>
### Tags:
    <color name = "primaryColor">#64dd17</color>
### Zusätzliche Namespaces:
    xmlns:android = "..."  
    android:id = "..."
~ Definition welche Tags & Attribute zulässig sind 
### Objekte
XML (deklarativ) --> Objekt erzeugt


### Events
Die Event-Quelle (Button,...) feuert ein Event sobald etwas passiert. Der Event-Listener lauscht auf das Event (click, touch, double click). Sobald ein Event bekannt ist wird überprüft ob hierfür ein Event-Handler definiert ist. Wenn ja - wird dieser ausgeführt

#### setListener:
    btnOK.setOnClickListener(view->addEntryPopUp());
    btnOK.setOnClickListener(view->{...});
~ wenn das nicht ausreicht:

    btnOk.setOnClickListener(new View.onClickListener(){
        @Override
        public void onClick(View v){
            ...
        }
    })



# Praktikum 2

Activity ist ein Screen, das was man auf dem Tablet sieht.
Wenn ich eine neue Activity startnnen will, muss ich angeben was es für eine Activity ist (Klasse). 
Ich kann noch weitere Daten mit liefern. Um Objekte mitliefern zu können muss es serialisierbar sein.
Durch die Serialisierung werden aus verschiedenen Objekten ein einfacher Datentyp gemacht, der dann übergeben wird. 
Anschließend wird der Datentyp wieder deserialisiert und die ursprünglichen Objekte werden wieder erzeugt.

- Activity life-cycle: aus Performancegründen wird die Activity aktiviert und deaktiviert.
- Activitys erzeugen: über New -> Activity
- Um getter und setter zu erzeugen: rechte maustaste auf die Attribute -> Refactor -> Encapsulate Fields


Zwischen Activities hin und her wechseln:
1.  btn.setOnClickListener(view -> startActivity(new Intent(MainActivity.this, StudentsActivity.class)));
2. Intent intent = new Intent(this, EditStudentActivity.class);
   intent.putExtra("student", student);
   this.startActivityForResult(intent, ACTIVITY_RESULT);


Inhalt holen:
```java
student = (Student) getIntent().getSerializableExtra("student");
````

Inhalt zurückgeben:
```java 
Intent resultIntent = new Intent();
resultIntent.putExtra("student", student);
setResult(Activity.RESULT_OK, resultIntent);
finish();
```

zurück navigieren: 
```java
@Override
 public boolean onSupportNavigateUp(){
        onBackPressed();
        return  true;
    }
```

Zurück-Button anzeigen:
```java
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setDisplayShowHomeEnabled(true);
```


### Resources 
    /app/res/

Um nicht für alles eine eigene App für alle größen zu schreiben benutzt man Density buckets.
- Größe in dp angeben (px = dp * (dpi/160))
- verschiedene Sprachen: res/values/strings.xml -> rechter Mausklick -> Open Translation Editor
- Style: im XML File (Farbe, Größe)
- Icon ändern: rechter Mausklick auf app -> new -> image asset -> Icon auswählen
- drawable -> bilder hinzufügen über Dateimanager
- einfache Grafiken als svg-Datei hinzufügen
- Material Design -> für die Farbanpassung -> Farbshema anpassen in res/values/colours


## BottomNavigation, NavigationDrawer, OptionsMenu

### BottomNavigation
Folgende Resoourcen müssen hinzugefügt/ergänzt werden:
1. Menu (rechtsklick auf res-> new android resource directory):  menu auswählen und default mäßig einstellen und neues File mit dem Namen: bottom_nav_menu hinzufügen
2. Values: dimens, styles, strings
3. Drawable: Icons runterladen in Unterverzeichnis dreawable hinzufügen
4. MainAcitivity mit allen Activity BottomNavigation implementieren 
 

 ### ToolBar
 1. main activity muss aufgesplittet werden:
    - Layout:
        - activity_main
        - app_bar_main
        - content_main
    - Values:
        - dimens
        - styles
        - strings
2. Manifest: style für MainActivity setzen android:theme="@style/AppTheme.NoActionBar"
3. ActivityMain:
    - on Create
        Toolbar toolbar = findViewByID(R.id.toolbar);
        setSupportActionBar(toolbar);

### NavigationDrawer
1. gleich Vorbereitung wie bei Toolbar
    - onSupportNavigateUp überschreiben und in onCreate: setTitle und get SupportActionBar().setDisplayHomeAsUpEnabled(true);
2. Folgende Ressourcen wie im Beispielürojekt hinzufügen/ergänzen:
    - Layout: activity_main(NavigationView), nav_header_main
    - Menu: activity_main_drawer
    - Values: dimens, styles, strings
    - Drawable: Icons runterladen

3. Activity Main:
    - implements NavigationView.OnNavigationItemSelectedListener
    - onCreate: Toggle, Toolbar etc

### OptionsMenu
1. Verbereitung wie bei Toolbar
2. Folgende Ressourcen wie im Beispielprojekt hinzufügen/ergänzen:
    - Menu: main_options_menu
    - Values: strings
    - Drawable: Icons runterladen
3. In zugehöriger Activity
    - onCreateOptionsMenu überschreiben und dort Menu mit Inflater:
    getMenuInflater().inflate(R.menu.main_options_menu, menu);

## Dialogs
### Toast, AlertDialog, Picker

- dependencies hinzufügen in build.gradle

```java 
    dependencies{
        implementation fileTree(dir: "libs", include: ["*jar"])
        implementation 'androidx.appcompat:1.1.0'
        implementation 'androidx.constraintlayout:constraintlaxout:1.1.3'
        implementation 'com.google.android.material:material:1.1.0'
    }
```

- Date Format
```java
private SimpleDateFormat dateFormat;
dateFormat = new SimpleDateFormat(getString(R.string.date_format_dmy));
```

#### Toast

```java
btnToast.setOnClickListener(view -> showToast("hello world"));
```
Methode showToast:
```java
(Toast.makeText(this, message, Toast.LENGTH_LONG)).show();
```

#### AlertDialog
```java
private void showAlertDialog() {
    AlertDialog.Builder builder = new AlertDialog.Builder(this);
    builder.setMessage("hello world").setTitle("Title");
    builder.setPositiveButton("ok", (dialog, id) -> {
        showToast("you pressed ok");
    });
    builder.setNegativeButton("cancel",(dialog, id) -> {
        showToast("you pressed cancel");
    });
    AlertDialog dialog = builder.create();
    dialog.show();
}
```

#### PickNumber

```java
private void pickNumber(){
    AlertDialog.Builder builder = new AlertDialog.Builder(this);
    LayoutInflater inflater.inflater = getLayoutInflater();
    View view = inflater.inflate(R.layout.number_picker_dialog, null);
    builder.setView(view);
    NumberPicker numberPicker = (NumberPicker) view.findViewById(R.id.numberPicker);
    numberPicker.setMaxValue(50);
    numberPicker.setMinValue(1);
    final int[] selectedNumber = new int [1];
    numberPicker.setOnValueChangedListener((numberPicker1, oldValue, newValue) -> {
        selectedNumber[0] = newValue;
    });
    builder.setMessage("Set your age").setTitle("Age");
    builder.setPositiveButton("set", (dialog, id) -> textViewNumber.setText(String.valueOf(selectedNumber[0])));
    AlertDialog dialog = builder.create();
    dialog.show();
}

```

#### PickDate

```java
private void pickDate() {
    MaterialDatePicker.Builder builder = MaterialDatePicker.Builder.datePicker();
    MaterialDatePicker picker = builder.build();
    picker..addOnPositiveButtonClickListener(dateLong -> {
        Date date = new Date();
        date.setTime((Long) dateLong);
        textViewDate.setText(dateFormat.format(date));
    });
    picker.show(getSupportFragmentManager(), picker.toString());
}

```


### Accessible 
- EditText: versehen mit **android:hint**
- Color Contrast: 4.5 check with [hier](https://contrast-ratio.com)
- Mindestgröße: 12 sp
Hit-size >= 48dp (minWidth, minHeight)
- Für imageview android:contentDescription bzw. android:importatntForAccessibility="no" sendAccessibilityEvent, z.B. 
````java
@Override
public boolean onKeyUp (int keyCode, KeyEvent event){
    if(keyCode == KeyEvent.KEYCODE_DPAD_LEFT) {
        currentValue--;
        sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_TEXT_CHANGED);
        return true;
    }
    ...
}
````


#### Zum Testen der Barierefreiheit: Accessibility Scanner
#### Material Design: Grid/Spacing [hier](https://material.io/design/layout/spacing-methods.html)

### Layout
- Widget
- Minimum bounding box
- padding (top, left, bottom, right) vom Objekt
- margin (top, left, bottom, right) abstand zum nächsten Objekt

ViewGroup:
- FrameLayout: Überlagerung von Objekten
- LinearLayout: horizontal oder vertikal + space angabe (weight, width) - dynamische Anpassung
- ConstraintLayout: sehr flexibel 
[hier](https://www.youtube.com/watch?v=1U5p69lQ-Ek&list=PLosiZFS_rnz7fUOCbmzEF7gE0wYbVreyO&index=7&t=0s): 8:07
    - layout width="0dp"  -> berechnet sich
```xml 
    <ImageView
    android:layout_width="0dp"
    android:layout_height="100dp"
    android:id="@+id/imageView"
    app:layout_constraintDimensionRatio="2:1"/>
```

### RecyclerView
- Listen  - nachfolger der Listview
- Adapter stellt die Verbindung her zwischen RecyclerView und den DatenObjekte und ViewHolder
- RecyclerView, LayoutManager, ItemLayout [zusamemnarbeit](https://www.youtube.com/watch?v=krHARBTzVWw&list=PLosiZFS_rnz7fUOCbmzEF7gE0wYbVreyO&index=9&t=0s) : 2:56
- DemoRecyclerView