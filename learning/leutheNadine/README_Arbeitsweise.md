# P1

## Arbeitsweise
Meine Arbeitsweise war sehr unkoordiniert und ich habe für alle Aufgaben mindestens die doppelte Zeit benötigt. 
### Probleme und Lösungen
Für meine selbst erstellte erste App hatte ich ein paar Probleme mit der verwendeten ViewList. Hierbei wurden die Einträge immer wieder komplett überschrieben statt lediglich nur hinzugefügt. Nach längerem googlen und Nachdenken habe ich teile des befüllens ausgelagert, sodass ich einen Eintrag nur noch an der richtigen Stelle hinzufüge auf die Liste. Hierbei war das verhalten der ViewList etwas anders als bei dem UI, welches wir letztes Semester mit QT erstellt hatten. 
### Plan

- 6.11. 18:00, 3h (alleine) Android Studio installieren und Videos durcharbeiten, Informationen über Java und XML raus schreiben
- 8.11. 9:00, 3h (zusammenarbeit mit Anna) User Research-Persona Video ansehen, an entsprechenden Stellen Pause machen und Notizen machen, Fragen/Besonderheiten/Schritte notieren anschließend erstellten wir die Glossareinträge, persona.md und persona.pdf
- 8.11. 15:00, 20 min (alleine) UX and Usability Informationen sammeln, Videos anschauen und dies ins Glossar eintragen
- 8.11. 17:00, 40 min (alleine) Kano-Modell Informationen sammeln, Videos anschauen und diese im Glossar ergänzen
- 9.11. 15:30, 4h (alleine) erste App im Android Studio anlegen und programmieren
- 12.11. 10:30, 2h (alle) Besprechung über unsere Issues
- 18.11 17:00, 80 min (mit Matthias) Durchführung Interview
- 20.11. 18:00, 2h (alle) Besprechung Interviews
- 21.11. 14:00, 40 min (mit anna) Finalisierung der Persona

Stand: 18/11/2020

### Planevaluation
Der Plan mit den Tagen und Zeiten hat größtenteils gepasst. Das einzige, was nicht gestimmt hatte war die Dauer. Hierbei kam es dann zu leichten Verschiebungen der Zeiten.
### Dauer
Ich hab bei allen Issues ca. die 1,5 Fache Zeit benötigt. 

## Arbeitsweise/Retrospektive
Da wir letztes Semester schon kleine UIs gebaut haben, wollte ich eine tolle erste App-Idee umsetzen. Ich habe gemerkt, dass es eine zu Zeitintensive Idee war. Nach längerem einarbeiten habe ich es zwar geschafft aber sie war viel Zeitaufwendiger als angenommen. Allerdings kann ich die Erfahrungen gut für unsere geplante App einsetzen.

Ich habe während der Bearbeitung der Issues gemerkt, dass ich mit den angegebenen Zeiten überhaupt nicht hinterher kam. Mein Problem daran war, dass ich oftmals die Videos angehalten habe, um ausführlich die Inhalte verstehen und mitschreiben zu können. Leider hat mir diese Zeit woanders gefehlt, daher muss ich dies das nächste mal anders machen. Ich muss versuchen währendessen ich die Videos schaue schon mit dem Computer mitzuschreiben statt mit der Hand, da dies das letzte mal zu viel Zeit kostete. Auch die Partnerarbeiten waren viel zu Zeitintensiv, hierbei müssen wir uns das nächste Mal besser absprechen um Videos aufzuteilen und uns anschließend nur noch zu informiern und die Informationen zusammen zutragen. 


# P2
## Arbeitsweise
Meine Arbeitsweise war schon viel strukturierter als in P1. Ich habe versucht gleich alle Informationen in das jeweilige Markdown-File zu schreiben, um hier die Zeit zusparen. Jedoch kam ich immernoch nicht mit den vorgegebenen Zeiten aus. Auch die Arbeitsweise im Team hatte sich verbessert. Wir benötigen nun nicht mehr so lange für unsere Besprechungen.

### Probleme und Lösungen
Dieses Praktikum hatte ich keine größeren Probleme. 
Bis darauf, dass ich meine glossary.md in hektik beim mergen überschieben und es nicht noch einmal vor dem Praktikum überprüft habe. Somit hatte leider mein Thinking-Aloud im Glossar gefehlt. Das nächste mal werde ich auf dem git unter dem master-branch nochmal alles vor der abgabe überprüfen, ob auch alles richtig gepushed wurde.

### Plan
- 23.11. 18:00, 160 min (alleine)  Videos durcharbeiten, Informationen raus schreiben
- 23.11. 9:00, 20 min (alleine) Readme schreiben
- 24.11. 15:00, 40 min (alleine) Gestaltprinzipien
- 25.11. 17:00, 210 min (alleine) App - Programmierung 
- 26.11. 15:00, 60 min (alleine) Thinking Aloud
- 26.11. 15:30, 120 min (alle) Besprechung
- 27.11. 15:00, 45 min (alleine) Prototyp erstellen
- 27.11. 15:00, 40 min (alleine) Thinking Aloud durchführen für den Paper-Prototypen
- 30.11. 15:00, 45 min (alle) Prototyp finalisieren

Stand: 04/12/2020

### Planevaluation
Der Plan mit den Tagen und Zeiten hat größtenteils gepasst. Das einzige, was nicht gestimmt hatte waren auch hier wieder die Dauer. Hierbei kam es wieder zu  Verschiebungen der Zeiten.

### Dauer
Bei den Videos und der readme konnte ich die Zeit in etwa einhalten. Für den Prototyp haben wir 3h gebraucht. Für die Gestaltprinzipien 1 1/2 Studen und für Thinking Aloud 2h. Für unsere gemeinsame App habe ich statt den 210 min 5 Studen. Um den Prototyp zu finalisieren haben wir zusammen mehrere Studen gesessen.

## Arbeitsweise/Retrospektive
Ich habe wie oben schon beschrieben meine Arbeitsweise im Gegensatz zu P1 schon sehr optimiert. Ich war bei der Bearbeitung der Prototypen sehr perfektionistisch, was sich natürlich dann in der Arbeitweise widergespiegelt hat. 

Auch wenn das Projekt sehr zeitintensiv ist, macht es mir sehr viel Spaß in der Gruppe an dem Projekt zu arbeiten und die ganzen neuen Themen kennenzulernen. Ich habe gemerkt, dass ich jetzt schon viel gelernt habe und ich jetzt schon bei den verschieden Anwendungen darauf achte, ob die Gestaltprinzipien sowie Hick's Law, ... eingehalten wurden. 
Auch durch die Programmierung der zweiten Applikation habe ich sehr viel gelernt. 
Wir haben nur leider etwas spät angefangen, sodass zeitlich alles etwas knapp wurde. Aber durch die gegenseitige Unterstützung haben wir es noch rechtzeitig geschafft unsere zweite App fertig zu stellen.
Wir wollen das nächste Mal früher anfangen und besser vorraus planen. 