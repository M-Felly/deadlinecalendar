package de.h_da.fbi.todolist;

import androidx.appcompat.app.AppCompatActivity;

import android.app.LauncherActivity;
import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;


public class MainActivity extends AppCompatActivity {

    private EditText editTextToDo;
    private ListView listViewToDos;


    private List <String> toDo =  new ArrayList<>();;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        try{
            listViewToDos = findViewById(R.id.ToDoList);
            listViewToDos.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

            ArrayAdapter<String> eintraege = new ArrayAdapter<>(this, android.R.layout.simple_list_item_multiple_choice, toDo);
            listViewToDos.setAdapter(eintraege);

            editTextToDo = findViewById(R.id.editToDo);

            Button btnAddToDo = findViewById(R.id.buttonAddToDo);
            btnAddToDo.setOnClickListener(view->getToDo(eintraege));

            Button btnDelete = findViewById(R.id.button_deleteAll);
            btnDelete.setOnClickListener(view->
            {
                eintraege.clear();
                listViewToDos.removeAllViewsInLayout();
                listViewToDos.clearChoices();
            });
        }
        catch (IllegalArgumentException ex){
            Toast.makeText(this, "ups error" + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
        catch (Exception ex){
            Toast.makeText(this, "serious error :(" , Toast.LENGTH_LONG).show();
        }

    }

    private void getToDo(ArrayAdapter<String> eintraege){
        if(editTextToDo.getText().toString().isEmpty()){
            editTextToDo.setError("Bitte tragen Sie etwas ein!");
        }
        else{
            toDo.add(editTextToDo.getText().toString());
            editTextToDo.setText("");

            eintraege.notifyDataSetChanged();

        }

    }


}