# Selbsteinschätzung
Wir haben alle in der Gruppe das bestmögliche gegeben, daher bin ich der Meinung, dass jeder aus dem Team die gleiche Note verdient hätte. 
Ich habe mich in allen 5 Praktika bestmöglich eingebracht und habe in dieser Zeit sehr viel dazu gelernt. Hierunter habe ich gelernt, was das arbeiten im Team betrifft, die Absprachen zu optimieren, die Kommunikation, das Zeitmanagement und noch vieles mehr. Aber nicht nur hier habe ich sehr viel gelernt und miteingebracht, sondern auch in der Vorgehensweise, wie man an ein solches Projekt ran geht und worauf man alles bei der Entwicklung bei z.B. so einer App alles achten muss. Auch bei P3 und P4 hatte ich eine sehr große Lernkurve, was die Programmierung angeht. Ich habe mich hierbei viel mit eingebracht und habe, wie Katharina Renk das beste für alles um die Kalender View gegeben, dass sie am Ende vollfunktionsfähig war. Ich bin stolz darauf, was wir zu zwei hier geschafft haben und das ganz ohne Vorerfahrung in der Appentwicklung.
Alles in allem bin ich der Überzeugung, dass ich das bestmögliche aus der uns zur Verfügung stehenden Zeit gemacht habe.
Wir hatten anfangs noch einige andere Ideen, welche wir leider zeitlich gesehen schnell wieder verwerfen mussten oder als Feature mit hinzugenommen haben. Da wir  anfangs nicht ganz im Blick hatten, wie zeitintensiv eine App-Programmierung ist. 
Daher haben wir ein MVP und eine Featureliste erstellt, um uns zu strukturieren und uns auf das wichtigste konzentrien zu können, was in dieser Zeit möglich ist. 
Wir haben es geschafft alles umgesetzt, was wir uns im MVP vorgenommen haben und konnten sogar noch ein paar Features umsetzen. Unsere App ist nicht markellos aber ich bin sehr stolz darauf, was jeder einzelne von uns während der Entwicklung der App geleistet hat und ich bin der Überzeugung, dass wir alle das bestmögliche in der uns zur Verfügung stehenden Zeit und der bisher gesammelten Erfahrung geleistet haben. Es ist klar, dass der ein oder andere ein Stück besser ist, bei manchen Teilprojekten, da hier schon Jahrelange Vorerfahrung z.B. in der Programmierung besteht. Ich finde, dass wir alle unter der Berücksichtigung, was wir für eine Vorerfahrung haben und was wir in dieser Zeit geleistet haben, eine gute App erschaffen haben. Wir haben alle sehr hart dafür gearbeitet, dass wir bis zum Endtermin auch das Präsentieren können, was wir uns anfangs vorgenommen haben. 
Daher würde ich mir/uns allen eine Endnote von 1,3 geben.



## P1 - User Research
### Arbeitspakete
- #38 Usability und UX
Als erstes habe ich das Material durcharbeitet. Danach die Gemeinsamkeiten und Unterschiede herausgearbeitet und den Glossareintrag ergänzt.

- #36 Kano-Modell
Als erstes habe ich das Material durcharbeitet, um anschließend den Glossareintrag mit Beispielen zu ergänzen.

- #44 User Research - Persona [partner: @istankilb]
Erarbeitung der Informationen zu User Reseach, Benutzer\*innen und Persona.
Zusammen mit Anna habe wir die Glossareinträge sowie die persona.md ergänzt und die persona.pdf mit den 3 Persona erstellt

- #32 Interview 1 [partner: @istmafeyl]
Zusammen mit Matthias habe ich das Interview durchgeführt und die dazugehörige md-Datei erstellt.

- #34 - Informationsaustausch [@all]
Nach dem sich alle für das erste Praktikum vorbereitet hatten, haben wir uns nochmal mit unserem Team zusammengesetzt um die Ergebnisse zu besprechen. Jeder hat seinen Arbeitsteil vorgestellt. Hierbei gab es einige Verbesserungsvorschläge, welche während bzw. nach dem Meeting noch verbessert wurden. Es wurden nochmal genauere Ideen erfasst, um unsere Vorstellungen auf einen Nenner zu bringen. 

- #31 - Informationsaustaisch [@all]
Hierbei haben wir uns wieder auf dem neusten Stand gebracht. Einige offene Fragen geklärt sowie die Interviews besprochen.

- #27 - Persona finalisieren [partner: @istankilb]
Die Persona wurden nochmal komplett überarbeitet und finalisiert.

### Anteile und Selbsteinschätzung 
Ich habe 18,6% in Praktikum 1 mitgearbeitet. Wir haben versucht die Aufgaben gleichmäßig zu verteilen, sodass bei einem 5er Team pro Person jeder ca. 18,6% der Aufgaben bearbeitet. Noch dazu habe ich hierbei versucht die Aufgaben bestmöglich abzuarbeiten und mich in die Gruppendiskussion einzubrigen.



## P2 Entwurf
### Arbeitspakete
- #25 Thinking aloud [single task]
Hierfür habe ich die bereitgestellten Informationen angeschaut und zusammengefasst. Noch dazu habe ich mir lange Gedanken darüber gemacht, was genau in der thinking-aloud-paper.md am besten stehen sollte. Ich habe darin nochmal die wichtigsten Inhalte zusammen gefasst und mir eine Durchführung und Aufgaben für den Prototypen Tests überlegt. 

- #23 Gestalt-Prinzipien [single task]
Als erstes habe ich das Material durcharbeitet, um anschließend den Glossareintrag ergänzt. Hierfür habe ich mir noch spezielle Beispiele ausgedacht, wie die Gesalt-Prinzipien in unserem Projek am besten eingesetzt/beachtet werden könnten.

- #15 Paper-Prototyp 1 erstellen [@Katharina Renk]
Zusammen mit Katharina Renk habe ich eine Prototypen erstellt. Wir haben uns hierfür überlegt wie wir die Aufaben, die unsere App erfüllen sollte, am besten in dem Prototyp umsetzen können und haben anhand dessen diesen erstellt. Wir sind selbst den Prototypen einige Male nnoch einmal durchgegangen, damit wir keinen Screen vergessen. 

- #14 thinking aloud mit Paper-Prototyp [@all]
Hierfür wurden aus Corona-Gründen einzeln vier Personen zu dem Prototyp befragt und die Tests wurden durchgeführt. Hierbei konnten wir gute Erkenntnisse ziehen und eventuellle Ideen und Verbesserungsvorschläge, die wir noch in unserer App umsetzen könnten.

- #17 Informationsaustausch [@all]
Hierbei haben wir uns wieder auf dem neusten Stand gebracht. Einige offene Fragen geklärt sowie alle Issues besprochen und die verschiedenen Themenbereiche erklärt.

- #13 Merge der Prototypen [@all]
Hierfür haben wir uns online in einem Raum getroffen und haben Screen für Screen zusammen abgestimmt. Währendessen hat Anna Kilb unsere Ideen und Vorschläge auf ihrem IPad gezeichnet. Hierbei haben wir versucht die Erkentnisse aus den vorherigen Testungen der alten Prototypen, sowie alle Ideen und Verbesserungsvorschläge in dem neuen Prototyp zu beachten und umzusetzen. 

### Anteile und Selbsteinschätzung 
Wir haben auch hier wieder versucht alle Aufgaben ziemlich gleichmäßig aufzuteilen. Falls jemand eine Frage oder Hilfe gebraucht hatte, haben wir uns gegenseitig immer zur Verfügung gestanden und haben versucht die jeweils andern Personen zu unterstützten. Uns ist es daher sehr schwer gefallen genaue Prozentzahlen festzulegen. Wir waren uns einig, dass wir alle versuchen uns bestmöglich an dem Projekt zu beteiligen und versuchen die Aufgaben so gut wie es geht gleichmäßg zu verteilen, damit jeder im Team gleich viel zutun hat. 
Bislang hat jeder seine zugeteilten Issues bearbeitet. Unsere Teambesprechungen werden mit jedem Mal koordinierter und es macht mit unserer Gruppe sehr viel Spaß zu arbeiten, da es eine sehr unkomplizierte und eine tolle zusammenarbeit ist.

Ich habe mich in diesem Praktikum versucht bestmöglich in das Projekt mit einzubringen und habe hier viele Ideen zum finalen Prototypen mit eingebracht. Noch dazu habe ich  versucht meine Issues bestmöglich abzuarbeiten und umzusetzen. Ich habe meine Teammitglieder über meine Issues in unserem Meeting über den Inhalt meiner Issues informiert. 
Außerdem habe ich zusammen mit Katharina Renk sehr viel Zeit in die Fertigstellung unseres ersten Prototyps gesteckt, um bei diesem das bestmöglicheste Ergebnis herauszuholen. Als erstes hatten wir besprochen, wie wir uns unseren Prototypen vorstellen und worauf wir achten müssen. Anschließend haben wir den Prototyp gezeichnet und den Thinking-Aloud-Test durch geführt. 
Diese Erfahrung hat uns sehr viel geholfen, um dann anschließend mit allen zusammen den finalen Prototypen zu erstellen. 
Hierbei habe ich versucht meine Ideen sowie Verbesserungvorschäge mit einzubringen. 

# P3
## Arbeitspakete
- #59 Detail Deadline View - create activity and implement navigation
Hier habe ich die activity und die hierfür nötige Navigation erstellt

- #61 Edit Deadline View - create activity and implement navigation
Hier habe ich die activity und die hierfür nötige Navigation erstellt

- #68 [Class Diagram] - First Draft [@stmafeyll]
Hier habe ich zusammen mit Matthias Feyll ein Klassendiagramm für unsere Activities erstellt.
Hier haben wir definitert, welche Attribute benötigt werden, wie die Activities heißen sollen und diese wurden anschließend auch von uns erstellt.

- #69 [Navigation] Rückschritt aus Editierung landet in DetailView [@stdehilz]
Hier habe ich zusammen mit Dennis Hilz die Navigation überarbeitet, da wir einen Fehler entdeckt haben.

- #84 [ToDoDetailView] create and implement functions [@stdehilz]
Hier habe ich zusammen mit Dennis Hilz die Activity für unsere 3. Beispiel-App erstellt. Hierunter fielen ein Constraint-Layout benutzen, Funktionen implementieren und die Details der ToDos anzeigen lassen.

- #89 [Overall style] Styling navigation and text elements as theme [@stdehilz]
Hier habe ich zusammen mit Dennis Hilz das Styling der App definiert. Hierunter fielen die Farben und Guidelines.


### Anteile und Selbsteinschätzung 
Auch hier haben wir wieder die Aufgaben versucht gleichmäßig aufzuteilen. Jeder war für eine Activity in der Beispiel-App zuständig.
Wir unterstützten uns immer gegenseitig, falls jemand etwas Hilfe benötigt hatte.

Ich habe mich auhch in diesem Praktikum versucht bestmöglich mit einzubringen und alle meine Issuses abzuarbeiten. Bei der Erstellung der Beispiel-App habe ich sehr viel über das Constraint-Layout gelernt, was wir bestimmt sehr in der großen App helfen wird. Aber nicht nur über das Constraint-Layout habe ich in diesem Praktikum gelernt sondern auch viel darüber, wie die Activities untereinander "kommunizieren". Bei der Erstellung unseres Klassendiagramms, welches ich zusammen mit Matthias Feyll erstellt habe, habe ich eine komplett neue herangehensweise an die Erstellung eines Klassendiagramms gelernt. 


# P4
## Arbeitspakete
- #75 [Calendar View] create functions in general view [@stkarenk]
Hier habe ich zusammen mit Katharina Renk die Calendar View erstellt. Hierzu zählt, die Erstellung des Layouts, die komplette Funktionsweise in Code sowie die Entfernung der Warnings. 

- #76 [Calendar View] create functions in Add Appointment View [@stkarenk]
Hier habe ich zusammen mit Katharina Renk die Add Appointment View erstellt. Hierzu zählt, die Erstellung des Layouts, die komplette Funktionsweise in Code sowie die Entfernung der Warnings. 

- #77 [Calendar View] create functions in Detail Appointment View [@stkarenk]
Hier habe ich zusammen mit Katharina Renk die Detail Appointment View erstellt. Hierzu zählt, die Erstellung des Layouts, die komplette Funktionsweise in Code sowie die Entfernung der Warnings. 

- #78 [Calendar View] create functions in Edit Appointment View [@stkarenk]
Hier habe ich zusammen mit Katharina Renk die Edit Appointment View erstellt. Hierzu zählt, die Erstellung des Layouts, die komplette Funktionsweise in Code sowie die Entfernung der Warnings. 

### Anteile und Selbsteinschätzung 
Bei der Erstellung der App habe ich sehr viel gelernt. Ich habe alle Views, die zum Teil "Kalender" gehören zusammen mit Katharina Renk bearbeitet. Hierfür haben wir uns immer wieder abgesprochen und versucht uns bei Problemen immer gegenseitig zu unterstützen. Wir haben die Aufgaben aufgeteilt. Ich war hierbei unteranderem für die Erstellung des Layouts, für die Navigation auf die Details der jeweiligen Elemente in der Recyclerview sowie für das Auslesen der Daten aus den verschieden Feldern sowie das Übergeben der Daten zwischen den Activities zuständig. Noch dazu habe ich mich um die Beseitigung der meisten warnings in unseren Views gekümmert. 
Hierbei habe ich sehr viel gelernt. Vorallem bei der Funktionsweise der Recyclerview und was ich hierbei machen muss, wenn ich ein Element, welches zwei verschiedene Elternactivities haben, auswähle. Auch bei der Funktionsweise von Gregorian Calendar habe ich sehr viel gelernt. Hierbei musste ich mich zuerst sehr in den Gregorian Calendar reinarbeiten, um die Funktionsweise zu verstehen. Auch um die Navigation habe ich mich noch einmal gekümmert, da wir anfangs zuerst nicht an die komplette Navigation dachten. Im Kalendar werden schließlich auch Elemente aus der Deadlineview angezeigt, wenn ich hierbei auf ein Element klicke komme ich auf die Details dieser Deadline. Wenn ich anschließend den zurück Button betätige, kam ich zurück in die Deadlineview statt in die Calendarview. Desweiteren habe ich mich sehr viel damit beschäftigt, wie die Activities arbeiten und habe durch vieles Debuggen versucht den Ablauf zu verstehen. Hierbei habe ich einen Bug erfolgreich behaben können. Unser Problem war, dass sobald wir ein paar mal zwischen den Activities gewechselt hatten, eine Deadline nicht mehr aus dem Kalendar gelöscht wurde. Grund dafür war, dass die neuen Informationen nicht aus der Datenbank geholt wurden. 
Bei den Aufgaben habe ich sehr viel gelernt und habe alle meine Aufgaben erfüllt auch bei der gegenseitigen Hilfestellung habe ich versucht mich bestmöglich einzubringen. 

Wir haben uns in unserer Gruppe regelmäßig getroffen, um uns auf den neusten Stand zu bringen. Hierbei haben wir uns immer gegenseitig bei Problemen unterstützt und haben wichtige Entscheidungen immer untereinander abgesprochen.



# P5
## Arbeitspakete
- #7 usability inspection durchführen [@stmafeyl]
Zusammen mit Matthias Feyll habe ich die usability inspection durchgeführt. Hierbei sind wir die zuvor erstellt Checkliste in unserer App durchgegangen und haben festgestellt, was unsere App bereits intergriert hat und was unserer App noch fehlt.

- #8 usability inspection vorbereiten [@stmafeyl]
Hier habe ich zusammen mit Matthias Feyll mich in das Thema Usability inspection einarbeitet. Hierbei haben wir eine Checkliste erstellt, um später die usability inspection durchführen zukönnen.

- #3 Informationsaustausch [@all]
Wir haben uns alle Gegenseitig über das was wir gemacht haben informiert und eventuelle Verbesserungsvorschläge gemacht.

- #1 Erstellung der Präsentation [@stdehilz]
Ich hab zusammen mit Dennis Hilz die Präsentation erstellt und gestaltet. Hierbei haben wir die Folien des öfteren  mit der ganzen Gruppe abgesprochen.

### Anteile und Selbsteinschätzung 
Bei meinen Arbeitspaketen habe ich  mich bestmöglich  eingebracht. Bei #7 und #8 habe ich mit Matthias Feyll sehr viel überlegt, was hierbei die besten Fragen wären und ob wir sie wirklich korrekt umgesetzt haben. Auch bei #1 habe ich versucht meine Kreativität freienlauf zulassen, da uns sehr wichtig war eine Powerpoint Präsentation nicht mit Text voll zu schreiben. 

Zusammenfassend finde ich, dass ich mich auch in diesem Praktikum bestmöglich miteingebracht habe, wie der Rest der Gruppe auch. 

## Fazit 
