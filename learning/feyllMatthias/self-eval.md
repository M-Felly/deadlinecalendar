# Self evaluation

# P1 - User research
 - \#43 -  **Teilstandardisierte Interviews** [partner: @stdehilz]<br>
    - Summarize and discuss definition and dos and don'ts about an interview
    - Contribute guiding questions for a partly standardised interview
 - \#35 - **Anwendungsszenarien** [partner: @istankilb]<br>
    - Create and write most of the use case scenarios and add glossary entry.
    - On post processing we roughly worked on equal terms
 - \#34 - **Informationsaustausch** [@all]<br>
    - Conclusion meeting to summarize the assigned issues
    - Mostly lead the team as organizer and time keeper through topics
 - \#32 - **Interview 1** [partner: @istnaleut]<br>
    - Conduct two interviews on one role respectively (interviewer and clerk)
    - Transcript recording [interview](https://code.fbi.h-da.de/nzse-praktikum-wise-2021/WiSe20-21_trapp/Mo3x-1/-/blob/master/ux/interviews/interview1_issue_32/transcript_interview1.md) 
 - \#45 & #46 - **Implement feedback** [partly]<br>
    - Improve issue #43 and #35 with the respective partner
    - Complete question set and formulate interview introduction and end with all team members
    
### Self evaluation and summary
- In our team meetings, I contributed a lot of ideas and gave food for thoughts. Furthermore, I engaged a lot of scrutinising questions to enhance a qualitative discussion.
- I did a lot of organisational stuff e.g. planning meetings or create an [Excel list](https://docs.google.com/spreadsheets/d/1ltzUPkmc2IPVZ9Hxcx6dFL8pCo3mAOutnXFz7hJphp4/edit?usp=sharing) to achieve a better work sharing overview 
- In background, team members asked for my advice in their issues at times. In addition to that, I helped to fix bugs in their first app or accidentally file deletions (e.g. fixing commit a62fc053)
- During issue processing I actively contribute ideas, and a lot of textual content.

### Conclusion
Our team atmosphere in this sprint was very nice and productive. We were able to see a lot of progress in our work and learn achievements.

---------------------

## P2 - Design
 - \#19 - **Material Design** [single task]
    - Getting into the material design documentation
    - Summarize and create a checklist for our app
    - Elaborate differences and area of use about controls (switch, checkbox, radio buttons)
 - \#18 & #55 - **Heuristics** [single task]
    - Research material and summarize it with a focus of Jakob' Law
    - Presenting reveal common design mistakes
    - On post processing, I also wrote out some points which we are aware of and how we are going to implement them
 - \#14 - **thinking aloud with paper prototype** [@all]
    - Discuss conspicuous characteristics
    - Conclude results
    - Adapt the subject's feedback 
 - \#17 - **Information report / general meeting** [@all] 
    - Present assigned issues and summarize
    - Discuss overall questions and next steps
 - \#13 **Merging prototypes** [@all]
    - Finalize one prototype during the practicum meeting and afterwards
 - P2-App [@all]
    - Due to the point that we are one working member more than needed, I also focused my work on the small app which we have to create for P2
    - During the app development for the P2-App I worked as supervisor, jumping from team member to team member to help out. Later on I merged the code together and finalized it.
    - Afterwards I created new working methods to improve our development workflow
    
### Self evaluation and summary
- For my part, I actively take part in team meetings as every other team member as well. Furthermore I partly lead discussions and activated team members with questions like "What's your opinion for this and that?"
- I worked with enthusiasm on my single task and were able to give the team a lot of knowledge and input for our prototypes.
- Beside that, I continued organizing feedback meetings and extending the [Excel list](https://docs.google.com/spreadsheets/d/1ltzUPkmc2IPVZ9Hxcx6dFL8pCo3mAOutnXFz7hJphp4/edit?usp=sharing), so we can see the percentage participation for each team member

### Conclusion
As in P1 our team atmosphere were nice and productive. Due to time limitation each one had to focus more on his part which he/she is good in it.
Big thanks to the team who mostly look after creating the prototypes. Especially to @istankilb who finalized the concluded merged prototype.


-------------------------


## P3 and P4 - Implementation
In this both internships I did a lot of stuff:

### Activities and corresponding Adapter and Dialogs
We assign the responsibilities for each activity. My activities were:
  - ProfileViewActivity [\#80] -> Displays the profile. Add and delete options for modules (studyModules)
  - EditProfileViewActivity [\#81] -> Edit profile. Same options for studyModules as in ProfileViewActivity
  - SettingsViewActivity [\#82] -> Possibility to enable / disable notifications and import an obs calendar

In addition to that I implemented following Activities:
  - EntranceActivity -> App-Logo which is getting displayed on every app start
  - EntranceProfileActivity -> Activity to create a profile on first app launch
  - EntranceStudyModuleActivity -> Second activity to create initial study modules on first app launch

### Database
- [\#83] Firstly together with @istnaleut we design the [ClassDiagramm](https://code.fbi.h-da.de/nzse-praktikum-wise-2021/WiSe20-21_trapp/Mo3x-1/-/blob/master/diagrams/Class_diagram.pdf) which is going to be mapped straight into our database with room (so actually we design a class diagramm and an ERM)
- [\#88] After that @stkarenk and I research about the room framework our needs and how we are going to implement this into our project
  - In the second step we implement room (dao, relations, typeConverters and the database itself) with a lot of struggle due to some strange room behaviour
   
In addition to that I implemented following features for the database:
   - BaseRepository -> A generic abstract class that handles synchronous and asynchronous database executions and its response.
      - The class validates the database output and make it more talkative on errors instead of accepting the requested database action silently
      - Implement a logging for better debugging
   - Specified repositories for entities which inherits the BaseRepository

### Presenter and Shapes
##### Presenter
For a better and easier presenting of data to an activity I implemented all presenter.
   - ImportServiceExceptionPresenter -> Resolves the proper error messages and fix suggestions to the corresponding error on the import service
   - ImportServicePresenter -> Resolves strings for the import service itself (especially status text in progress bar)
   - StudyModuleColorPresenter -> This presenter relates to the StudyModuleShape and resolves the color hex value
   - SubmissionTypePresenter -> This class provides a bidirectional communication (view to model and backwards)

##### Shape
In our app the according study module is getting displayed by an ellipse. The ellipse is mathematically painted in the StudyModuleShape class

### Managers, Services and Receiver 
##### Services
For the obs calendar import [\#54] I implemented an **importService** which pulls the data from the server and parse them into the database.
The parsing process itself is implemented by @stdehilz. For my part I implemented a proper thread (task) and exception handling with the corresponding error message output and fix suggestions plus several feedback messages during the import execution 

##### Receiver
For our app I wrote two receiver:
   - NotificationReceiver [\#52] -> Creates and fires notifications for notifiable classes (Appointments and Deadlines) 
   - SystemBootReceiver -> Register all notifiable classes after the smartphone finishes booting

##### Manager
Manager handles tasks that does not fit into the UI-Thread. These tasks will executed by services and receivers. 
- ManagerHolder -> This class its like a manager for managers
- AlarmManager -> Notifiable objects (Appointments and Deadlines) can register / unregister / update at this manager to fire notifications on their perspective dates 
- NotificationManager -> This manager creates a notification channel required by the system to associate the notifications properly. It also holds the "blueprint" for a notification as an internal Builder class  
- ServiceManager -> Holds and manges every service with their corresponding thread. Most at all this is important when we decide to extend our app and write more services to have a consistent service handling

### Factory
Last but not least I wrote a **DialogFragmentFactory** to build up dialog fragments properly and give a opportunity to parse parameter to the dialog class 


### Self evaluation and summary
- In this two internships I would describe myself as indispensable and the backbone and leading member of team. I did a lot of work in background to keep the programming life for the others as easy as I could.
- I helped other members a lot e.g. for design complex problems (AccessHandler) or various git problems or just suspicious exceptions.
- Personally I learned so much in this project. I regret some design decisions but was able to learn a lot of it. This part delighted me to learn more in android development. 

### Conclusion
Done! We are all done, but happy, satisfied and very positive for the upcoming challenges.


## P5
- \#8 - **Usability inspection - preparation** [partner: @stnaleut]
   - Acquire information and write a generic definition   
   - Summarize a usability inspection checklist and write a specefied for our app needs
- \#7 - **Usability inspection - conduction** [partner: @stnaleut]
   - Conduct the prepared checklist
   - Write a short introduction and conclusion
- \#9 - **Software ergonomic** [single task]
   - Research information on foils and in the internet
   - Summarize them to a general definition
   - Point out two important that deviate from the heuristics
- \#102 - **Evaluation cirteria** [single task]
   - Research information on foils
   - Summarize them to a general definition
   - Point out some important questions and generify them as well as possible 
- \#3 & #1 **Information report / general meeting** [@all]
   - Presented the researches
   - Lead the team as product owner through topics and moderate the meeting
   - Discuss the PowerPoint presentation

### Self evaluation and summary
- The last internship was a bit slowing down. I worked on my task and lead the team (as many internships before) as official product owner
- Last minute I undertake an additional issue (#102) to keep the deadline
- The team atmosphere was partly a bit clunky due to dissensions. But we are very happy and proud keep toughness until the last minute

-------------------------------------------

# Self assessment & final mark
First things goes first: We agreed that all of us contribute equally over all internships. So everyone worked to 20% on the project.<br>

We have worked hard. Hard and precisely. During meetings our first priority was to produce a qualitative content. Therefore, it was quietly hard to keep the 80/20 rule.
On the first and second internship, I would give myself a 1.3 mark. On some points we had to go back to our 80/20 part but mostly we give 100% to accomplish the issues flawless.

In internship three and four I definitely evaluate myself to 1.0 cause of the highly productive amount of invest and outcome I produce. I lead the whole database research, design, implement and refactoring process.
I wrote all Manager, Receiver, Presenter, Shapes(custom layout widgets) and Factory classes. In addition to that I implement 6 Activities and their layouts with the respective dialog and adapter classes on my own.
My work resolves in more than 200 commits and a lead of more than 40 commits to the other tightly staggered team members. The lead refers mainly to this phase. Besides that I helped my team a lot on several problems (e.g. bug fixing, design thinking or complex git problems)

In the last internship I would mark myself with 1.3 again actually with the same reasons as in the first and second one.
During the whole project I worked a lot on organisation stuff and leading the team forward and achieve a good balance for the team to keep the eyes on the main path.

### Conclusion
| Internship | Mark |
|---------|-----|
| P1 & P2 | 1.3 |
| P3 & P4 | 1.0 |
| P5      | 1.3 |

All in all I would estimate myself to 1.3 with a slightly chance to 1.0


