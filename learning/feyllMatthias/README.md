# Learnings

## P1
- Use interfaces for declaration and spec Class for initialisation
### Constraint-Layout
- Views getting bound to other views or layout border.
- You can prioritise constraints by changing the axis bias

### Intents
There are two types of intents
- explicit: Is a direct call to another activity
- implicit: The activity calls out a challange. Registered activities who are able to solve the challange can take the intent

### Event Handling
- "we call you" principle -> When a event is getting fired (e.g. a button is clicked) a handler gets called.

### Java differences to c++
- one file for one class
- final instad of const
- throw identifier to forward unhandled exceptions
- super instead of parent class name
- String instead of string
- @Override declaration as anntotation
- Wrapper classes for parsing primitive data types
- lambda calls: (parm1, parm2) -> {// my lovely code}
- Interfaces <3
- Templates are now generics
- Java is also an island :D

### Exceptions
- checked exceptions: get catched on compile time (e.g. FileNotFoundException)
- unchecked: exception occurres on runtime (e.g. StackOverflowException)

### Read Ressource
````
try (BufferedReader br = new BufferedReader(new Filereader(path))) {
    return br.readLine();
}
````

### Common javadoc tags
- @author
- @see
- @version
- @return
- @exception
- @param
- @link


-------------------------------------------
## P2

### Activity life-cycle [Docu](https://developer.android.com/guide/components/activities/activity-lifecycle)
- You can hook into the life cycles of an activity to customize their behaviour
- With the onCreate() hook you can initialize your elements from the layout
- Good to know: Due to performance improvements the app go into sleep modes when it waits in the background
    - hooks: onPause(), onResume()


### Intent
You can parse objects through activities with by intents. To do that you have to serialize the object 
(with the serializable interface identifier) and call the method "putExtra()" of an intent

### Callbacks from started Activities
By calling the finish() method at the started Activity, the onActivityResult() in the parent activity is getting called.
Parse objects with the help of [Intents](#Intent)

### Resources
To get a consistent layout you can create own styles, dimension, themes and colors in separate .xml files. 

#### Icons
You can create new vector images by clicking "new" and select "vector asset". There you can choose your actual icon that you 
want to convert to a vector asset.
A list of Material design icons is [here](https://material.io/resources/icons/?style=baseline)

#### Colors
Choose a set of colors with the material design [tool](https://material.io/resources/color/#!/). Add the resulting xml file 
into your colors.xml

#### Themes
Material design predefine a plate of [themes](https://developer.android.com/guide/topics/ui/look-and-feel/themes). Use them
in the styles.xml by setting the "parent" attribute on the style tag. Deposit the style at the root element of your layout.

#### Dimensions
You can define different dimensions for different device resolution. Define them in dimens.xml
- formula for calculate density pixels: dp = (px * 160) / dpi

### Navigation
#### Back button
Enables the back button for an activity with these two commands: 
````
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setDisplayShowHomeEnabled(true);
````

Catch the callback with following code:
````
@Override
public boolean onSupportNavigateUp(){
    onBackPressed();
    return true;
}
````

#### Bottom navigation
The bottom navigation is a view container which can be placed into your layout. 
- The actual layout for the bottom navigation is in the folder menu
- By clicking a button the method onNavigationItemSelected gets called to determine the clicked button 


### LayoutInflater
LayoutInflater are the joint between activity (view) classes and its corresponding layout

-------------------------------------------
## P3

#### Best practises implementing a layout
- Keep the layout simple! Don't overuse guidelines
- Use FrameLayout for overlapping elements

#### Notes to constraint layout
- weight -> behaves like a dispensation for elements where both want to fill out the space
- gravity -> behaves like float in css

#### Accessibility
- Give clickable elements a min size of 48dp
- Text should have at least 12dp
- use sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_TEXT_CHANGED) to inform handicapped users about changes
- use the AccessibilityScanner App to check your accessibility
- put related elements together. Don't cascade them in a box or something like windows millennial's stuff.

#### RecyclerView
- The recycler view architecture is an adapter pattern

#### RecyclerView - ViewHolder
- ViewHolder just contains the displaying elements. Not the data.
- The elements getting accessible by getter

#### RecyclerView - Adapter
- The adapter containing the list of elements
- It inserts them by calling the getter methods of the ViewHolder

#### Adapter pattern
- The adapter pattern is useful when you have a class that does not fit to a general
  class (e.g. in RecyclerView the general class is the RecyclerView itself)
- In an adapter you call the methods implemented by an interface that fit to the general class.
- Wrap these methods with our own logic and return a proper value 

#### Images
- Use the Glide class to handle images properly
- Be aware of that images are clunky to load
