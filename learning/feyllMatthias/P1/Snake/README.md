# Snake

### Description
This app is the home town of a little snake. She's very hungry, so you better feed her with apples.
Why apples? Well, this story goes back to Adam and Eva. If you wanna hear you just had to reach out 100.000 Points and you will get the answer :)

Good luck!

## Description (bit more serious)
This app is a typical snake game. Select a fancy name for your snake and press the button (or press enter on your keyboard-layout).
Then the game starts. You can navigate your snake with the navigation buttons on the bottom side.
Try to collect as many apples as possible. They are displayed as a green square. By hitting the border or biting yourself you lost.
The view jumps back to the previous view. You can see your score on in the list above the play button.

## Game parameter
| Name                          | Description                                                             | Default value | DataType      |
|-------------------------------|-------------------------------------------------------------------------|:-------------:|---------------|
| GameEngine.FIELD_COUNT_X      | Number of X fields                                                      |       50      | Integer       |
| GameEngine.FIELD_COUNT_Y      | Number of Y fields                                                      |       50      | Integer       |
| GameEngine.UPDATE_DELAY       | Refresh rate in ms. A lower number increases the velocity               |       70      | Integer       |
| GameEngine.APPLE_DROP_RATE    | Percentage that a apple drops on the field on each map update           |      0.05     | Float         |
| Apple.TTL                     | Number how many update iteration are the apples alive before they decay |      200      | Integer       |
| Snake.EAT_APPLE_POINTS        | Number how many points does a user receive for one apple snack          |      100      | Integer       |
| Snake.DEFAULT_START_DIRECTION | Start step direction when the game begins                               |     RIGHT     | StepDirection |

## Author
Matthias Feyll
