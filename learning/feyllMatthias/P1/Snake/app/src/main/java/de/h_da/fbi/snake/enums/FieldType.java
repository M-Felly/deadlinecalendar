package de.h_da.fbi.snake.enums;

public enum FieldType {
    NOTHING, WALL, SNAKE, APPLE
}
