package de.h_da.fbi.snake.classes;

import androidx.annotation.Nullable;

import java.io.Serializable;

public class Coordinate implements Serializable {
    private int x;
    private int y;

    public Coordinate(int x, int y) {
        this.x = x;
        this.y = y;
    }


    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj == null) {
            return false;
        }

        return this.getClass() == obj.getClass() &&
                ((Coordinate) obj).getX() == this.getX() &&
                ((Coordinate) obj).getY() == this.getY()
        ;
    }
}
