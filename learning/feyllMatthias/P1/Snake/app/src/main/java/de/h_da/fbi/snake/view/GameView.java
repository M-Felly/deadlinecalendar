package de.h_da.fbi.snake.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

import de.h_da.fbi.snake.enums.FieldType;

public class GameView extends View {
    private static final int FIELD_PADDING = 1;


    private final Paint field;
    private FieldType[][] map;

    public GameView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);

        this.field = new Paint();
    }


    /**
     * Render view with {@link #map} data
     * @param canvas canvas
     */
    @SuppressLint("DrawAllocation")
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (this.map == null) {
            return;
        }

        for (int x = 0; x < this.map.length; x++) {
            final int squareSizeX = getWidth() / this.map.length;
            final int squareSizeY = getHeight() / this.map.length;

            // calculate X-axis positions
            final int positionLeft = x * squareSizeX + FIELD_PADDING;
            final int positionRight = x * squareSizeX + squareSizeX - FIELD_PADDING;

            for (int y = 0; y < this.map[x].length; y++) {
                // select color
                switch(this.map[x][y]) {
                    case NOTHING:   this.field.setColor(Color.WHITE);   break;
                    case WALL:      this.field.setColor(Color.BLUE);    break;
                    case SNAKE:     this.field.setColor(Color.BLACK);   break;
                    case APPLE:     this.field.setColor(Color.GREEN);   break;
                }

                // calculate Y-axis positions
                int positionTop = y * squareSizeY + FIELD_PADDING;
                int positionBottom = y * squareSizeY + squareSizeY - FIELD_PADDING;

                // draw rectangle
                canvas.drawRect(new Rect(
                        positionLeft,
                        positionTop,
                        positionRight,
                        positionBottom
                ), this.field);
            }
        }
    }

    public void setMap(FieldType[][] map) {
        this.map = map;
    }
}
