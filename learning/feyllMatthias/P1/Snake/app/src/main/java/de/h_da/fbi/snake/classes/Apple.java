package de.h_da.fbi.snake.classes;

import de.h_da.fbi.snake.enums.AppleState;

public class Apple {
    private static final int TTL = 200;
    
    private int ttl;
    private Coordinate coordinate;

    private AppleState state;

    public Apple (Coordinate coordinate) {
        this.coordinate = coordinate;
        this.ttl = TTL;
        this.state = AppleState.ALIVE;
    }

    public void decreaseTtl() {
        this.ttl--;

        if (ttl < 0) {
            this.state = AppleState.DEAD;
        }
    }

    public AppleState getState() {
        return state;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public void setState(AppleState state) {
        this.state = state;
    }
}
