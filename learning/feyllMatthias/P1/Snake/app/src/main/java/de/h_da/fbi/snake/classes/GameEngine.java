package de.h_da.fbi.snake.classes;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Looper;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import de.h_da.fbi.snake.enums.AppleState;
import de.h_da.fbi.snake.enums.FieldType;
import de.h_da.fbi.snake.enums.GameState;
import de.h_da.fbi.snake.interfaces.GameHandler;
import de.h_da.fbi.snake.view.GameView;

public class GameEngine {
    // Number of x and y fields
    private static final int FIELD_COUNT_X = 50;
    private static final int FIELD_COUNT_Y = 50;

    private static final int UPDATE_DELAY = 70;
    private static final float APPLE_DROP_RATE = 0.05f;


    private final List<Coordinate> border;
    private final List<Apple> apples;
    private final Snake snake;

    private GameState state;
    private final Handler updateHandler;

    private final GameView fieldView;
    private final TextView scoreView;

    private final GameHandler gameHandler;

    public GameEngine(Snake snake, GameView view, TextView scoreView, GameHandler gameHandler) {
        this.fieldView = view;
        this.scoreView = scoreView;
        this.gameHandler = gameHandler;
        this.snake = snake;

        this.border = new ArrayList<>();
        this.apples = new ArrayList<>();
        this.updateHandler = new Handler(Looper.getMainLooper());
    }


    /**
     * Init game properties. Set {@link #state} to init
     */
    public void init() {
        this.createBorder();
        this.snake.create();
        this.fieldView.setMap(this.createMap());

        state = GameState.INIT;
    }

    /**
     * Run game until {@link #state} is lost
     */
    public void run() {
        this.updateHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                step();

                if (state == GameState.LOST) {
                    gameHandler.onLostHandler();
                    return;
                }

                updateHandler.postDelayed(this, UPDATE_DELAY);

                updateView();
            }
        }, UPDATE_DELAY);

        this.state = GameState.RUNNING;
    }

    /**
     * Update map by {@link #createMap()} and set new score
     */
    @SuppressLint("SetTextI18n")
    private void updateView() {
        fieldView.setMap(createMap());
        scoreView.setText(Integer.toString(snake.getScore()));
        fieldView.invalidate();
    }

    /**
     * Execute one step in game
     */
    private void step() {
        this.snake.move();
        this.appleStep();

        if (this.hasPlayerLost()) {
            this.state = GameState.LOST;
        }
    }

    /**
     * Update apples -> removes dead apples and create new on {@link #APPLE_DROP_RATE probability} hit
     */
    private void appleStep() {
        // decrease apple ttl
        for (Iterator<Apple> iter = this.apples.iterator(); iter.hasNext();) {
            Apple apple = iter.next();
            apple.decreaseTtl();

            // head of snake is at position of one apple
            if (this.snake.getHeadPosition().equals(apple.getCoordinate())) {
                apple.setState(AppleState.EATEN);
                this.snake.eatApple();
            }

            // remove all dead or eaten apples
            if (apple.getState() == AppleState.DEAD || apple.getState() == AppleState.EATEN) {
                iter.remove();
            }
        }

        // create new apple
        if (Math.random() <= APPLE_DROP_RATE) {
            Random r = new Random();

            this.apples.add(new Apple(new Coordinate(
                    r.nextInt(FIELD_COUNT_X -2) + 1,
                    r.nextInt(FIELD_COUNT_Y - 2) + 1
            )));
        }
    }


    /**
     * Set {@link #border} coordinates
     */
    private void createBorder () {
        for (int x = 0; x < FIELD_COUNT_X; x++) {
            this.border.add(new Coordinate(x, 0));
            this.border.add(new Coordinate(x, FIELD_COUNT_Y -1));
        }

        for (int y = 0; y < FIELD_COUNT_Y; y++) {
            this.border.add(new Coordinate(0, y));
            this.border.add(new Coordinate(FIELD_COUNT_X -1, y));
        }
    }

    /**
     * Create new map and set border
     * @return new created map
     */
    public FieldType[][] createMap() {
        FieldType[][] map = new FieldType[FIELD_COUNT_X][FIELD_COUNT_Y];

        // set all fields to nothing
        for (int x = 0; x < FIELD_COUNT_X; x++) {
            for (int y = 0; y < FIELD_COUNT_Y; y++) {
                map[x][y] = FieldType.NOTHING;
            }
        }

        // set border
        for (Coordinate coordinate: border) {
            map[coordinate.getX()][coordinate.getY()] = FieldType.WALL;
        }

        // set snake
        for (Coordinate coordinate: snake.getPosition()) {
            map[coordinate.getX()][coordinate.getY()] = FieldType.SNAKE;
        }

        // set apple
        for (Apple apple: this.apples) {
            map[apple.getCoordinate().getX()][apple.getCoordinate().getY()] = FieldType.APPLE;
        }

        return map;
    }

    /**
     * Check if game is lost. Game is lost by hitting the border snake head is stepping into her tail (that makes outch)
     * @return boolean
     */
    public boolean hasPlayerLost() {
        // check if snake hits the border
        for (Coordinate cord: border) {
            if (cord.equals(snake.getHeadPosition())) {
                return true;
            }
        }

        // check if snake steps into her tail and bites herself
        for (Coordinate cord: snake.getPosition()) {
            if (cord.equals(snake.getHeadPosition()) && cord.hashCode() != snake.getHeadPosition().hashCode()) {
                return true;
            }
        }

        return false;
    }
}
