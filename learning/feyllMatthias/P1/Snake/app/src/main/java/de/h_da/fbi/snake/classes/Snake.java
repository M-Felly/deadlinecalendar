package de.h_da.fbi.snake.classes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import de.h_da.fbi.snake.enums.StepDirection;

public class Snake implements Serializable {
    // Define the default step direction for the snake on game start
    private static final StepDirection DEFAULT_START_DIRECTION = StepDirection.RIGHT;
    private static final int EAT_APPLE_POINTS = 100;

    private final String name;
    private int score;


    // game properties
    private final List<Coordinate> position;
    private StepDirection stepDirection;

    public Snake(String name) {
        this.name = name;
        this.score = 0;

        this.position = new ArrayList<>();
        this.stepDirection = DEFAULT_START_DIRECTION;
    }

    /**
     * Create first snake with a length of 5
     */
    public void create() {
        this.position.add(new Coordinate(9, 10));
        this.position.add(new Coordinate(8, 20));
        this.position.add(new Coordinate(7, 20));
        this.position.add(new Coordinate(6, 20));
        this.position.add(new Coordinate(5, 20));
    }

    /**
     * Move snake one step depending on its current {@link #stepDirection}
     */
    public void move() {
        switch (this.stepDirection) {
            case DOWN:  updateSnakeTail(0, 1); break;
            case UP:    updateSnakeTail(0, -1);  break;
            case LEFT:  updateSnakeTail(-1, 0); break;
            case RIGHT: updateSnakeTail(1, 0);  break;
        }
    }

    /**
     * Append new tail. And eat this juicy apple :D
     */
    public void eatApple() {
        Coordinate lastCord = this.position.get(this.position.size() -1);

        int x = lastCord.getX();
        int y = lastCord.getY();

        // append new tail depending on the snakes direction
        switch (this.stepDirection) {
            case LEFT: x++; break;
            case RIGHT: x--;break;
            case UP: y++; break;
            case DOWN: y--; break;
        }

        this.position.add(new Coordinate(x, y));
        this.score += EAT_APPLE_POINTS;
    }

    /**
     * @param relativeX relative x axis movement to current head position (should be between 1 and-1)
     * @param relativeY relative y axis movement to current head position (should be between 1 and -1)
     */
    private void updateSnakeTail(int relativeX, int relativeY) {
        // shift tail one step forward
        for (int i = this.position.size() - 1; i > 0; i--) {
            this.position.set(i, this.position.get(i - 1));
        }

        // step head
        this.position.set(0, new Coordinate(
                this.position.get(0).getX() + relativeX,
                    this.position.get(0).getY() + relativeY
        ));
    }

    /***********************************************
     *               Getter / Setter
     ***********************************************/
    public void setStepDirection(StepDirection stepDirection) {
        this.stepDirection = stepDirection;
    }

    public List<Coordinate> getPosition() {
        return position;
    }

    public Coordinate getHeadPosition() {
        return position.get(0);
    }

    public String getName() {
        return name;
    }

    public int getScore() {
        return score;
    }
}
