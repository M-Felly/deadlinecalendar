package de.h_da.fbi.snake;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageButton;
import android.widget.TextView;

import de.h_da.fbi.snake.classes.GameEngine;
import de.h_da.fbi.snake.classes.Snake;
import de.h_da.fbi.snake.enums.StepDirection;
import de.h_da.fbi.snake.view.GameView;

public class GameActivity extends AppCompatActivity {

    Snake snake;
    GameEngine game;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        // action left right up and down button
        ImageButton actionBtnLeft = findViewById(R.id.game_actionButtonLeft);
        ImageButton actionBtnRight = findViewById(R.id.game_actionButtonRight);
        ImageButton actionBtnUp = findViewById(R.id.game_actionButtonUp);
        ImageButton actionBtnDown = findViewById(R.id.game_actionButtonDown);

        actionBtnLeft.setOnClickListener(v -> changeSnakeDirection(StepDirection.LEFT));
        actionBtnRight.setOnClickListener(v -> changeSnakeDirection(StepDirection.RIGHT));
        actionBtnUp.setOnClickListener(v -> changeSnakeDirection(StepDirection.UP));
        actionBtnDown.setOnClickListener(v -> changeSnakeDirection(StepDirection.DOWN));

        // score field
        TextView scoreView = findViewById(R.id.game_scoreTextValue);

        // game
        GameView fieldView = findViewById(R.id.game_fieldView);
        snake = (Snake) getIntent().getSerializableExtra("snake");

        game = new GameEngine(snake, fieldView, scoreView, this::onLost);
        game.init();
    }

    /**
     * Parse score to {@link StartActivity}
     */
    private void onLost() {
        Intent scoreIntent = new Intent();
        scoreIntent.putExtra("snake", snake);
        setResult(Activity.RESULT_OK, scoreIntent);

        finish();
    }

    /**
     * Change step direction on button click
     * @param direction next direction
     */
    private void changeSnakeDirection(StepDirection direction) {
        this.snake.setStepDirection(direction);
    }


    /**
     * Start game
     */
    @Override
    protected void onStart() {
        super.onStart();
        this.game.run();
    }
}