package de.h_da.fbi.snake.enums;

/**
 * Snakes walking direction
 */
public enum StepDirection {
    LEFT, RIGHT, UP, DOWN
}
