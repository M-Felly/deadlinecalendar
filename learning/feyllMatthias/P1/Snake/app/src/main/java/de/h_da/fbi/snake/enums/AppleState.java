package de.h_da.fbi.snake.enums;

public enum AppleState {
    ALIVE, DEAD, EATEN
}
