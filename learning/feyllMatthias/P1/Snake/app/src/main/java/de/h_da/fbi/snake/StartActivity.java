package de.h_da.fbi.snake;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import de.h_da.fbi.snake.classes.Snake;

public class StartActivity extends AppCompatActivity {
    private static final int ACTIVITY_RESULT = 1;
    private EditText nameInput;

    private TableLayout table;


    /**
     * Workaround for double triggering {@link #nameInputKeyHandler}
     */
    private boolean gameStarted = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        nameInput = findViewById(R.id.start_nameInput);
        nameInput.setOnKeyListener((v, keyCode, event) -> nameInputKeyHandler(keyCode));

        Button goButton = findViewById(R.id.start_goButton);
        goButton.setOnClickListener(view -> this.startGame());

        table = findViewById(R.id.start_tableLayout);
    }


    /**
     * Create new row on score list and insert snake data
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data == null) {
            Toast.makeText(this, "Ooops something went wrong", Toast.LENGTH_SHORT).show();
            return;
        }

        Snake snake = (Snake) data.getSerializableExtra("snake");

        TableRow row = new TableRow(this);
        TextView name = new TextView(this);
        TextView score = new TextView(this);

        name.setText(snake.getName());
        name.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT, 1f));
        name.setGravity(Gravity.START);

        score.setText(String.valueOf(snake.getScore()));
        score.setGravity(Gravity.END);

        row.addView(name);
        row.addView(score);

        this.table.addView(row);
        this.gameStarted = false;
        this.nameInput.getText().clear();
    }

    /**
     * Handle enter key pressed in {@link #nameInput}
     *
     * @param keyCode pressed key
     * @return event is consumed on enter button pressed
     */
    private boolean nameInputKeyHandler(int keyCode) {
        if (this.gameStarted) {
            return false;
        }

        if (keyCode == KeyEvent.KEYCODE_ENTER) {
            this.startGame();
            return true;
        }

        return false;
    }

    /**
     * Switch activity to {@link GameActivity} when {@link #isGameStartable()} is true.
     * Create a new {@link Snake}
     */
    private void startGame() {
        if (!this.isGameStartable()) {
            this.nameInput.setError(getString(R.string.start_nameInput_error));

            return;
        }

        // create new snake
        Snake newGameSnake = new Snake(this.nameInput.getText().toString());

        // create intent and add
        Intent newGameIntent = new Intent(StartActivity.this, GameActivity.class);
        newGameIntent.putExtra("snake", newGameSnake);

        startActivityForResult(newGameIntent, ACTIVITY_RESULT);
        this.gameStarted = true;
    }


    /**
     * Return true if {@link #nameInput} is not empty
     *
     * @return boolean
     */
    private boolean isGameStartable() {
        return !this.nameInput.getText().toString().isEmpty();
    }
}