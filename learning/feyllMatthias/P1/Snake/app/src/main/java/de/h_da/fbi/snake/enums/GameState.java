package de.h_da.fbi.snake.enums;

import de.h_da.fbi.snake.classes.GameEngine;

/**
 * Get the game state for in {@link GameEngine}
 */
public enum GameState {
    INIT, RUNNING, LOST
}
