package de.h_da.fbi.datecalendar.classes;


import android.text.Editable;

import java.io.Serializable;
import java.util.Date;

/**
 * This class describes an appointment as it would be described in nature.
 *
 * Users can create appointments which get displayed on MainActivity by selecting the respective date
 * in the calendar widget
 */
public class Appointment implements Serializable  {
    private static int counter = 0;
    private final int id;
    private String title;
    private Date date;

    /**
     * The Constructor is setting the date, title and the id
     * @param title is a information which kind of appointment it is
     * @param date is the date which is selected at the CalendarDate
     */
    public Appointment(String title, Date date) {
        this.title = title;
        this.date = date;

        id = counter++;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Date getDate() {
        return date;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
