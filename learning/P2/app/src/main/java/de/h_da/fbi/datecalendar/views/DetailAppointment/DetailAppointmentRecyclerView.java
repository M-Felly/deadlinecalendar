package de.h_da.fbi.datecalendar.views.DetailAppointment;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import java.util.ArrayList;
import java.util.List;

import de.h_da.fbi.datecalendar.classes.Appointment;

/**
 * This class is the container for the RecyclerView component.
 *
 * The structure has been kept simple: It just uses a LinearLayout to display appointments one by
 * one. Furthermore we just use one item {@link Adapter#getItemViewType type}
 */
public class DetailAppointmentRecyclerView extends RecyclerView  {
    private DetailAppointmentAdapter adapter;

    /**
     * Creates the RecyclerView which gets displayed by the default RecyclerView layout.
     *
     * @param context parent context
     * @param attrs layout attributes
     */
    public DetailAppointmentRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater.from(context);
        adapter = new DetailAppointmentAdapter();

        this.setAdapter(adapter);
        this.setLayoutManager(new LinearLayoutManager(context));
    }

    /**
     * Sets a list of appointments that will be displayed into the recycler view
     *
     * @param appointments list of appointments that will be displayed
     */
    public void setAppointments(List<Appointment> appointments) {
        this.adapter = new DetailAppointmentAdapter(appointments);

        this.swapAdapter(this.adapter, true);
    }

    public void clearAppointments() {
        this.setAppointments(new ArrayList<>());
    }
}
