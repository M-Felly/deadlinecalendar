package de.h_da.fbi.datecalendar.views.DetailAppointment;

import android.annotation.SuppressLint;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import de.h_da.fbi.datecalendar.R;
import de.h_da.fbi.datecalendar.classes.Appointment;


/**
 * This class represents the view of an appointment item included from the RecyclerView list,
 * named as ViewHolder.
 */
public class DetailAppointmentItem {

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // field elements
        private final TextView titleField;
        private final TextView dateField;

        //Date formatter
        DateFormat dateFormat;

        // appointment which get displayed to this item
        private Appointment appointment;

        /**
         * The constructor initializes the view elements and the dateFormatter.
         *
         * @param itemView generated view context by {@link android.widget.Adapter}
         */
        @SuppressLint("SimpleDateFormat")
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.dateFormat = new SimpleDateFormat("hh:mm");

            this.titleField = itemView.findViewById(R.id.mainActivityDetailAppointmentItem_titleField);
            this.dateField = itemView.findViewById(R.id.mainActivityDetailAppointmentItem_dateField);
        }

        /**
         * Extracts attributes information out of the object and parse them into the view elements.
         *
         * @param appointment appointment to be displayed
         */
        public void setAppointmentItem(@NonNull final Appointment appointment) {
            this.appointment = appointment;

            this.titleField.setText(appointment.getTitle());
            this.dateField.setText(this.dateFormat.format(appointment.getDate()));
        }
    }
}