package de.h_da.fbi.datecalendar.views.DetailAppointment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import de.h_da.fbi.datecalendar.R;
import de.h_da.fbi.datecalendar.classes.Appointment;
import de.h_da.fbi.datecalendar.views.DetailAppointment.DetailAppointmentItem.ViewHolder;

/**
 * The adapter is part of the RecyclerView architecture.
 * It connects the {@link DetailAppointmentItem.ViewHolder} to the actual RecyclerView for a dynamic
 * item view purpose.
 *
 * @see DetailAppointmentRecyclerView
 */
public class DetailAppointmentAdapter extends RecyclerView.Adapter<ViewHolder> {
    private final List<Appointment> appointments;

    /**
     * Creates a RecyclerView adapter with an empty list.
     */
    public DetailAppointmentAdapter() {
        this.appointments = new ArrayList<>();
    }

    /**
     * Creates a RecyclerView adapter with appointments to be displayed
     *
     * @param appointments list of appointments to be displayed in a list
     */
    public DetailAppointmentAdapter(@NonNull List<Appointment> appointments) {
        this.appointments = appointments;
    }

    /**
     * Creates new ViewHolder to display a new appointment in list
     *
     * @param parent parent group object which contains all ViewHolder in context
     * @param viewType for different types of ViewHolder. Can be ignored in this way because we just use
     *                 one view.
     *
     * @see DetailAppointmentItem.ViewHolder
     *
     * @return created ViewHolder
     */
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_main_detail_appointment_item ,parent, false);

        return new ViewHolder(v);
    }

    /**
     * Forward Appointment object to view holder
     *
     * @param holder corresponding view holder
     * @param position index of appointment list
     */
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.setAppointmentItem(this.appointments.get(position));
    }

    /**
     * Get number of appointments in {@link #appointments} to get the number list items to be
     * generated
     *
     * @return int number of appointments
     */
    @Override
    public int getItemCount() {
        return this.appointments.size();
    }
}
