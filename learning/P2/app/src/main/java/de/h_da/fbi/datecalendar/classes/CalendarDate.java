package de.h_da.fbi.datecalendar.classes;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.Date;

/**
 * CalendarDate
 * This class manages the Appointments of a Date
 *
 * In this class you can manage all Appointments of one Day.
 * You can add, remove or get an Appointment.
 * You can set or get a Date or the complete List of Appointments.
 * The Appointments are managed in an ArrayList
 */
public class CalendarDate {

    private ArrayList<Appointment> appointments;
    private Date date;

    /**
     * The constructor will create a new ArrayList of the object Appointment
     */
    public CalendarDate(Date date){
        appointments = new ArrayList<>();
        this.date = date;
    }

    /**
     * This Method creates a new Appointment and adds it to the ArrayList appointments
     * @param date is the selected day of the Calendar
     * @param description is the information about the Appointment
     */
    public void addAppointment(Appointment appointment){
        appointments.add(appointment);
    }

    /**
     * This Method searches for the Appointment with the provided id
     * @param id is a primary key of Appointments per CalendarDate
     * @return the appointment with the provided id or a nullptr if it doesn't exist
     */
    public Appointment getAppointment(int id){
        for(int appointmentCounter = 0; appointmentCounter < appointments.size(); appointmentCounter++){
            if(appointments.get(appointmentCounter).getId() == id){
                return appointments.get(appointmentCounter);
            }
        }
        return null;
    }

    /**
     * This Method will remove an Appointment from the ArrayList
     * @param id is a primary key of an Appointment
     */
    public void removeAppointment(int id){
        for(int appointmentCounter = 0; appointmentCounter < appointments.size(); appointmentCounter++){
            if(appointments.get(appointmentCounter).getId() == id){
                appointments.remove(id);
            }
        }
    }


    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public ArrayList<Appointment> getAppointments() {
        return appointments;
    }

}
