package de.h_da.fbi.datecalendar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CalendarView;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;



import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import de.h_da.fbi.datecalendar.classes.Appointment;
import de.h_da.fbi.datecalendar.classes.CalendarDate;

import de.h_da.fbi.datecalendar.views.DetailAppointment.DetailAppointmentItem;
import de.h_da.fbi.datecalendar.views.DetailAppointment.DetailAppointmentRecyclerView;

public class MainActivity extends AppCompatActivity {
    private static final int ACTIVITY_EDIT_APPOINTMENT_RESULT = 1;
    public static final long ONE_DAY_IN_MILLISECONDS = 8640000;

    private ArrayList<CalendarDate> calendarDates;
    private Date selectedDate;
    private CalendarView calendarView;
    private FloatingActionButton addBtn;
    private DetailAppointmentRecyclerView detailAppointmentRecycler;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Toolbar
        toolbar = findViewById(R.id.appToolBar);
        setSupportActionBar(toolbar);

        selectedDate = Calendar.getInstance().getTime();
        calendarDates = new ArrayList<>();

        // view elements
        detailAppointmentRecycler = findViewById(R.id.mainActivity_detailAppointmentView);
        calendarView = findViewById(R.id.mainActivity_calendarView);

        // if the Button plus is pushed you can create a new appointment with newAppointment in calendar
        addBtn = findViewById(R.id.mainActivity_addBtn);
        addBtn.setOnClickListener(view->newAppointment());

        //if one Date is selected the method showList is called and handed over are the variables year, month and year as an int
        calendarView.setOnDateChangeListener((calendarView, year, month, dayOfMonth) -> {
            selectedDate = new Date(year-1900, month, dayOfMonth);
            displayAppointments();
        });
    }


    /**
     * Result handler for edited appointment from the editing activity
     *
     * Save the edited appointment to the respective DayCalendar and update the {@link DetailAppointmentRecyclerView}
     *
     * @param requestCode request code is {@link #ACTIVITY_EDIT_APPOINTMENT_RESULT}
     * @param resultCode result code is {@link #RESULT_OK}
     * @param data expected Appointment with name 'appointment'
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data == null) {
            return;
        }

        Appointment appointment = (Appointment) data.getSerializableExtra("appointment");
        CalendarDate calendarDate = checkAlreadyExistCalendarDate(appointment.getDate());

        if (calendarDate == null) {
            calendarDate = this.createANewCalendarDate();
        }

        calendarDate.addAppointment(appointment);
        this.displayAppointments();
    }

    /**
     * Displays all appointments which take place on the current selected day in the calendar view
     */
    private void displayAppointments() {
        CalendarDate date = this.getSelectedCalendarDay();

        if (date == null) {
            this.detailAppointmentRecycler.clearAppointments();
            return;
        }

        this.detailAppointmentRecycler.setAppointments(date.getAppointments());
    }

    /**
     * If the selected day has a listed CalendarDate return it.Otherwise return null
     *
     * @return returns founded CalendarDate or null;
     */
    @Nullable
    private CalendarDate getSelectedCalendarDay() {
        for (CalendarDate calendarDate : calendarDates) {
            long timeDelta = this.selectedDate.getTime() - calendarDate.getDate().getTime();
            timeDelta = Math.abs(timeDelta);

            if (timeDelta < ONE_DAY_IN_MILLISECONDS) {
                return calendarDate;
            }
        }

        return null;
    }

    /**
     * The method newAppointment will create a new date
     *
     * At first we have to call the activity which creates a new appointment.
     * If the new appointment is saved at within the activity, the method will call the method
     * addAppointmentToCalendarDates and add the Appointment to the ArrayList calendarDates
     * with the information from the activity which creates the new appointment.
     *
     * date is the date which is selected in the activity which creates the appointment
     * description is the variable which describes which kind of appointment it is
     *
     */
    private void newAppointment(){
        Intent newAppointmentIntent = new Intent(MainActivity.this, CRUDAppointmentActivity.class);
        newAppointmentIntent.putExtra("date", selectedDate);
        startActivityForResult(newAppointmentIntent, ACTIVITY_EDIT_APPOINTMENT_RESULT);
    }


    /**
     * Creates a new calendar date in the ArrayList CalendarDate
     */
    private CalendarDate createANewCalendarDate(){
        CalendarDate newCalendarDate = new CalendarDate(selectedDate);
        calendarDates.add(newCalendarDate);

        return newCalendarDate;
    }

    /**
     * This method checks if the CalendarDate already existsf
     * The method compares all dates within the ArrayList against the date provided as argument
     * @param date is the date which is being searched
     * @return position of the date in the ArrayList calendarDate
     */
    @Nullable
    private CalendarDate checkAlreadyExistCalendarDate(Date date){
        for (CalendarDate calendarDate : this.calendarDates) {
            if(calendarDate.getDate().equals(date)){
                return calendarDate;
            }
        }

        return null;
    }

    /** This method is needed to create the individual Toolbar with buttons.
     * It will position all Toolbar buttons.
     *
     * @param menu is the type of all button elements.
     * @return true if successfully
     */

    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
        //return super.onCreateOptionsMenu(menu);
    }

    /** If Options Toolbar button is clicked, Settings Activity will be started.
     *
     * @param item represents the clicked button
     * @return true if successful
     */
    public boolean onOptionsItemSelected(@NonNull MenuItem item){
        startActivity(new Intent(MainActivity.this, SettingsActivity.class));
        return true;

    }

}
