package de.h_da.fbi.datecalendar;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Calendar;
import java.util.Date;

import de.h_da.fbi.datecalendar.classes.Appointment;

public class CRUDAppointmentActivity extends AppCompatActivity {

    private static final String TAG = "CRUDAppointmentActivity";

    private TextView displayDate;
    private DatePickerDialog.OnDateSetListener dateSetListener;

    private TextView displayTime;
    private TimePickerDialog.OnTimeSetListener timeSetListener;

    private ImageButton CRUDCancelButton;
    private ImageButton CRUDConfirmButton;
    private EditText CRUDAppointmentName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_appointment);
        CRUDAppointmentName = findViewById(R.id.CRUDAppointment_editText_name);
        displayDate = (TextView) findViewById(R.id.CRUDAppointment_textView_date);
        displayTime = (TextView) findViewById(R.id.CRUDAppointment_textView_time);


        CRUDCancelButton = findViewById(R.id.CRUDAppointment_imageButton_cancel);
        CRUDConfirmButton = findViewById(R.id.CRUPAppointment_imageButton_confirm);

        Date currDate = (Date) getIntent().getExtras().getSerializable("date");
        String currTime = DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT).format(LocalTime.now());
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");
        displayDate.setText(dateFormatter.format(currDate));
        displayTime.setText(currTime);

        displayDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        CRUDAppointmentActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        dateSetListener,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Log.d(TAG, "onDateSet: dd.mm.yyyy: " + dayOfMonth + "." + month + "." + year);

                String date = dayOfMonth + "." + month + "." + year;
                displayDate.setText(date);
            }
        };


        displayTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int hourOfDay = cal.get(Calendar.HOUR_OF_DAY);
                int minute = cal.get(Calendar.MINUTE);

                TimePickerDialog dialog = new TimePickerDialog(
                        CRUDAppointmentActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        timeSetListener,
                        hourOfDay, minute, true);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        timeSetListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                Log.d(TAG, "onTimeSet: hh:mm: " + hourOfDay + ":" + minute);

                String time = hourOfDay + ":" + minute;
                displayTime.setText(time);
            }
        };
        CRUDCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CRUDAppointmentActivity.this.finish();
            }
        });

        CRUDConfirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                try {
                    Date appointmentDate = new SimpleDateFormat("dd.MM.yyyy").parse(displayDate.getText().toString());

                    Appointment appointment = new Appointment(CRUDAppointmentName.getText().toString(), appointmentDate);
                    intent.putExtra("appointment", appointment);
                    setResult(RESULT_OK, intent);
                    finish();

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}