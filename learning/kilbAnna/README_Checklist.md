# Rückblick

## Besonderheiten

###Java
Da ich bereits etwas java Erfahrungen habe, sind die Unterschiede mir bereits bekannt.
Ich habe aber seit länger Zeit nicht mehr mit java gearbeitet und musste mich erstmal wieder einarbeiten.
- die Arbeit mit Referenzen und Pointern ist nicht mehr nötig
- **Lambda**-Ausdrücke
- keine Header und cpp Files mehr (Deklaration und Implementierung ist in der gleichen Datei)
- Quellcode in **.java**
- Bytcode in **.class**
- spezielle **GUI-Bibliotheken**
- Java Programme werden **Packages** strukturiert (Name ohne Bindestrich!) -> import für das importieren von anderen Packages
- **super()** ruft den Konstruktor der Basisklasse auf
- **final** statt const
- nur Einfachvererbung
- keine Operatorenüberladung oder Default-Parameter
- Objekte werden immer dynamisch allokiert -> **new** liefert Zeiger auf das Objekt
- es gibt kein delete
- es gibt keinen Destruktor -> dafür die Methode protected void finalize()
- Variablen werden automatisch initialisiert, außer lokale Variablen
- **String** statt string
- **==** ist ein Referenz-Vergleich
- **.equals()** vergleicht Textinhalte
- **Wrapperklassen** z.B Integer, Long, Double, ...
- **Collections:** List(ArrayList, LinkedList, Vector) & Set(HashSet, SortedSet)
- **Map:** SortedMap & HashMap
- **Arrays** werden kaum benutzt -> meistens wird das Interface **List**
- **Interface** class 
``
    public class Class extends BaseClass implements Interface`
``
- **@Override:** wenn man eine Methode der Basisklasse überschreiben möchte -> immer super() aufrufen wenn nicht abstract
- bei Exceptions gibt es **finally** -> wird immer ausgeführt
- **Genererics**: <br>
  List\<Car\>,...  -> in der Liste sind nur Objekte vom Typ Car
- **Streams:** sequentielle Ein- und Ausgabe read() & write(int)

#### Checkliste/Achtung/typische Fehler
- == anstelle von equals
- String statt string
- final statt const

#### CheatSheet
Wichtige Befehle o.ä.

- Lambda
- super()
- equals()
- student.forEach(student -> ...); oder for(Student student : students) {...}

### App-Entwicklung
Mit der App-Entwicklung hatte ich bisher noch keine Erfahrungen.
- erste Zeile:
````
<?xml version="1.0! encoding="utf-8"?>
````
- bei den meisten Strings und Colors ist es sinnvoll sie in eine eigene XML-Datei zuschreiben.
- OnCreate(): Hier wird die Aktivität initialisiert
    - setContentView(int)
    - findViewById(int): Abrufen der Widgets
- View.onClickedListener()
- Layouts
- Canvas ist wichtig für das Zeichnen -> Bitmap
- dp -> px = dp * (dpi/160)