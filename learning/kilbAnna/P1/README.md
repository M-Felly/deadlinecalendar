#### Autorin
Anna Kilb <br>
Version 1 <br>
Letzte Änderung: 14.11.2020

##Praktikum 1

Ich habe eine App entwickelt, die die verschiedenen Positionen zweier Kreise ausgibt.

#### Beschreibung meiner App

Wenn sich die App öffnet sieht man Textfelder für das Eingeben der Positionen und der Radien zweier Kreise. Gibt man diese korrekt ein und drückt auf "Kreise anzeigen" wird die Lage der Kreise berechnen und wie folgt ausgegeben:
- "Die Kreise sind identisch"
- "Kreis 1/2 liegt vollstaendig in Kreis 2/1."
- "Es gibt keine Überlappung der Kreise."
- "Die beiden Kreise schneiden sich."

Außerdem werden die beiden Kreise unten eingezeichnet (In blau Kreis 1 und in cyan Kreis 2).

#### Berechnungen
- identisch: Wenn alle Angaben des ersten Kreises mit den Angaben des zweiten Kreises übereinstimmen
- Interior: Wenn d < |K1r - K2r|
- External: Wenn d > (K1r + K2r)
- Intersect: Wenn nicht identisch und Interior und External

d = sqrt((C1X - C2X) * (C1X - C2X) + (C1Y - C2Y) * (C1Y - C2Y)) -> Abstand zwischen den beiden Kreisen
<br>
K1: Kreis 1 <br>
K2: Kreis 2 <br>
X: X-Position <br>
Y: Y-Position <br>
r: Radius

#### Errors
- Es wird ein Fehler angezeigt, wenn in ein oder mehreren Felder nichts rein geschrieben wurde und man die Kreise schon anzeigen möchte.
- Es wird ein Fehler angezeigt, wenn bei beiden Radien 0 ausgewählt wurde