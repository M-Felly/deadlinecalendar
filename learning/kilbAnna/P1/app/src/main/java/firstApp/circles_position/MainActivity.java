package firstApp.circles_position;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText edit_C1X, edit_C1Y, edit_C1r, edit_C2X, edit_C2Y, edit_C2r;
    private TextView txt_CirclesPosition;
    private ImageView img_Circles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edit_C1X = findViewById(R.id.editTextC1X);
        edit_C1Y = findViewById(R.id.editTextC1Y);
        edit_C1r = findViewById(R.id.editTextC1r);
        edit_C2X = findViewById(R.id.editTextC2X);
        edit_C2Y = findViewById(R.id.editTextC2Y);
        edit_C2r = findViewById(R.id.editTextC2r);
        txt_CirclesPosition = findViewById(R.id.textViewCirclesPosition);
        img_Circles = findViewById(R.id.imageView_Circles);
        Button btnCalculate = findViewById(R.id.buttonCalculate);
        
        btnCalculate.setOnClickListener(v -> {
            if (isValid()) {
                float C1X = Float.parseFloat(edit_C1X.getText().toString());
                float C1Y = Float.parseFloat(edit_C1Y.getText().toString());
                float C1r = Float.parseFloat(edit_C1r.getText().toString());
                float C2X = Float.parseFloat(edit_C2X.getText().toString());
                float C2Y = Float.parseFloat(edit_C2Y.getText().toString());
                float C2r = Float.parseFloat(edit_C2r.getText().toString());
                String position = getPosition(C1X, C1Y, C1r, C2X, C2Y, C2r);
                setPosition(position);

                // set paint
                Paint paint = new Paint();
                paint.setColor(Color.BLUE);
                paint.setStyle(Paint.Style.STROKE);
                paint.setStrokeWidth(0.5f);

                // create Bitmap
                Bitmap bmp = Bitmap.createBitmap(img_Circles.getWidth(), img_Circles.getHeight(), Bitmap.Config.ARGB_8888);

                // Canvas
                Canvas canvas = new Canvas(bmp);

                // vector bottom left
                float blX = Math.min(C1X - C1r, C2X - C2r);
                float blY = Math.min(C1Y - C1r, C2Y - C2r);

                // vector top right
                float trX = Math.max(C1X + C1r, C2X + C2r);
                float trY = Math.max(C1Y + C1r, C2Y + C2r);

                // move origin to the bottom left and mirror coordinate system about the x-axes
                canvas.translate(0, canvas.getHeight());
                canvas.scale(1, -1);
                canvas.translate(20, 20);

                C1X -= blX;
                C1Y -= blY;
                C2X -= blX;
                C2Y -= blY;

                // scaling factor
                float sf = Math.min((img_Circles.getWidth() - 20) / (trX - blX), (img_Circles.getHeight() - 20) / (trY - blY)) * 0.97f;

                // scale and draw
                canvas.scale(sf, sf);
                canvas.drawCircle(C1X, C1Y, C1r, paint);
                paint.setColor(Color.CYAN);
                canvas.drawCircle(C2X, C2Y, C2r, paint);

                // output circles
                img_Circles.setImageBitmap(bmp);
            }

        });
    }

    /**
     * input errors are recognized here
     *
     * @return true if there is no error and otherwise false
     */
    private boolean isValid() {
        if (edit_C1X.getText().toString().isEmpty()) {
            edit_C1X.setError(getString(R.string.warning_C1X));
            return false;
        }
        if (edit_C1Y.getText().toString().isEmpty()) {
            edit_C1Y.setError(getString(R.string.warning_C1Y));
            return false;
        }
        if (edit_C1r.getText().toString().isEmpty()) {
            edit_C1r.setError(getString(R.string.warning_C1r));
            return false;
        }
        if (edit_C2X.getText().toString().isEmpty()) {
            edit_C2X.setError(getString(R.string.warning_C2X));
            return false;
        }
        if (edit_C2Y.getText().toString().isEmpty()) {
            edit_C2Y.setError(getString(R.string.warning_C2Y));
            return false;
        }
        if (edit_C2r.getText().toString().isEmpty()) {
            edit_C2r.setError(getString(R.string.warning_C2r));
            return false;
        }
        if (Float.parseFloat(edit_C1r.getText().toString()) == 0 && Float.parseFloat(edit_C2r.getText().toString()) == 0) {
            edit_C2r.setError(getString(R.string.warning_radien0));
        }
        return true;
    }

    private String getPosition(float C1X, float C1Y, float C1r, float C2X, float C2Y, float C2r) {
        double d = Math.sqrt(Math.pow(C1X - C2X, 2) + Math.pow(C1Y - C2Y, 2));      // distance between the two circles
        if (C1X == C2X && C1Y == C2Y && C1r == C2r) {
            return "Same Circle";
        } else {
            if (d < Math.abs(C1r - C2r)) {
                return "Interior";
            } else {
                if ((C1r + C2r) < d) {
                    return "External";
                } else {
                    return "Intersect";
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private void setPosition(String position) {
        if (position.equals("Same Circle")) {
            txt_CirclesPosition.setText("Die Kreise sind identisch");
        }
        if (position.equals("Interior")) {
            if (Float.parseFloat(edit_C1r.getText().toString()) > Float.parseFloat(edit_C2r.getText().toString())) {
                txt_CirclesPosition.setText("Kreis 2 liegt vollstaendig in Kreis 1.");
            } else {
                txt_CirclesPosition.setText("Kreis 1 liegt vollstaendig in Kreis 2.");
            }
        }
        if (position.equals("External")) {
            txt_CirclesPosition.setText("Es gibt keine Überlappung der Kreise.");
        }
        if (position.equals("Intersect")) {
            txt_CirclesPosition.setText("Die beiden Kreise schneiden sich.");
        }
    }
}