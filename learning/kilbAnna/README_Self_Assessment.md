# Selbsteinschätzung
Schreiben Sie hier am Ende, welche Note Sie sich selbst geben würden. Begründen Sie dies mit Hinweis
auf das Kompetenzraster und Ihre Arbeit.

## P1 – User Research
### Arbeitspakete
- Vorbereitung:
    - Videos und Material durcharbeiten und Notizen machen (README_Checklist)
    - **User Research und Persona:** [Partner: Nadine Leuthe]<br> Nadine und ich haben zusammen bei einem Treffen die Begriffserklärung für User Research, Benutzer\*innen und Persona erstellt. Außerdem haben wir uns die persona.md und die PERSONA.pdf erarbeitet.
    - **Anwendungsszenarien:** [@stmafeyl]<br> Matthias und ich haben zusammen bei einem Treffen die Begriffserklärung für Anwendungsszenarien erstellt, drei Anwendungsszenarien in szenarios.md und den Informationsbedarf in guidelines.md uns erarbeitet.
    - **User Stories:** <br> Ich habe in Einzelarbeit die Begriffe User Stories und Akzeptanzkriterien mir erarbeitet und zusammengefasst. Außerdem habe ich mir jeweils drei Beispiele für User Stories und Akzeptanzkriterien im Bezug auf das Projekt überlegt.
    - **Informationsaustausch:** [alle] <br> Unser Team hat sich zusammengesetzt und jeder hat sein Thema vorgestellt. Es gab teilweise Verbesserungsvorschläge, welche dann wenn gewünscht sofort oder später übernommen wurden. Außerdem haben wir schon eine kurze Vision erstellt.
- Praktikum:
    - Wir haben alle zusammen die Leitlinien des Interviews uns erarbeitet.
- Nachbereitung:
    - Nach dem Feedback haben Nadine und ich zusammen die Persona und Matthias und ich die Anwendungsszenarien verbessert. Außerdem habe ich mir neue und bessere Use-Cases ausgedacht.
    - **2.Interview:** [@stdehilz]<br> Dennis und ich haben zusammen eine Person aus dem Umkreis interviewt, dieses Interview aufgenommen und danach aufgeschrieben.
    - **Informationsaustausch:** [alle]<br> Wir haben dabei die Interviews vorgestellt und eine Schlussfolgerung daraus gezogen.
    - **Persona:** [Partner: Nadine Leuthe]<br> Nadine und ich haben zusammen die Persona finalisiert.
    - **Anwendungsszenarien:** [@stmafeyl]<br> Matthias und ich haben zusammen die Anwendungsszenarien finalisiert.
    - **User Stories:** [@stkarenk]<br> Katharina und ich haben zusammen Issues mit User Stories, Akzeptanzkriterien und Labels für die MVPs erstellt.

Durch das Feedback habe ich bemerkt, dass die Vorbereitung doch nicht ganz so gut war, wie ich mir das gedacht hatte und man noch viele Verbesserungen machen kann (o). Das Feedback hat mir dann dabei geholfen z.B. bei den Use-Cases die richtigen Beispiele zu finden. <br>
Dadurch finde ich ist das Endergebnis mir und meinen jeweiligen Partnern sehr gut gelungen (+). Wir haben uns auch immer mehr Zeit genommen, als eigentlich dafür gedacht war um ein gutes Ergebnis zu erzielen.


## P2 - Entwurf
### Arbeitspakete
- Videos und Material durcharbeiten und Notizen machen
- **Paper-Prototyp Zeichnung:** [@stdehilz] <br> Dennis hat die meisten groben Skizzen erstellt, wir haben sie dann besprochen und ich habe dann anhand dessen die Skizzen der Prototypen gezeichnet.
- **thinking-aloud mit Paper-Prototyp:** [@stdehilz] <br> Wir haben jeweils eine Testperson den Prototypen ausprobieren lassen. Dabei haben wir Aufgaben an diese Personen gestellt und dies dokumentiert.

Die Paper-Prototypen sind Dennis und mir sehr gut gelungen. Wir haben sehr viel Zeit investiert einen Prototypen zu kreieren, der übersichtlich und für die Testpersonen ansprechend aussieht.
 
 
## P3 und P4 - Layout inkl. Listen und Logik und Persistenz
### Arbeitspakete
- Issue 72 [Deadline View] create functions in general view [@stdehilz]
    - Dennis und ich haben anfangs mittels Pair-Programming diese View angepasst und die Anbindung zur Datenbank hergestellt. In dieser View sind einige Probleme aufgetreten, um die sich Dennis dann hauptsächlich gekümmert hat. In der Zeit habe ich schon angefangen die drei anderen Views anzupassen.
- Issue 73 [Deadline View] create functions in Detail Deadline View [@stdehilz]
    - In dieser View habe ich anfangs das Layout angepasst und die Verbindung zur Datenbank hergestellt, sodass es dann möglich war sich die Deadline in der DetailDeadlineView anzeigen zu lassen, ToDos hinzuzufügen und zu löschen..
    Diese View hat Dennis später noch überarbeitet und ein paar Features hinzugefügt. Ich habe mich dann hauptsächlich noch um die ToDos gekümmert. Hier gab es einige Probleme, die ich aber beheben konnte.
- Issue 74 [Deadline View] create functions in Edit Deadline View
    - In dieser View habe ich anfangs das Layout angepasst und die Verbindung zur Datenbank hergestellt, sodass es dann möglich war eine Deadline zu bearbeiten, ToDos hinzuzufügen und zu löschen.. In dieser View und in der AddDeadlineView hatte ich ebenfalls Probleme mit den ToDos, besonders beim Ändern des Namens eines ToDos. Leider musste ich mich dann für eine TextView anstatt eines EditTexts entscheiden. Jetzt muss man um den Namen eines ToDos zu ändern leider erst das ToDo löschen und neu erstellen. Dies könnte man in einer der nächsten Versionen der App wenn man mehr Zeit hat wieder hinzufügen.
- Issue 79 [Deadline View] create functions in AddDeadline View
    - In dieser View habe ich anfangs das Layout angepasst und die Verbindung zur Datenbank hergestellt, sodass es dann möglich war eine neue Deadline zu erstellen, ToDos hinzuzufügen und zu löschen.
- DeadlineAccessHandler, AddDeadlineDialog, DeadlineDeleteDialog
- Issue 53 [App Logo] create App Logo
    - Ich habe mit Hilfe der Ideen von meinen Teammitgliedern ein App Logo kreiert und in die app eingebunden. Dafür habe eine Vectorgrafik an meinem Tablet gezeichnet. Dies hat sehr viel Zeit in Anspruch genommen und ich musste oft Änderungen vornehmen. Ich finde es ist mir sehr gut gelungen die Funktionen der App in einem Icon darzustellen.

Ich habe wirklich sehr viel Zeit in dieses Praktikum gesteckt und ich denke das haben auch die Anderen aus dem Team. Wir haben wirklich versucht eine effiziente und auch gut aussehende App zu entwickeln. Ich denke das ist uns sehr gut gelungen. Natürlich kann man immer irgendwo noch etwas verbessern und es war auch nicht möglich alles in der kurzen Zeit umzusetzen. Es gibt immer noch Verbesserungen, die auf meiner Liste stehen, aber noch nicht abgearbeitet sind, da die Zeit nicht mehr gereicht hat. Dies sind aber nur Kleinigkeiten und können in den nächsten Versionen erledigt werden. <br>
Es ist schön jetzt das Ergebnis unserer harten Arbeit zu sehen. Es hat sehr viel Spaß gemacht in einem so tollen Team zusammenzuarbeiten.


## P5 - Projektabschluss
### Arbeitspakete
- Issue 2 **Video der App** [@stkarenk] <br> Katharina und ich haben uns zusammengesetzt und Vorschläge für den Ablauf, das Design und die Hintergrundmusik gesammelt. Katharina hat im Anschluss die Bildschirmaufnahmen vom Handy gemacht und Audioaufnahmen von ihrer Stimme aufgenommen (Später haben wir uns dazu entschieden anstatt mit der Stimme mit Stichwörtern in dem Video zu arbeiten). Ich habe danach einen Hintergrund ausgewählt und die Videos zusammengeschnitten. Außerdem habe ich eine Hintergrundmusik erstellt und die Stichwörter auf dem Bild einblenden lassen. Wir haben uns gegenseitig unterstützt, um päzise und gut zu arbeiten. <br>
- Issue 5 **thinking aloud durchführen** [@stdehilz] <br> Dennis und ich haben uns zusammengesetzt und die Einleitung, die Aufgaben und den Abschluss geschrieben. Dann hat jeder mit einer Person das thinking aloud durchgeführt und später dokumentiert.

In diesem Praktikum ist mir besonders gut das Video gelungen. Ich habe sehr viel Zeit investiert und es hat sich auch gelohnt. Dafür, dass wir nicht so viel Zeit hatten, um das Video zu erstellen, ist es wirklich gut geworden.

## Selbsteinschätzung und Fazit
Was ist mir besonders gut gelungen?
- Paper-Prototyp
- Implementieren der DeadlineViews
- App-Logo
- Video der App

Die Praktika haben mir sehr viel Freude bereitet. Ich habe nicht nur was neues gelernt, das mir auch Spaß macht, sondern ich habe auch einfach das beste Team. Wir agieren sehr gut miteinander und helfen uns gegenseitig. Ohne meine Teammitglieder wäre das Endergebnis wahrscheinlich nur halb so gut geworden. Ich hoffe das wir uns entweder noch weiter mit der App beschäftigen oder in einem anderen Projekt nochmal zusammenarbeiten können. <br>
Wir haben von Anfang an gesagt, dass wir in das Projekt viel Zeit investieren wollen und das haben wir auch eindeutig gemacht.

**Note insgesamt: 1,3**
Ich würde mir für meine Leistung und auch die Leistung meiner Teammitglieder die Note 1,3 geben. Wir haben die uns selbst gesetzten Ziele erreicht und die meisten auftretenden Probleme erfolgreich gelöst. Außerdem haben wir eine effiziente und auch gut aussehende App entwickelt. Jeder von uns hatte neue und erfolgsversprechende Ideen, die eingebracht wurden. <br>
Bei meinen Aufgaben habe ich mir immer sehr viel Mühe gegeben, um ein möglichst gutes Ergebnis zu erzielen. Ich habe Verbesserungsvorschläge gut aufgenommen und in die Tat umgesetzt.