# Self Evaluation

## P1 - User Research
### Work packages
- **Issue #43 P1-V: teilstandardisierte Interviews (Pair)** [Partner: Matthias Feyll]
  - Discussion about different forms of interviews in general and about the provided video content
  - Phrasing definition for glossary entry
  - Phrasing guidelines for a good interview (brainstorming and role playing)
- **Issue #41 P1-V: User Research - Hypothesen (Pair)** [Partner: Katharina Renk]
  - Phrasing definition for glossary entry
  - Phrasing hypotheses based on experiences and possible scenarios (exchanging stories and forming ideas)
- **Issue #34 P1-V: Informationsaustausch (alle)** [Whole Group]
  - Open feedback round about tasks
  - Contribution as part-time moderator for better structure of discussion and agenda
  - Assigning tasks to group members for refactoring phrases and editing certain files
- **Issue #45 ToDos P1 / Feedback**
  - Improved issue #43 and #41 with the respective partner
- **Issue #33 P1-N: Interview 2 (Pair)** [Partner: Anna Katharina Kilb]
  - Conducting the Interview and transcribing the recording
  - Presenting results and thoughts about the interviews to the group in our meeting
  - Improving structure of transcript
- **Issue #47 P1-N: User-Journey-Map**
  - Gathering information about the topic
  - Creating definition and general information in README.md
  - Creating user journey maps for each persona (archtype)

### Participation and self evaluation
  - My participation in every task can be described as active and inviting.
  - All team members contributed equally to the named issues
  - My role in open discussions or brainstormings could often be described as moderator or idea-collector (I came up with suggestions and was asking about feedback on those ideas and further additions)

In conclusion I would say that my work was sufficient and good but I need to work on the structure/organisation of said discussions or idea exchanges beforehand.

## P2 - Draft
### Work packages
- **Issue #12 P2-V: Navigationsübersicht (Pair)** [Partner: Katharina Renk]
  - Presenting each other the acquired knowledge through the course material (information exchange)
  - Drawing first sketches to provide activity and navigation overview
  - Reviewing ideas and finalizing drawing for group presentation
- **Issue #20 P2-V: (Theorie erarbeiten) Fitt's Law (1 Person)**
  - Working through course material and familiarizing with content by looking at bad examples from Play Store
  - Summarize the facts for glossary
- **Issue #24 P2-V: (Theorie erarbeiten) Prototypes (1 Person)**
  - Working through course material
  - Gathering additional knowledge (internet research)
  - Creating Summary for glossary
- **Issue #16 P2-V: Paper-Prototyp 1 erstellen (Pair)** [Partner: Anna Katharina Kilb]
  - Going through MVP-ideas from group discussion to determine the most important activities and functionalities
  - Creating first sketches of every activity and possible options
  - Revising first sketches for next iterations of the prototype
  - Finishing final version for group discussion
- **Issue #14 P2-V: Thinking aloud mit Paper-Prototyp (alle)**
  - Creating questions and tasks for thinking aloud
  - Cutting out all the pieces of the paper prototype and sorting them
  - Conducting thinking aloud and summarizing results for markdown file
  - Creating pictures for "diashow" of the paper prototype
- **Issue #17 P2-V: Informationsaustausch (alle)**
  - Presenting results of paper prototype and discussing feedback of thinking aloud test run
  - Exchanging first approaches on how to merge our two prototypes
  - Discussing course material and planning details for the next app
- **Issue #13 P2-V: Merge der Prototypen (alle)**
  - Conducting group meeting to merge the two prototypes into one and updating the navigation overview
  - Finalizing prototype in live session together (digital drawing)
- **Issue #55 ToDos / Feedback P2**
  - Revising Issue #20 with better examples fitting our group project
  - Conducting group retrospective and discussing future methods to avoid timing issues

### Participation and self evaluation
- Our group dynamic is still getting better every time we discuss or work together
- Like before, every team member contributed equally to the named issues
- For P2, one must add, that Anna did a really great job of finalizing our first paper prototype as well as drawing the final prototype with influence of the ideas of the whole group
- For the first prototype we decided, that it would be best to let us sketch half of the screens in a short form with few details, give all our sketches up for discussion between us two and finalizing the prototype by letting her draw and me giving additional ideas on how to organize the elements within our layout
- Our group meetings are getting more productive by discussing a quick agenda beforehand but with keeping all of the jokes and fun within our discussions ;)
- Timing issues before the P2 Deadline were resolved by everyone holding on to their tasks and working together without letting anyone work alone.

## P3 / P4
Reflecting those two practicals could be quite difficult :) <br>
This time I will not list every specific issue but rather explaining my contributions to the project in general and referencing code snippets, activities etc. for a better overview. <br>

### Activities
We decided to divide our workload by assigning responsibility for each activity to a group member or group-parts. As for Anna and me, we worked mainly on the following Activities at first:
- DeadlineViewActivity [\#72]
- EditDeadlineActivity [\#74]
- AddDeadlineActivity [\#79]

Of course this included the implementation of corresponding adapters, viewholders etc. as well.

### Repositories
- After the initial implementation of the database connectivity (Thanks to Matthias and Katharina :) ) we were able to implement the specific abstraction layer (Repositories) for each needed DAO-class (Abstract base class "BaseRepository" was provided by Matthias! - Great work! - Logging helps a lot while debugging ;))

- Implementing methods within the Repos (pointing to a specific query in DAO) for our use cases

### Relations
- Creating Relation-class for every "Deadline-related" relation

- Each Relation-class is needed for persisting the relationship in database-context

### Miscellaneous
#### DeadlineAccessHandler
- While Room as the chosen OR-Mapper, was not working as we initially expected (Just referencing classes and persist them like that - Like JPA ) we needed a way to handle our relation-classes outside the database in a different way.

- We created a second abstraction layer above the mentioned repositories to provide CRUD-functionalities for our deadline objects and all of their relations. 
  - Brainstorming together (Anna, Matthias and me) - Matthias came up with the idea of a second layer
  - Initial code provided by me and put up for Review with Anna for further implemenations and corrections

The DeadlineAccessHandler manages all of our CRUD-functions while always keeping a persistent state of the database data and heap allocated objects.

#### RemainingTimeConverter
- Anna and I tried to find a way to easily display the remaining time of our deadlines in a easy and understandable way and came up with a complex-looking but easy-to-understand solution.

- We declared time units as constants (dividing milliseconds) and only display two units at a time for a good overview.

- During refactoring I outsourced the parsing and assembling functionability to reuse it within different contexts.

#### ImportService / AppointmentParser
- Matthias and I took over the part for the import of external calendar data (or in our case at least the data from OBS) [\#54]

- We created the initial class and the first working solution for the network connection together and then split up to focus on improving the code structure and adding advanced Task- and Exceptionhandling (Matthias) and parsing the obtained data and persisting it into our database scheme (Me).

- The AppointmentParser class provides methods to read, process and persist calendar-data from an ics file obtained by the OBS-Calendar Subscription-Link. The data is split into separate appointments and their
attributes by iterating through the file while using RegEx for matching specific strings to obtain the corresponding values.

- I did prefer the RegEx approach to substring-parsing for a cleaner code

### Participation and self evaluation
Within those two practicals, I would describe myself as a really valueable member of this group, who always stayed up late to fix bugs or to refactor code. I came up with ideas and contributed them to provide a expandable codebase (DeadlineAccessHandler, Outsourcing for reuse etc.) and always provided help if needed. I developed better understanding of many complex concepts regarding android development and Java and tried to make the best out of this chance. I'm truly thankful for Matthias as our top-member of the group, who has a great understanding of all the concepts and has always put great ideas in our minds.

### Conclusion
Done! Many nights and days of hard work, but we learned a lot and I would still prefer this project-based module over the regular examination. Thanks for the opportunity :)

## P5

- \#6 - **P5-V: thinking aloud vorbereiten (Pair)** [partner: @stankilb]
  - Brainstorming ideas on how to cover all functions of our app with the assignments
  - Preparing layout and assignments for usability test
  - Schedule an appointment with the test person and preparing notes

- \#5 - **P5-V: thinking aloud durchführen (Pair)** [partner: @stankilb]
  - Giving a proper introduction of our app idea to the test person
  - Walking through all the assignments while taking notes and recording the session
  - Discussing enhancement potential afterwards

- \#4 - **P5-V: Usability-Bericht erstellen (Pair)** [partner: @stankilb]
  - Transcripting the usability testing from the recording
  - Filling out our report template

- \#1 - **P5-V: Präsentation für P5 (alle)** [partner: @stnaleut]
  - Creating the presentation and choosing a good strategy on how to keep our design as simple as possible while covering all our content
  - Preparing notes and thoughts for the final presentation

### Self evaluation and summary
- The last practical was mainly about making our hard work transparent to everyone listening to our final presentation and reading our files after submission.
- The creation of the presentation (with Nadine) was going pretty well, because we agreed on almost every design decision and strategy and therefore were able to achieve a simple but effective presentation of our app.
- The thinking aloud went also well and I was happy to see, that our app navigates well and is intuitive enough in most of the situations.

### Self assessment & final mark
For the whole project we agreed, that we contributed equally and therefore want to be assessed equally.<br>

Throughout the whole project I would describe myself as an active member of the group, who always wanted to get the best out of everything. That often lead us to a little longer brainstorming phase and the result was always good. Despite our workswas never completely flawless, we always delivered the best results, which we could in the respective time frame. During practical 4 we could have gone with better approaches on how to solve things in some cases, but we were always able to complete our task without reducing the overall quality of our final product. We worked hard, had countless meetings about decisions and issues, but finally happy to see, that the work we did resulted in something great and handy in future study times.<br>

### Conclusion
| Internship | Mark |
|---------|-----|
| P1 & P2 | 1.0 |
| P3 & P4 | 1.3 |
| P5      | 1.3 |

All in all I would estimate myself and all of us to 1.3 with a slightly chance to 1.0