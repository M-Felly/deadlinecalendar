package com.example.firstapp_hilz;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Button;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    private EditText sideLengthA, sideLengthB, sideLengthC;
    private TextView outputInfo, outputArea, outputPerim, outputAngles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sideLengthA = findViewById(R.id.editText_a);
        sideLengthB = findViewById(R.id.editText_b);
        sideLengthC = findViewById(R.id.editText_c);
        outputInfo = findViewById(R.id.outputInfo);
        outputArea = findViewById(R.id.outputArea);
        outputPerim = findViewById(R.id.outputPerim);
        outputAngles = findViewById(R.id.outputAngles);

        Button btnCalc = findViewById(R.id.btn_calculate);
        btnCalc.setOnClickListener(view -> calcHandler());
    }

    DecimalFormat df = new DecimalFormat("###.##");

    private void alertDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setMessage(R.string.errorDialogDesc);
        dialog.setTitle(R.string.errorDialogTitle);
        AlertDialog alertDialog = dialog.create();
        alertDialog.show();
    }

    private boolean isValidInput(){
        if (sideLengthA.getText().toString().isEmpty()){
            sideLengthA.setError(getString(R.string.inputField1ErrMsg));
            return false;
        }
        if (sideLengthB.getText().toString().isEmpty()){
            sideLengthB.setError(getString(R.string.inputField2ErrMsg));
            return false;
        }
        if (sideLengthC.getText().toString().isEmpty()){
            sideLengthC.setError(getString(R.string.inputField3ErrMsg));
            return false;
        }
        return true;
    }

    private boolean isValidTriangle(double a, double b, double c){
        if (a + b <= c || a + c <= b || b + c <= a)
            return false;
        return true;
    }

    private String describeTriangle(double a, double b, double c, List<Double> angleList){
        if (a == b && b == c )
            return getString(R.string.triangleEqu);
        if (a == b || b == c || a == c)
            return getString(R.string.triangleIso);
        if (angleList.get(0) == 90 && angleList.get(1) == 90 && angleList.get(2) == 90)
            return getString(R.string.triangleAcu);
        if (angleList.get(0) == 90 || angleList.get(1) == 90 || angleList.get(2) == 90)
            return getString(R.string.triangleRight);
        if (angleList.get(0) >= 90 || angleList.get(1) >= 90 || angleList.get(2) == 90)
            return getString(R.string.triangleObt);
        else
            return getString(R.string.triangleSca);
    }

    private double calcArea(double a, double b, double c){
        double semi_perim = (a+b+c)/2;
        return Math.sqrt(semi_perim*(semi_perim-a)*(semi_perim-b)*(semi_perim-c));
    }

    private double calcPerim(double a, double b, double c){
        return a+b+c;
    }

    private List<Double> calcAngles(double a, double b, double c){
        double alpha = Math.toDegrees(Math.acos((-(a*a)+(b*b)+(c*c))/(2*b*c)));
        double beta = Math.toDegrees(Math.acos((-(b*b)+(a*a)+(c*c))/(2*a*c)));
        double gamma = Math.toDegrees(Math.acos((-(c*c)+(a*a)+(b*b))/(2*a*b)));

        return Arrays.asList(alpha, beta, gamma);
    }

    private void resetInputFields(){
        sideLengthA.setText("");
        sideLengthB.setText("");
        sideLengthC.setText("");
    }

    private void resetOutputFields(){
        outputInfo.setText("");
        outputArea.setText("");
        outputPerim.setText("");
        outputAngles.setText("");
    }

    private void calcHandler() {

        if (isValidInput()){
            double a = Double.parseDouble(sideLengthA.getText().toString());
            double b = Double.parseDouble(sideLengthB.getText().toString());
            double c = Double.parseDouble(sideLengthC.getText().toString());

            if (isValidTriangle(a,b,c)){
                List<Double> angles = calcAngles(a,b,c);
                outputInfo.setText(new StringBuilder().append(getString(R.string.triangleType)).append("\n").append(describeTriangle(a, b, c, angles)).toString());
                outputArea.setText(new StringBuilder().append(getString(R.string.triangleArea)).append("\n").append(df.format(calcArea(a, b, c))).toString());
                outputPerim.setText(new StringBuilder().append(getString(R.string.trianglePerim)).append("\n").append(df.format(calcPerim(a, b, c))).toString());
                outputAngles.setText(new StringBuilder().append(getString(R.string.triangleAngles)).append("\n\u03B1 = ").append(df.format(angles.get(0))).append("\u00B0\n\u03B2 =  ").append(df.format(angles.get(1))).append("\u00B0\n\u03B3 = ").append(df.format(angles.get(2))).append("\u00B0").toString());
            }
            else {
                alertDialog();
                resetInputFields();
                resetOutputFields();
            }
        }
    }
}
