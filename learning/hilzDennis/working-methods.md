# Working methods

## Issues and solutions
### Overview
-  **[Issue]** Getting started with all the information and structuring tasks withouth losing sight of the overall topic
-  **[Solution]** Starting every task with a quick brainstorming of what we might need to accomplish and how it could fit with all the other tasks prepared by the rest of the group

### Structured discussion and information exchange
-  **[Issue]** The information exchange after the preparation of the individual tasks took too long and lacked in information density.
-  **[Solution]** Structuring said discussions and exchanges beforehand and sticking to a plan approved by the whole team

## Plan
- Meetings for individual tasks and for the information exchange were terminated with enough time buffer
- Time tracking with Pomodeiro technique in P2

## Plan evaluation
- Meetings lasted longer than expected but were productive

## Duration
- The average time for the meetings exceeded the expected amount of time by about 20%-60% 

## Retrospective
- The tasks and discussions were approached highly motivated despite the fact that sometimes we could not put every task and part of the practical in perspective
- The learning curve was steep and tasks became easier to fulfill