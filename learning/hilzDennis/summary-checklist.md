# Practical #1
## General
- Introduction to JVM and it's characteristics
  - **Source code** --compiler--> **Byte Code** --(JIT-)interpreter--> **Machine Code**
  - JVM can be used with multiple languages e.g. Python etc.
- Nice to know about Java in general (What I haven't known yet)
  - All methods are *virtual* as default
  - No default parameters
  - Wrapper classes for Generics and classes
  - const (C++) -> final (Java)
  - Interfaces as outer view and description but no implementation
    - Interface[List] --> Implementation[Arraylist, LinkedList etc.]
  - Checked Exceptions --> detected by compiler
  - Unchecked Exceptions --> appearing during runtime (errors, runtime exceptions...)


## Specifics
- Working with Android-Emulator speeds up the debugging process
- Working in xml layout code is faster than using the design view
- One file per java class
- Concatenating strings with StringBuilder and .append() for memory efficiency
- Overriding method of base class with decorator @override
- Accessing layout-objects not directly -> initializing via findViewById after Inflation
- Working with strings.xml instead of hardcoded strings

## Checklist
- String not string :)
- Primitive datatype? -> compare with ==
- Complex datatype? (object)
  - Compare with .equals() for value comparison
  - Compare with == for reference comparison
- Outsourcing functions where possible!

## Code Snippets
### Declaration of button with corresponding handler function via lambda expression

```java
Button btnCalc = findViewById(R.id.btn_calculate);
        btnCalc.setOnClickListener(view -> calcHandler());
```

### Retrieving string out of strings.xml
```java
getString(R.string.identifier) // getString expects identifier as integer
```

### Formatting output (decimal numbers)
```java
DecimalFormat df = new DecimalFormat("###.##");
// (...)
df.format()
```

```java
String.format("Random String:" %.f, floatvariable)
```

# Practical #2
## General
- Generating Listeners, Getter/Setter etc. is way more faster than writing it out manually
- Using *dp as dimension unit for better compatibility among different devices
- BottomNavigation > Other Navigation elements
- Date- and Timepickers can use different themes (Spinner element provided best usability)
- Toasts > Alerts

## Specifics
### Inter-Activity Communication
- Communication/Serialization between Activities resembles AJAX / JSON handling in Web-Development
- Intent objects include data bundle with key-value pairs of data (complex objects or primitive datatypes)
- Implementing Serializable is needed for classes/objects to be transferred between activities



## Code Snippets
### Communication between Activities
#### 1. Activity (Sending)
```java
Intent newAppointmentIntent = new Intent(MainActivity.this, CRUDAppointmentActivity.class);
newAppointmentIntent.putExtra("date", currentDate);
startActivityForResult(newAppointmentIntent, ACTIVITY_EDIT_APPOINTMENT_RESULT);
```

#### 2. Activity (Receiving / Returning)
```java
getIntent().getExtras().getSerializable("date");
```

```java
intent.putExtra("appointment", appointment);
setResult(RESULT_OK, intent);
finish();
```

#### 1. Activity Receiving
```java
super.onActivityResult(requestCode, resultCode, data);
data.getSerializableExtra("appointment");
```

### Global Handling of OnClickListeners
```java
...implements View.OnClickListener

@Override
public void onClick(View view) {
    switch (view.getId()) {
        case R.id.mytextviewone: // doStuff
            break;
        case R.id.mytextviewone: // doStuff
            break;
    }
}
```

# Practical 3

## General
- At least 7mm for each touch target and 2mm gap between those items
- Color contrast at least: 4.5 (check with: contrast-ratio.com)
- Minimum font size: 12sp
- Hit-size >= 48dp
- Use Accessibility Scanner App to ensure these standards

### Layout
- FrameLayout for fragments or overlays
- LinearLayout for simple use cases (vertical or horizontal)
- Constraintlayout for most of the use cases (At least two constraints needed: horizontal and vertical)
- Odp to match constraints
- Setting **constraintDimensionRatio** to calculate the dimensions according to the ratio


### RecyclerView
#### Components
- Data objects
- ItemLayout
- Adapter [Constructor(), onBindViewHOlder(), getItemCount()]
  - establishes connection between RecyclerView and the view itself
- Viewholder(s) [Constructor(), onCreateViewHolder()]
- RecyclerView [Set LayoutManager, Set Adapter]
- LayoutManager (has to be set within the respective RecyclerView)

#### Procedure
- LayoutManager calls Adapter
- Adapter creates ViewHolder (onCreateViewHolder) --> returns ViewHolder


### Activity Interaction
- Using integer constants (RequestCode) and provide each startActivity with such constant
  - OnActivityResult returns those RequestCodes to differentiate between the return from different activities

## Code Snippets

### onCreate Method in Activity
```java
todosAdapter = new ToDosAdapter(this, todos, this);
        recyclerViewToDos = findViewById(R.id.toDoOverviewActivity_recyclerView_toDos);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerViewToDos.setLayoutManager(linearLayoutManager);
        recyclerViewToDos.setAdapter(todosAdapter);
```

### onActivityResult (Differentiate requestCodes)
```java
@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == ADD_TODO_VIEW_ACTIVITY_REQUEST_CODE) {
          // Do stuff here
        }
        else if(requestCode == DETAIL_TODO_VIEW_ACTIVITY_REQUEST_CODE) {
          // Do stuff here

        }
    }
```

### Building short dialog
```java

private void onAddTaskButtonClicked(View v) {
        // Create an alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Task Name");
        // Set the dialog layout
        final View dialogLayout = getLayoutInflater().inflate(R.layout.activity_detail_to_do_view_dialog, null);
        builder.setView(dialogLayout);

        // Add the dialog confirm button
        builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Send data from the dialog back to the activity
                EditText editText = dialogLayout.findViewById(R.id.detailToDoViewActivity_editText_dialogTaskName);
                sendDialogDataToActivity(editText.getText().toString());}
        });
        // Create and show the dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

```