## Java Besonderheiten
### Allgemein
- Im Gegensatz zu C++: Keine separaten Header-Dateien
- Plattformunabhängig
- Programmstrukturierung in Packages (enthalten URL in umgekehrter Reihenfolge) 
- super() ruft Basisklassenkonstruktor auf
- Einfachvererbung 
- nur dynamische Allokation (nicht wie bei C++) 
- Objekt existiert so lange wie Referenz vorhanden ist -> Garbage Collector
- List anstatt Arrays verwenden
- Const heißt final
- Lambdas zur Übersichtlichkeit bzw. Codeminimierung
- Scanner zum Zerlegen einfacher Datentypen wie .txt oder .csv
- Mit "new" werden erst Instanzen erzeugt
### Strings
- In Schleifen: StringBuilder nutzen (Performance)
- Operator+ allkoiert implizit neuen String
- **String.equals nutzen, nicht ==**
### Wrapper Klassen
- Für jede Standardklasse wie int, float usw gibt es groß geschriebene Wrapper-Klassen wie Integer, Float, usw.
- Wird für Generics, Interfaces und Klassen verwendet
- Vergleich mit .equals
### Interfaces
- Realisieren von Polymorphie
- Implementation nur in den Klassen, die Interfaces implementieren -> Interfaces selbst haben keine Implementation
### Try mit Ressourcen
- Ressourcen öffnen und freigeben nach Ende der Funktion
### Serialisierung/ Deserialisierung
- Speicherung von Zuständen, lineare Übertragung (Umwandlung) und erneute Umwandlung in seriellen Datengraph
- wichtig wenn Objekte zwischen verschiedenen Aktivitäten übergeben werden sollen

## Application Model 
### Allgemein
- Vollständig komponentenbasierend: Aktivitäten sind eigenständige Programmteile (eigene Javaklasse), die oftmals genau einen "Screen" einer App darstellen
- Alle Aktiviäten erben von Parent AppCompatActivity
- Life Cycle Methoden (durch Android System vorgegeben) koordinieren Start/Ende/Pause einer App (z.B. bei einem kurzzeitigen Verlassen der App)
- Hollywood-Prinzip: Eventgesteuerter Programmablauf/Programmierung
### Aktivitäten
- Kommunikation untereinander über sogenannte **Intents**
- Laufzeitsystem verarbeitet Intents und initiiert Übergang von Aktivität A in Aktivität B
- Explicit Intents: Hier wird explizit bekannt gegeben, welche Aktivität als nächstes aufgerufen werden soll
- Implicit Intents: Durch Intentfilter vollständige Flexibilität -> Laufzeitsystem entscheidet, welche Aktivität das Informationsbedürfnis am besten erfüllen kann
- Können über App-Grenzen hinaus andere Aktivitäten anderer Apps aufrufen und instanziieren
### Generics
- = typsichere Collections
- parametrisierte Typen: Zusätzliche Variablen für Typen
- **List<Klasse>**
### Exceptions
- Checked Exceptions sind dem Compiler bekannt und müssen gecatched werden
- Unchecked Exceptions entstehen zur Laufzeit
- try -> catch -> finally (Aufräumarbeiten)

## Eigene Erfahrung in der App-Entwicklung
- Alle Strings in XML
- Colors sind ebenfalls in einer XML
- In onCreate sozusagen alles, was sonst in Qt mit c++ in den Konstruktor gekommen wäre
- Keine Connectors, aber mit **implements View.OnVlickListener** und einem Switch Case in der Interface Funktion kann zwischen verschiedenen Aktivitäten geswitched werden
- Um auf eine Komponente zuzugreifen: "lokales Objekt" erstellen und mithilfe der Funktion **findViewById** zuweisen
- Bei Strings mit ".equals" arbeiten, nicht mit "=="
- Toasts sind eine schöne Möglichkeit, um kleine Meldungen anzuzeigen
- Es können in ein lineares Layout andere Layouts "hineingezogen" werden
- Mit Margin (Start und End) können Grenzen 45von Komponenten angepasst werden
- **dp nicht vergessen**
- onActivityResult wird aufgerufen, wenn andere Klasse einen Datensatz zurückliefern möchte

## Recycler View
- Main Activity
    - Datenstruktur für benötigte Klasse implementieren (Vector, Liste)
    - Recycler View implementieren (zuletzt)
    - Layout Manager (zuletzt)
    - Adapter setzen (zuletzt)
- Main Acitivity Layout
    - Recycler View implementieren
- Item Layout (Viewholder)
    - Linear Layout mit darin enthaltenen Items
- Package Model
    - Alle Klassen werden hierin gespeichert
- Adapter Klasse (eigenes Package ist Best Practice)
    - Viewholder zum Verwalten der Elemente (als Klasse in Klasse dargestellt)
        - Konstruktor
        - OnCreateViewHolder (Inflaten der Items)
    - MyAdapter **extends** RecyclerView.Apdapter<MyAdapter.ViewHolder> 
    - Konstruktor braucht Zugriff auf Daten
    - onBindViewHolder
    - getItemCount

## Items in Recycler View clickable machen
- setOnClickListener in Adapter implementieren

## Animationen
- Eigenen Ordner für XML
- Wenn Animation in Recycler View: In Adapter in Methode OnCreateViewHolder anpassen





