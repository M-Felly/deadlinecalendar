## P1 - User Research

### Arbeitspakete

 - Issue #37 - Teufelsquadrat Sneed
    - Material durchgearbeitet
    - Glossareintrag ergänzt
 - Issue #39 - Satzschablone, funktionale und nicht-funktionale Anforderungen
    - Material durchgearbeitet
    - Glossareintrag ergänzt
    - Beispiele anhand des Projekts verfasst 
 - Issue #41 - User Research - Hypothesen @istdehilz
    - Material durchgearbeitet
    - Glossareintrag ergänzt
    - Merkmale für Benutzer*innengruppen abfragen und in Hypothesen eingearbeitet
    - Hypothesen formuliert
 - Issue #42 - Mentale Modelle
    - Material durchgearbeitet
    - Glossareintrag ergänzt inklusive Beispiele
    - Vermutete Mentale Modelle in Projekt formuliert

### Anteile und Selbsteinschätzung

- Meine Anteile im Projekt bestanden unter anderem aus meinen neuen Ideen zur Umsetzung. Da ich manchmal "um die Ecke denke", fallen mir dehalb Punkte auf, die zuerst noch nicht bedacht wurden. 
- Ich habe/hatte in Teammeetings Spaß daran, Aussagen und Ausarbeitungen zu formulieren/paraphrasieren. 
- Ich bin sehr genau und lege Wert auf eine qualitativ hochwertige Abgabe, weswegen ich mein Zeitmanagement noch optimieren muss. In Diskussionen und unseren gemeinsamen Teammeetings frage ich vermehrt nach, um sicher zu gehen, dass wir die Fragestellungen und die zugehörigen Ausarbeitungen richtig verstanden haben.
- Die Issues wurden alle gerecht aufgeteilt, weswegen jedes Mitglied eine Arbeitsbeteiligung von 20% hatte.

Fazit: Das Klima in unserem Team ist super und wir sind alle hochmotiviert, gemeinsam das Praktikum zu bearbeiten. Unsere Teammeetings sind "erfrischend" - jeder hat interessante Ideen und ist den anderen Mitgliedern gegenüber hilfsbereit und offen.

## P2 Entwurf

### Arbeitspakete

- Issue #12 - Navigationsübersicht @istdehilz <br>
   - Material (Folien/Vorlesung) durchgearbeitet
   - Navigationsübersicht erstellt und als Bild in gitlab eingefügt
- Issue #15 - Paper-Prototyp 2 erstellen @istnaleut <br>
   - Brainstorming und anschließende Umsetzung
   - Thinking aloud mit Testperson 2
   - Anschließende Analyse des Gesprächs und Verassen eines Fazit
- Issue #21 - Hick's Law <br>
   - Material durchgearbeitet
   - Glossareintrag mit Beispielen (generell und Projekt-bezogen) ergänzt
- Issue #22 - Affordance <br>
   - Material durchgearbeitet
   - Glossareintrag mit Beispielen ergänzt
   - Möglichkeiten zum Einbau im Glossareintrag ergänzt
- Gemeinsame Entwicklung einer Test-App zu Vorbereitung @alle <br>
   - Ressourcen: Auswahl eines App-Logos, Implementation von Vektorgrafiken, Übersetzung der Strings in Deutsch und Englisch, Auswahl und Impelementation eines Farbschemas
   - Implementation einer Toolbar mit Button, um zu den Einstellungen zu navigieren
   - Implementation einer Standard-Einstellungsaktivität 

### Anteile und Selbsteinschätzung

- In diesem Praktikum bestanden auch hierbei große Anteile meiner Arbeit in neuen Überlegungen zur Umsetzung und möglichen Lösungen von Problemen konzeptioneller und organisatorischer Art.
- Ich habe mich in die Lage von Nutzer\*innen hineinversetzt, um ein bestmögliches Ergebnis der Usablitiy für unser Team zu erzielen.
- Zum besseren Verständis legte ich viel Wert auf die klar Definition von Begrifflichkeiten, damit wir alle "die gleiche Sprache sprechen". 
- Erwähnenswert ist außerdem, dass vor Allem @istmafeyl viel Hilfe in Bezug auf die gemeinsame App-Programmierung anbot und @istankilb über ein Zeichentool unseren gemeinsamen Prototypen anhand unserer Ideen und vorigen Prototypen sehr detailliert und aufwendig ausgearbeitet hat. 

Fazit: Unser Team ist nach wie vor hochmotiviert und wir pflegen ein sehr respektvolles Miteinander. Jeder beteiligt sich und wird in seinen Ideen, Anregungen und Verbesserungsvorschlägen angehört. Die Arbeitseinteilung verläuft sehr fair, unser Zeitmanagement werden wir in den zukünftigen Praktika noch weiter anpassen. 

## P3 Laylout inkl. Listen

### Arbeitspakete
- Issue #86 Erstellen der ToDoOverView Activity @istankilb <br>
   - Erstellen der P3-App: In dieser App haben wir eine ToDo-Liste realisiert. Anna Kilb und ich ware für die Erstellung des Projekts, sowie den Bau des Grundgerüsts zuständig. <br>
   - Erstellen einer Recycler View zum Anzeigen der einzelnen ToDo's.<br>
   - Die ToDoOverView Activity wurde haben wir in einem linearen Layout realisiert. <br>
   - Zusätzliche Erstellung des Floating Action Buttons, um zu der AddToDoView zu navigieren, damit ein neues ToDo erstellt werden kann.
   - Implementation der Klassenstruktur eines ToDo's.<br>
- Alle Arbeitspakete, die mit der Programmierung der DeadlineCalendar App zusammenhängen, wurden unter P4 aufgeführt, da einige Aufgaben, wie das Erstellen des Layouts, mit anderen Aufgaben ineinander gegriffen haben. <br>

### Anteile und Selbsteinschätzung

In diesem Praktikum habe ich durch die erstellte Test-App sehr viel dazu gelernt. Anna Kilb und ich haben als Programming-Pair die Navigation, sowie das Einbinden einer Recycler View realisiert. Die Herausforderung hierbei war, dass bei einem Klick auf ein Element aus der Recycler View die entsprechende Detail-Ansicht des ToDo's aufgerufen werden soll. Zusammen haben wir dies jedoch sehr gut umsetzen können. 

Fazit: Unser Team hat durch diese kleine App sehr viel Neues gelernt und ist nun auf unser eigentliches Projekt, den DeadlineCalendar, optimal vorbereitet!

## P4 Logik und Persistenz

### Arbeitspakete
- Issue #75 Erstellen der CalendarView mit @istnaleut <br>
   - Erstellen der Layouts (Smartphone und Landscape) <br>
   - Erstellen der Navigation zu den entsprechenden Activities <br>
   - Programmierung der Funktionalitäten <br>
   - Umsetzung einer Recycler View für Items, die einen unterschiedlichen Datentyp zugehörig sind (Deadlines und Appointments). Dies haben wir durch die Umsetzung zweier Viewholder in einem Adapter umgesetzt. <br> 
   - Implementation der Room - Datenbankanbindung <br>
   - Debugging und Entfernen der Warnings <br>
- Issue #76 Erstellen der AddAppointmentView mit @istnaleut <br>
   - Erstellen der Layouts (Smartphone und Landscape) <br>
   - Erstellen der Navigation zu den entsprechenden Activities <br>
   - Programmierung der Funktionalitäten <br>
   - Implementation der Room - Datenbankanbindung <br>
   - Debugging und Entfernen der Warnings <br>
- Issue #77 Erstellen der DetailCalendarView mit @istnaleut <br>
   - Erstellen der Layouts (Smartphone und Landscape) <br>
   - Erstellen der Navigation zu den entsprechenden Activities <br>
   - Programmierung der Funktionalitäten <br>
   - Implementation der Room - Datenbankanbindung <br>
   - Debugging und Entfernen der Warnings <br>
- Issue #78 Erstellen der EditCalendarView mit @istnaleut <br>
   - Erstellen der Layouts (Smartphone und Landscape) <br>
   - Erstellen der Navigation zu den entsprechenden Activities <br>
   - Programmierung der Funktionalitäten <br>
   - Implementation der Room - Datenbankanbindung <br>
   - Debugging und Entfernen der Warnings <br>
- Issue #88 Erstellen der Room-Datenbankanbindung mit @istmafeyl <br>
   - Erstellen der Datenbank <br>
   - Realisierung von DAO-Klassen für die verschiedenen Datentypen <br>
   - Bei Datentypen, die andere Klassen oder Attribute enthielten, die nicht ohne weiteres in die Datenbank gespeichert werden konnten, wurden Typeconverter und Relationship Entities erstellt. <br>
   - Erstellen des Appointment Repositories, das auf dem BaseRepository von Matthias Feyll basiert bzw. erbt. <br>

### Anteile und Selbsteinschätzung

Die Erstellung der Room-Datenbank war für Matthias Feyll und mich anfangs eine große Herausforderung, da sie sich in ihrer inneren Struktur sehr von anderen Datenbankkonzepten unterscheidet, vor allem bei komplexeren Klassenstrukturen wie bei unserem Deadline-Projekt. Viele Tutorials und Webseiten behandelten eher Datenbankanbindungen mit Kotlin, was dies erschwerte. Dennoch wurden wir bei der Dokumentation von Android fündig und haben nach und nach die Funktionsweise verstanden und wie diese zu implementieren gilt. <br>

Auch die Erstellung der Recycler View in der CalendarViewActivity stellte für Nadine Leuthe und mich an eine Herausforderung dar, da wir zwei verschiedene Datentypen in einer RecyclerView vereinen und nach Uhrzeit sortieren mussten. Wir haben uns hierbei entschieden, eine Klasse CalendarElement einzuführen, die den eigentlichen Datenytp Appointment oder Dealine zu einem CalendarElement castet und alle Attribute, die relevant für die Viewholder sind (es mussten 2 Viewholder implementiert werden, da Deadlines und Appointments anders angezeigt werden) übernimmt. Der Datentyp verweist zusätzlich auf das Referenzobjekt in einer Liste, das in diesen Typ geparsed wurde, um später trotzdem auf die "richtigen" Datentypen weiterhin zugreifen zu können. <br>
Für die wechselnde Ansicht der RecyclerView, je nachdem welcher Tag im Kalender ausgewählt wurde, haben wir eine weitere Klasse hinzugefügt, die für genau ein Datum alle Deadlines und Appointments enthält. Diese Liste wurde dann an den Adapter der RcyclerView übergeben. <br>

Bezogen auf die gemeinsamen Activties, haben Nadine Leuthe und ich unsere Aufgaben teils aufgeteilt, teils haben wir als Programming-Pair die Tasks erfüllt. Auch bei Bugs und Problemen haben wir viel Rücksprache gehalten. Dies funktionierte sehr gut, da wir auch bei Problemen unterschiedliche Herangehensweisen haben und somit durch den jeweils anderen nochmal einen ganz anderen Blickwinkel auf das Problem/Bug erhalten haben, um es zu lösen. <br>

Fazit: Wie auch bereits in den anderen Praktika erwähnt, bin ich sehr zufrieden mit unserer Teamarbeit, vor allem was die gegenseitige Unterstützung betrifft. Mir macht die Arbeit in diesem Projekt sehr viel Spaß und ich habe in dieser kurzen Zeit sehr viel Neues gelernt, programmiertechnisch sowie menschlich (primär auf die Teamarbeit bezogen). <br>
 
 ## P5 Projektabschluss

 ### Arbeitspakete
 - Issue #2 Erstellen des App Videos mit @istankilb
   - Ablauf planen <br>
   - Video erstellen <br>
- Issue #10 Design Sprint, Design Thinking, Double Diamond
   - Material durchgearbeitet <br>
   - Glossar ergänzt <br>
   - Tabelle erstellt <br>
- Issue #11 Usability Evaluation
   - Material durchgearbeitet <br>
   - Glossar ergänzt <br>

   ### Anteile und Selbsteinschätzung
   Die Arbeitspakete #10 und #11 habe ich gründlich recherchiert und zusammengefasst, sodass ich diese Themengebiete gut meinen Teamkollegen erklären konnte.
   In Issue #2 haben Anna Kilb und ich ein App-Vorstellungsvideo erstellt. Wir haben uns in unseren Ideen sehr gut ergänzt und konnten trotz der begrenzten Zeit ein gutes, stimmiges Video erstellen, dass die wesentlichen Inhalte unserer DeadlineCalendar App widerspiegelt. Diese kreative Arbeit hat mir viel Spaß gemacht und ich hätte aber gerne noch mehr Zeit investiert, was aber leider aufgrund anderer Abgaben nicht möglich war.

   ## Fazit - Endnote: 1,3
   Wir haben alle unser Bestes in diesen Praktika gegeben und haben uns damit sehr zeitintensiv beschäftigt. Ich habe viel Neues dazu gelernt was Lerntechniken, Problemlösungen und Kommunikation im Team betrifft und war stets motiviert, neue Ideen und Verbesserungsvorschläge mit einzubringen. Die Teamarbeit funktionierte hervorragend und wir hatten trotz des engen Zeitfensters einen gepflegten Umgang miteinander und haben uns gegenseitig Hilfestellungen geleistet. <br>
   Ich erledigte meine Issues und Programmiertasks erfolgreich und nahm mir  Verbesserungen bzw. Bugfixes vor. Ich würde mir, sowie dem gesamten Team, die Note 1,3 geben, da wir in dieser kurzen Zeit neben vielen anderen Modulen und Nebenjobs viel Zeit und Herzblut in dieses Projekt gesteckt haben. Aber nicht nur die aufgewandte Zeit zählt, sondern auch die Qualität des Projekts. Wir waren bei Entscheidungen und Vorgehensweisen sehr granular, um ein möglichst perfektes Endergebnis zu erzielen. Wir konnten dennoch einige Ideen nicht umsetzen, sind aber positiv gestimmt, dieses Projekt nach dem Abschluss des Moduls Nutzer\*innenzentrierte Softwareentwicklung weiter zu verfolgen. <br>


