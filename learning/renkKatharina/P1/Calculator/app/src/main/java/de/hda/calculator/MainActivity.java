package de.hda.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText decimal1, decimal2;
    private TextView result;
    private boolean valid = true;
    private char operator = 'x';

    //Layout
    //Hochladen
    //Readme
    //restliche Videos schauen

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    decimal1 = findViewById(R.id.editTextNumberDecimal1);
    decimal2 = findViewById(R.id.editTextNumberDecimal2);
    result = findViewById(R.id.textViewResult);

    Button btnPlus = findViewById(R.id.buttonPlus);
    Button btnMinus = findViewById(R.id.buttonMinus);
    Button btnMultiplication = findViewById(R.id.buttonMultiplication);
    Button btnDivision = findViewById(R.id.buttonDivision);
    Button btnCalculate = findViewById(R.id.buttonIstGleich);
    Button btnReset = findViewById(R.id.buttonReset);

    btnPlus.setOnClickListener(this);
    btnMinus.setOnClickListener(this);
    btnMultiplication.setOnClickListener(this);
    btnDivision.setOnClickListener(this);
    btnCalculate.setOnClickListener(this);
    btnReset.setOnClickListener(this);

    }

    //Check if all chosen values are correct/valid
    private boolean isValid(){


        //Check if Values are empty
        if ((decimal1.getText().toString().isEmpty()) || (decimal2.getText().toString().isEmpty())){

            Toast toast = Toast.makeText(getApplicationContext(),getString(R.string.Error_Empty),Toast.LENGTH_LONG);
            toast.setGravity(Gravity.TOP|Gravity.RIGHT,0,0);
            toast.show();
           return false;
        }

        else if (operator == 'x'){

            Toast toast = Toast.makeText(getApplicationContext(),getString(R.string.Error_Operator),Toast.LENGTH_LONG);
            toast.setGravity(Gravity.TOP|Gravity.RIGHT,0,0);
            toast.show();
            return false;
        }

        //Check if Zero Division is detected
        if ((operator == '/') && (decimal2.getText().toString().equals("0"))){
            Toast toast = Toast.makeText(getApplicationContext(),getString(R.string.Error_Zero_Division),Toast.LENGTH_LONG);
            toast.setGravity(Gravity.TOP|Gravity.RIGHT,0,0);
            toast.show();
            return false;
        }
        return true;
    }

    //Calculating the result
    private float getResult(float z1, float z2){

        float erg = 0;


        if (operator == '+'){

            erg = z1 + z2;
        }
        else if (operator == '-'){

            erg = z1 - z2;
        }
        else if (operator == '*'){

            erg = z1 * z2;
        }
        else if (operator == '/'){

            erg = z1 / z2;
        }

        return erg;
    }

    //Setting the result
    private void setResult(){

        if (isValid()){

            TextView resultView = findViewById(R.id.textViewResult);
            float dec1 = Float.parseFloat(decimal1.getText().toString());
            float dec2 = Float.parseFloat(decimal2.getText().toString());

            float result = getResult(dec1,dec2);

            resultView.setText(String.format("%.2f",result));
        }


    }

    //Reset all values
    private void reset(){
        resetButtonColors();
        TextView decimal1 = findViewById(R.id.editTextNumberDecimal1);
        TextView decimal2 = findViewById(R.id.editTextNumberDecimal2);

        decimal1.setText("");
        decimal2.setText("");

        operator = 'x';

    }

    //Reset button colors after pressing another button
    @SuppressLint("ResourceType")
    private void resetButtonColors(){
        Button plus = findViewById(R.id.buttonPlus);
        Button minus = findViewById(R.id.buttonMinus);
        Button multi = findViewById(R.id.buttonMultiplication);
        Button div = findViewById(R.id.buttonDivision);

        plus.setBackgroundColor(Color.parseColor(getString(R.color.purple_500)));
        minus.setBackgroundColor(Color.parseColor(getString(R.color.purple_500)));
        multi.setBackgroundColor(Color.parseColor(getString(R.color.purple_500)));
        div.setBackgroundColor(Color.parseColor(getString(R.color.purple_500)));
    }

    //Listener for all pressable buttons
    @SuppressLint("ResourceType")
    @Override
    public void onClick(View v) {
        Button btn;

        switch (v.getId()) {
            case R.id.buttonPlus: {
                resetButtonColors();
                btn = findViewById(R.id.buttonPlus);
                btn.setBackgroundColor(Color.parseColor(getString(R.color.purple_200)));
                operator = '+';
                break;
            }
            case R.id.buttonMinus: {
                resetButtonColors();
                btn = findViewById(R.id.buttonMinus);
                btn.setBackgroundColor(Color.parseColor(getString(R.color.purple_200)));
                operator = '-';
                break;
            }
            case R.id.buttonMultiplication: {
                resetButtonColors();
                btn = findViewById(R.id.buttonMultiplication);
                btn.setBackgroundColor(Color.parseColor(getString(R.color.purple_200)));
                operator = '*';
                break;
            }
            case R.id.buttonDivision: {
                resetButtonColors();
                btn = findViewById(R.id.buttonDivision);
                btn.setBackgroundColor(Color.parseColor(getString(R.color.purple_200)));
                operator = '/';
                break;
            }
            case R.id.buttonIstGleich: {
                setResult();
                break;
            }
            case R.id.buttonReset: {
                reset();
                break;
            }
        }

    }
}