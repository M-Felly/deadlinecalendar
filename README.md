# Deadline Calendar


## Anmerkungen
### Known Issues
- Deadline Filter Dialog noch nicht scrollable.
- In manchen Fällen wird das Erinnerungsdatum noch falsch gesetzt. 
- Noch keine Datenbankmigrationen vorhanden. -> Änderungen am DB-Schema zwingt zur Neuinstallation der App.
- Performance warnings: unused resources (39)
    -> sobald man diese entfernt bekommt man errors, da diese benutzt werden.

### Aussicht (Mögliche Features für spätere Versionen)
#### Allgemein
- Multilingualität weiter ausbauen
- Abgabestatistik
- Swipen zwischen den verschiedenen Hauptansichten
- Intervallbasiertes Importieren des Kalenders (Polling / Subscription)
- Undo-delete Toast oder Button für Appointments und Deadlines

#### Kalender
- Kalendertage, an denen ein Termin oder eine Deadline stattfindet, werden farblich hervorgehoben, um es dem/der User*in es zu erleichtern alle Termine und Abgaben im Überblick zu behalten.
- Teilnehmer hinzufügen
- Dateien in einem Termin hinzufügen
- Termine für mehrere Tage


## Team
### Definition der Rollen
#### Scrum Master
- Sorgt für Einhaltung der Regeln nach Scrum
- Moderatorenrolle für Statusmeetings und Terminvereinbarungen
- Gewährleistet eine gute Arbeitsatmosphäre und löst Konflikte
- Bearbeitung von Issues

#### Product Owner
- Anlegen von Issues und Aufgabenverteilung inkl. Time-tracking
- Verantwortet den geschäftlichen Erfolg (Vorstellung im Praktikum)
- Repräsentant gegenüber Product Management
- Projektkoordination
- Bearbeitung von Issues

#### Teammitglied
- Aktives Mitwirken in Entwicklungs- und Planungsprozess
- Bearbeitung von Issues

Zu den o.g. Aufgaben des Scrum Masters und Product Owner kommen zusätzlich noch die Aufgaben eines Teammitgliedes hinzu. 
(Abwandlung für unser Projekt / Alle sollen aktiv am Entwicklungsprozess beteiligt werden)


P1: Feyll [Scrum Master], Leuthe [Product Owner], Kilb [Teammitglied #1], Renk [Teammitglied #2], Hilz [Teammitglied #3]<br>
P2: Feyll [Teammitglied #1], Leuthe [Teammitglied #2], Kilb [Product Owner], Renk [Teammitglied #3], Hilz [Scrum Master]<br>
P3: Feyll [Teammitglied #1], Leuthe [Teammitglied #2], Kilb [Scrum Master], Renk [Product Owner], Hilz [Teammitglied #3]<br>
P4: Feyll [Teammitglied #1], Leuthe [Teammitglied #2], Kilb [Teammitglied #3], Renk [Scrum Master], Hilz [Product Owner]<br>
P5: Feyll [Product Owner], Leuthe [Scrum Master], Kilb [Teammitglied #1], Renk [Teammitglied #2], Hilz [Teammitglied #3]<br><br>

## Vision

Unsere Applikation richtet sich an Studierende, die während des Semesters ihre Zeit vorrangig für die Studieninhalte und weniger mit deren Organisation aufwenden möchten. Die Studierenden werden hierbei individuell in ihrer Planungs- und Organisationsstrategie unterstützt. Die häufig im Studienalltag auftretende Problematik der Unübersichtlichkeit und des fehlenden Überlicks der anfallenden Abgabetermine gehört mit Nutzung des Deadline Calendar's nun der Vergangenheit an! <br>
Der Deadline Calendar stellt eine Applikation auf Android-Basis dar, welche das Verwalten der anfallenden Deadlines im jeweiligen Studiengang und dessen Modulen erleichtert. Nutzer\*innen können nach Wahl zwischen Listen- oder Kalenderansicht die Deadlines und ihre dazugehörigen Aufgaben und Eigenschaften einsehen und werden rechtzeitig erinnert.
Anstelle analoger Organisationshilfen begleitet die Applikation die Nutzer\*innen mobil und flexibel durch den Alltag und reduziert den Planungsaufwand. Die dadurch gewonnene Zeit können die Nutzer\*innen in ihre Studienarbeiten investieren. Dies führt langfristig zu einem größeren Studienerfolg und senkt den Stresspegel während des Semesters. <br>
In unserer App stellen wir im Gegensatz zu anderen Wettbewerbern die Nutzer\*innen in den Mittelpunkt und ermöglichen eine Bildungsinstitiutionsunabhängige Plattform, welche Integration bestehender Kalender unterstützt und den Nutzer\*innen freie Hand über die Umsetzung der Abgabenplanung lässt.  

### MVP (Minimal Viable Product)
- CRUD von mehreren Deadlines pro Modul
- Listview mit allen Deadlines
- Kalenderansicht
- Initiale User-Konfiguration (Studiengang, Module, Infos über Praktika) + spätere Editierung
- Erinnerungsfunktion
- App-Logo
- Importieren von Terminen


### Features
- Multilingualität
- Hinzufügen von Projektpartnern
- Anzeige von Semesterferien zur Planung
- Countdown (Timer) für Deadlines
- Recap über Arbeitsstatistik
- Deadlines können ein entsprechender Status (Abgeschlossen etc.) zugeordnet oder eine erneute Erinnerung ausgelöst werden 



### Beschreibung der Activities
#### Initiales Profil anlegen (EntranceProfileActivity, erstellt von: Matthias Feyll)
Beim ersten Starten der App wird nach dem Logo auf diese Activity verwiesen. Hier <b>kann</b> der Benutzer\inn  ein Profil anlegen indem er die Felder ausfüllt und oben rechts auf den Haken klickt.
Alternativ kann er diesen mit einem Klick auf den Button 'Überspringen' überspringen. In dem Fall wird ein leeres Profil erzeugt.

#### Initiale Module anlegen (EntranceStudyModuleActivity, erstellt von: Matthias Feyll) 
Nach dem initialen Profil anlegen <b>muss</b> der Benutzer mindestens ein Modul anlegen. Dies kann er durch den Button 'Modul hinzufügen' ausführen.
Hat der Benutzer\inn  alle gewünschten Module angelegt, so klickt er wieder oben rechts auf den Button und gelangt zur eigentlichen App

#### Profilansicht (ProfileViewActivity, erstellt von: Matthias Feyll)
Mit einem Klick auf das Profilicon in der bottomNavigation gelangt man zur Profilansicht. Dort kann der Benutzer\inn seine Profildaten und angelegten Module einsehen.
Module kann er/sie durch einen Klick auf den Mülleimer löschen. Sollte es Deadlines geben die auf das zu löschende Modul verweisen wird in einem Dialog fragend darauf hingewiesen, dass auch
die dazugehörigen Deadlines gelöscht werden.
Zusätzlich kann man noch weitere Module hinzufügen in dem man auf das Plus-Icon oder den Text "Modul hinzufügen" klickt.
Über das Zahnrad-Icon oben rechts gelangt man zu Einstellungsansicht.
Über das Stift-Icon oben rechts gelangt man zu Bearbeitungsansicht des Profils.

#### Bearbeitungsansicht des Profils (EditProfileViewActivity, erstellt von: Matthias Feyll)
Die Bearbeitungsansicht gleicht der Profilansicht einen Schritt zuvor. Allerdings kann der/die Benutzer/inn die Profilinformationen anpassen.
Nach dem Anpassen muss er/sie die Eingabe noch durch einen Klick auf das Haken-Icon bestätigen. Durch ein Klick auf das Kreuz-Icon werden die Änderungen verworfen.
Wie auch in der Profilansicht kann der/die Benutzer/inn Module hinzufügen und löschen.

#### Einstellungsansicht (SettingsViewActivity, erstellt von: Matthias Feyll)
Der/die Benutzer/in hat drei auswahlKategorien:
 - "Kalender importieren"
   der/die Benutzer/inn hat die Möglichkeit einen OBS Kalender zu importieren. Hierzu muss er/sie in das URL-Feld den Abonnieren-Link aus dem OBS dort eintragen
   Mit einem Klick auf "Importieren" wird dann der Kalender importiert und der Fortschritt in einem Ladebalken angezeigt. Sollte der Import fehlschlagen werden je nach Fehler spezifische Fehlermeldungen als Dialog angezeigt.
 - "Benachrichtigungen"
   Unter dem Feld hat der/die Benutzer/inn die Möglichkeit die Benachrichtigungen für Deadlines und Termine an und auszuschalten
 - "Hilfe"
   Im Reiter Hilfe findet der/die Benutzer/inn eine simple, immer zutreffende, und allumfassende Hilfestellung für sämtliche Probleme im Berreich IT.

#### Kalenderansicht (CalendarViewActivity, erstellt von: Nadine Leuthe, Katharina Renk)
Über diese Ansicht können Benutzer**innen die erstellten Kalendertermine und Appointments einsehen. Wenn das entsprechende Datum via Klick auf den jeweiligen Kalendertag ausgewählt wurde, werden alle für diesen Tag anstehenden Elemente (Termine und Deadlines) in sortierter Reihenfolge angezeigt, beginnend mit dem Element, das am frühesten anfängt.
Über diese Ansicht können Benutzer*innen folgende Interaktionen vornehmen:
- Erstellen eines neuen Termins <br>
  Mit einem Klick auf den Floating Action Button unten rechts werden Benutzer*innen zu der Terminerstellungsansicht weitergeleitet (AddAppointmentView). Somit können sie einen neuen Termin erstellen, der anschließend dem Kalender hinzugefügt wird.
- Auswählen eines Termins/Deadline zum Einsehen der Details <br>
  Via Klick auf das jeweilige Listensymbol können sich Benutzer*innen den ausgewählten Termin (DetailCalendarViewActivity) oder die ausgewählte Deadline (DetailDeadlineViewActivity) detailliert anzeigen lassen, die über die kompakte Listenansicht nicht einsehbar sind.

#### Terminerstellungsansicht (AddAppointmentViewActivity, erstellt von: Nadine Leuthe, Katharina Renk)
Benutzer**innen können über diese Aktivität neue Termine anlegen und sie mit relevanten Details ergänzen. Folgende Werte müssen eingetragen werden: <br>
- Name des Termins
- Datum
- Uhrzeit (von ... bis ...) <br>

Folgende Werte sind optional: <br>
- Ortsangabe
- Erinnerungsfunktion
- zusätzliche Notizen <br>

Benutzer*innen haben die Möglichkeit, diesen Termin zu verwerfen (Kreuz in der Toolbar oben links) oder zu speichern (Haken in der Toolbar oben rechts). Bei Letzterem wird der Termin dem Kalender hinzugefügt und ist von nun an über die Kalenderansicht einseh- und editierbar. Nach erfolgreicher Erstellung des Termins wird der/die Benutzer**in zu der Termin-Detailansicht (DetailCalendarViewActivity) weitergeleitet, wo er/sie alle erstellen Informationen nochmal auf einen Blick einsehen können, bevor er/sie zu der Kalenderansicht zurück wechselt.

#### Termin-Detailansicht (DetailCalendarViewActivity, erstellt von: Nadine Leuthe, Katharina Renk)
Diese Ansicht beinhaltet alle Informationen zu einem Termin. Einsehbar sind folgende Attribute: <br>
- Name des Termins
- Datum
- Uhrzeit (von ... bis ...)
- Ortsangabe
- Erinnerungsfunktion
- zusätzliche Notizen <br>

Benutzer**innen können hierüber bei Klick auf das Kreuz, welches sich links oben in der Toolbar befindet, zurück in die Kalenderansicht (CalendarViewActivity) wechseln. Über das Stift-Icon (rechts oben in der Toolbar) kann ein/e Benutzer/in die Bearbeitungsansicht (EditCalendarViewActivity) eines Termins aufrufen, um nachträgliche Änderungen vorzunehmen.

#### Termin-Berabeitungsansicht (EditCalendarViewActivity, erstellt von: Nadine Leuthe, Katharina Renk)
Über diese Activity können Benutzer**innen nachträglich die Attribute ihres Kalenders bearbeiten. 
Folgende Attribute müssen ausgefüllt sein/bleiben: <br>
- Name des Termins
- Datum
- Uhrzeit (von ... bis ...) <br>

Folgende Attribute sind optional: <br>
- Ortsangabe
- Erinnerungsfunktion
- zusätzliche Notizen <br>

Ein/e Benutzer/in kann den bearbeiteten Termin entweder über einen Klick auf den Haken rechts oben in der Toolbar speichern, sodass er aktualisiert in der Kalenderansicht erscheint, oder ihn über das Kreuz links oben verwerfen, um den alten Zustand des Termins beizubehalten. Sowohl nach erfolgreicher Speicherung als auch bei Verwerfen des Termins wird der/die Benutzer**in in die Termin-Detailansicht (DetailCalendarViewActivity) des jweiligen Termins weitergeleitet.

#### Deadline-Ansicht (DeadlineViewActivity), erstellt von Anna Kilb, Dennis Hilz
Über diese Ansicht können Nutzer*innen ihre aktuellen Deadlines und deren Status einsehen. Neben dem Titel der Deadline, ist es den Nutzer*innen möglich, auf einen Blick alle relevanten Informationen wie das zugehörige Modul, die verbleibende Zeit als Countdown sowie den Status der zugewiesenen ToDo's zu überblicken. Falls alle ToDos der jeweiligen Deadline auf erledigt gesetzt sind, wird dies entsprechend mit einem grünen Pfeil am rechten Rand gekennzeichnet. Die aufsteigende Sortierung der Deadlines nach verbleibender Restzeit ist dabei bewusst gewählt, um die dringendsten Fälle direkt im Blickfeld zu haben. <br>

Von hier aus haben die Nutzer*innen folgende Interaktions- / Navigationsmöglichkeiten: <br>

- Anlage einer neuen Deadline über den entsprechend gekennzeichneten Floating Action Button (AddDeadlineActivity)
- Filtern der Deadlines nach Modul über eine Dialogeinblendung (Aktivierung durch den zweiten Floating Action Button mit Filtersymbol)
- Wechsel in die Detailansicht einer bestimmten Deadline (DetailDeadlineViewActivity)

#### Deadline-Detailansicht (DetailDeadlineViewActivity), erstellt von Anna Kilb, Dennis Hilz
Diese Ansicht gibt den Nutzer*innen eine detailierte Übersicht aller Informationen zur ausgewählten Deadline. Darüber hinaus ist es möglich, entsprechend für diese Deadline spezifische ToDos zu definieren, hinzuzufügen und diese bei Bedarf abzuhaken (Checkbox auf linker Seite des jeweiligen ToDos) oder zu löschen (Mülleimer-Symbol auf rechter Seite des jewiligen ToDos). <br> 
Sollten alle ToDos erledigt worden sein und die Nutzer*innen haben die Abgabe entsprechend des Abgabetyps eingereicht, kann die Deadline nun über den länglichen grünen Button im unteren Bereich der Activity abgeschlossen werden. Anschließend erfolgt eine Weiterleitung auf die Hauptansicht (DeadlineViewActivity) in der die soeben abgeschlossene Deadline nicht mehr zu sehen ist. <br>

Informationsübersicht: <br>
- Titel
- Zugewiesenes Studienmodul
- Abgabedatum und Uhrzeit
- Abgabetyp (Wie oder Wo muss ich meine Abgabe einreichen?)
- Erinnerungskonfiguration
- Countdown für verbleibende Zeit
- Statusanzeige der zugewiesenen ToDos
- Auflistung aller ToDos mit Möglichkeit zur Statusänderung oder Löschung


#### Deadline-Erstellungsansicht (AddDeadlineViewActivity), erstellt von Anna Kilb, Dennis Hilz
Nutzer*innen können in dieser Ansicht neue Deadlines anlegen und mit den notwendigen Informationen ergänzen: <br>

Pflichtfelder: <br>
- Titel
- Studienmodul (Auwahl aus Liste)
- Abgabetyp (Auswahl aus Liste oder eigene Definition) -> digital (Moodle o.Ä.), präsent etc.
- Datum und Uhrzeit als Abgabetermin

Ergänzende Informationen: <br>
- Erinnerungszeitpunkt (1 Std. vorher, 1 Tag vorher etc.)
- ToDos (Hinzufügen und Löschen)

#### Deadline-Bearbeitungsansicht (EditDeadlineViewActivity), erstellt von Anna Kilb, Dennis Hilz
In dieser Ansicht, können Nutzer*innen die Informationen ihrer Deadlines nachträglich bearbeiten oder um entsprechende Informationen ergänzen. Die Änderungen können entweder durch das Hakensymbol im rechten oberen Bereich gespeichert oder durch das Kreuzsymbol im linken oberen Bereich verworfen werden. <br>
Sollte eine Deadline nicht mehr relevant sein, kann diese über den länglichen roten Button im unteren Bereich der Activity gelöscht werden. Nach Bestätigung der Löschung über ein Dialogfeld, erfolgt eien Weiterleitung in die Hauptansicht (DeadlineViewActivity) in der die soeben gelöschte Deadline nicht mehr zu sehen ist. <br>

Pflichtfelder: <br>
- Titel
- Studienmodul (Auwahl aus Liste)
- Abgabetyp (Auswahl aus Liste oder eigene Definition) -> digital (Moodle o.Ä.), präsent etc.
- Datum und Uhrzeit als Abgabetermin

Ergänzende Informationen: <br>
- Erinnerungszeitpunkt (1 Std. vorher, 1 Tag vorher etc.)
- ToDos (Hinzufügen und Löschen)

